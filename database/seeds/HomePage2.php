<?php

use Illuminate\Database\Seeder;

class HomePage2 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('component_try')->insert([
            'id' => '4',
            'title_en' => 'This is temporary component',
        ]);

        DB::table('component_opinion')->insert([
            'id' => '2',
        ]);
        DB::table('page_index')->insert([
            'id' => '1',
            'opinion_id' => '2',
            'try_id' => '4'
        ]);
    }
}
