<?php

use Illuminate\Database\Seeder;

class PrivacyPageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('page_privacy')->insert([
            'id' => '1',
            'text_en' => ' ',
            'text_de' => ' ',
            'text_fr' => ' ',
        ]);
    }
}
