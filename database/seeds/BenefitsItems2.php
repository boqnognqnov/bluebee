<?php

use Illuminate\Database\Seeder;

class BenefitsItems2 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //BENEFITS MAIN ITEMS

//        END BENEFITS MAIN ITEMS
        DB::table('component_header')->insert([
            'id' => '23',
            'title_en' => 'High Performance header',
            'title_de' => 'High Performance header',
            'title_fr' => 'High Performance header',
        ]);

        DB::table('component_opinion')->insert([
            'id' => '1',
            'name' => 'This is temporary component',
        ]);

        DB::table('component_try')->insert([
            'id' => '1',
            'title_en' => 'This is temporary component',
        ]);

        DB::table('page_benefits')->insert([
            'opinion_id' => '1',
            'try_id' => '1',
            'head_id' => '23',
        ]);

//        BENEFITS INNER ITEMS
//ALL FAV RECORDS
        DB::table('component_favorite')->insert([
            'id' => '1',
            'vector_model_name' => 'page_benefits_inner',
            'vector_model_id' => '1',
        ]);
        DB::table('component_favorite')->insert([
            'id' => '2',
            'vector_model_name' => 'page_benefits_inner',
            'vector_model_id' => '2',
        ]);
        DB::table('component_favorite')->insert([
            'id' => '3',
            'vector_model_name' => 'page_benefits_inner',
            'vector_model_id' => '3',
        ]);
        DB::table('component_favorite')->insert([
            'id' => '4',
            'vector_model_name' => 'page_benefits_inner',
            'vector_model_id' => '4',
        ]);
//        END OF ALL FAV RECORDS

        DB::table('component_header')->insert([
            'id' => '1',
            'title_en' => 'High Performance header',
        ]);

        DB::table('component_opinion')->insert([
            'id' => '4',
            'name' => 'This is temporary component',
        ]);

        DB::table('component_try')->insert([
            'id' => '17',
            'title_en' => 'This is temporary component',
        ]);


        DB::table('page_benefits_inner')->insert([
            'head_id' => '1',
            'favorites_id' => '1',
            'opinion_id' => '4',
            'try_id' => '17',

            'front_title_en' => 'High Performance',
            'front_title_de' => 'High Performance',
            'front_title_fr' => 'High Performance',

            'front_text_en' => 'High Performance text En',
            'front_text_de' => 'High Performance text De',
            'front_text_fr' => 'High Performance text Fr',

            'slug_en' => 'high-performance',
            'slug_de' => 'high-performance',
            'slug_fr' => 'high-performance',

        ]);


        DB::table('component_header')->insert([
            'id' => '2',
            'title_en' => 'Security header',
        ]);

        DB::table('component_opinion')->insert([
            'id' => '5',
            'name' => 'This is temporary component',
        ]);
        DB::table('component_try')->insert([
            'id' => '18',
            'title_en' => 'This is temporary component',
        ]);

        DB::table('page_benefits_inner')->insert([
            'head_id' => '2',
            'favorites_id' => '2',
            'opinion_id' => '5',
            'try_id' => '18',

            'front_title_en' => 'Security',
            'front_title_de' => 'Security',
            'front_title_fr' => 'Security',

            'front_text_en' => 'Security text En',
            'front_text_de' => 'Security text De',
            'front_title_fr' => 'Security text Fr',

            'slug_en' => 'security',
            'slug_de' => 'security',
            'slug_fr' => 'security',
        ]);

        DB::table('component_header')->insert([
            'id' => '3',
            'title_en' => 'Convenience header',
        ]);
        DB::table('component_opinion')->insert([
            'id' => '6',
            'name' => 'This is temporary component',
        ]);
        DB::table('component_try')->insert([
            'id' => '19',
            'title_en' => 'This is temporary component',
        ]);

        DB::table('page_benefits_inner')->insert([
            'head_id' => '3',
            'favorites_id' => '3',
            'opinion_id' => '6',
            'try_id' => '19',

            'front_title_en' => 'Convenience',
            'front_title_de' => 'Convenience',
            'front_title_fr' => 'Convenience',

            'front_text_en' => 'Convenience text En',
            'front_text_de' => 'Convenience text De',
            'front_title_fr' => 'Convenience text Fr',

            'slug_en' => 'convenience',
            'slug_de' => 'convenience',
            'slug_fr' => 'convenience',
        ]);


        DB::table('component_header')->insert([
            'id' => '4',
            'title_en' => 'Cost Effective header',
        ]);
        DB::table('component_opinion')->insert([
            'id' => '7',
            'name' => 'This is temporary component',
        ]);

        DB::table('component_try')->insert([
            'id' => '20',
            'title_en' => 'This is temporary component',
        ]);


        DB::table('page_benefits_inner')->insert([
            'head_id' => '4',
            'favorites_id' => '4',
            'opinion_id' => '7',
            'try_id' => '20',

            'front_title_en' => 'Cost Effective',
            'front_title_de' => 'Cost Effective',
            'front_title_fr' => 'High Performance',

            'front_text_en' => 'Cost Effective text En',
            'front_text_de' => 'Cost Effective text De',
            'front_title_fr' => 'Cost Effective text Fr',

            'slug_en' => 'cost-effective',
            'slug_de' => 'cost-effective',
            'slug_fr' => 'cost-effective',
        ]);
    }
}
