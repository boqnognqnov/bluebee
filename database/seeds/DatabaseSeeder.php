<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTableSeeder2::class);

        $this->call(ComponentSeeder2::class);
        $this->call(BenefitsItems2::class);
        $this->call(HomePage2::class);
        $this->call(methodsPage2::class);
        $this->call(PartnersItems2::class);
        $this->call(Cases2::class);
        $this->call(NewsSeed2::class);
        $this->call(EventsSeeder2::class);
        $this->call(DownloadsSeeder2::class);
        $this->call(KnowledgeCenterSeeder2::class);
        $this->call(FeaturesSeeder2::class);
        $this->call(ContactsAndOfficessSeeder2::class);
        $this->call(PipeLinesSeeder2::class);
        $this->call(TeamDataSeeder2::class);
        $this->call(AboutPageSeeder2::class);
        $this->call(JobItemsSeeder2::class);
        $this->call(DisclaimerPageSeeder::class);
        $this->call(PrivacyPageSeeder::class);


    }
}
