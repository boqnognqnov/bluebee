<?php

use Illuminate\Database\Seeder;

class FeaturesSeeder2 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('component_favorite')->insert([
            'id' => '7',
            'vector_model_name' => 'page_features',
            'vector_model_id' => '1',
        ]);

        DB::table('component_try')->insert([
            'id' => '10',
            'title_en' => 'This is temporary component',
            'title_de' => 'This is temporary component',
            'title_fr' => 'This is temporary component',
        ]);

        DB::table('component_header')->insert([
            'id' => '15',
            'title_en' => 'This is temporary component',
            'title_de' => 'This is temporary component',
            'title_fr' => 'This is temporary component',
        ]);

        DB::table('page_features')->insert([
            'id' => '1',
            'favorites_id' => '7',
            'head_id' => '15',
            'try_id' => '10',
        ]);
    }
}
