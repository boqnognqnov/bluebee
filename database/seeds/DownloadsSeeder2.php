<?php

use Illuminate\Database\Seeder;

class DownloadsSeeder2 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('component_header')->insert([
            'id' => '13',
            'title_en' => 'This is temporary component',
            'title_de' => 'This is temporary component',
            'title_fr' => 'This is temporary component',
        ]);


        DB::table('page_downloads')->insert([
            'id' => '1',
            'head_id' => '13',
            'sign_up_id' => '1',
        ]);

    }
}
