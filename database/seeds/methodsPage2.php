<?php

use Illuminate\Database\Seeder;

class methodsPage2 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('component_try')->insert([
            'id' => '2',
            'title_en' => 'This is temporary component',
        ]);

        DB::table('component_header')->insert([
            'id' => '24',
            'title_en' => 'High Performance header',
            'title_de' => 'High Performance header',
            'title_fr' => 'High Performance header',
        ]);

        DB::table('page_methods')->insert([
            'id' => '1',
            'title_en' => 'This is temporary title',

            'one_title_en' => 'Step 1',
            'second_title_en' => 'Step 2',
            'third_title_en' => 'Step 3',
            'head_id' => '24',

            'try_id' => '2',
        ]);
    }
}
