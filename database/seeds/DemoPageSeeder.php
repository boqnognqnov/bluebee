<?php

use Illuminate\Database\Seeder;

class DemoPageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('component_header')->insert([
            'id' => '22',
            'title_en' => 'This is temporary component',
            'title_de' => 'This is temporary component',
            'title_fr' => 'This is temporary component',
        ]);

        DB::table('page_demo')->insert([
            'id' => '1',
            'head_id' => '22',
        ]);

    }
}
