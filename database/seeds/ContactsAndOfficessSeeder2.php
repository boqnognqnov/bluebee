<?php

use Illuminate\Database\Seeder;

class ContactsAndOfficessSeeder2 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('component_header')->insert([
            'id' => '16',
            'title_en' => 'This is temporary component',
            'title_de' => 'This is temporary component',
            'title_fr' => 'This is temporary component',
        ]);

        DB::table('component_try')->insert([
            'id' => '11',
            'title_en' => 'This is temporary component',
            'title_de' => 'This is temporary component',
            'title_fr' => 'This is temporary component',
        ]);

        DB::table('page_contact')->insert([
            'id' => '1',
            'head_id' => '16',
            'try_id' => '11',
        ]);


        DB::table('component_header')->insert([
            'id' => '17',
            'title_en' => 'This is temporary component',
            'title_de' => 'This is temporary component',
            'title_fr' => 'This is temporary component',
        ]);

        DB::table('component_try')->insert([
            'id' => '12',
            'title_en' => 'This is temporary component',
            'title_de' => 'This is temporary component',
            'title_fr' => 'This is temporary component',
        ]);

        DB::table('page_contacts_inner')->insert([
            'id' => '1',
            'head_id' => '17',
            'try_id' => '12',
        ]);

    }
}
