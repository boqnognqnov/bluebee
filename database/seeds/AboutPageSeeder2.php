<?php

use Illuminate\Database\Seeder;

class AboutPageSeeder2 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('component_header')->insert([
            'id' => '19',
            'title_en' => 'High Partners page header',
            'title_de' => 'High Partners page header',
            'title_fr' => 'High Partners page header',
        ]);


        DB::table('component_try')->insert([
            'id' => '15',
            'title_en' => 'This is temporary component',
            'title_de' => 'This is temporary component',
            'title_fr' => 'This is temporary component',
        ]);

        DB::table('page_about')->insert([
            'id' => '1',
            'head_id' => '19',
            'try_id' => '15'
        ]);
    }
}
