<?php

use Illuminate\Database\Seeder;

class PipeLinesSeeder2 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('component_header')->insert([
            'id' => '21',
            'title_en' => 'This is temporary component',
            'title_de' => 'This is temporary component',
            'title_fr' => 'This is temporary component',
        ]);

        DB::table('component_try')->insert([
            'id' => '13',
            'title_en' => 'This is temporary component',
            'title_de' => 'This is temporary component',
            'title_fr' => 'This is temporary component',
        ]);

        DB::table('page_pipelines')->insert([
            'id' => '1',
            'try_id' => '13',
            'head_id' => '21',
        ]);

        DB::table('pipeline_cols')->insert([
            'id' => '1',
            'title_en' => 'This is title for col 1',
        ]);

        DB::table('pipeline_cols')->insert([
            'id' => '2',
            'title_en' => 'This is title for col 2',
        ]);

        DB::table('pipeline_cols')->insert([
            'id' => '3',
            'title_en' => 'This is title for col 3',
        ]);
    }
}
