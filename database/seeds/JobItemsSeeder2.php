<?php

use Illuminate\Database\Seeder;

class JobItemsSeeder2 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('component_header')->insert([
            'id' => '20',
            'title_en' => 'High Partners page header',
            'title_de' => 'High Partners page header',
            'title_fr' => 'High Partners page header',
        ]);

        DB::table('component_try')->insert([
            'id' => '16',
            'title_en' => 'This is temporary component',
            'title_de' => 'This is temporary component',
            'title_fr' => 'This is temporary component',
        ]);

        DB::table('page_join_team')->insert([
            'id' => '1',
            'head_id' => '20',
            'try_id' => '16',
        ]);
    }
}
