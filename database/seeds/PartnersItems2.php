<?php

use Illuminate\Database\Seeder;

class PartnersItems2 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('component_header')->insert([
            'id' => '5',
            'title_en' => 'High Partners page header',
        ]);

        DB::table('component_try')->insert([
            'id' => '3',
            'title_en' => 'This is temporary component',
        ]);

        DB::table('page_partners')->insert([
            'id' => '1',
            'head_id' => '5',
            'try_id' => '3',
        ]);
    }
}
