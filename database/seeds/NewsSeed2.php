<?php

use Illuminate\Database\Seeder;

class NewsSeed2 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('component_header')->insert([
            'id' => '11',
            'title_en' => 'This is temporary component',
        ]);


        DB::table('page_news')->insert([
            'id' => '1',
            'head_id' => '11',
            'sign_up_id' => '1',
        ]);

        DB::table('component_favorite')->insert([
            'id' => '5',
            'vector_model_name' => 'page_news_inner',
            'vector_model_id' => '1',
        ]);

        DB::table('component_header')->insert([
            'id' => '25',
            'title_en' => 'This is temporary component',
        ]);

        DB::table('page_news_inner')->insert([
            'id' => '1',
            'head_id' => '25',
            'favorites_id' => '5',
            'sign_up_id' => '1',
        ]);

    }
}
