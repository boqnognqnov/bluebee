<?php

use Illuminate\Database\Seeder;

class Cases2 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //        MAIN CASE PAGE
        DB::table('component_header')->insert([
            'id' => '6',
            'title_en' => 'This is temporary component',
        ]);
        DB::table('component_opinion')->insert([
            'id' => '3',
        ]);

        DB::table('component_try')->insert([
            'id' => '5',
            'title_en' => 'This is temporary component',
        ]);


        DB::table('page_cases')->insert([
            'id' => '1',
            'head_id' => '6',
            'opinion_id' => '3',
            'try_id' => '5',
        ]);
//END MAIN CASE PAGE
//        INNER CASE PAGE 1
        DB::table('component_opinion')->insert([
            'id' => '8',
        ]);

        DB::table('component_header')->insert([
            'id' => '7',
            'title_en' => 'This is temporary component',
        ]);

        DB::table('component_try')->insert([
            'id' => '6',
            'title_en' => 'This is temporary component',
        ]);

        DB::table('component_favorite')->insert([
            'id' => '8',
            'vector_model_name' => 'page_case_inner',
            'vector_model_id' => '1',
        ]);
//        DB::table('component_favorite')->insert([
//            'id' => '9',
//        ]);
//        DB::table('component_favorite')->insert([
//            'id' => '10',
//        ]);
//        DB::table('component_favorite')->insert([
//            'id' => '11',
//        ]);


        DB::table('page_case_inner')->insert([
            'id' => '1',
            'head_id' => '7',
            'try_id' => '6',
            'favorites_id' => '8',

            'opinion_id' => '8',

            'front_title_en' => 'Research Center',
            'front_title_de' => 'Research Center',
            'front_title_fr' => 'Research Center',

            'slug_en' => 'research-center',
            'slug_de' => 'research-center',
            'slug_fr' => 'research-center',


        ]);
        //   END INNER CASE PAGE 1
//        INNER CASE PAGE 2
        DB::table('component_opinion')->insert([
            'id' => '9',
        ]);

        DB::table('component_header')->insert([
            'id' => '8',
            'title_en' => 'This is temporary component',
        ]);

        DB::table('component_try')->insert([
            'id' => '7',
            'title_en' => 'This is temporary component',
        ]);

        DB::table('component_favorite')->insert([
            'id' => '9',
            'vector_model_name' => 'page_case_inner',
            'vector_model_id' => '2',
        ]);

        DB::table('page_case_inner')->insert([
            'id' => '2',
            'head_id' => '8',
            'try_id' => '7',
            'favorites_id' => '9',

            'opinion_id' => '9',

            'front_title_en' => 'Clinical Lab',
            'front_title_de' => 'Clinical Lab',
            'front_title_fr' => 'Clinical Lab',

            'slug_en' => 'clinical-lab',
            'slug_de' => 'clinical-lab',
            'slug_fr' => 'clinical-lab',
        ]);


        //        END INNER CASE PAGE 2
//        INNER CASE PAGE 3
        DB::table('component_opinion')->insert([
            'id' => '10',
        ]);

        DB::table('component_header')->insert([
            'id' => '9',
            'title_en' => 'This is temporary component',
        ]);

        DB::table('component_try')->insert([
            'id' => '8',
            'title_en' => 'This is temporary component',
        ]);

        DB::table('component_favorite')->insert([
            'id' => '10',
            'vector_model_name' => 'page_case_inner',
            'vector_model_id' => '3',
        ]);

        DB::table('page_case_inner')->insert([
            'id' => '3',
            'head_id' => '9',
            'try_id' => '8',
            'favorites_id' => '10',
            'opinion_id' => '10',

            'front_title_en' => 'Diagnostic Service Provider',
            'front_title_de' => 'Diagnostic Service Provider',
            'front_title_fr' => 'Diagnostic Service Provider',

            'slug_en' => 'diagnostic-service-provider',
            'slug_de' => 'diagnostic-service-provider',
            'slug_fr' => 'diagnostic-service-provider',
        ]);

//    END INNER CASE PAGE 3
//        INNER CASE PAGE 4
        DB::table('component_opinion')->insert([
            'id' => '11',
        ]);

        DB::table('component_header')->insert([
            'id' => '10',
            'title_en' => 'This is temporary component',
        ]);

        DB::table('component_try')->insert([
            'id' => '9',
            'title_en' => 'This is temporary component',
        ]);

        DB::table('component_favorite')->insert([
            'id' => '11',
            'vector_model_name' => 'page_case_inner',
            'vector_model_id' => '4',
        ]);

        DB::table('page_case_inner')->insert([
            'id' => '4',
            'head_id' => '10',
            'try_id' => '9',
            'favorites_id' => '11',
            'opinion_id' => '11',

            'front_title_en' => 'Sequencing Provider',
            'front_title_de' => 'Sequencing Provider',
            'front_title_fr' => 'Sequencing Provider',

            'slug_en' => 'sequencing-provider',
            'slug_de' => 'sequencing-provider',
            'slug_fr' => 'sequencing-provider',
        ]);

//    INNER CASE PAGE 4
    }
}
