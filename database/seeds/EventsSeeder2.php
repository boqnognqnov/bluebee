<?php

use Illuminate\Database\Seeder;

class EventsSeeder2 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('component_header')->insert([
            'id' => '12',
            'title_en' => 'This is temporary component',
        ]);
//        DB::table('component_sign_up')->insert([
//            'id' => '2',
//        ]);

        DB::table('page_events')->insert([
            'id' => '1',
            'head_id' => '12',
            'sign_up_id' => '1',
        ]);


        DB::table('component_favorite')->insert([
            'id' => '6',
            'vector_model_name' => 'page_events_inner',
            'vector_model_id' => '1',
        ]);

        DB::table('component_header')->insert([
            'id' => '26',
            'title_en' => 'This is temporary component',
        ]);
        DB::table('page_events_inner')->insert([
            'id' => '1',
            'head_id' => '26',
            'favorites_id' => '6',
            'sign_up_id' => '1',
        ]);
    }
}
