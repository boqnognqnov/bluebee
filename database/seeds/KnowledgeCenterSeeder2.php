<?php

use Illuminate\Database\Seeder;

class KnowledgeCenterSeeder2 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('component_header')->insert([
            'id' => '14',
            'title_en' => 'This is temporary component',
            'title_de' => 'This is temporary component',
            'title_fr' => 'This is temporary component',
        ]);

        DB::table('page_knowledge_center')->insert([
            'id' => '1',
            'head_id' => '14',
            'sign_up_id' => '1',
        ]);
    }
}
