<?php

use Illuminate\Database\Seeder;

class DisclaimerPageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('page_diclaimer')->insert([
            'id' => '1',
            'text_en' => ' ',
            'text_de' => ' ',
            'text_fr' => ' ',
        ]);
    }
}
