<?php

use Illuminate\Database\Seeder;

class UserTableSeeder2 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'id' => '1',
            'name' => 'bluebee',
            'email' => 'bluebee@abv.bg',
            'password' => Hash::make("Blue123"),
            'is_admin' => true,
            'is_approve' => true,
        ]);
    }
}
