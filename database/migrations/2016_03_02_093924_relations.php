<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Relations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('user_history', function (Blueprint $table) {
            $table->integer('user_id')->unsigned()->change();
            $table->foreign('user_id')->references('id')->on('users');
        });

        Schema::table('user_history', function (Blueprint $table) {
            $table->integer('coockie_id')->unsigned()->change();
            $table->foreign('coockie_id')->references('id')->on('user_by_coockie');
        });

        Schema::table('history_download', function (Blueprint $table) {
//            $table->integer('user_download_id')->unsigned()->change();
//            $table->foreign('user_download_id')->references('id')->on('user_history');
        });

        Schema::table('history_page', function (Blueprint $table) {
            $table->integer('user_page_id')->unsigned()->change();
            $table->foreign('user_page_id')->references('id')->on('user_history');
        });


        Schema::table('comment_news', function (Blueprint $table) {
            $table->integer('news_id')->unsigned()->change();
            $table->foreign('news_id')->references('id')->on('news');
        });

        Schema::table('news_to_tag', function (Blueprint $table) {
            $table->integer('news_id')->unsigned()->change();
            $table->foreign('news_id')->references('id')->on('news');
        });

        Schema::table('news_to_tag', function (Blueprint $table) {
            $table->integer('tag_id')->unsigned()->change();
            $table->foreign('tag_id')->references('id')->on('tags_news');
        });

        Schema::table('news', function (Blueprint $table) {
            $table->integer('category_id')->unsigned()->change();
            $table->foreign('category_id')->references('id')->on('category_news');
        });


        Schema::table('benefits_in_elements', function (Blueprint $table) {
            $table->integer('inner_benefit_id')->unsigned()->change();
            $table->foreign('inner_benefit_id')->references('id')->on('page_benefits_inner');
        });


        Schema::table('comment_events', function (Blueprint $table) {
            $table->integer('event_id')->unsigned()->change();
            $table->foreign('event_id')->references('id')->on('events');
        });

        Schema::table('events_to_tag', function (Blueprint $table) {
            $table->integer('event_id')->unsigned()->change();
            $table->foreign('event_id')->references('id')->on('events');
        });

        Schema::table('events_to_tag', function (Blueprint $table) {
            $table->integer('tag_id')->unsigned()->change();
            $table->foreign('tag_id')->references('id')->on('tags_events');
        });

        Schema::table('events', function (Blueprint $table) {
            $table->integer('category_id')->unsigned()->change();
            $table->foreign('category_id')->references('id')->on('category_events');
        });


        Schema::table('downloads', function (Blueprint $table) {
            $table->integer('category_id')->unsigned()->change();
            $table->foreign('category_id')->references('id')->on('category_downloads');
        });


        Schema::table('job_apply', function (Blueprint $table) {
            $table->integer('position_id')->unsigned()->change();
            $table->foreign('position_id')->references('id')->on('positions_to_apply');
        });


        Schema::table('page_team', function (Blueprint $table) {
            $table->integer('head_id')->unsigned()->change();
            $table->integer('try_id')->unsigned()->change();

            $table->foreign('head_id')->references('id')->on('component_header');
            $table->foreign('try_id')->references('id')->on('component_try');
        });


        Schema::table('page_join_team', function (Blueprint $table) {
            $table->integer('head_id')->unsigned()->change();
            $table->integer('try_id')->unsigned()->change();

            $table->foreign('head_id')->references('id')->on('component_header');
            $table->foreign('try_id')->references('id')->on('component_try');
        });


        Schema::table('page_contacts_inner', function (Blueprint $table) {
            $table->integer('head_id')->unsigned()->change();
            $table->integer('try_id')->unsigned()->change();

            $table->foreign('head_id')->references('id')->on('component_header');
            $table->foreign('try_id')->references('id')->on('component_try');
        });


        Schema::table('page_partners', function (Blueprint $table) {
            $table->integer('head_id')->unsigned()->change();
            $table->integer('try_id')->unsigned()->change();

            $table->foreign('head_id')->references('id')->on('component_header');
            $table->foreign('try_id')->references('id')->on('component_try');
        });

        Schema::table('page_features', function (Blueprint $table) {
            $table->integer('head_id')->unsigned()->change();
            $table->integer('try_id')->unsigned()->change();

            $table->foreign('head_id')->references('id')->on('component_header');
            $table->foreign('try_id')->references('id')->on('component_try');
        });


        Schema::table('page_downloads', function (Blueprint $table) {
            $table->integer('head_id')->unsigned()->change();

            $table->foreign('head_id')->references('id')->on('component_header');
        });

        Schema::table('page_benefits', function (Blueprint $table) {
            $table->integer('try_id')->unsigned()->change();
            $table->integer('opinion_id')->unsigned()->change();

            $table->foreign('try_id')->references('id')->on('component_try');
            $table->foreign('opinion_id')->references('id')->on('component_opinion');
        });

        Schema::table('page_events', function (Blueprint $table) {
            $table->integer('head_id')->unsigned()->change();

            $table->foreign('head_id')->references('id')->on('component_header');
        });

        Schema::table('page_benefits_inner', function (Blueprint $table) {
            $table->integer('head_id')->unsigned()->change();

            $table->foreign('head_id')->references('id')->on('component_header');
        });

        Schema::table('page_knowledge_center', function (Blueprint $table) {
            $table->integer('head_id')->unsigned()->change();

            $table->foreign('head_id')->references('id')->on('component_header');
        });

        Schema::table('page_about', function (Blueprint $table) {
            $table->integer('head_id')->unsigned()->change();
            $table->integer('try_id')->unsigned()->change();

            $table->foreign('head_id')->references('id')->on('component_header');
            $table->foreign('try_id')->references('id')->on('component_try');
        });

        Schema::table('page_case_inner', function (Blueprint $table) {
            $table->integer('head_id')->unsigned()->change();
            $table->integer('try_id')->unsigned()->change();

            $table->foreign('head_id')->references('id')->on('component_header');
            $table->foreign('try_id')->references('id')->on('component_try');
        });


        Schema::table('page_cases', function (Blueprint $table) {
            $table->integer('head_id')->unsigned()->change();
            $table->integer('opinion_id')->unsigned()->change();

            $table->foreign('head_id')->references('id')->on('component_header');
            $table->foreign('opinion_id')->references('id')->on('component_opinion');
        });

        Schema::table('page_contact', function (Blueprint $table) {
            $table->integer('head_id')->unsigned()->change();
            $table->integer('try_id')->unsigned()->change();

            $table->foreign('head_id')->references('id')->on('component_header');
            $table->foreign('try_id')->references('id')->on('component_try');
        });

        Schema::table('page_cases', function (Blueprint $table) {
            $table->integer('try_id')->unsigned()->change();

            $table->foreign('try_id')->references('id')->on('component_try');
        });

        Schema::table('page_methods', function (Blueprint $table) {
            $table->integer('try_id')->unsigned()->change();

            $table->foreign('try_id')->references('id')->on('component_try');
        });

        Schema::table('page_index', function (Blueprint $table) {
            $table->integer('try_id')->unsigned()->change();
            $table->integer('opinion_id')->unsigned()->change();

            $table->foreign('try_id')->references('id')->on('component_try');
            $table->foreign('opinion_id')->references('id')->on('component_opinion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
