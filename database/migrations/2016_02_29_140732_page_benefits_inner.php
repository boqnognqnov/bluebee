<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PageBenefitsInner extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_benefits_inner', function (Blueprint $table) {
            $table->increments('id');

            $table->boolean('head_is_on')->default(false);
            $table->integer('head_id');

            $table->boolean('favorites_is_on')->default(false);


            $table->string('front_title_en');
            $table->text('front_text_en');

            $table->string('front_title_de');
            $table->text('front_text_de');

            $table->string('front_title_fr');
            $table->text('front_text_fr');

            $table->text('front_text_s_en');
            $table->text('front_text_s_de');
            $table->text('front_text_s_fr');

            $table->string('front_image');
            $table->string('front_negative_image');
            $table->string('front_background_image');

            $table->integer('favorites_id');

            $table->boolean('opinion_is_on')->default(false);
            $table->integer('opinion_id');

            $table->boolean('try_is_on')->default(false);
            $table->integer('try_id');

            $table->string('slug_en');
            $table->string('slug_de');
            $table->string('slug_fr');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('page_benefits_inner');
    }
}
