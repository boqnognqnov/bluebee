<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PagePipelines extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_pipelines', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('head_is_on')->default(false);
            $table->integer('head_id');

            $table->string('image');

            $table->text('pipe_text_en');
            $table->text('pipe_text_de');
            $table->text('pipe_text_fr');


            $table->text('s_pipe_text_en');
            $table->text('s_pipe_text_de');
            $table->text('s_pipe_text_fr');


            $table->boolean('why_blue_is_on')->default(false);

            $table->boolean('try_is_on')->default(false);
            $table->integer('try_id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('page_pipelines');
    }
}
