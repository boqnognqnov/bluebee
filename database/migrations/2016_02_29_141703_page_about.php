<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PageAbout extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_about', function (Blueprint $table) {
            $table->increments('id');

            $table->boolean('head_is_on')->default(false);
            $table->integer('head_id');

            $table->string('intro_image');

            $table->string('intro_title_en');
            $table->text('intro_text_en');

            $table->string('intro_title_de');
            $table->text('intro_text_de');

            $table->string('intro_title_fr');
            $table->text('intro_text_fr');

            $table->string('intro_button_ref');
            $table->string('intro_button_en');
            $table->string('intro_button_de');
            $table->string('intro_button_fr');

            $table->string('services_title_en');
            $table->text('services_text_en');


            $table->string('services_title_de');
            $table->text('services_text_de');


            $table->string('services_title_fr');
            $table->text('services_text_fr');


            $table->string('team_image');

            $table->string('achievement_title_en');
            $table->text('achievement_text_en');

            $table->string('achievement_title_de');
            $table->text('achievement_text_de');


            $table->string('achievement_title_fr');
            $table->text('achievement_text_fr');


            $table->string('team_title_en');
            $table->text('team_text_en');
            $table->string('team_button_en');

            $table->string('team_title_de');
            $table->text('team_text_de');
            $table->string('team_button_de');

            $table->string('team_title_fr');
            $table->text('team_text_fr');
            $table->string('team_button_fr');


            $table->boolean('partners_is_on')->default(false);
            $table->boolean('try_is_on')->default(false);
            $table->integer('try_id');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('page_about');
    }
}
