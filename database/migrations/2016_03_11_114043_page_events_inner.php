<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PageEventsInner extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_events_inner', function (Blueprint $table) {
            $table->increments('id');

            $table->boolean('head_is_on')->default(false);
//            $table->string('head_image');
            $table->integer('head_id');

            $table->boolean('favorites_is_on')->default(false);
            $table->integer('favorites_id');
            $table->boolean('sign_up_is_on')->default(false);
            $table->integer('sign_up_id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('page_events_inner');
    }
}
