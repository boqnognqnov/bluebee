<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PageMethods extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_methods', function (Blueprint $table) {
            $table->increments('id');

            $table->boolean('head_is_on')->default(false);
//            $table->string('head_image');
            $table->integer('head_id');

            $table->string('title_en');
            $table->text('text_en');

            $table->string('title_de');
            $table->text('text_de');

            $table->string('title_fr');
            $table->text('text_fr');

            $table->string('one_image');

            $table->string('one_title_en');
            $table->text('one_text_en');

            $table->string('one_title_de');
            $table->text('one_text_de');

            $table->string('one_title_fr');
            $table->text('one_text_fr');

            $table->string('second_image');

            $table->string('second_title_en');
            $table->text('second_text_en');

            $table->string('second_title_de');
            $table->text('second_text_de');

            $table->string('second_title_fr');
            $table->text('second_text_fr');

            $table->string('third_image');

            $table->string('third_title_en');
            $table->text('third_text_en');

            $table->string('third_title_de');
            $table->text('third_text_de');

            $table->string('third_title_fr');
            $table->text('third_text_fr');

            $table->string('fourth_image');

            $table->string('fourth_title_en');
            $table->text('fourth_text_en');

            $table->string('fourth_title_de');
            $table->text('fourth_text_de');

            $table->string('fourth_title_fr');
            $table->text('fourth_text_fr');

            $table->boolean('why_blue_is_on')->default(false);

            $table->boolean('try_is_on')->default(false);
            $table->integer('try_id');


            $table->string('one_title_short_en');
            $table->text('one_text_short_en');

            $table->string('one_title_short_de');
            $table->text('one_text_short_de');

            $table->string('one_title_short_fr');
            $table->text('one_text_short_fr');
//opoppo
            $table->string('second_title_short_en');
            $table->text('second_text_short_en');

            $table->string('second_title_short_de');
            $table->text('second_text_short_de');

            $table->string('second_title_short_fr');
            $table->text('second_text_short_fr');
            //opoppo
            $table->string('third_title_short_en');
            $table->text('third_text_short_en');

            $table->string('third_title_short_de');
            $table->text('third_text_short_de');

            $table->string('third_title_short_fr');
            $table->text('third_text_short_fr');
            //opoppo
            $table->string('fourth_title_short_en');
            $table->text('fourth_text_short_en');

            $table->string('fourth_title_short_de');
            $table->text('fourth_text_short_de');

            $table->string('fourth_title_short_fr');
            $table->text('fourth_text_short_fr');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('page_methods');
    }
}
