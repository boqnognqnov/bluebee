<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image_avatar');
            $table->string('name');
            $table->string('phone');
            $table->string('company_name');
            $table->string('job_position');
            $table->string('email')->unique();
            $table->string('country');
            $table->string('account_type');

            $table->string('linkedin_url');
            $table->string('facebook_url');
            $table->string('twitter_url');

            $table->string('nickname');
            $table->string('network_id');


//            $table->string('handle');
//            $table->integer('twitter_id');

            $table->string('password', 60);
            $table->boolean('is_admin')->default(false);
            $table->boolean('is_approve')->default(false);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
