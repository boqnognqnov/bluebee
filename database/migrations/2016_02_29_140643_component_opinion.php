<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ComponentOpinion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('component_opinion', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->text('text_en');
            $table->string('button_en');

            $table->text('text_de');
            $table->string('button_de');

            $table->text('text_fr');
            $table->string('button_fr');

            $table->string('url');

            $table->string('image');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('component_opinion');
    }
}
