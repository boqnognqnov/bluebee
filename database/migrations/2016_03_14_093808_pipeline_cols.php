<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PipelineCols extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pipeline_cols', function (Blueprint $table) {
            $table->increments('id');

            $table->string('title_en');
            $table->string('title_de');
            $table->string('title_fr');

            $table->text('text_en');
            $table->text('text_de');
            $table->text('text_fr');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pipeline_cols');
    }
}
