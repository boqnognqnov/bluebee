<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ComponentTry extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('component_try', function (Blueprint $table) {
            $table->increments('id');

            $table->string('title_en');
            $table->text('text_en');
            $table->string('button_en');

            $table->string('title_de');
            $table->text('text_de');
            $table->string('button_de');

            $table->string('title_fr');
            $table->text('text_fr');
            $table->string('button_fr');

            $table->string('image');
            $table->string('url');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('component_try');
    }
}
