<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ComponentFavorites extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('component_favorite', function (Blueprint $table) {
            $table->increments('id');

            $table->string('f_model_name');
            $table->integer('f_model_id')->default(-1);

            $table->string('s_model_name');
            $table->integer('s_model_id')->default(-1);

            $table->string('t_model_name');
            $table->integer('t_model_id')->default(-1);

//            $table->boolean('is_child_model')->default(false);
            $table->string('vector_model_name');
            $table->integer('vector_model_id');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('component_favorite');
    }
}
