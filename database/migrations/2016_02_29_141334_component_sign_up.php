<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ComponentSignUp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('component_sign_up', function (Blueprint $table) {
            $table->increments('id');

            $table->string('text_en');
            $table->string('button_en');

            $table->string('text_fr');
            $table->string('button_fr');

            $table->string('text_de');
            $table->string('button_de');

            $table->string('image');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('component_sign_up');
    }
}
