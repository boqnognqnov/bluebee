<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class JobApply extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_apply', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('user_id');
            $table->integer('position_id');
            $table->string('image');
            $table->string('cv');
            $table->string('file_1');
            $table->string('file_2');
            $table->boolean('is_readed')->default(false);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('job_apply');
    }
}
