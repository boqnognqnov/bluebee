<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PageContact extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_contact', function (Blueprint $table) {
            $table->increments('id');

            $table->boolean('head_is_on')->default(false);
            $table->integer('head_id');

            $table->boolean('try_is_on')->default(false);
            $table->integer('try_id');

            $table->text('demo_requests_emails1');
            $table->text('demo_requests_emails2');
            $table->text('demo_requests_emails3');

            $table->text('pipelines_requests_emails1');
            $table->text('pipelines_requests_emails2');
            $table->text('pipelines_requests_emails3');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('page_contact');
    }
}
