<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CommentNews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comment_news', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('news_id');
            $table->integer('user_id');
            $table->string('image');
            $table->string('name');
            $table->string('email');

            $table->text('text');
            $table->boolean('is_approve')->default(false);
            $table->boolean('is_readed')->default(false);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('comment_news');
    }
}
