<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BenefitsInElements extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('benefits_in_elements', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('inner_benefit_id');

            $table->string('title_en');
            $table->text('text_en');

            $table->string('title_de');
            $table->text('text_de');

            $table->string('title_fr');
            $table->text('text_fr');

            $table->string('image');

            $table->string('slug_en');
            $table->string('slug_de');
            $table->string('slug_fr');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('benefits_in_elements');
    }
}
