<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HistoryPage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_page', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('user_page_id');
            $table->string('page_type');
            $table->integer('page_id');
            $table->integer('views');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('history_page');
    }
}
