<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PositionsToApply extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('positions_to_apply', function (Blueprint $table) {
            $table->increments('id');

            $table->string('image');
            $table->string('title_en');
            $table->text('requirements_en');
            $table->string('title_de');
            $table->text('requirements_de');
            $table->string('title_fr');
            $table->text('requirements_fr');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('positions_to_apply');
    }
}
