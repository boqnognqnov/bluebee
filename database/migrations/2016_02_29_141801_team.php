<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Team extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('team', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');

            $table->string('position_en');
            $table->string('position_de');
            $table->string('position_fr');

            $table->string('image');
            $table->text('description_en');
            $table->text('description_de');
            $table->text('description_fr');

            $table->boolean('is_manager')->default(false);

            $table->string('twitter');
            $table->string('linkedin');
            $table->string('email');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('team');
    }
}
