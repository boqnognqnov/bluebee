<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PageFeatures extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_features', function (Blueprint $table) {
            $table->increments('id');

            $table->boolean('head_is_on')->default(false);
            $table->integer('head_id');

            $table->boolean('try_is_on')->default(false);
            $table->integer('try_id');

            $table->boolean('favorites_is_on')->default(false);
            $table->integer('favorites_id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('page_features');
    }
}
