<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PageCaseInner extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_case_inner', function (Blueprint $table) {
            $table->increments('id');

            $table->boolean('head_is_on')->default(false);
            $table->integer('head_id');


            $table->string('case_image');

            $table->text('case_text_en');
            $table->string('case_second_title_en');
            $table->text('case_second_text_en');

            $table->text('case_text_de');
            $table->string('case_second_title_de');
            $table->text('case_second_text_de');

            $table->text('case_text_fr');
            $table->string('case_second_title_fr');
            $table->text('case_second_text_fr');

            $table->boolean('try_is_on')->default(false);
            $table->integer('try_id');

            $table->string('front_image');
            $table->string('front_title_en');
            $table->text('front_text_en');

            $table->string('front_title_de');
            $table->text('front_text_de');

            $table->string('front_title_fr');
            $table->text('front_text_fr');

            $table->string('slug_en');
            $table->string('slug_de');
            $table->string('slug_fr');

            $table->boolean('opinion_is_on')->default(false);
            $table->integer('opinion_id');
            

            $table->boolean('favorites_is_on')->default(false);
            $table->integer('favorites_id');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('page_case_inner');
    }
}
