<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ComponentHeader extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('component_header', function (Blueprint $table) {
            $table->increments('id');

            $table->string('image');
            $table->string('title_en');
            $table->text('text_en');

            $table->string('title_de');
            $table->text('text_de');

            $table->string('title_fr');
            $table->text('text_fr');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('component_header');
    }
}
