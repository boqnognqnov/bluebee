<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PageIndex extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_index', function (Blueprint $table) {
            $table->increments('id');

            $table->boolean('head_is_on')->default(false);
            $table->string('head_image');

            $table->string('head_title_en');
            $table->text('head_text_en');
            $table->string('head_button_en');

            $table->string('head_title_de');
            $table->text('head_text_de');
            $table->string('head_button_de');

            $table->string('head_title_fr');
            $table->text('head_text_fr');
            $table->string('head_button_fr');

            $table->string('head_button_url');

            $table->boolean('how_work_is_on');

            $table->boolean('why_blue_is_on');

            $table->boolean('partners_is_on');

            $table->boolean('opinion_is_on')->default(false);
            $table->integer('opinion_id');

            $table->boolean('try_is_on')->default(false);
            $table->integer('try_id');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('page_index');
    }
}
