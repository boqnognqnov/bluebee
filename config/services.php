<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'mandrill' => [
        'secret' => env('MANDRILL_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

//    'twitter' => [
//        'client_id' => env('Vw0wT83DMqn6z4b84BNOfCcWX'),
//        'client_secret' => env('HQEiq5DRKYZLFQswMGDS97hcuj0ggvbC4MNNBfdSpQ8GUYXz3g'),
//        'redirect' => env('http://twitter-auth.app:8000/auth/twitter/callback'),
//    ],

    'twitter' => [
        'client_id' => 'wPPLlalVdfGGAmg4jSKuiYeYK',
        'client_secret' => '8srPSW85bTdKCFaZz5kdeuX3qKN5uAnSkZ0sOI5rwSaNeRQ0ho',
//        'redirect' => 'http://bluebee.onecreative.eu/auth/twitter/login/callback',
        'redirect' => 'http://bluebee.onecreative.eu/twitter/login',
    ],
    'linkedin' => [
        'client_id' => '773r5tiebqk35y',
        'client_secret' => 'CoxHL1OHnhHZfEzk',
        'redirect' => 'http://bluebee.onecreative.eu/linkedin/login',
    ],
    'facebook' => [
        'client_id' => '1572623149695716',
        'client_secret' => '873af08b56629de429e0798a9df5721b',
        'redirect' => 'http://bluebee.onecreative.eu/facebook/login',
    ],
];
