<?php
return [

    'month' => [
        '01' => 'JAN',
        '02' => 'FEB',
        '03' => 'MAR',
        '04' => 'APR',
        '05' => 'MAY',
        '06' => 'JUN',
        '07' => 'JUL',
        '08' => 'AUG',
        '09' => 'SEP',
        '10' => 'OCT',
        '11' => 'NOV',
        '12' => 'DEC',
    ],
    'monthNamed' => [
        '01' => 'JANUARY',
        '02' => 'FEBRUARY',
        '03' => 'MARCH',
        '04' => 'APRIL',
        '05' => 'MAY',
        '06' => 'JUNE',
        '07' => 'JULE',
        '08' => 'AUGUST',
        '09' => 'SEPTEMBER',
        '10' => 'OCTOBER',
        '11' => 'NOVEMBER',
        '12' => 'DECEMBER',
    ],

    'defaultImagePath' => 'img/BBdefault.jpg',
    'defaultAvatar' => 'defaultAvatar.jpg',


    'contactReasons' => [
        0 => 'General Info',
        1 => 'Support Enquiry',
        2 => 'Pricing Enquiry',
        3 => 'Media &amp; PR Info',
        4 => 'Request from main contact page'
    ],
    'demoRequestUrls' => [
        'home' => [
            'button' => 'demo/header/home/0',
            'cTrial' => 'demo/component/home/0',
        ],
        'features' => [
            'button' => 'demo/header/features/0',
            'cTrial' => 'demo/component/features/0',
        ],
        'why_bluebee' => [
            'button' => 'demo/header/why_bluebee/',
            'cTrial' => 'demo/component/why_bluebee/',
        ],
        'how_work' => [
            'button' => 'demo/header/how_work/0',
            'cTrial' => 'demo/component/how_work/0',
        ],
        'pipelines' => [
            'button' => 'demo/header/pipelines/0',
            'cTrial' => 'demo/component/pipelines/0',
        ],
        'cases' => [
            'button' => 'demo/header/cases/',
            'cTrial' => 'demo/component/cases/',
        ],
        'join_the_team' => [
            'button' => 'demo/header/join_the_team/0',
            'cTrial' => 'demo/component/join_the_team/0',
        ],
        'about' => [
            'button' => 'demo/header/about/0',
            'cTrial' => 'demo/component/about/0',
        ],

    ],
    'request_types' => [
//        0 => 'Please select',
        1 => 'Schedule a demo',
        2 => 'Trial request',
        3 => 'Talk with an expert',
        4 => 'Pricing request',
    ],
    'job_scope' => [
//        '0' => 'choise',
        '1' => 'Research center',
        '2' => 'Clinical lab',
        '3' => 'Diagnostics test provider',
        '4' => 'Sequencing service provider',
        '5' => 'Other',

    ],
    'social_def_pass' => 'CAAWWStw0AuQBACCPG',
];