<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class SignUpComponentEntity extends Model
{

    protected $table = 'component_sign_up';

    public static $path = 'uploads' . '/' . 'images' . '/' . 'components' . '/' . 'signUpComponents' . '/' . 'default' . '/';
    public static $pathMedium = 'uploads' . '/' . 'images' . '/' . 'components' . '/' . 'signUpComponents' . '/' . 'medium' . '/';
    public static $pathSmall = 'uploads' . '/' . 'images' . '/' . 'components' . '/' . 'signUpComponents' . '/' . 'small' . '/';

    public static function editRules()
    {
        return array(
            'image' => 'image',

//            'title_en' => 'required',
            'text_en' => 'required',
            'button_en' => 'required',

//            'title_de' => 'required',
            'text_de' => 'required',
            'button_de' => 'required',

//            'title_fr' => 'required',
            'text_fr' => 'required',
            'button_fr' => 'required',


        );
    }

    public static function editMessages()
    {
        return [
//            'title_en.required' => 'Newsletter component title EN is required',
            'text_en.required' => 'Newsletter component text EN is required',
            'button_en.required' => 'Newsletter component button name EN is required',

//            'title_de.required' => 'Newsletter component title DE is required',
            'text_de.required' => 'Newsletter component text DE is required',
            'button_de.required' => 'Newsletter component button name DE is required',

//            'title_fr.required' => 'Trial CTA component title FR is required',
            'text_fr.required' => 'Newsletter component text FR is required',
            'button_fr.required' => 'Newsletter component button name FR is required',

//            'url.required' => 'Trial CTA component button url is required',
            'image.required' => 'Newsletter component the image is required',
            'image.image' => 'Newsletter component the file must be an image',

        ];
    }

    public static function validateSignUp($data, $id)
    {
        $component = SignUpComponentEntity::find($id);
        $rules = self::editRules();
        if (empty($component->image)) {
            $rules['image'] = 'required|image';
        }
        $validator = \Validator::make($data, $rules, self::editMessages());

        if ($validator->fails()) {
            return $validator;
        }

        return 'success';
    }

    public static function getSignUpObj($sign_id)
    {
        return SignUpComponentEntity::find($sign_id)->toArray();
    }

    public static function editSignComponent($data, $signId)
    {
        $langArr = ['en', 'de', 'fr'];

        try {

            $sign = SignUpComponentEntity::find($signId);


            foreach ($langArr as $oneLang) {
//                $title = 'title_' . $oneLang;
                $text = 'text_' . $oneLang;
                $button = 'button_' . $oneLang;
//                if (isset($data[$title])) {
//                    $sign->$title = $data[$title];
//                }
                if (isset($data[$text])) {
                    $sign->$text = $data[$text];
                }
                if (isset($data[$button])) {
                    $sign->$button = $data[$button];
                }
            }


            if (isset($data['image'])) {

                if (!empty($sign->image)) {
                    File::delete(self::$path . $sign->image);
                    File::delete(self::$pathMedium . $sign->image);
                    File::delete(self::$pathSmall . $sign->image);
                }

//                $time = time();
//                $fileName = $time . '.' . $data['image']->getClientOriginalExtension();
//                Image::make($data['image']->getRealPath())->save(self::$path . $fileName);
                $time = time();
                $fileName = $time . '.' . $data['image']->getClientOriginalExtension();
                $image = Image::make($data['image']->getRealPath());

//                $image->save(self::$path . $fileName);

                $image->resize(2400, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(self::$path . $fileName);

                $image->resize(1200, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(self::$pathMedium . $fileName);

                $image->resize(600, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(self::$pathSmall . $fileName);

                $sign->image = $fileName;
            }


            $sign->save();

        } catch (\Exception $ex) {
            \Log::error($ex);
        }

        return $sign->id;

    }


}
