<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class MethodsEntity extends Model
{

    protected $table = 'page_methods';

//    public static $path = 'uploads' . '/' . 'images' . '/' . 'pages' . '/' . 'whyBluebee' . '/' . 'default' . '/';
//    public static $pathMedium = 'uploads' . '/' . 'images' . '/' . 'pages' . '/' . 'whyBluebee' . '/' . 'medium' . '/';
//    public static $pathSmall = 'uploads' . '/' . 'images' . '/' . 'pages' . '/' . 'whyBluebee' . '/' . 'small' . '/';

    public static $pathStepsIMG = [
        'pathStep1' => 'uploads' . '/' . 'images' . '/' . 'pages' . '/' . 'whyBluebee' . '/' . 'step1' . '/' . 'default' . '/',
        'pathStep1m' => 'uploads' . '/' . 'images' . '/' . 'pages' . '/' . 'whyBluebee' . '/' . 'step1' . '/' . 'medium' . '/',
        'pathStep1s' => 'uploads' . '/' . 'images' . '/' . 'pages' . '/' . 'whyBluebee' . '/' . 'step1' . '/' . 'small' . '/',

        'pathStep2' => 'uploads' . '/' . 'images' . '/' . 'pages' . '/' . 'whyBluebee' . '/' . 'step2' . '/' . 'default' . '/',
        'pathStep2m' => 'uploads' . '/' . 'images' . '/' . 'pages' . '/' . 'whyBluebee' . '/' . 'step2' . '/' . 'medium' . '/',
        'pathStep2s' => 'uploads' . '/' . 'images' . '/' . 'pages' . '/' . 'whyBluebee' . '/' . 'step2' . '/' . 'small' . '/',

        'pathStep3' => 'uploads' . '/' . 'images' . '/' . 'pages' . '/' . 'whyBluebee' . '/' . 'step3' . '/' . 'default' . '/',
        'pathStep3m' => 'uploads' . '/' . 'images' . '/' . 'pages' . '/' . 'whyBluebee' . '/' . 'step3' . '/' . 'medium' . '/',
        'pathStep3s' => 'uploads' . '/' . 'images' . '/' . 'pages' . '/' . 'whyBluebee' . '/' . 'step3' . '/' . 'small' . '/',


        'pathStep4' => 'uploads' . '/' . 'images' . '/' . 'pages' . '/' . 'whyBluebee' . '/' . 'step4' . '/' . 'default' . '/',
        'pathStep4m' => 'uploads' . '/' . 'images' . '/' . 'pages' . '/' . 'whyBluebee' . '/' . 'step4' . '/' . 'medium' . '/',
        'pathStep4s' => 'uploads' . '/' . 'images' . '/' . 'pages' . '/' . 'whyBluebee' . '/' . 'step4' . '/' . 'small' . '/',
    ];

    public static function pageRules()
    {
        return array(
            'head_image' => 'image',

//            'title_en' => 'required',
//            'title_de' => 'required',
//            'title_fr' => 'required',

            'text_en' => 'required',
//            'text_de' => 'required',
//            'text_fr' => 'required',
//STEPS
            'one_image' => 'image',
            'one_title_en' => 'required',
//            'one_title_de' => 'required',
//            'one_title_fr' => 'required',

            'one_text_en' => 'required',
//            'one_text_de' => 'required',
//            'one_text_fr' => 'required',

            'second_image' => 'image',

            'second_title_en' => 'required',
//            'second_title_de' => 'required',
//            'second_title_fr' => 'required',

            'second_text_en' => 'required',
//            'second_text_de' => 'required',
//            'second_text_fr' => 'required',

            'third_image' => 'image',
            'third_title_en' => 'required',
//            'third_title_de' => 'required',
//            'third_title_fr' => 'required',

            'third_text_en' => 'required',
//            'third_text_de' => 'required',
//            'third_text_fr' => 'required',

            'fourth_image' => 'image',
            'fourth_title_en' => 'required',
//            'fourth_title_de' => 'required',
//            'fourth_title_fr' => 'required',

            'fourth_text_en' => 'required',
//            'fourth_text_de' => 'required',
//            'fourth_text_fr' => 'required',

//            STEPS

        );
    }

//

    public static function pageMessages()
    {
        return [
            'head_image.image' => 'The intro background image is not valid format',
            'head_image.required' => 'The intro background image is not set',

            'title_en.required' => 'The main section body title is not set in some languages.',
            'text_en.required' => 'The main section body text is not set in some languages.',

            'title_de.required' => 'The main section body title is not set in some languages.',
            'text_de.required' => 'The main section body text is not set in some languages.',

            'title_fr.required' => 'The main section body title is not set in some languages.',
            'text_fr.required' => 'The main section body text is not set in some languages.',
//            STEPS
            'one_image.image' => 'Step 1 image is not valid format',
            'one_image.required' => 'Step 1 image is not set',
            'one_title_en.required' => 'Step 1 title is not set in some languages.',
            'one_title_de.required' => 'Step 1 title is not set in some languages.',
            'one_title_fr.required' => 'Step 1 title is not set in some languages.',
            'one_text_en.required' => 'Step 1 text is not set in some languages.',
            'one_text_de.required' => 'Step 1 text is not set in some languages.',
            'one_text_fr.required' => 'Step 1 text is not set in some languages.',

            'second_image.image' => 'Step 2 image is not valid format',
            'second_image.required' => 'Step 2 image is not set',
            'second_title_en.required' => 'Step 2 title is not set in some languages.',
            'second_title_de.required' => 'Step 2 title is not set in some languages.',
            'second_title_fr.required' => 'Step 2 title is not set in some languages.',
            'second_text_en.required' => 'Step 2 text is not set in some languages.',
            'second_text_de.required' => 'Step 2 text is not set in some languages.',
            'second_text_fr.required' => 'Step 2 text is not set in some languages.',

            'third_image.image' => 'Step 3 image is not valid format',
            'third_image.required' => 'Step 3 image is not set',
            'third_title_en.required' => 'Step 3 title is not set in some languages.',
            'third_title_de.required' => 'Step 3 title is not set in some languages.',
            'third_title_fr.required' => 'Step 3 title is not set in some languages.',
            'third_text_en.required' => 'Step 3 text is not set in some languages.',
            'third_text_de.required' => 'Step 3 text is not set in some languages.',
            'third_text_fr.required' => 'Step 3 text is not set in some languages.',

            'fourth_image.image' => 'Step 4 image is not valid format',
            'fourth_image.required' => 'Step 4 image is not set',
            'fourth_title_en.required' => 'Step 4 title is not set in some languages.',
            'fourth_title_de.required' => 'Step 4 title is not set in some languages.',
            'fourth_title_fr.required' => 'Step 4 title is not set in some languages.',
            'fourth_text_en.required' => 'Step 4 text is not set in some languages.',
            'fourth_text_de.required' => 'Step 4 text is not set in some languages.',
            'fourth_text_fr.required' => 'Step 4 text is not set in some languages.',


//        STEPS
        ];
    }


    public static function editMethodsPage($data)
    {
        $rules = self::pageRules();
        $methods = MethodsEntity::all()->first();

        $headIsOn = false;
        if (isset($data['head_is_on'])) {
            $headIsOn = true;

            $headValidStatus = HeaderComponentEntity::validateComponent($data['head_data'], $methods->head_id);
            if ($headValidStatus != 'success') {
                return array('validationError', $headValidStatus);
            }
        }


        $methods->head_is_on = $headIsOn;

        $validator = \Validator::make($data, $rules, self::pageMessages());

        if ($validator->fails()) {
            return array('validationError', $validator);
        }

        $try_is_on = false;
        if (isset($data['try_is_on'])) {
            $try_is_on = true;

            $tryValidStatus = TryComponentEntity::validateTry($data['try_data'], $methods->try_id);
            if ($tryValidStatus != 'success') {
                return array('validationError', $tryValidStatus);
            }
        }

        $langArr = ['en', 'de', 'fr'];


        try {


            $methods->try_is_on = $try_is_on;


            $whyBlue_is_on = false;
            if (isset($data['why_blue_is_on'])) {
                $whyBlue_is_on = true;
            }
            $methods->why_blue_is_on = $whyBlue_is_on;

            foreach ($langArr as $oneLang) {

                $title = 'title_' . $oneLang;
                $text = 'text_' . $oneLang;


                if (isset($data[$title])) {
                    $methods->$title = $data[$title];
                }

                if (isset($data[$text])) {
                    $methods->$text = $data[$text];
                }


            }


//            $item->inner_benefit_id = $data['innerPageId'];


            $stepsImagesArr = ['1' => 'one_image', '2' => 'second_image', '3' => 'third_image', '4' => 'fourth_image'];

            foreach ($stepsImagesArr as $key => $oneImage) {
                if (isset($data[$oneImage])) {

                    if (!empty($methods->$oneImage)) {
                        File::delete(self::$pathStepsIMG['pathStep' . $key] . $methods->$oneImage);
                        File::delete(self::$pathStepsIMG['pathStep' . $key . 'm'] . $methods->$oneImage);
                        File::delete(self::$pathStepsIMG['pathStep' . $key . 's'] . $methods->$oneImage);
                    }

//                    $time = time();
//                    $fileName = $time . '.' . $data[$oneImage]->getClientOriginalExtension();
//                    Image::make($data[$oneImage]->getRealPath())->save(self::$pathStepsIMG['pathStep' . $key] . $fileName);
//                    $methods->$oneImage = $fileName;

                    $time = time();
                    $fileName = $time . '.' . $data[$oneImage]->getClientOriginalExtension();
                    $image = Image::make($data[$oneImage]->getRealPath());

//                $image->save(self::$path . $fileName);

                    $image->resize(2400, null, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save(self::$pathStepsIMG['pathStep' . $key] . $fileName);

                    $image->resize(1200, null, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save(self::$pathStepsIMG['pathStep' . $key . 'm'] . $fileName);

                    $image->resize(600, null, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save(self::$pathStepsIMG['pathStep' . $key . 's'] . $fileName);

                    $methods->$oneImage = $fileName;
                }
            }

            $stepsTitlesArr = ['one_title_', 'second_title_', 'third_title_', 'fourth_title_'];
            foreach ($stepsTitlesArr as $oneTitle) {
                foreach ($langArr as $oneLang) {
                    $tempTitle = $oneTitle . $oneLang;
                    if (isset($data[$tempTitle])) {
                        $methods->$tempTitle = $data[$tempTitle];
                    }

                    $tempShortTitle = $oneTitle . 'short_' . $oneLang;
                    if (isset($data[$tempShortTitle])) {
                        $methods->$tempShortTitle = $data[$tempShortTitle];
                    }
                }
            }

            $stepsTextsArr = ['one_text_', 'second_text_', 'third_text_', 'fourth_text_'];
            foreach ($stepsTextsArr as $oneText) {
                foreach ($langArr as $oneLang) {
                    $tempText = $oneText . $oneLang;
                    if (isset($data[$tempText])) {
                        $methods->$tempText = $data[$tempText];
                    }
                    $tempShortText = $oneText . 'short_' . $oneLang;
                    if (isset($data[$tempShortText])) {
                        $methods->$tempShortText = $data[$tempShortText];
                    }
                }
            }

//            FOR FIX
//            if (isset($data['one_title_short_en'])) {
//                $methods->one_title_short_en = $data['one_title_short_en'];
//            }
//            if (isset($data['one_text_short_en'])) {
//                $methods->one_text_short_en = $data['one_text_short_en'];
//            }
//
//            if (isset($data['second_title_short_en'])) {
//                $methods->second_title_short_en = $data['second_title_short_en'];
//            }
//            if (isset($data['second_text_short_en'])) {
//                $methods->second_text_short_en = $data['second_text_short_en'];
//            }
//
//            if (isset($data['third_title_short_en'])) {
//                $methods->third_title_short_en = $data['third_title_short_en'];
//            }
//            if (isset($data['third_text_short_en'])) {
//                $methods->third_text_short_en = $data['third_text_short_en'];
//            }
//
//            if (isset($data['fourth_title_short_en'])) {
//                $methods->fourth_title_short_en = $data['fourth_title_short_en'];
//            }
//            if (isset($data['fourth_text_short_en'])) {
//                $methods->fourth_text_short_en = $data['fourth_text_short_en'];
//            }
//            FOR FIX


            $methods->save();

            if (isset($data['try_data'])) {
                TryComponentEntity::editTryComponent($data['try_data'], $methods->try_id);
            }
            if (isset($data['head_data'])) {
                HeaderComponentEntity::editHeaderComponent($data['head_data'], $methods->head_id);
            }


        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('creatingError', $ex);
        }

        return array('successMessage', $methods);

    }

    public static function deleteImage($stepImage, $step)
    {
        $page = MethodsEntity::all()->first();

        $path = self::$pathStepsIMG;
        if ($stepImage == 'one') {
            File::delete($path['pathStep1'] . $stepImage);
            File::delete($path['pathStep1m'] . $stepImage);
            File::delete($path['pathStep1s'] . $page->$stepImage);
        } elseif ($stepImage == 'second') {
            File::delete($path['pathStep2'] . $stepImage);
            File::delete($path['pathStep2m'] . $stepImage);
            File::delete($path['pathStep2s'] . $stepImage);

        } elseif ($stepImage == 'third') {
            File::delete($path['pathStep3'] . $stepImage);
            File::delete($path['pathStep3m'] . $stepImage);
            File::delete($path['pathStep3s'] . $stepImage);

        } elseif ($stepImage == 'fourth') {
            File::delete($path['pathStep4'] . $stepImage);
            File::delete($path['pathStep4m'] . $stepImage);
            File::delete($path['pathStep4s'] . $stepImage);

        }

        $positionName = $step . '_image';
        $page->$positionName = '';
        $page->save();


    }

}
