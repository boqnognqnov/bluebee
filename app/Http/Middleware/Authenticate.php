<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  string|null $guard
     * @return mixed
     */


    public function handle($request, Closure $next, $guard = null)
    {

        if (Auth::guard($guard)->guest()) {
            if ($request->ajax()) {
                return redirect()->guest('auth/login');
            } else {
                return redirect()->guest('auth/login');
            }
        }

        if (Auth::check() && Auth::user()->is_admin == 1) {
            return $next($request);
        } else {
            Auth::logout();
            return redirect('auth/login');
        }

    }
//    public function handle($request, Closure $next, $guard = null)
//    {
//
////        if (\Auth::user()->id == 1 || \Auth::user()->is_admin == true) {
////
////        } else {
////            \Auth::logout();
////            return redirect('auth/login');
////        }
//
//        if (Auth::guard($guard)->guest()) {
//
////            if (Auth::user()->id == 1) {
////                return $next($request);
////            } else
//            if (Auth::user()->is_admin == true) {
//                return $next($request);
//            } else {
//                return redirect('admin/administrator/login');
//            }
//
////            if ($request->ajax() || $request->wantsJson()) {
////                return response('Unauthorized.', 401);
////            } else {
////                return redirect()->guest('login');
////            }
//        }
//
//        return $next($request);
//    }
}
