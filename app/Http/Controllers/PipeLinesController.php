<?php

namespace App\Http\Controllers;

use App\BenefitElementsEntity;
use App\BenefitInnerPagesEntity;
use App\BenefitPageEntity;
use App\DownloadsCategoriesEntity;
use App\DownloadsItemsEntity;
use App\DownloadsMainPageEntity;
use App\EventCategoriesEntity;
use App\EventChildPageEntity;
use App\EventItemsEntity;
use App\EventMainPageEntity;
use App\EventTagsEntity;
use App\Http\Requests;
use App\JobApplyItemsEntity;
use App\JobPositionsItemsEntity;
use App\JoinTheTeamPageEntity;
use App\MethodsEntity;
use App\NewsCategoriesEntity;
use App\NewsChildPageEntity;
use App\NewsEntity;
use App\NewsItems;
use App\NewsItemsEntity;
use App\NewsMainPageEntity;
use App\NewsTagsEntity;
use App\PartnersListEntity;
use App\PartnersPageEntity;
use App\PipeLinesColumnsEntity;
use App\PipeLinesItemsEntity;
use App\PipeLinesPageEntity;
use App\TagToEventsEntity;
use App\TagToNewsEntity;
use App\TeamItemsEntity;
use App\TeamPageEntity;
use Illuminate\Http\Request;


class PipeLinesController extends Controller
{

//NEWS MAIN PAGE
    public function showPage()
    {
        $pipeCols = PipeLinesColumnsEntity::all()->toArray();
        $pageData = PipeLinesPageEntity::all()->first()->toArray();
        return view('admin.pipeLines.page', ['pageData' => $pageData, 'pipeCols' => $pipeCols]);
    }

    public function updatePage(Request $request)
    {
        $data = $request->all();
//        return dump($data);
        $status = PipeLinesPageEntity::editPage($data);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'creatingError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'imageFailed':
                return back()->withInput()->withErrors(trans('modelStatusMessages.imageFailed'));
            case 'successMessage':
                return redirect()->back()->with('successMessage', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }

    public function deleteImage()
    {
        $status = PipeLinesPageEntity::deleteImage();
        return redirect()->back();
    }


//END OF MAIN NEWS PAGE


}