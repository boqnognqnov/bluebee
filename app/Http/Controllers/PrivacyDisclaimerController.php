<?php

namespace App\Http\Controllers;

use App\AboutPageEntity;
use App\BenefitElementsEntity;
use App\BenefitInnerPagesEntity;
use App\BenefitPageEntity;
use App\DisclaimerPageEntity;
use App\Http\Requests;
use App\MethodsEntity;
use App\NewsCategoriesEntity;
use App\NewsChildPageEntity;
use App\NewsEntity;
use App\NewsItems;
use App\NewsItemsEntity;
use App\NewsMainPageEntity;
use App\NewsTagsEntity;
use App\PartnersListEntity;
use App\PartnersPageEntity;
use App\PrivacyPageEntity;
use App\TagToNewsEntity;
use Illuminate\Http\Request;
use App\Classes\GlobalFunctions;


class PrivacyDisclaimerController extends Controller
{

    public function showPrivacyClient()
    {
        $page = PrivacyPageEntity::all()->first()->toArray();
        return view('privacy', ['pageData' => $page]);
    }

    public function showDisclaimerClient()
    {
        $page = DisclaimerPageEntity::all()->first()->toArray();
        return view('disclaimer', ['pageData' => $page]);
    }

    public function showPrivacy()
    {
        $page = PrivacyPageEntity::all()->first()->toArray();
//        return dump($page);
        return view('admin.privacyAndDisclaimer.pagePrivacy', ['pageData' => $page]);

    }

    public function updatePrivacy(Request $request)
    {
        $data = $request->all();
//        return dump($data);
        $status = PrivacyPageEntity::editPage($data);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'creatingError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'imageFailed':
                return back()->withInput()->withErrors(trans('modelStatusMessages.imageFailed'));
            case 'successMessage':
                return redirect()->back()->with('successMessage', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }


    public function showDisclaimer()
    {
        $page = DisclaimerPageEntity::all()->first()->toArray();
//        return dump($page);
        return view('admin.privacyAndDisclaimer.pageDisclaimer', ['pageData' => $page]);

    }

    public function updateDisclaimer(Request $request)
    {
        $data = $request->all();
//        return dump($data);
        $status = DisclaimerPageEntity::editPage($data);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'creatingError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'imageFailed':
                return back()->withInput()->withErrors(trans('modelStatusMessages.imageFailed'));
            case 'successMessage':
                return redirect()->back()->with('successMessage', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }


}