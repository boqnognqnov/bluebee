<?php

namespace App\Http\Controllers;

use App\BenefitElementsEntity;
use App\BenefitInnerPagesEntity;
use App\BenefitPageEntity;
use App\DownloadsCategoriesEntity;
use App\DownloadsItemsEntity;
use App\DownloadsMainPageEntity;
use App\EventCategoriesEntity;
use App\EventChildPageEntity;
use App\EventItemsEntity;
use App\EventMainPageEntity;
use App\EventTagsEntity;
use App\Http\Requests;
use App\MethodsEntity;
use App\NewsCategoriesEntity;
use App\NewsChildPageEntity;
use App\NewsEntity;
use App\NewsItems;
use App\NewsItemsEntity;
use App\NewsMainPageEntity;
use App\NewsTagsEntity;
use App\PartnersListEntity;
use App\PartnersPageEntity;
use App\TagToEventsEntity;
use App\TagToNewsEntity;
use Illuminate\Http\Request;


class FilesController extends Controller
{

//CATEGORIES

    public function listCategories($selectedCatId = 0)
    {
        $selectedCat = [];
        if ($selectedCatId != 0) {
//            return dump($selectedCatId);
            $selectedCat = DownloadsCategoriesEntity::find($selectedCatId)->toArray();
//            return dump($selectedCat);

        }
        $categories = DownloadsCategoriesEntity::all()->toArray();
        return view('admin.downloads.categories', ['categories' => $categories, 'selectedCat' => $selectedCat]);
    }

    public function addOrEditCategories(Request $request)
    {
        $data = $request->all();
        $status = DownloadsCategoriesEntity::addOrEditCategories($data);
        return redirect()->back();
    }

    public function destroyCategory(Request $request)
    {
        $id = $request->get('catId');
        $status = DownloadsCategoriesEntity::destroyCat($id);
        return redirect()->back();
    }






//END OF CATEGORIES
//NEWS MAIN PAGE
    public function showPage()
    {
        $mainPage = DownloadsMainPageEntity::all()->first()->toArray();
//        return dump($mainNewsPage);
        return view('admin.downloads.page', ['pageData' => $mainPage]);
    }

    public function updatePage(Request $request)
    {
        $data = $request->all();
//        return dump($data);
        $status = DownloadsMainPageEntity::editMainPage($data);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'creatingError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'imageFailed':
                return back()->withInput()->withErrors(trans('modelStatusMessages.imageFailed'));
            case 'successMessage':
                return redirect()->back()->with('successMessage', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }


//END OF MAIN NEWS PAGE


//NEWS ITEMS
    public function listFiles()
    {
        $fileList = DownloadsItemsEntity::all()->toArray();
        return view('admin.downloads.list', ['fileList' => $fileList]);

    }

    public function createFile()
    {
        $catListRaw = DownloadsCategoriesEntity::all()->toArray();
        $catList = [];
        foreach ($catListRaw as $oneCat) {
            $catList[$oneCat['id']] = $oneCat['title_en'];
        }
        return view('admin.downloads.addFile', ['catList' => $catList]);
    }

    public function showFileRecord($downloadId)
    {
        $record = DownloadsItemsEntity::find($downloadId)->toArray();
        $catListRaw = DownloadsCategoriesEntity::all()->toArray();
        $catList = [];
        foreach ($catListRaw as $oneCat) {
            $catList[$oneCat['id']] = $oneCat['title_en'];
        }
        return view('admin.downloads.updateFile', ['catList' => $catList, 'record' => $record]);
    }

    public function addOrUpdateFileRecord(Request $request)
    {
        $data = $request->all();
//
        $status = DownloadsItemsEntity::addOrEditRecord($data, $request);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'creatingError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'imageFailed':
                return back()->withInput()->withErrors(trans('modelStatusMessages.imageFailed'));
            case 'successMessage':
                return redirect('admin/downloads/list')->with('successMessage', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }


    public function destroyFileRecord(Request $request)
    {
        $id = $request->get('downloadRecordId');
        $status = DownloadsItemsEntity::destroyRecord($id);
        return redirect()->back();
    }

    public function downloadFile($file_id)
    {
        $fileRec = DownloadsItemsEntity::find($file_id);
        $pathToFile = DownloadsItemsEntity::$pathFile . $fileRec->file;
        return response()->download($pathToFile);
    }


//END OF NEWS ITEMS


}