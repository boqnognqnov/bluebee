<?php

namespace App\Http\Controllers;

use App\AboutPageEntity;
use App\BenefitElementsEntity;
use App\BenefitInnerPagesEntity;
use App\BenefitPageEntity;
use App\Classes\GlobalFunctions;
use App\ContactsChildPageEntity;
use App\ContactsMainPageEntity;
use App\DownloadsItemsEntity;
use App\EventItemsEntity;
use App\HomePageEntity;
use App\Http\Requests;
use App\JobPositionsItemsEntity;
use App\JoinTheTeamPageEntity;
use App\KnowCenterPageEntity;
use App\MethodsEntity;
use App\NewsItemsEntity;
use App\OfficesItemsEntity;
use App\OpinionComponentEntity;
use App\PartnersListEntity;
use App\PartnersPageEntity;
use App\TeamItemsEntity;
use App\TeamPageEntity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;


class CCollaborationController extends Controller
{


    public function indexMainPage()
    {


        $page = PartnersPageEntity::all()->first()->toArray();

        $pageData['raw'] = $page;

        $pageData['intro'] = GlobalFunctions::getHeaderCompData($page['head_id']);
        $pageData['trial'] = GlobalFunctions::getTrialCompData($page['try_id']);


        $partnersRaw = PartnersListEntity::all()->toArray();

        $counterRow = 0;
        $counterRec = 0;
        foreach ($partnersRaw as $key => $onePartner) {
            $counterRec++;
            $pageData['partners'][$counterRow][$counterRec] = $onePartner;
            if ($counterRec == 3) {
                $counterRow++;
            }

        }



//        return dump($pageData['partners']);

        return view('collaborations', ['pageData' => $pageData]);
    }


}