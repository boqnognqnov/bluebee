<?php

namespace App\Http\Controllers;

use App\BenefitElementsEntity;
use App\BenefitInnerPagesEntity;
use App\BenefitPageEntity;
use App\Classes\GlobalFunctions;
use App\DownloadsItemsEntity;
use App\EventItemsEntity;
use App\HomePageEntity;
use App\Http\Requests;
use App\KnowCenterPageEntity;
use App\MethodsEntity;
use App\NewsItemsEntity;
use App\OpinionComponentEntity;
use App\PartnersListEntity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;


class CHowWorksController extends Controller
{


    public function index()
    {
        $pageData = [];


        $page = MethodsEntity::all()->first()->toArray();
        $pageData['raw'] = $page;

        $pageData['benefit']['items'] = GlobalFunctions::getBenefitsCompData();

        $pageData['trial'] = GlobalFunctions::getTrialCompData($page['try_id']);
//        return dump($page['head_id']);

        $pageData = GlobalFunctions::getMethodsCompData($pageData);

//        $pageData['intro']['background']['default'] = MethodsEntity::$path . $page['head_image'];
//        $pageData['intro']['background']['medium'] = HomePageEntity::$pathMedium . $page['head_image'];
//        $pageData['intro']['background']['small'] = HomePageEntity::$pathSmall . $page['head_image'];
//        $pageData['intro']['title'] = $page['title_' . \App::getLocale()];
//        $pageData['intro']['body'] =  $page['text_' . \App::getLocale()];
//        $pageData['intro']['breadcrumb'] = '';
        $pageData['intro'] = GlobalFunctions::getHeaderCompData($page['head_id']);

        $demoRequestUrls = Config::get('GlobalArrays.demoRequestUrls');

        $trialSeeder = $demoRequestUrls['how_work'];
        $pageData['trial']['url'] = $trialSeeder['cTrial'];

//        return dump($pageData);


        return view('how-it-works', ['pageData' => $pageData, 'demoSeeder' => $trialSeeder['button']]);

    }


}