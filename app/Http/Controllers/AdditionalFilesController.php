<?php

namespace App\Http\Controllers;

use App\AboutPageEntity;
use App\AdditionalFilesEntity;
use App\BenefitElementsEntity;
use App\BenefitInnerPagesEntity;
use App\BenefitPageEntity;
use App\Http\Requests;
use App\MethodsEntity;
use App\NewsCategoriesEntity;
use App\NewsChildPageEntity;
use App\NewsEntity;
use App\NewsItems;
use App\NewsItemsEntity;
use App\NewsMainPageEntity;
use App\NewsTagsEntity;
use App\PartnersListEntity;
use App\PartnersPageEntity;
use App\TagToNewsEntity;
use Illuminate\Http\Request;
use App\Classes\GlobalFunctions;


class AdditionalFilesController extends Controller
{


    public function showPage()
    {
        $files = AdditionalFilesEntity::all()->toArray();

        return view('admin.additional.list', ['files' => $files]);

    }

    public function showCreateForm()
    {
        return view('admin.additional.createForm');
    }

    public function storeFile(Request $request)
    {
        $data = $request->all();
        $status = AdditionalFilesEntity::editFile($data, $request);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'creatingError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'imageFailed':
                return back()->withInput()->withErrors(trans('modelStatusMessages.imageFailed'));
            case 'successMessage':
                return redirect('admin/additioanal_files/list')->with('successMessage', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }

    }


    public function deleteFile(Request $request)
    {
        $data = $request->all();
        $status = AdditionalFilesEntity::destroyRecord($data['fileId']);
        return redirect()->back();
    }


    public function downloadFile($file_id)
    {
        $fileRec = AdditionalFilesEntity::find($file_id);
        $pathToFile = AdditionalFilesEntity::$path . $fileRec->file_name;
        return response()->download($pathToFile);
    }


}