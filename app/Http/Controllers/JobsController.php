<?php

namespace App\Http\Controllers;

use App\BenefitElementsEntity;
use App\BenefitInnerPagesEntity;
use App\BenefitPageEntity;
use App\DownloadsCategoriesEntity;
use App\DownloadsItemsEntity;
use App\DownloadsMainPageEntity;
use App\EventCategoriesEntity;
use App\EventChildPageEntity;
use App\EventItemsEntity;
use App\EventMainPageEntity;
use App\EventTagsEntity;
use App\Http\Requests;
use App\JobApplyItemsEntity;
use App\JobPositionsItemsEntity;
use App\JoinTheTeamPageEntity;
use App\MethodsEntity;
use App\NewsCategoriesEntity;
use App\NewsChildPageEntity;
use App\NewsEntity;
use App\NewsItems;
use App\NewsItemsEntity;
use App\NewsMainPageEntity;
use App\NewsTagsEntity;
use App\PartnersListEntity;
use App\PartnersPageEntity;
use App\TagToEventsEntity;
use App\TagToNewsEntity;
use App\TeamItemsEntity;
use App\TeamPageEntity;
use Illuminate\Http\Request;


class JobsController extends Controller
{

//NEWS MAIN PAGE
    public function showPage()
    {
        $mainPage = JoinTheTeamPageEntity::all()->first()->toArray();
        return view('admin.jobs.page', ['pageData' => $mainPage]);
    }

    public function updatePage(Request $request)
    {
        $data = $request->all();
//        return dump($data);
        $status = JoinTheTeamPageEntity::editPage($data);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'creatingError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'imageFailed':
                return back()->withInput()->withErrors(trans('modelStatusMessages.imageFailed'));
            case 'successMessage':
                return redirect()->back()->with('successMessage', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }


//END OF MAIN NEWS PAGE


//NEWS ITEMS
    public function listItems()
    {
        $list = JobPositionsItemsEntity::all()->toArray();
        return view('admin.jobs.openPositionlist', ['list' => $list]);

    }

    public function createItem()
    {
        return view('admin.jobs.createPosition');
    }

    public function updateOrStoreNewItem(Request $request)
    {
        $data = $request->all();
//        return dump($data);
        $status = JobPositionsItemsEntity::addOrEdit($data);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'creatingError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'imageFailed':
                return back()->withInput()->withErrors(trans('modelStatusMessages.imageFailed'));
            case 'successMessage':
                return redirect('admin/jobs/items/list')->with('successMessage', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }

    public function showItem($positionId)
    {
        $record = JobPositionsItemsEntity::find($positionId)->toArray();
        return view('admin.jobs.updatePosition', ['record' => $record]);
    }


    public function destroyItemById(Request $request)
    {
        $id = $request->get('positionId');
        $status = JobPositionsItemsEntity::destroyItem($id);
        return redirect()->back();
    }

    public function applyList()
    {
        $list = JobApplyItemsEntity::all()->toArray();
        return view('admin.jobs.applyList', ['data' => $list]);
    }

//END OF NEWS ITEMS


    public function deleteJoinTheTeamImage()
    {
        $status = JoinTheTeamPageEntity::deleteInnerImage();

        return redirect()->back();
    }


}