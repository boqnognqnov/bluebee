<?php

namespace App\Http\Controllers;

use App\BenefitElementsEntity;
use App\BenefitInnerPagesEntity;
use App\BenefitPageEntity;
use App\CasesInnerPageEntity;
use App\CasesMainPageEntity;
use App\Classes\GlobalFunctions;
use App\ContactsMainPageEntity;
use App\DownloadsCategoriesEntity;
use App\DownloadsItemsEntity;
use App\FeaturesItemEntity;
use App\FeaturesPageEntity;
use App\HomePageEntity;
use App\Http\Requests;
use App\MethodsEntity;
use App\OpinionComponentEntity;
use App\PartnersListEntity;
use App\PipeLinesColumnsEntity;
use App\PipeLinesPageEntity;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;


class CPipelinesController extends Controller
{


    public function index()
    {
        $pipePage = PipeLinesPageEntity::all()->first()->toArray();
        $pipeCols = PipeLinesColumnsEntity::all()->toArray();

        $pageData['intro'] = GlobalFunctions::getHeaderCompData($pipePage['head_id']);


        $pageData['trial'] = GlobalFunctions::getTrialCompData($pipePage['try_id']);
        $pageData['raw'] = $pipePage;
        $pageData['colls'] = $pipeCols;

        $downloadCatId = DownloadsCategoriesEntity::where('title_en', 'LIKE', 'Data cards')->get()->first()->id;
        $pageData['datacards'] = DownloadsItemsEntity::where('category_id', '=', $downloadCatId)->get()->toArray();

        $demoRequestUrls = Config::get('GlobalArrays.demoRequestUrls');

        $trialSeeder = $demoRequestUrls['pipelines'];
        $pageData['trial']['url'] = $trialSeeder['cTrial'];

        $pageData['userData'] = '';
        if (Auth::check() == true) {
            $pageData['userData']['email'] = Auth::user()->email;
        }

        return view('pipelines', ['pageData' => $pageData, 'demoSeeder' => $trialSeeder['button']]);
    }


    public
    static function postRules()
    {
        return array(
//            'g-recaptcha-response' => 'required|captcha',
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
//            'phone' => 'required',
            'company' => 'required',
            'job_title' => 'required',
        );
    }

    public static function postMessages()
    {
        return [
            'first_name.required' => 'The first name is required.',
            'last_name.required' => 'The last message is required.',
            'email.required' => 'The email is required.',
//            'phone.required' => 'The phone is required.',
            'company.required' => 'The your company is required.',
            'job_title.required' => 'The job title is required.',
        ];
    }


    public function pipeRequest(Request $request)
    {
        $data = $request->all();
//        return dump($data);


        $validator = \Validator::make($data, self::postRules(), self::postMessages());
        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator);
        }

        $user = User::where('email', 'LIKE', $data['email'])->get()->first();
        if ($user) {
            if (isset($data['phone'])) {
                $user->phone = $data['phone'];
            }
            $user->company_name = $data['company'];
            $user->job_position = $data['job_title'];
            $user->save();
        }
        try {

            \Mail::send('emails.dataCardRequest', $data, function ($message) use ($data) {
                $message->from($data['email'], $data['first_name'] . ' ' . $data['last_name']);

                $message->to('bozhidarovboyan@gmail.com', 'DataCard request')->subject('DataCard request');

                $emailList = ContactsMainPageEntity::all()->first()->toArray();
                for ($i = 1; $i < 4; $i++) {
                    if (!empty($emailList['pipelines_requests_emails' . $i])) {
                        $message->bcc($emailList['pipelines_requests_emails' . $i], 'DataCard request')->subject('DataCard request');
                    }
                }


//                $message->bcc('kbobokov@gmail.com', 'DataCard request')->subject('DataCard request');
//                $message->bcc('ivan.karabaliev@bluebee.com', 'DataCard request')->subject('DataCard request');


//                $message->bcc('ivan.karabaliev@bluebee.com', 'DataCard request')->subject('DataCard request');


//                $message->to('bozhidarovboyan@gmail.com', 'Info')->subject('Оставено съобщение от контакт формата на Royal Estate');
//                    $message->bcc('alarma7a@gmail.com', 'Info')->subject('Оставено съобщение от контакт формата на Royal Estate');
            });

        } catch (\Exception $ex) {
//            return back()->withErrors(trans('contacts.errorException'));
            return redirect()->back()->with('errorMessage', trans('contacts.errorException'));
        }
//        return redirect()->back()->with('successMessage', trans('contacts.successMessage'));

        return redirect()->back()->with('successMessage', 'Thank you for requesting our datacard, we will send it to you by email shortly.');


    }


}