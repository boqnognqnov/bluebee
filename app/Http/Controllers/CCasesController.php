<?php

namespace App\Http\Controllers;

use App\BenefitElementsEntity;
use App\BenefitInnerPagesEntity;
use App\BenefitPageEntity;
use App\CasesInnerPageEntity;
use App\CasesMainPageEntity;
use App\Classes\GlobalFunctions;
use App\FeaturesItemEntity;
use App\FeaturesPageEntity;
use App\HomePageEntity;
use App\Http\Requests;
use App\MethodsEntity;
use App\OpinionComponentEntity;
use App\PartnersListEntity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;


class CCasesController extends Controller
{


    public function index()
    {
//        return dump('sadasd');

        $lang = App::getLocale();

        $page = CasesMainPageEntity::all()->first()->toArray();

        $pageData['raw'] = $page;


        $pageData['intro'] = GlobalFunctions::getHeaderCompData($page['head_id']);

        $items = CasesInnerPageEntity::all()->toArray();

        foreach ($items as $key => $oneCase) {
            $pageData['uses'][$key]['title'] = $oneCase['front_title_' . $lang];
            $pageData['uses'][$key]['text'] = $oneCase['front_text_' . $lang];
            $imgPath = CasesInnerPageEntity::$pathStepsFront . $oneCase['front_image'];
            if (!empty($oneCase['front_image'])) {
                $pageData['uses'][$key]['image'] = $imgPath;
                $pageData['uses'][$key]['imgClass'] = GlobalFunctions::checkImgWidthHeight($imgPath);
            }
            $pageData['uses'][$key]['slug'] = $oneCase['slug_' . $lang];
        }

        $pageData['quote'] = GlobalFunctions::getOpinionCompData($page['opinion_id']);
        $pageData['trial'] = GlobalFunctions::getTrialCompData($page['try_id']);

        $demoRequestUrls = Config::get('GlobalArrays.demoRequestUrls');

        $trialSeeder = $demoRequestUrls['cases'];
        $pageData['trial']['url'] = $trialSeeder['cTrial'] . '0';

//        return dump($pageData);


        return view('use-cases', ['pageData' => $pageData, 'demoSeeder' => $trialSeeder['button'] . '0']);
    }

    public function indexInnerPage($slug)
    {
        $lang = App::getLocale();
        $page = CasesInnerPageEntity::where('slug_' . $lang, '=', $slug)->get()->first()->toArray();

        $pageData['raw'] = $page;
        $pageData['intro'] = GlobalFunctions::getHeaderCompData($page['head_id']);
        $pageData['trial'] = GlobalFunctions::getTrialCompData($page['try_id']);
        $pageData['quote'] = GlobalFunctions::getOpinionCompData($page['opinion_id']);
        $pageData['favoriteId'] = $page['favorites_id'];

        $demoRequestUrls = Config::get('GlobalArrays.demoRequestUrls');


        $trialSeeder = $demoRequestUrls['cases'];

        $pageData['trial']['url'] = $trialSeeder['cTrial'] . $page['id'];


        return view('use-cases-inner', ['pageData' => $pageData, 'demoSeeder' => $trialSeeder['button'] . $page['id']]);
    }


}