<?php

namespace App\Http\Controllers;

use App\BenefitElementsEntity;
use App\BenefitInnerPagesEntity;
use App\BenefitPageEntity;
use App\Classes\GlobalFunctions;
use App\FeaturesItemEntity;
use App\FeaturesPageEntity;
use App\HomePageEntity;
use App\Http\Requests;
use App\MethodsEntity;
use App\OpinionComponentEntity;
use App\PartnersListEntity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;


class CFeaturesController extends Controller
{


    public function index()
    {
//        return dump('sadasd');

        $lang = App::getLocale();

        $page = FeaturesPageEntity::all()->first()->toArray();

        $pageData = $page;


        $pageData['intro'] = GlobalFunctions::getHeaderCompData($page['head_id']);

        $featuresItems = FeaturesItemEntity::all()->toArray();

        foreach ($featuresItems as $key => $oneFeature) {
            $pageData['features'][$key + 1]['title'] = $oneFeature['title_' . $lang];
            $pageData['features'][$key + 1]['content'] = $oneFeature['text_' . $lang];
            $pageData['features'][$key + 1]['image'] = '';
            if (!empty($oneFeature['image'])) {
                $pageData['features'][$key + 1]['image'] = FeaturesItemEntity::$path . $oneFeature['image'];
            }

        }
        $pageData['trial'] = GlobalFunctions::getTrialCompData($page['try_id']);
        $pageData['favoriteId'] = $page['favorites_id'];

        $demoRequestUrls = Config::get('GlobalArrays.demoRequestUrls');

        $trialSeeder = $demoRequestUrls['features'];
        $pageData['trial']['url'] = $trialSeeder['cTrial'];

        return view('features', ['pageData' => $pageData, 'demoSeeder' => $trialSeeder['button']]);
    }

}