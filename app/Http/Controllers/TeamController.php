<?php

namespace App\Http\Controllers;

use App\BenefitElementsEntity;
use App\BenefitInnerPagesEntity;
use App\BenefitPageEntity;
use App\DownloadsCategoriesEntity;
use App\DownloadsItemsEntity;
use App\DownloadsMainPageEntity;
use App\EventCategoriesEntity;
use App\EventChildPageEntity;
use App\EventItemsEntity;
use App\EventMainPageEntity;
use App\EventTagsEntity;
use App\Http\Requests;
use App\MethodsEntity;
use App\NewsCategoriesEntity;
use App\NewsChildPageEntity;
use App\NewsEntity;
use App\NewsItems;
use App\NewsItemsEntity;
use App\NewsMainPageEntity;
use App\NewsTagsEntity;
use App\PartnersListEntity;
use App\PartnersPageEntity;
use App\TagToEventsEntity;
use App\TagToNewsEntity;
use App\TeamItemsEntity;
use App\TeamPageEntity;
use Illuminate\Http\Request;


class TeamController extends Controller
{

//NEWS MAIN PAGE
    public function showPage()
    {
        $mainPage = TeamPageEntity::all()->first()->toArray();
//        return dump($mainNewsPage);
        return view('admin.team.page', ['pageData' => $mainPage]);
    }

    public function updatePage(Request $request)
    {
        $data = $request->all();
//        return dump($data);
        $status = TeamPageEntity::editPage($data);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'creatingError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'imageFailed':
                return back()->withInput()->withErrors(trans('modelStatusMessages.imageFailed'));
            case 'successMessage':
                return redirect()->back()->with('successMessage', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }


//END OF MAIN NEWS PAGE


//NEWS ITEMS
    public function listEmployee()
    {
        $team = TeamItemsEntity::all()->toArray();
        return view('admin.team.list', ['teamList' => $team]);

    }

    public function createEmployee()
    {
        return view('admin.team.createEmployee');
    }

    public function addOrUpdateEmployee(Request $request)
    {
        $data = $request->all();
//        return dump($data);
        $status = TeamItemsEntity::addOrEdit($data);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'creatingError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'imageFailed':
                return back()->withInput()->withErrors(trans('modelStatusMessages.imageFailed'));
            case 'successMessage':
                return redirect('admin/team/list')->with('successMessage', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }

    public function showEmployee($downloadId)
    {
        $record = TeamItemsEntity::find($downloadId)->toArray();
        return view('admin.team.updateEmployee', ['record' => $record]);
    }


    public function destroyEmployee(Request $request)
    {
        $id = $request->get('employeeId');
        $status = TeamItemsEntity::destroyEmployye($id);
        return redirect()->back();
    }


//END OF NEWS ITEMS


}