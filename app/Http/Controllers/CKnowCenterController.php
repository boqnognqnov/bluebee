<?php

namespace App\Http\Controllers;

use App\BenefitElementsEntity;
use App\BenefitInnerPagesEntity;
use App\BenefitPageEntity;
use App\Classes\GlobalFunctions;
use App\DownloadsCategoriesEntity;
use App\DownloadsItemsEntity;
use App\EventCategoriesEntity;
use App\EventItemsEntity;
use App\HomePageEntity;
use App\Http\Requests;
use App\KnowCenterPageEntity;
use App\MethodsEntity;
use App\NewsItemsEntity;
use App\OpinionComponentEntity;
use App\PartnersListEntity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;


class CKnowCenterController extends Controller
{


    public function index()
    {
        $blueBeeDefaultPath = Config::get('GlobalArrays.defaultImagePath');
        $lang = App::getLocale();

        $page = KnowCenterPageEntity::all()->first()->toArray();

        $pageData = $page;

        $pageData['intro'] = GlobalFunctions::getHeaderCompData($page['head_id']);

        GlobalFunctions::oldEventsChecker();

        $news = NewsItemsEntity::orderBy('created_at', 'DESC')->get()->toArray();
        foreach ($news as $key => $oneNews) {
            if (empty($oneNews['image'])) {
                $pageData['newsData'][$key]['background']['default'] = $blueBeeDefaultPath;
                $pageData['newsData'][$key]['background']['medium'] = $blueBeeDefaultPath;
                $pageData['newsData'][$key]['background']['small'] = $blueBeeDefaultPath;
            } else {
                $pageData['newsData'][$key]['background']['default'] = NewsItemsEntity::$path . $oneNews['image'];
                $pageData['newsData'][$key]['background']['medium'] = NewsItemsEntity::$pathMedium . $oneNews['image'];
                $pageData['newsData'][$key]['background']['small'] = NewsItemsEntity::$pathSmall . $oneNews['image'];
            }


            $pageData['newsData'][$key]['title'] = $oneNews['title_' . $lang];
            $pageData['newsData'][$key]['text'] = $oneNews['text_' . $lang];
            $pageData['newsData'][$key]['slug'] = $oneNews['slug_' . $lang];


        }

        $currentDate = date("Y-m-d") . ' 00:00:00';


        $events = EventItemsEntity::orderBy('event_date', 'ASC')->where('event_date', '>=', $currentDate)->get()->toArray();
        foreach ($events as $key => $oneEvent) {

            if (empty($oneEvent['image'])) {
                $pageData['eventList'][$key]['background']['default'] = $blueBeeDefaultPath;
                $pageData['eventList'][$key]['background']['medium'] = $blueBeeDefaultPath;
                $pageData['eventList'][$key]['background']['small'] = $blueBeeDefaultPath;
            } else {
                $pageData['eventList'][$key]['background']['default'] = EventItemsEntity::$path . $oneEvent['image'];
                $pageData['eventList'][$key]['background']['medium'] = EventItemsEntity::$pathMedium . $oneEvent['image'];
                $pageData['eventList'][$key]['background']['small'] = EventItemsEntity::$pathSmall . $oneEvent['image'];
            }


            $pageData['eventList'][$key]['title'] = $oneEvent['title_' . $lang];
            $pageData['eventList'][$key]['text'] = $oneEvent['text_' . $lang];
            $pageData['eventList'][$key]['slug'] = $oneEvent['slug_' . $lang];
            $pageData['eventList'][$key]['event_date'] = $oneEvent['event_date'];


        }


        $downloadList = DownloadsItemsEntity::orderBy('created_at', 'DESC')->get()->toArray();
        foreach ($downloadList as $key => $download) {

            if (empty($download['image'])) {
                $pageData['downloads'][$key]['background']['default'] = $blueBeeDefaultPath;
                $pageData['downloads'][$key]['background']['medium'] = $blueBeeDefaultPath;
                $pageData['downloads'][$key]['background']['small'] = $blueBeeDefaultPath;
            } else {
                $pageData['downloads'][$key]['background']['default'] = DownloadsItemsEntity::$pathFrontImage . $download['image'];
                $pageData['downloads'][$key]['background']['medium'] = DownloadsItemsEntity::$pathFrontImageMedium . $download['image'];
                $pageData['downloads'][$key]['background']['small'] = DownloadsItemsEntity::$pathFrontImageSmall . $download['image'];
            }


            $pageData['downloads'][$key]['title'] = $download['title_' . $lang];
            $pageData['downloads'][$key]['text'] = $download['text_' . $lang];
            $pageData['downloads'][$key]['id'] = $download['id'];
            $pageData['downloads'][$key]['category_id'] = $download['category_id'];

        }


        $pageData['newsletter'] = GlobalFunctions::getNewsLetterCompData($page['sign_up_id']);
        $pageData['currentDataCardId'] = DownloadsCategoriesEntity::where('title_en', 'LIKE', 'Data cards')->get()->first()->id;


        return view('knowledge-center', ['pageData' => $pageData]);
    }


}