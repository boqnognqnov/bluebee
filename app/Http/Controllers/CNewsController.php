<?php

namespace App\Http\Controllers;

use App\BenefitElementsEntity;
use App\BenefitInnerPagesEntity;
use App\BenefitPageEntity;
use App\Classes\GlobalFunctions;
use App\CommentEventEntity;
use App\CommentNewsEntity;
use App\DownloadsItemsEntity;
use App\EventItemsEntity;
use App\HomePageEntity;
use App\Http\Requests;
use App\KnowCenterPageEntity;
use App\MethodsEntity;
use App\NewsCategoriesEntity;
use App\NewsChildPageEntity;
use App\NewsItemsEntity;
use App\NewsMainPageEntity;
use App\NewsTagsEntity;
use App\OpinionComponentEntity;
use App\PartnersListEntity;
use App\TagToNewsEntity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;


class CNewsController extends Controller
{


    public function index($criterion = null, $val = null)
    {

        $lang = App::getLocale();
        $page = NewsMainPageEntity::all()->first()->toArray();

        $newsList = [];
        if ($criterion == null) {
            $newsList = NewsItemsEntity::orderBy('created_at', 'DESC')->paginate(5);
        } elseif ($criterion == 'tag') {
            $newsList = NewsItemsEntity
                ::join('news_to_tag', 'news_to_tag.news_id', '=', 'news.id')
                ->join('tags_news', 'tags_news.id', '=', 'news_to_tag.tag_id')
                ->where('tags_news.title_' . $lang, 'LIKE', '%' . $val . '%')
                ->orderBy('created_at', 'DESC')
                ->select('news.*')
                ->paginate(5);

        } elseif ($criterion == 'category') {
            $newsList = NewsItemsEntity
                ::join('category_news', 'category_news.id', '=', 'news.category_id')
                ->where('category_news.title_' . $lang, 'LIKE', '%' . $val . '%')
                ->orderBy('created_at', 'DESC')
                ->select('news.*')
                ->paginate(5);
        } elseif ($criterion == 'year') {
            $newsList = NewsItemsEntity::where('created_at', '<', ($val + 1) . '-01-01 00:00:00')
                ->where('created_at', '>=', ($val) . '-01-01 00:00:00')
                ->orderBy('created_at', 'DESC')
                ->paginate(5);
        }


        $pageData['raw'] = $page;
        $pageData['intro'] = GlobalFunctions::getHeaderCompData($page['head_id']);
        $pageData['newsletter'] = GlobalFunctions::getNewsLetterCompData($page['sign_up_id']);


        $newsUpdateDateList = NewsItemsEntity::orderBy('created_at', 'DESC')->get(['created_at']);


        $pageData['newsList'] = $newsList;

        $pageData['sidebar']['categories'] = NewsCategoriesEntity
            ::join('news', 'news.category_id', '=', 'category_news.id')
//            ->where('news.title_en', 'NOT LIKE', '')
            ->select(\DB::raw('DISTINCT category_news.*'))
            ->get()->toArray();


        $pageData['sidebar']['tags'] = NewsTagsEntity
            ::join('news_to_tag', 'news_to_tag.tag_id', '=', 'tags_news.id')
//            ->where('news.title_en', 'NOT LIKE', '')
            ->select(\DB::raw('DISTINCT tags_news.*'))
            ->get()->toArray();


        $pageData['sidebar']['years'] = GlobalFunctions::collectYears($newsUpdateDateList, 'created_at');
//        return dump($pageData['sidebar']['years']);
        $pageData['sidebar']['itemType'] = 'news';


        return view('news', ['pageData' => $pageData]);
    }

    public function indexInnerPage($slug)
    {
        $lang = App::getLocale();
        $newsData = NewsItemsEntity::where('slug_' . $lang, 'LIKE', $slug)->get()->first()->toArray();
        $pageData['raw'] = NewsChildPageEntity::all()->first()->toArray();


        $newsUpdateDateList = NewsItemsEntity::orderBy('created_at', 'DESC')->get(['created_at']);

        $pageData['sidebar']['categories'] = NewsCategoriesEntity
            ::join('news', 'news.category_id', '=', 'category_news.id')
//            ->where('news.title_en', 'NOT LIKE', '')
            ->select(\DB::raw('DISTINCT category_news.*'))
            ->get()->toArray();

        $pageData['sidebar']['tags'] = NewsTagsEntity
            ::join('news_to_tag', 'news_to_tag.tag_id', '=', 'tags_news.id')
//            ->where('news.title_en', 'NOT LIKE', '')
            ->select(\DB::raw('DISTINCT tags_news.*'))
            ->get()->toArray();

        $pageData['sidebar']['years'] = GlobalFunctions::collectYears($newsUpdateDateList, 'created_at');

        $pageData['sidebar']['itemType'] = 'news';

        $pageData['newsData'] = $newsData;

        $pageData['newsLetter'] = GlobalFunctions::getNewsLetterCompData($pageData['raw']['sign_up_id']);

//        $pageData['intro']['background']['default'] = NewsChildPageEntity::$path . $pageData['raw']['head_image'];
//        $pageData['intro']['background']['medium'] = NewsChildPageEntity::$pathMedium . $pageData['raw']['head_image'];
//        $pageData['intro']['background']['small'] = NewsChildPageEntity::$pathSmall . $pageData['raw']['head_image'];
        $breadcrumbArrData['url'] = 'news/' . $newsData['slug_' . $lang];
        $breadcrumbArrData['title'] = $newsData['title_' . $lang];
        $pageData['intro'] = GlobalFunctions::getHeaderCompData($pageData['raw']['head_id'], $breadcrumbArrData);

        $pageData['newsletter'] = GlobalFunctions::getNewsLetterCompData($pageData['raw']['sign_up_id']);

        $pageData['comments'] = CommentNewsEntity::where('news_id', '=', $newsData['id'])->orderBy('created_at', 'DESC')->get()->toArray();

        $pageData['favoriteId'] = $pageData['raw']['favorites_id'];

        $pageData['raw']['userData'] = '';
        if (Auth::check() == true) {
            $pageData['raw']['userData']['name'] = Auth::user()->name;
            $pageData['raw']['userData']['email'] = Auth::user()->email;
        }


        return view('news-single', ['pageData' => $pageData]);
    }

    public function postComment(Request $request)
    {
        $data = $request->all();
//        return dump($data);
        $status = CommentNewsEntity::postComment($data);
//        return dump($status);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'creatingError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'imageFailed':
                return back()->withInput()->withErrors(trans('modelStatusMessages.imageFailed'));
            case 'successMessage':
                return redirect()->back()->with('successMessage', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }


}