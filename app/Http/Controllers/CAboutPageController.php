<?php

namespace App\Http\Controllers;

use App\AboutPageEntity;
use App\BenefitElementsEntity;
use App\BenefitInnerPagesEntity;
use App\BenefitPageEntity;
use App\Classes\GlobalFunctions;
use App\DownloadsItemsEntity;
use App\EventItemsEntity;
use App\HomePageEntity;
use App\Http\Requests;
use App\KnowCenterPageEntity;
use App\MethodsEntity;
use App\NewsItemsEntity;
use App\OfficesItemsEntity;
use App\OpinionComponentEntity;
use App\PartnersListEntity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;


class CAboutPageController extends Controller
{


    public function index()
    {

//        $lang = App::getLocale();

        $page = AboutPageEntity::all()->first()->toArray();

        $pageData['raw'] = $page;
//        return dump($page['head_id']);
        $pageData['intro'] = GlobalFunctions::getHeaderCompData($page['head_id']);
        $pageData['trial'] = GlobalFunctions::getTrialCompData($page['try_id']);
        $pageData['partners'] = GlobalFunctions::getPartnersCompData();
        $pageData['offices'] = OfficesItemsEntity::all()->toArray();

        $demoRequestUrls = Config::get('GlobalArrays.demoRequestUrls');

        $trialSeeder = $demoRequestUrls['about'];
        $pageData['trial']['url'] = $trialSeeder['cTrial'];


        return view('about', ['pageData' => $pageData, 'demoSeeder' => $trialSeeder['button']]);
    }


}