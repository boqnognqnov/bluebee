<?php

namespace App\Http\Controllers;

use App\BenefitElementsEntity;
use App\BenefitInnerPagesEntity;
use App\BenefitPageEntity;
use App\Classes\GlobalFunctions;
use App\HomePageEntity;
use App\Http\Requests;
use App\MethodsEntity;
use App\OpinionComponentEntity;
use App\PartnersListEntity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;


class CHomeController extends Controller
{


    public function index()
    {
//        return dump('sadasd');
        $pageData = [];
        $lang = App::getLocale();

        $homePage = HomePageEntity::all()->first()->toArray();

        $pageData = $homePage;

        $pageData['intro']['background']['default'] = HomePageEntity::$pathHeader . $homePage['head_image'];
        $pageData['intro']['background']['medium'] = HomePageEntity::$pathMedium . $homePage['head_image'];
        $pageData['intro']['background']['small'] = HomePageEntity::$pathSmall . $homePage['head_image'];
        $pageData['intro']['breadcrumb'] = [];

        $pageData['intro']['title'] = $homePage['head_title_' . $lang];
        $pageData['intro']['body'] = $homePage['head_text_' . $lang];



        $pageData['intro']['button']['text'] = $homePage['head_button_' . $lang];
        $pageData['intro']['button']['link'] = $homePage['head_button_url'];

        $pageData = GlobalFunctions::getMethodsCompData($pageData, true);

        $benefitPage = BenefitPageEntity::all()->first()->toArray();
        $pageData['benefit']['main_title'] = $benefitPage['title_' . $lang];
        $pageData['benefit']['main_text'] = $benefitPage['text_' . $lang];

        $pageData['benefit']['items'] = GlobalFunctions::getBenefitsCompData();

        //        BENEFIT


        $pageData['quote'] = GlobalFunctions::getOpinionCompData($homePage['opinion_id']);

        $pageData['partners'] = GlobalFunctions::getPartnersCompData();

        $pageData['trial'] = GlobalFunctions::getTrialCompData($homePage['try_id']);
//        return dump($pageData);

        $demoRequestUrls = Config::get('GlobalArrays.demoRequestUrls');

        $trialSeeder = $demoRequestUrls['home'];
        $pageData['trial']['url'] = $trialSeeder['cTrial'];


        return view('home', ['pageData' => $pageData, 'demoSeeder' => $trialSeeder['button']]);
    }

}