<?php

namespace App\Http\Controllers;

use App\AboutPageEntity;
use App\BenefitElementsEntity;
use App\BenefitInnerPagesEntity;
use App\BenefitPageEntity;
use App\Http\Requests;
use App\MethodsEntity;
use App\NewsCategoriesEntity;
use App\NewsChildPageEntity;
use App\NewsEntity;
use App\NewsItems;
use App\NewsItemsEntity;
use App\NewsMainPageEntity;
use App\NewsTagsEntity;
use App\PartnersListEntity;
use App\PartnersPageEntity;
use App\TagToNewsEntity;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Hash;
use App\Http\Controllers\Auth\AuthController;
use SocialNorm\Exceptions\ApplicationRejectedException;
use SocialNorm\Exceptions\InvalidAuthorizationCodeException;


class CSocialLoginController extends Controller
{
    public function getFacebookData($action)
    {

        \Session::put('network_action', $action);
        $sd = \SocialAuth::authorize('facebook');
        return $sd;
    }

    public function getLinkedinData($oldUrl)
    {
        $oldUrl = str_replace('-', '/', $oldUrl);
        \Session::put('old_url', $oldUrl);
        return \SocialAuth::authorize('linkedin');
    }


    public function facebookLogin(Request $request)
    {
//        return dump('DAAAA');
//        Auth::logout();

        $user = \Socialite::driver('facebook')->user();
        return dump($user);


        \SocialAuth::login('facebook', function ($user, $details) {

//            $user->email = $details->email;
//            $user->name = $details->raw()['first_name'] . ' ' . $details->raw()['last_name'];
//            if (empty($user->image_avatar)) {
//                $user->image_avatar = $details->avatar;
//            }
//            $user->save();
            return dump($user);
            $action = \Session::get('network_action');
            if ($action == 'photo') {
                return dump($details);
            }
        });

       return dump('asdasdas');


//        $user = Auth::user();
//
//        return redirect($oldUrl);
//
//        return \Redirect::intended();
    }

    public function linkedinLogin(Request $request)
    {
        \SocialAuth::login('linkedin', function ($user, $details) {


            $user->email = $details->email;
            $user->name = $details->raw()['firstName'] . ' ' . $details->raw()['lastName'];

            if (empty($user->image_avatar)) {
                $user->image_avatar = $details->avatar;
            }
//
            $user->save();
        });

        $user = Auth::user();
        $oldUrl = \Session::get('old_url');
        return redirect($oldUrl);
////
//        return \Redirect::intended();
    }


}