<?php

namespace App\Http\Controllers;

use App\BenefitElementsEntity;
use App\BenefitInnerPagesEntity;
use App\BenefitPageEntity;
use App\CasesInnerPageEntity;
use App\Classes\GlobalFunctions;
use App\ContactsMainPageEntity;
use App\DemoPageEntity;
use App\HomePageEntity;
use App\Http\Requests;
use App\MethodsEntity;
use App\OpinionComponentEntity;
use App\PartnersListEntity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;


class CDemoRequestController extends Controller
{

    private function getSelectedReqType($page, $clickPos)
    {
        $request_type = 0;

        if ($page == 'about') {
            if ($clickPos == 'Trial button on trial component') {
                $request_type = 3;
//            Talk with expert
            } else {
                $request_type = 2;
//            TRIAL
            }
        }

        if ($page == 'home') {
            if ($clickPos == 'Trial button on trial component') {
                $request_type = 2;
//            TRIAL
            } else {
                $request_type = 2;
//            TRIAL
            }
        }

        if ($page == 'pipelines') {
            if ($clickPos == 'Trial button on trial component') {
                $request_type = 3;
//            Talk with expert
            } else {
                $request_type = 2;
//            TRIAL
            }
        }

        if ($page == 'how_work') {
            if ($clickPos == 'Trial button on trial component') {
                $request_type = 2;
//            TRIAL
            } else {
                $request_type = 2;
//            TRIAL
            }
        }

        if ($page == 'features') {
            if ($clickPos == 'Trial button on trial component') {
                $request_type = 1;
//            DEMO
            } else {
                $request_type = 2;
//            TRIAL
            }
        }

        if ($page == 'why_bluebee') {
            if ($clickPos == 'Trial button on trial component') {
                $request_type = 1;
///            DEMO
            } else {
                $request_type = 2;
//            TRIAL
            }
        }

        if ($page == BenefitInnerPagesEntity::find(1)->front_title_en) {
            if ($clickPos == 'Trial button on trial component') {
                $request_type = 3;
//            Talk with expert
            } else {
                $request_type = 2;
//            TRIAL
            }
        }
        if ($page == BenefitInnerPagesEntity::find(2)->front_title_en) {
            if ($clickPos == 'Trial button on trial component') {
                $request_type = 3;
//            Talk with expert
            } else {
                $request_type = 2;
//            TRIAL
            }
        }
        if ($page == BenefitInnerPagesEntity::find(3)->front_title_en) {
            if ($clickPos == 'Trial button on trial component') {
                $request_type = 1;
//            DEMO
            } else {
                $request_type = 2;
//            TRIAL
            }
        }
        if ($page == BenefitInnerPagesEntity::find(4)->front_title_en) {
            if ($clickPos == 'Trial button on trial component') {
                $request_type = 1;
//            DEMO
            } else {
                $request_type = 2;
//            TRIAL
            }
        }


        if ($page == 'cases') {
            if ($clickPos == 'Trial button on trial component') {
                $request_type = 1;
//            DEMO
            } else {
                $request_type = 2;
//            TRIAL
            }
        }

        if ($page == CasesInnerPageEntity::find(1)->front_title_en) {
            if ($clickPos == 'Trial button on trial component') {
                $request_type = 3;
//            Talk with expert
            } else {
                $request_type = 2;
//            TRIAL
            }
        }

        if ($page == CasesInnerPageEntity::find(2)->front_title_en) {
            if ($clickPos == 'Trial button on trial component') {
                $request_type = 3;
//            Talk with expert
            } else {
                $request_type = 2;
//            TRIAL
            }
        }

        if ($page == CasesInnerPageEntity::find(3)->front_title_en) {
            if ($clickPos == 'Trial button on trial component') {
                $request_type = 3;
//            Talk with expert
            } else {
                $request_type = 2;
//            TRIAL
            }
        }

        if ($page == CasesInnerPageEntity::find(4)->front_title_en) {
            if ($clickPos == 'Trial button on trial component') {
                $request_type = 3;
//            Talk with expert
            } else {
                $request_type = 2;
//            TRIAL
            }
        }


        if ($page == 'Unknown page') {
            if ($clickPos == 'Trial button on trial component') {
                $request_type = 1;
//            DEMO
            } else {
                $request_type = 2;
//            TRIAL
            }
        }


        return $request_type;
    }

    public function indexDemoView($headerOrComponent = '', $page = '', $isInnerPage = 0)
    {

        $data = [];


        if ($page == 'why_bluebee' && $isInnerPage != '0') {
            $page = BenefitInnerPagesEntity::find($isInnerPage)->front_title_en;
        } elseif ($page == 'cases' && $isInnerPage != '0') {
            $page = CasesInnerPageEntity::find($isInnerPage)->front_title_en;
        } else if ($page == 'unknown') {
            $page = 'Unknown page';
        }


        if ($headerOrComponent == 'header') {
            $headerOrComponent = 'Trial button on header';
        } else {
            $headerOrComponent = 'Trial button on trial component';
        }


        $data['selected_req_type'] = $this->getSelectedReqType($page, $headerOrComponent);


        $data['request_page'] = $page;
        $data['request_click_position'] = $headerOrComponent;

        $dropdowns['request_types'] = Config::get('GlobalArrays.request_types');
        $dropdowns['job_scope'] = Config::get('GlobalArrays.job_scope');

//        return dump($data);
//        $data['request_types_dropdown'] = $requestDropdown;
        $demoPage = DemoPageEntity::all()->first()->toArray();
        $pageData['raw'] = $demoPage;
        $pageData['intro'] = GlobalFunctions::getHeaderCompData($demoPage['head_id']);


        return view('demo', ['pageData' => $pageData, 'dropdowns' => $dropdowns, 'data' => $data]);
    }

    public
    static function postRules()
    {
        return array(
            'g-recaptcha-response' => 'required|captcha',
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required',
//            'phone' => 'required',
            'company' => 'required',
            'job_title' => 'required',
//            'job_scope' => 'required',
            'request_type' => 'required',
//            'project_description' => 'required',
        );
    }

    public static function postMessages()
    {
        return [
            'first_name.required' => 'The first name is required.',
            'last_name.required' => 'The last message is required.',
            'email.required' => 'The email is required.',
            'phone.required' => 'The phone is required.',
            'company.required' => 'The company is required.',
            'job_title.required' => 'The job title is required.',
            'job_scope.required' => 'The job scope is required.',
            'request_type.required' => 'The request type is not set.',
            'project_description.required' => 'The message is required.',
        ];
    }

    public function postDemoRequest(Request $request)
    {
        $data = $request->all();
//        return dump($data);

        if ($data['job_scope'] == 0) {
            $data['job_scope'] = '';
        }

        if ($data['request_type'] == 0) {
            $data['request_type'] = '';
        }

        $validator = \Validator::make($data, self::postRules(), self::postMessages());
        if ($validator->fails()) {
//            return dump($validator);
//            return array('validationError', $validator);
            return back()->withInput()->withErrors($validator);
        }


        $request_type = Config::get('GlobalArrays.request_types');
        $job_scope = Config::get('GlobalArrays.job_scope');

        if (isset($job_scope[$data['job_scope']])) {
            $data['job_scope'] = $job_scope[$data['job_scope']];
        }

        $data['request_type'] = $request_type[$data['request_type']];

//        $listOfMailsRaw = ContactsMainPageEntity::all()->first()->demo_requests_emails;
//        $data['listOfMails'] = explode(';', $listOfMailsRaw);

//        return dump($data);
        try {

            \Mail::send('emails.demoRequest', $data, function ($message) use ($data) {
                $message->from($data['email'], $data['first_name'] . ' ' . $data['last_name']);


//                $message->to('ivan.karabaliev@bluebee.com', 'Request Demo')->subject('Request Demo');
                $message->to('bozhidarovboyan@gmail.com', 'Request Demo')->subject('Request Demo');
                $emailList = ContactsMainPageEntity::all()->first()->toArray();
                for ($i = 1; $i < 4; $i++) {
                    if (!empty($emailList['demo_requests_emails' . $i])) {
                        $message->bcc($emailList['demo_requests_emails' . $i], 'DataCard request')->subject('DataCard request');
                    }
                }

//                foreach ($data['listOfMails'] as $key => $onemail) {
//                    if ($key == 0) {
//                        $message->to($onemail, 'Request Demo')->subject('Request Demo');
//                    } else {
//                        $message->bcc($onemail, 'Request Demo')->subject('Request Demo');
//                    }
//                }
//                $message->bcc('kbobokov@gmail.com', 'Request Demo')->subject('Request Demo');
//                $message->bcc('kbobokov@gmail.com', 'Request Demo')->subject('Request Demo');
//                $message->to('bozhidarovboyan@gmail.com', 'Info')->subject('Оставено съобщение от контакт формата на Royal Estate');
//                    $message->bcc('alarma7a@gmail.com', 'Info')->subject('Оставено съобщение от контакт формата на Royal Estate');
            });

        } catch (\Exception $ex) {
            return back()->withErrors(trans('modelStatusMessages.messageError'));
        }
        return redirect()->back()->with('successMessage', trans('modelStatusMessages.messageSuccess'));

    }

}