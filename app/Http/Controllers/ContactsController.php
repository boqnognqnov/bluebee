<?php

namespace App\Http\Controllers;

use App\BenefitElementsEntity;
use App\BenefitInnerPagesEntity;
use App\BenefitPageEntity;
use App\ContactsChildPageEntity;
use App\ContactsMainPageEntity;
use App\Http\Requests;
use App\MethodsEntity;
use App\NewsCategoriesEntity;
use App\NewsChildPageEntity;
use App\NewsEntity;
use App\NewsItems;
use App\NewsItemsEntity;
use App\NewsMainPageEntity;
use App\NewsTagsEntity;
use App\OfficesItemsEntity;
use App\PartnersListEntity;
use App\PartnersPageEntity;
use App\TagToNewsEntity;
use Illuminate\Http\Request;
use App\Classes\GlobalFunctions;


class ContactsController extends Controller
{


//NEWS MAIN PAGE
    public function showMainPage()
    {
        $mainPage = ContactsMainPageEntity::all()->first()->toArray();


        $mails['pipelines'][1] = $mainPage['pipelines_requests_emails1'];
        $mails['pipelines'][2] = $mainPage['pipelines_requests_emails2'];
        $mails['pipelines'][3] = $mainPage['pipelines_requests_emails3'];

        $mails['demos'][1] = $mainPage['demo_requests_emails1'];
        $mails['demos'][2] = $mainPage['demo_requests_emails2'];
        $mails['demos'][3] = $mainPage['demo_requests_emails3'];


        return view('admin.contacts.page', ['pageData' => $mainPage, 'mails' => $mails]);
    }

    public function updateDemoRequestMails(Request $request)
    {
        $data = $request->all();
        $status = ContactsMainPageEntity::updateDemoMails($data['demo']);
        switch ($status[0]) {
            case 'emailIsNotValid':
                return back()->withInput()->withErrors($status[1]);
            case 'creatingError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'imageFailed':
                return back()->withInput()->withErrors(trans('modelStatusMessages.imageFailed'));
            case 'successMessage':
                return redirect()->back()->with('successMessage', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }

    public function updateDataCardRequestMails(Request $request)
    {
        $data = $request->all();
        $status = ContactsMainPageEntity::updateDataCardsMails($data['pipes']);
        switch ($status[0]) {
            case 'emailIsNotValid':
                return back()->withInput()->withErrors($status[1]);
            case 'creatingError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'imageFailed':
                return back()->withInput()->withErrors(trans('modelStatusMessages.imageFailed'));
            case 'successMessage':
                return redirect()->back()->with('successMessage', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }


    public function updateMainPage(Request $request)
    {
        $data = $request->all();
//        return dump($data);
        $status = ContactsMainPageEntity::editPage($data);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'creatingError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'imageFailed':
                return back()->withInput()->withErrors(trans('modelStatusMessages.imageFailed'));
            case 'successMessage':
                return redirect()->back()->with('successMessage', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }


//END OF MAIN NEWS PAGE

//CHILD NEWS PAGE
    public function showChildPage()
    {
        $childPage = ContactsChildPageEntity::all()->first()->toArray();
        return view('admin.contacts.childPage', ['pageData' => $childPage]);
    }

    public function updateChildPage(Request $request)
    {
        $data = $request->all();
//        return dump($data);
        $status = ContactsChildPageEntity::editPage($data);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'creatingError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'imageFailed':
                return back()->withInput()->withErrors(trans('modelStatusMessages.imageFailed'));
            case 'successMessage':
                return redirect()->back()->with('successMessage', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }

//END OF CHILD NEWS PAGE


//NEWS ITEMS
    public function listOffices()
    {
        $listOffices = OfficesItemsEntity::all()->toArray();
        return view('admin.contacts.list', ['officeList' => $listOffices]);

    }

    public function createOffice()
    {
        return view('admin.contacts.createOffice');
    }

    public function addOrUpdateOffice(Request $request)
    {
        $data = $request->all();

        $status = OfficesItemsEntity::addOrEdit($data);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'creatingError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'imageFailed':
                return back()->withInput()->withErrors(trans('modelStatusMessages.imageFailed'));
            case 'successMessage':
                return redirect('admin/contacts/offices/list')->with('successMessage', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }

    public function showOffice($officeId)
    {


        $office = OfficesItemsEntity::find($officeId)->toArray();
        return view('admin.contacts.updateOffice', ['office' => $office]);
    }


    public function destroyOffice(Request $request)
    {
        $officeId = $request->get('officeId');
        $status = OfficesItemsEntity::destroyOffice($officeId);
        return redirect()->back();
    }


//END OF NEWS ITEMS


}