<?php

namespace App\Http\Controllers;

use App\BenefitElementsEntity;
use App\BenefitInnerPagesEntity;
use App\BenefitPageEntity;
use App\CasesInnerPageEntity;
use App\CasesMainPageEntity;
use App\Classes\GlobalFunctions;
use App\DownloadsCategoriesEntity;
use App\FeaturesItemEntity;
use App\FeaturesPageEntity;
use App\HistoryDownloadEntity;
use App\HomePageEntity;
use App\Http\Requests;
use App\MethodsEntity;
use App\OpinionComponentEntity;
use App\PartnersListEntity;
use App\User;
use App\UserHistoryEntity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Hash;


class CProfileController extends Controller
{


    public function index()
    {
        $user = null;

        $formData = [];
        if (\Auth::check()) {
            $user = \Auth::user();
            $formData['id'] = $user->id;
            $formData['name'] = $user->name;
            $formData['email'] = $user->email;
            $formData['company_name'] = $user->company_name;
            $formData['job_position'] = $user->job_position;
            $formData['phone'] = $user->phone;
            $formData['country'] = $user->country;
            $formData['image_avatar'] = $user->image_avatar;

            $formData['account_type'] = $user->account_type;
            $formData['linkedin_url'] = $user->linkedin_url;
            $formData['facebook_url'] = $user->facebook_url;
            $formData['twitter_url'] = $user->twitter_url;
            $formData['nickname'] = $user->nickname;
        }

        $user_history = UserHistoryEntity::where('user_id', '=', $user->id)->get()->first();

        $historiesToDownload=[];
        $currentDataCardId = DownloadsCategoriesEntity::where('title_en', 'LIKE', 'Data cards')->get()->first()->id;
        if ($user_history) {
            $user_history_id = $user_history->id;
            $historiesToDownload = HistoryDownloadEntity::where('download_rec_id', '=', $user_history_id)->get()->toArray();

        }


//        return dump($historiesToDownload);


        return view('profile', ['userData' => $formData,
            'downloadHistory' => $historiesToDownload,
            'currentDataCardId' => $currentDataCardId

        ]);
    }

    public function updateProfile(Request $request)
    {
        $msg = '';
        $data = $request->all();

        $status = User::updateUserClient($request);

//        return dump($status);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'creatingError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'imageFailed':
                return back()->withInput()->withErrors(trans('modelStatusMessages.imageFailed'));
            case 'successMessage':
                return redirect()->back()->with('successMessage', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }

        return redirect()->back();
    }


}