<?php

namespace App\Http\Controllers;

use App\BenefitElementsEntity;
use App\BenefitInnerPagesEntity;
use App\BenefitPageEntity;
use App\CasesInnerPageEntity;
use App\CasesMainPageEntity;
use App\Http\Requests;
use App\MethodsEntity;
use Illuminate\Http\Request;


class CasesController extends Controller
{


    public function showMainPage()
    {
        $mainPage = CasesMainPageEntity::all()->first()->toArray();
        $listOfInnerPages = CasesInnerPageEntity::all()->toArray();
        return view('admin.cases.page', [
            'pageData' => $mainPage,
            'innerPagesList' => $listOfInnerPages,
        ]);
    }


    public function updateMainPage(Request $request)
    {
        $data = $request->all();
//        return dump($data);
        $status = CasesMainPageEntity::editPage($data);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'creatingError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'imageFailed':
                return back()->withInput()->withErrors(trans('modelStatusMessages.imageFailed'));
            case 'successMessage':
                return redirect()->back()->with('successMessage', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }

    }


    public function showInnerPage($innerPageId)
    {
        $innerPage = CasesInnerPageEntity::find($innerPageId)->toArray();
        return view('admin.cases.innerPage.page', [
            'pageData' => $innerPage,
        ]);
    }

    public function updateInnerPage(Request $request)
    {
        $data = $request->all();
        $status = CasesInnerPageEntity::editPage($data);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'creatingError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'imageFailed':
                return back()->withInput()->withErrors(trans('modelStatusMessages.imageFailed'));
            case 'successMessage':
                return redirect()->back()->with('successMessage', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }

    public function updateFrontCaseView(Request $request)
    {
        $data = $request->all();
        $status = CasesInnerPageEntity::editFrontSIde($data);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'creatingError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'imageFailed':
                return back()->withInput()->withErrors(trans('modelStatusMessages.imageFailed'));
            case 'successMessage':
                return redirect()->back()->with('successMessage', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }

    public function deleteCaseInnerImage($case_id)
    {
        $status = CasesInnerPageEntity::deleteInnerImage($case_id);
        return redirect()->back();
    }


}