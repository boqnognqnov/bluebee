<?php

namespace App\Http\Controllers;

use App\BenefitElementsEntity;
use App\BenefitInnerPagesEntity;
use App\BenefitPageEntity;
use App\Http\Requests;
use App\MethodsEntity;
use Illuminate\Http\Request;


class MethodsController extends Controller
{


    public function index()
    {
        $methodsData = MethodsEntity::all()->first()->toArray();
        return view('admin.methods.list', ['methodsData' => $methodsData]);
    }


    public function update(Request $request)
    {
        $data = $request->all();
//        return dump($data);
        $status = MethodsEntity::editMethodsPage($data);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'creatingError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'imageFailed':
                return back()->withInput()->withErrors(trans('modelStatusMessages.imageFailed'));
            case 'successMessage':
                return redirect()->back()->with('successMessage', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }

    }

    public function deleteStepImage($imageName, $step)
    {


        $status = MethodsEntity::deleteImage($imageName, $step);
        return redirect()->back();

    }


}