<?php

namespace App\Http\Controllers;

use App\AboutPageEntity;
use App\BenefitElementsEntity;
use App\BenefitInnerPagesEntity;
use App\BenefitPageEntity;
use App\Classes\GlobalFunctions;
use App\ContactsChildPageEntity;
use App\ContactsMainPageEntity;
use App\DownloadsItemsEntity;
use App\EventItemsEntity;
use App\HomePageEntity;
use App\Http\Requests;
use App\JobPositionsItemsEntity;
use App\JoinTheTeamPageEntity;
use App\KnowCenterPageEntity;
use App\MethodsEntity;
use App\NewsItemsEntity;
use App\OfficesItemsEntity;
use App\OpinionComponentEntity;
use App\PartnersListEntity;
use App\TeamItemsEntity;
use App\TeamPageEntity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;


class CContactsController extends Controller
{


    public function indexMainPage()
    {
        $reasons = Config::get('GlobalArrays.contactReasons');
        $officesListRaw = OfficesItemsEntity::all()->toArray();
        $officesList = [];
        foreach ($officesListRaw as $oneOffice) {
            $officesList[$oneOffice['id']] = $oneOffice['title_' . \App::getLocale()];
        }

        $page = ContactsMainPageEntity::all()->first()->toArray();

        $pageData['raw'] = $page;

        $pageData['intro'] = GlobalFunctions::getHeaderCompData($page['head_id']);
        $pageData['trial'] = GlobalFunctions::getTrialCompData($page['try_id']);

        $pageData['offices'] = OfficesItemsEntity::all()->toArray();

        return view('contact', ['pageData' => $pageData, 'reasons' => $reasons, 'officesList' => $officesList]);
    }

    public function indexOfficePage($officeID)
    {
        $reasons = Config::get('GlobalArrays.contactReasons');
        $officesListRaw = OfficesItemsEntity::all()->toArray();
        $officesList = [];
        foreach ($officesListRaw as $oneOffice) {
            $officesList[$oneOffice['id']] = $oneOffice['title_' . \App::getLocale()];
        }

        $page = ContactsChildPageEntity::all()->first()->toArray();
        $office = OfficesItemsEntity::find($officeID)->toArray();

        $pageData['raw'] = $page;

        $pageData['intro'] = GlobalFunctions::getHeaderCompData($page['head_id']);
        $pageData['trial'] = GlobalFunctions::getTrialCompData($page['try_id']);

        $pageData['office'] = $office;
        $coordinates = $pageData['office']['coordinates'];
        $coordArr = explode(',', $coordinates);
        $pageData['office']['coordA'] = $coordArr[0];
        $pageData['office']['coordB'] = $coordArr[1];


        return view('contact-office', ['pageData' => $pageData, 'reasons' => $reasons, 'officesList' => $officesList]);
    }

    public
    static function postRules()
    {
        return array(
//            'g-recaptcha-response' => 'required|captcha',
            'g-recaptcha-response' => 'captcha',
            'firstName' => 'required',
            'lastName' => 'required',
            'company' => 'required',
//            'phone' => 'required',
            'email' => 'required',
            'message' => 'required',
        );
    }

    public static function postMessages()
    {
        return [
            'firstName.required' => 'The first name is required.',
            'lastName.required' => 'The last message is required.',
            'company.required' => 'The company is required.',
            'phone.required' => 'The phone is required.',
            'email.required' => 'The email is required.',
            'message.required' => 'The message is required.',
        ];
    }

    public function contactMail(Request $request)
    {
        $requestArr = $request->all();
//        return dump($requestArr);
        $reasons = Config::get('GlobalArrays.contactReasons');
        $office = OfficesItemsEntity::find($requestArr['officeId'])->toArray();

        $data['office']['contactReason'] = $reasons[$requestArr['contactReason']];
        $data['office']['email'] = $office['email'];
//        CLIENT
        $data['client']['firstName'] = $requestArr['firstName'];
        $data['client']['lastName'] = $requestArr['lastName'];
        $data['client']['company'] = $requestArr['company'];
        $data['client']['phone'] = $requestArr['phone'];
        $data['client']['email'] = $requestArr['email'];
        $data['client']['message'] = $requestArr['message'];

        $validator = \Validator::make($data['client'], self::postRules(), self::postMessages());
        if ($validator->fails()) {
//            return dump($validator);
            return back()->withInput()->withErrors($validator);
//            return array('validationError', $validator);
        }


        try {

            \Mail::send('emails.contactRequest', $data, function ($message) use ($data) {
                $message->from($data['client']['email'], $data['client']['firstName'] . ' ' . $data['client']['lastName']);

                $message->to($data['office']['email'], 'Contact form')->subject($data['office']['contactReason']);
                $message->bcc('ivan.karabaliev@bluebee.com', 'Contact form')->subject($data['office']['contactReason']);
//                $message->to('bozhidarovboyan@gmail.com', 'Info')->subject('Оставено съобщение от контакт формата на Royal Estate');
//                    $message->bcc('alarma7a@gmail.com', 'Info')->subject('Оставено съобщение от контакт формата на Royal Estate');
            });

        } catch (\Exception $ex) {
            return back()->withErrors(trans('successMessage.messageError'));
        }
        return redirect()->back()->with('successMessage', trans('modelStatusMessages.messageSuccess'));


    }


}