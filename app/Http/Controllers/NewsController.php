<?php

namespace App\Http\Controllers;

use App\BenefitElementsEntity;
use App\BenefitInnerPagesEntity;
use App\BenefitPageEntity;
use App\CommentNewsEntity;
use App\Http\Requests;
use App\MethodsEntity;
use App\NewsCategoriesEntity;
use App\NewsChildPageEntity;
use App\NewsEntity;
use App\NewsItems;
use App\NewsItemsEntity;
use App\NewsMainPageEntity;
use App\NewsTagsEntity;
use App\PartnersListEntity;
use App\PartnersPageEntity;
use App\TagToNewsEntity;
use Illuminate\Http\Request;
use App\Classes\GlobalFunctions;


class NewsController extends Controller
{

//CATEGORIES
    public function listCategories($categoryId = 0)
    {
        $categoryList = NewsCategoriesEntity::all()->toArray();
        $selectedCat = [];
        if ($categoryId != 0) {
            $selectedCat = NewsCategoriesEntity::find($categoryId)->toArray();
        }
        return view('admin.news.categoryList', [
            'catList' => $categoryList,
            'selectedCat' => $selectedCat
        ]);
    }

    public function addOrEditCategories(Request $request)
    {
        $data = $request->all();
        $status = NewsCategoriesEntity::addOrEditCategories($data);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'creatingError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'imageFailed':
                return back()->withInput()->withErrors(trans('modelStatusMessages.imageFailed'));
            case 'successMessage':
                return redirect()->back()->with('successMessage', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }

    }

    public function destroyCategory(Request $request)
    {
        $categoyId = $request->get('catId');

        $status = NewsCategoriesEntity::destroyCat($categoyId);
        return redirect()->back();
    }

//END OF CATEGORIES


//NEWS MAIN PAGE
    public function editMainPage()
    {
        $mainNewsPage = NewsMainPageEntity::all()->first()->toArray();
//        return dump($mainNewsPage);
        return view('admin.news.page', ['pageData' => $mainNewsPage]);
    }

    public function updateNewsMainPage(Request $request)
    {
        $data = $request->all();
//
        $status = NewsMainPageEntity::editMainNewsPagePage($data);
//        return dump($status);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'creatingError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'imageFailed':
                return back()->withInput()->withErrors(trans('modelStatusMessages.imageFailed'));
            case 'successMessage':
                return redirect()->back()->with('successMessage', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }


//END OF MAIN NEWS PAGE


//NEWS ITEMS
    public function listNews()
    {
        $listOfNews = NewsItemsEntity::all()->toArray();
        return view('admin.news.list', ['newsList' => $listOfNews]);

    }

    public function listCommentNews()
    {
        $listOfComment = CommentNewsEntity::orderBy('news_id', 'ASC')->get()->toArray();
        return view('admin.news.commentList', ['commentList' => $listOfComment]);

    }

    public function deleteCommentNews(Request $request)
    {
        $data = $request->all();
        CommentNewsEntity::destroy($data['commentId']);
        return redirect()->back();
    }

    public function createNews()
    {
        $newsCatListRaw = NewsCategoriesEntity::all()->toArray();
        $newsCatList = [];
        foreach ($newsCatListRaw as $oneCat) {
            $newsCatList[$oneCat['id']] = $oneCat['title_en'];
        }

        return view('admin.news.createNews', ['newsCatList' => $newsCatList]);
    }

    public function showNews($newsId)
    {


        $news = NewsItemsEntity::find($newsId)->toArray();
        $newsCatListRaw = NewsCategoriesEntity::all()->toArray();
        $newsCatList = [];
        foreach ($newsCatListRaw as $oneCat) {
            $newsCatList[$oneCat['id']] = $oneCat['title_en'];
        }
//        return dump($news);
        $news['created_at'] = GlobalFunctions::generateDateTimeToStr($news['created_at']);

        return view('admin.news.updateNews', ['newsCatList' => $newsCatList, 'selectedNews' => $news]);
    }

    public function addOrUpdateNews(Request $request)
    {
        $data = $request->all();
//        return dump($data);
        $status = NewsItemsEntity::addOrEditNews($data);
//        return dump($status);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'creatingError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'imageFailed':
                return back()->withInput()->withErrors(trans('modelStatusMessages.imageFailed'));
            case 'successMessage':
                return redirect('admin/news/list')->with('successMessage', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }


    public function destroyNews(Request $request)
    {
        $newsId = $request->get('newsId');
//        return dump($newsId);
        $status = NewsItemsEntity::destroyNews($newsId);
        return redirect()->back();
    }


//END OF NEWS ITEMS

//TAGS OF NEWS
    public function listTags($tagId = 0)
    {
        $selectedTag = [];
        if ($tagId != 0) {
            $selectedTag = NewsTagsEntity::find($tagId)->toArray();
        }
        $tagList = NewsTagsEntity::all()->toArray();

        return view('admin.news.tagList', [
            'tagList' => $tagList,
            'selectedTag' => $selectedTag
        ]);

    }

    public function addOrEditTag(Request $request)
    {

        $data = $request->all();
//        return dump($data);
        $status = NewsTagsEntity::addOrEditTag($data);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'creatingError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'imageFailed':
                return back()->withInput()->withErrors(trans('modelStatusMessages.imageFailed'));
            case 'successMessage':
                return redirect('admin/news/tagList')->with('successMessage', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }

    public function setNewsTag(Request $request)
    {
        $data = $request->all();
//        return dump($data);
        $newsId = $data['news_id'];

        try {
            $oldTags = TagToNewsEntity::where('news_id', '=', $newsId)->get();
            foreach ($oldTags as $oneOld) {
                $oneOld->delete();
            }

            if (isset($data['check_boxes'])) {
                $selectedTags = $data['check_boxes'];

                foreach ($selectedTags as $oneSelected) {
                    $tempRecord = new TagToNewsEntity();
                    $tempRecord->news_id = $newsId;
                    $tempRecord->tag_id = $oneSelected;
                    $tempRecord->save();
                }
            }

        } catch (\Exception $ex) {
            return array('databaseError', $ex);
        }
        return redirect()->back()->with('successSetTags', $newsId);
    }

    public function destroyTag(Request $request)
    {
        $tagId = $request->get('tagId');
        $status = NewsTagsEntity::destroyTag($tagId);
        return redirect()->back();
    }

//END TAGS OF NEWS

//CHILD NEWS PAGE
    public function showChildNewsPage()
    {
        $childPage = NewsChildPageEntity::all()->first()->toArray();
        return view('admin.news.childPage', ['pageData' => $childPage]);

    }

    public function updateChildNewsPage(Request $request)
    {
        $data = $request->all();
        $status = NewsChildPageEntity::editChildNewsPagePage($data);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'creatingError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'imageFailed':
                return back()->withInput()->withErrors(trans('modelStatusMessages.imageFailed'));
            case 'successMessage':
                return redirect()->back()->with('successMessage', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }

//END OF CHILD NEWS PAGE

    public function deleteNewsImage($newsId)
    {
//        return dump($newsId);
        $status = NewsItemsEntity::deleteImage($newsId);
        return redirect()->back();
    }


}