<?php

namespace App\Http\Controllers;

use App\BenefitElementsEntity;
use App\BenefitInnerPagesEntity;
use App\BenefitPageEntity;
use App\FeaturesItemEntity;
use App\FeaturesPageEntity;
use App\Http\Requests;
use Illuminate\Http\Request;


class FeaturesController extends Controller
{


    public function indexPage()
    {
        $pageData = FeaturesPageEntity::all()->first()->toArray();

        return view('admin.features.page', [
            'pageData' => $pageData
        ]);
    }

    public function updatePage(Request $request)
    {
        $data = $request->all();
        $status = FeaturesPageEntity::editPage($data);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'creatingError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'imageFailed':
                return back()->withInput()->withErrors(trans('modelStatusMessages.imageFailed'));
            case 'successMessage':
                return redirect()->back()->with('successMessage', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }

    }


    public function listItems()
    {
        $items = FeaturesItemEntity::all()->toArray();

        return view('admin.features.list', [
            'items' => $items
        ]);
    }

    public function destroyItemById(Request $request)
    {
        $data = $request->all();
        $status = FeaturesItemEntity::destroyItem($data['itemId']);
        return redirect()->back();
//        return dump($status);
    }


    public function createItem()
    {
        return view('admin.features.createItem');
    }

    public function updateOrStoreNewItem(Request $request)
    {
        $data = $request->all();
        $status = FeaturesItemEntity::updateOrCreateNewItem($data);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'creatingError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'imageFailed':
                return back()->withInput()->withErrors(trans('modelStatusMessages.imageFailed'));
            case 'successMessage':
                return redirect('admin/features/items/list')->with('successMessage', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }


    public function showItem($itemId)
    {
        $item = FeaturesItemEntity::find($itemId)->toArray();
        return view('admin.features.updateItem', ['item' => $item]);
    }

    public function updateBenefitItem(Request $request)
    {
        $data = $request->all();

        return redirect()->back();
        return dump($data);
    }


}