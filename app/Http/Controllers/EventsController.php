<?php

namespace App\Http\Controllers;

use App\BenefitElementsEntity;
use App\BenefitInnerPagesEntity;
use App\BenefitPageEntity;
use App\Classes\GlobalFunctions;
use App\CommentEventEntity;
use App\EventCategoriesEntity;
use App\EventChildPageEntity;
use App\EventItemsEntity;
use App\EventMainPageEntity;
use App\EventTagsEntity;
use App\Http\Requests;
use App\MethodsEntity;
use App\NewsCategoriesEntity;
use App\NewsChildPageEntity;
use App\NewsEntity;
use App\NewsItems;
use App\NewsItemsEntity;
use App\NewsMainPageEntity;
use App\NewsTagsEntity;
use App\PartnersListEntity;
use App\PartnersPageEntity;
use App\TagToEventsEntity;
use App\TagToNewsEntity;
use Illuminate\Http\Request;


class EventsController extends Controller
{

    public function listCommentEvents()
    {
        $listOfComment = CommentEventEntity::orderBy('event_id', 'ASC')->get()->toArray();
        return view('admin.events.commentList', ['commentList' => $listOfComment]);

    }

    public function deleteCommentEvent(Request $request)
    {
        $data = $request->all();
        CommentEventEntity::destroy($data['commentId']);
        return redirect()->back();
    }


//CATEGORIES
    public function listCategories($categoryId = 0)
    {
        $categoryList = EventCategoriesEntity::all()->toArray();
        $selectedCat = [];
        if ($categoryId != 0) {
            $selectedCat = EventCategoriesEntity::find($categoryId)->toArray();
        }
        return view('admin.events.categoryList', [
            'catList' => $categoryList,
            'selectedCat' => $selectedCat
        ]);
    }

    public function addOrEditCategories(Request $request)
    {
        $data = $request->all();
        $status = EventCategoriesEntity::addOrEditCategories($data);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'creatingError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'imageFailed':
                return back()->withInput()->withErrors(trans('modelStatusMessages.imageFailed'));
            case 'successMessage':
                return redirect()->back()->with('successMessage', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }

    }

    public function destroyCategory(Request $request)
    {
        $categoyId = $request->get('catId');
        $status = EventCategoriesEntity::destroyCat($categoyId);
        return redirect()->back();
    }

//END OF CATEGORIES


//NEWS MAIN PAGE
    public function editMainPage()
    {
        $mainPage = EventMainPageEntity::all()->first()->toArray();

        return view('admin.events.page', ['pageData' => $mainPage]);
    }

    public function updateMainPage(Request $request)
    {
        $data = $request->all();
//        return dump($data);
        $status = EventMainPageEntity::editMainPage($data);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'creatingError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'imageFailed':
                return back()->withInput()->withErrors(trans('modelStatusMessages.imageFailed'));
            case 'successMessage':
                return redirect()->back()->with('successMessage', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }


//END OF MAIN NEWS PAGE


//NEWS ITEMS
    public function listEvents()
    {
        $eventList = EventItemsEntity::all()->toArray();
        return view('admin.events.list', ['eventList' => $eventList]);

    }

    public function createEvent()
    {
        $eventCatListRaw = EventCategoriesEntity::all()->toArray();
        $eventCatList = [];
        foreach ($eventCatListRaw as $oneCat) {
            $eventCatList[$oneCat['id']] = $oneCat['title_en'];
        }

        return view('admin.events.createEvent', ['eventCatList' => $eventCatList]);
    }

    public function showEvents($eventId)
    {
        $event = EventItemsEntity::find($eventId)->toArray();
        $eventCatListRaw = EventCategoriesEntity::all()->toArray();
        $eventCatList = [];
        foreach ($eventCatListRaw as $oneCat) {
            $eventCatList[$oneCat['id']] = $oneCat['title_en'];
        }
//        return dump($event);
        $event['created_at'] = GlobalFunctions::generateDateTimeToStr($event['created_at']);
        $event['event_date'] = GlobalFunctions::generateDateTimeToStr($event['event_date']);


        return view('admin.events.updateEvent', ['eventCatList' => $eventCatList, 'selectedEvent' => $event]);
    }

    public function addOrUpdateEvent(Request $request)
    {
        $data = $request->all();
//        return dump($data);
        $status = EventItemsEntity::addOrEditEvent($data);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'creatingError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'imageFailed':
                return back()->withInput()->withErrors(trans('modelStatusMessages.imageFailed'));
            case 'successMessage':
                return redirect('admin/events/list')->with('successMessage', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }


    public function destroyEvent(Request $request)
    {
        $eventId = $request->get('eventId');
        $status = EventItemsEntity::destroyEvent($eventId);
        return redirect()->back();
    }


//END OF NEWS ITEMS

//TAGS OF NEWS
    public function listTags($tagId = 0)
    {
        $selectedTag = [];
        if ($tagId != 0) {
            $selectedTag = EventTagsEntity::find($tagId)->toArray();
        }
        $tagList = EventTagsEntity::all()->toArray();

        return view('admin.events.tagList', [
            'tagList' => $tagList,
            'selectedTag' => $selectedTag
        ]);

    }

    public function addOrEditTag(Request $request)
    {

        $data = $request->all();
//        return dump($data);
        $status = EventTagsEntity::addOrEditTag($data);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'creatingError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'imageFailed':
                return back()->withInput()->withErrors(trans('modelStatusMessages.imageFailed'));
            case 'successMessage':
                return redirect()->back()->with('successMessage', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }

    public function setEventsTag(Request $request)
    {
        $data = $request->all();
//        return dump($data);
        $eventId = $data['event_id'];

        try {
            $oldTags = TagToEventsEntity::where('event_id', '=', $eventId)->get();
            foreach ($oldTags as $oneOld) {
                $oneOld->delete();
            }

            if (isset($data['check_boxes'])) {
                $selectedTags = $data['check_boxes'];

                foreach ($selectedTags as $oneSelected) {
                    $tempRecord = new TagToEventsEntity();
                    $tempRecord->event_id = $eventId;
                    $tempRecord->tag_id = $oneSelected;
                    $tempRecord->save();
                }
            }

        } catch (\Exception $ex) {
            return array('databaseError', $ex);
        }
        return redirect()->back()->with('successSetTags', $eventId);
    }

    public function destroyTag(Request $request)
    {
        $tagId = $request->get('tagId');
        $status = EventTagsEntity::destroyTag($tagId);
        return redirect()->back();
    }

//END TAGS OF NEWS

//CHILD NEWS PAGE
    public function showChildPage()
    {
        $childPage = EventChildPageEntity::all()->first()->toArray();
        return view('admin.events.childPage', ['pageData' => $childPage]);

    }

    public function updateChildPage(Request $request)
    {
        $data = $request->all();
        $status = EventChildPageEntity::editChildPage($data);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'creatingError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'imageFailed':
                return back()->withInput()->withErrors(trans('modelStatusMessages.imageFailed'));
            case 'successMessage':
                return redirect()->back()->with('successMessage', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }

//END OF CHILD NEWS PAGE

    public function deleteEventImage($eventId)
    {
//        return dump($eventId);
        $status = EventItemsEntity::deleteImage($eventId);
        return redirect()->back();
    }


}