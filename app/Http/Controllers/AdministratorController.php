<?php

namespace App\Http\Controllers;

use App\AboutPageEntity;
use App\BenefitElementsEntity;
use App\BenefitInnerPagesEntity;
use App\BenefitPageEntity;
use App\Http\Requests;
use App\MethodsEntity;
use App\NewsCategoriesEntity;
use App\NewsChildPageEntity;
use App\NewsEntity;
use App\NewsItems;
use App\NewsItemsEntity;
use App\NewsMainPageEntity;
use App\NewsTagsEntity;
use App\PartnersListEntity;
use App\PartnersPageEntity;
use App\TagToNewsEntity;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Hash;
use App\Http\Controllers\Auth\AuthController;


class AdministratorController extends Controller
{


    public function postAdminLogin(Request $request)
    {
        $data = $request->all();
//        $password = bcrypt($data['password']);
        $password = $data['password'];
//        return dump($data);
        if (Auth::attempt(['email' => $data['email'], 'password' => $password])) {
            return redirect()->intended('admin');
        } else {
            return back();
        }
    }

//    public function sendAdminLogin()
//    {
////        return dump('sad');
//        if (!Auth::attempt(['email' => Input::get('email'), 'password' => Input::get('password')])) {
//            $arrToreturn = array();
//            $arrToreturn['status'] = 'error';
//            $arrToreturn['message'] = null;
//            if (\Session::get('locale') == 'bg') {
//                $arrToreturn['message'] = '<center>Грешно потрeбителско име или парола</center>';
//            } else {
//                $arrToreturn['message'] = '<center>Wrong name or password</center>';
//            }
//
//            return response()->json($arrToreturn);
//        }
////        return \Redirect::to('/');
//
//    }

    public function showAdministratorPage()
    {
        $users = User::all()->toArray();
        unset($users[0]);
        return view('admin.administration.userList', ['users' => $users]);
    }

    public function createAdmin()
    {
        return view('admin.administration.createAdmin');
    }

    public function showUser($userId)
    {
        $user = User::find($userId)->toArray();
//        return dump($user);
        return view('admin.administration.updateUser', ['user' => $user]);
    }

    public function storeUser(Request $request)
    {
        $data = $request->all();

        $status = User::addUser($request, $data);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'creatingError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'imageFailed':
                return back()->withInput()->withErrors(trans('modelStatusMessages.imageFailed'));
            case 'successMessage':
                return redirect('admin/administrator/page/show')->with('successMessage', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }


    public function updateUser(Request $request)
    {
        $status = User::updateUserAdmin($request);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'creatingError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'imageFailed':
                return back()->withInput()->withErrors(trans('modelStatusMessages.imageFailed'));
            case 'successMessage':
                return redirect('admin/administrator/page/show')->with('successMessage', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }

    public function changeApprove(Request $request)
    {
        $user = User::find($request->get('id'));

        if ($user->is_approve == false) {
            $user->is_approve = true;
        } else {
            $user->is_approve = false;
        }

        $user->save();

        return redirect()->back()->with('successMessage', trans('modelStatusMessages.successMessage'));
    }

    public function deleteUser(Request $request)
    {
        $id = $request->get('userId');
//        return dump($id);
        $status = User::destroyUser($id);
        if ($status == true) {
            return redirect()->back();
        }
        return redirect()->withErrors('Error !!!');
    }


}