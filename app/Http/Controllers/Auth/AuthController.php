<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\UserSocialNetworksEntity;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Auth;
use Laravel\Socialite\Facades\Socialite;
use Hash;
use Illuminate\Support\Facades\Config;
use Intervention\Image\ImageManagerStatic as Image;
use File;


class AuthController extends Controller
{


    use AuthenticatesAndRegistersUsers, ThrottlesLogins;


    protected $redirectTo = '/home';


    public function __construct()
    {
//        $this->middleware('guest', ['except' => 'logout']);
    }


    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }


    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }


    public function redirectToTwitter($oldUrl, $action = '')
    {

        $oldUrl = str_replace('-', '/', $oldUrl);
        \Session::put('old_url', $oldUrl);

        if ($action != '') {
            \Session::put('network_action', $action);
        }
        return Socialite::driver('twitter')->redirect();
    }


    public function redirectLinkedinData($oldUrl, $action = '')
    {
        $oldUrl = str_replace('-', '/', $oldUrl);
        \Session::put('old_url', $oldUrl);

        if ($action != '') {
            \Session::put('network_action', $action);
        }
        return Socialite::driver('linkedin')->redirect();
    }


    private function collectUserData($socialNetworkName, $userObj)
    {
        $data = [];
        $defPass = Config::get('GlobalArrays.social_def_pass');

        if ($socialNetworkName == 'linkedin') {
            $data['name'] = $userObj->name;
            $data['image_avatar'] = $userObj->avatar_original;
            $data['email'] = $userObj->email;
            $data['country'] = $userObj->user['location']['country']['code'];
            $data['password'] = $defPass;
            $data['network_id'] = $userObj->id;
            $data['linkedin_url'] = $userObj->user['publicProfileUrl'];
        } elseif ($socialNetworkName == 'facebook') {
            $data['name'] = $userObj->name;
            $data['image_avatar'] = $userObj->avatar_original;
            $data['email'] = $userObj->email;
//            $data['country'] = $userObj->user['location']['country']['code'];
            $data['password'] = $defPass;
            $data['network_id'] = $userObj->id;
//            $data['facebook_url'] = $userObj->user['publicProfileUrl'];
        } elseif ($socialNetworkName == 'twitter') {
            $data['name'] = $userObj->name;
            $data['image_avatar'] = $userObj->avatar_original;
            $data['email'] = $userObj->email;
            if (empty($data['email'])) {
                $data['email'] = $userObj->id;
            }
//            $data['country'] = $userObj->user['location']['country']['code'];
            $data['password'] = $defPass;
            $data['network_id'] = $userObj->id;
            $data['nickname'] = $userObj->nickname;
//            $data['twitter_url'] = $userObj->user['publicProfileUrl'];
        }
        return $data;

    }

    private function getMainUserRecord($userData)
    {
        $mainUser = null;
        if ($userData['account_type'] == 'twitter') {
//            $mainUser = User::where('nickname', 'LIKE', $userData['nickname'])->get()->first();
            $mainUser = User::where('network_id', 'LIKE', $userData['network_id'])->where('account_type', 'LIKE', 'twitter')->get()->first();
        } else {
            $mainUser = User::where('email', 'LIKE', $userData['email'])->where('account_type', 'LIKE', $userData['account_type'])->get()->first();
        }

        if (!$mainUser) {
            $mainUser = new User();
            $mainUser->name = $userData['name'];
            $mainUser->email = $userData['email'];
            $mainUser->network_id = $userData['network_id'];
            if ($userData['email'] == null) {
                $mainUser->email = time();
            }

            $fileName = $this->storeAvatar($userData['image_avatar']);
            $mainUser->image_avatar = $fileName;

            if (isset($userData['country'])) {
                $mainUser->country = $userData['country'];
            }
            $mainUser->password = Hash::make($userData['password']);
            $mainUser->account_type = $userData['account_type'];
            if ($userData['account_type'] == 'linkedin') {
                $mainUser->linkedin_url = $userData['linkedin_url'];
            } elseif ($userData['account_type'] == 'facebook') {
//
            } elseif ($userData['account_type'] == 'twitter') {
                $mainUser->nickname = $userData['nickname'];
            }
            $mainUser->save();
        }
        return $mainUser;
    }

    private function storeAvatar($imgUrl)
    {
        if ($imgUrl == '') {
            return '';
        }
        $img = Image::make(file_get_contents($imgUrl));
        $img->encode('jpg');

        $time = time();
        $fileName = $time . '.' . 'jpg';
        $img->save(User::$avatar_path . $fileName);
        return $fileName;
    }

    public function twitterCallback()
    {
        $defPass = Config::get('GlobalArrays.social_def_pass');
        $oldUrl = \Session::get('old_url');

        try {
            $user = Socialite::driver('twitter')->user();

        } catch (Exception $e) {
            return redirect('auth/twitter');
        }


        $networkAction = \Session::get('network_action');

        if ($networkAction == 'photo') {
//            USER EMAIL = NULL !!!
//            $userToUpdate = User::where('network_id', 'LIKE', $user->id)->where('account_type', 'LIKE', 'twitter')->get()->first();
            $userToUpdate = User::find(\Auth::user()->id);

            $userToUpdate->image_avatar = $this->storeAvatar($user->avatar_original);
            $userToUpdate->save();
            \Session::forget('network_action');
            return redirect($oldUrl);
        }

        $userData = $this->collectUserData('twitter', $user);
        $userData['account_type'] = 'twitter';


//        $userWithTheSameEmail = User::where('email', 'LIKE', $userData['email'])->get()->first();
//        $userWithTheSameEmail = User::where('email', 'LIKE', $userData['email'])->get()->first();
//        return dump($userWithTheSameEmail);
//        if ($userWithTheSameEmail) {
//            return redirect($oldUrl)->with('loginError', 'THIS EMAIL IS TAKEN !!!');
//        }

        $userMainRecord = $this->getMainUserRecord($userData);


        Auth::attempt(['id' => $userMainRecord->id, 'password' => $defPass]);
        return redirect($oldUrl);
    }


    public function linkedinLogin()
    {
        $oldUrl = \Session::get('old_url');
        $defPass = Config::get('GlobalArrays.social_def_pass');

        try {
            $user = Socialite::driver('linkedin')->user();
        } catch (Exception $e) {
            return redirect('auth/linkedin');
        }

        $networkAction = \Session::get('network_action');
        if ($networkAction == 'photo') {
//            $userToUpdate = User::where('email', 'LIKE', $user->email)->get()->first();
            $userToUpdate = User::find(\Auth::user()->id);
            $userToUpdate->image_avatar = $this->storeAvatar($user->avatar_original);
            $userToUpdate->save();
            \Session::forget('network_action');
            return redirect($oldUrl);

        }

        $userData = $this->collectUserData('linkedin', $user);
        $userData['account_type'] = 'linkedin';


        $userWithTheSameEmail = User::where('email', 'LIKE', $userData['email'])->get()->first();
        if ($userWithTheSameEmail) {
            return redirect($oldUrl)->with('loginError', 'THIS EMAIL IS TAKEN !!!');
        }

        $userMainRecord = $this->getMainUserRecord($userData);


        Auth::attempt(['email' => $userMainRecord->email, 'password' => $defPass]);
        return redirect($oldUrl);
    }

    public function redirectToFacebook($oldUrl, $action = '')
    {

        $oldUrl = str_replace('-', '/', $oldUrl);
        \Session::put('old_url', $oldUrl);

        if ($action != '') {
            \Session::put('network_action', $action);
        }
        return Socialite::driver('facebook')->redirect();
    }

    public function facebookLogin()
    {

        $oldUrl = \Session::get('old_url');
        $defPass = Config::get('GlobalArrays.social_def_pass');


        try {
            $user = Socialite::driver('facebook')->user();
        } catch (Exception $e) {
            return redirect('auth/facebook');
        }

        $networkAction = \Session::get('network_action');
        if ($networkAction == 'photo') {
//            $userToUpdate = User::where('email', 'LIKE', $user->email)->get()->first();
            $userToUpdate = User::find(\Auth::user()->id);

            $userToUpdate->image_avatar = $this->storeAvatar($user->avatar_original);
            $userToUpdate->save();
            \Session::forget('network_action');
            return redirect($oldUrl);

        }


        $userData = $this->collectUserData('facebook', $user);
        $userData['account_type'] = 'facebook';


        $userWithTheSameEmail = User::where('email', 'LIKE', $userData['email'])->get()->first();
        if ($userWithTheSameEmail) {
            return redirect($oldUrl)->with('loginError', 'THIS EMAIL IS TAKEN !!!');
        }


        $userMainRecord = $this->getMainUserRecord($userData);


        Auth::attempt(['email' => $userMainRecord->email, 'password' => $defPass]);

        return redirect($oldUrl);
    }

}
