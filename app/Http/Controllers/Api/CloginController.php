<?php namespace App\Http\Controllers\Api;

use App\Classes\GlobalFunctions;
use App\Complex;
use App\ComplexRooms;
use App\EarlyBooking;
use App\EventTagsEntity;
use App\Extras;
use App\NewsTagsEntity;
use App\RoomExtras;
use App\RoomOccupancy;
use App\TagToEventsEntity;
use App\TagToNewsEntity;
use App\User;
use Illuminate\Http\Request;

use App\Reservations;
use App\ReservedRooms;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class CloginController extends Controller
{


    public function postLogin(Request $request)
    {
        $data = $request->all();
        $messages = '';

        if (empty($data['email'])) {
            $messages['email'] = 'Email input is not set.';
        }
        if (empty($data['password'])) {
            $messages['password'] = 'Password input is not set.';
        }


        if (\Auth::attempt(['email' => $data['email'], 'password' => $data['password']])) {
            $status = 'success';
            $messages = 'Success';
        } else {
            $status = 'denied';
            $messages['mainError'] = 'Access Denied';
        }

        return \Response::json(array(
            'status' => $status,
            'messages' => $messages
        ), 200);
    }

    private function signUpValidationStatus($request)
    {
        $status['status'] = 'success';
        $status['messages'] = [];

        $data = $request->all();

        if (empty($data['name'])) {
            $status['status'] = 'error';
            $status['messages'][0] = 'Your name is not set';
        }

        if (empty($data['email'])) {
            $status['status'] = 'error';
            $status['messages'][1] = 'Your email is not set';
        }

        if (empty($data['company'])) {
            $status['status'] = 'error';
            $status['messages'][2] = 'Your company is not set';
        }

        if (empty($data['password']) || empty($data['re_password']) || strlen($data['password']) < 6) {
            $status['status'] = 'error';
            $status['messages'][3] = 'The password is not set or the password is too short';
        }
        if ($data['password'] !== $data['re_password']) {
            $status['status'] = 'error';
            $status['messages'][4] = 'Password and re-password are not the same';
        }
        return $status;
    }

    public function postSignUp(Request $request)
    {
        $data = $request->all();
        $statusToReturn = $this->signUpValidationStatus($request);

        if ($statusToReturn['status'] == 'error') {
            return \Response::json(array(
                'status' => $statusToReturn['status'],
                'messages' => $statusToReturn['messages']
            ), 200);
        }

        $data['is_ajax'] = true;

        $status = User::addUser($request, $data);

        switch ($status[0]) {
            case 'creatingError':
                $statusToReturn2 = 'error';
                $messagesToReturn[0] = 'Runtime error';
                return \Response::json(array(
                    'status' => $statusToReturn2,
                    'messages' => $messagesToReturn[0]
                ), 200);
                break;
            case 'successMessage':
                \Auth::attempt(['email' => $data['email'], 'password' => $data['password']]);
                return \Response::json(array(
                    'status' => 'success'
                ), 200);
            default:
                $messagesToReturn[0] = 'Runtime error';
                return \Response::json(array(
                    'status' => 'error',
                    'messages' => $messagesToReturn[0]
                ), 200);

        }


//        switch ($status[0]) {
//            case 'creatingError':
//                return \Response::json(array('status' => 'creatingError'), 200);
//            case 'successMessage':
//                \Auth::attempt(['email' => $data['email'], 'password' => $data['password']]);
//                return \Response::json(array('status' => 'successMessage'), 200);
//            default:
//                return \Response::json(array('status' => 'runtimeError'), 200);
//        }


    }
}
