<?php namespace App\Http\Controllers\Api;

use App\Classes\GlobalFunctions;
use App\Complex;
use App\ComplexRooms;
use App\EarlyBooking;
use App\Extras;
use App\NewsTagsEntity;
use App\RoomExtras;
use App\RoomOccupancy;
use App\TagToNewsEntity;
use Illuminate\Http\Request;

use App\Reservations;
use App\ReservedRooms;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class GetNewsTags extends Controller
{
    public function getTags()
    {

        $data = Input::all();

        $newsId = $data['id'];


        $tagOfNews = $this->initialTags($newsId);


        return \Response::json(array(
            'status' => 'success',
            'data' => $tagOfNews
        ), 200);


    }

    private function initialTags($newsId)
    {

        $allTags = NewsTagsEntity::all()->toArray();

        $tagsToReturn = [];
        foreach ($allTags as $oneTag) {

            $tempTagMiddleTable = TagToNewsEntity::where('news_id', '=', $newsId)->where('tag_id', '=', $oneTag['id'])->get()->toArray();

            $tagsToReturn[$oneTag['id']]['label'] = $oneTag['title_en'];
            if (count($tempTagMiddleTable) > 0) {
                $tagsToReturn[$oneTag['id']]['checked'] = true;
            } else {
                $tagsToReturn[$oneTag['id']]['checked'] = false;
            }
        }
        return $tagsToReturn;
    }
}
