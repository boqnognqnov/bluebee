<?php namespace App\Http\Controllers\Api;

use App\Classes\GlobalFunctions;
use App\Complex;
use App\ComplexRooms;
use App\EarlyBooking;
use App\EventTagsEntity;
use App\Extras;
use App\NewsTagsEntity;
use App\RoomExtras;
use App\RoomOccupancy;
use App\TagToEventsEntity;
use App\TagToNewsEntity;
use Illuminate\Http\Request;

use App\Reservations;
use App\ReservedRooms;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class GetEventTags extends Controller
{
    public function getTags()
    {

        $data = Input::all();

        $eventId = $data['id'];


        $tagOfNews = $this->initialTags($eventId);


        return \Response::json(array(
            'status' => 'success',
            'data' => $tagOfNews
        ), 200);


    }

    private function initialTags($eventId)
    {

        $allTags = EventTagsEntity::all()->toArray();

        $tagsToReturn = [];
        foreach ($allTags as $oneTag) {

            $tempTagMiddleTable = TagToEventsEntity::where('event_id', '=', $eventId)->where('tag_id', '=', $oneTag['id'])->get()->toArray();

            $tagsToReturn[$oneTag['id']]['label'] = $oneTag['title_en'];
            if (count($tempTagMiddleTable) > 0) {
                $tagsToReturn[$oneTag['id']]['checked'] = true;
            } else {
                $tagsToReturn[$oneTag['id']]['checked'] = false;
            }
        }
        return $tagsToReturn;
    }
}
