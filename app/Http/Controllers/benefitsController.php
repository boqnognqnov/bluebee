<?php

namespace App\Http\Controllers;

use App\BenefitElementsEntity;
use App\BenefitInnerPagesEntity;
use App\BenefitPageEntity;
use App\Http\Requests;
use Illuminate\Http\Request;


class BenefitsController extends Controller
{


    public function indexPage()
    {
        $bInnerPages = BenefitInnerPagesEntity::all()->toArray();
        $bPage = BenefitPageEntity::all()->first()->toArray();

        return view('admin.benefits.list', [
            'bInnerPages' => $bInnerPages,
            'bPage' => $bPage
        ]);
    }

    public function updateMainPage(Request $request)
    {
        $data = $request->all();

        $status = BenefitPageEntity::editPage($data);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'creatingError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'imageFailed':
                return back()->withInput()->withErrors(trans('modelStatusMessages.imageFailed'));
            case 'successMessage':
                return redirect()->back()->with('successMessage', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }

    public function showBenefitInnerPage($benefitPageId)
    {
        $bInnerPage = BenefitInnerPagesEntity::find($benefitPageId)->toArray();
//        return dump($bInnerPage);
        return view('admin.benefitInnerPages.list', ['innerPage' => $bInnerPage]);

    }

    public function editBenefitInnerPage(Request $request)
    {

        $data = $request->all();
//        return dump($data);
        $status = BenefitInnerPagesEntity::editInnerBenefitPage($data);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'creatingError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'imageFailed':
                return back()->withInput()->withErrors(trans('modelStatusMessages.imageFailed'));
            case 'successMessage':
                return redirect()->back()->with('successMessage', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }

    }

    public function listBenefitItems($benefitInnerPageId)
    {
        $benefitInnerPage = BenefitInnerPagesEntity::find($benefitInnerPageId)->toArray();
        $items = BenefitElementsEntity::where('inner_benefit_id', '=', $benefitInnerPageId)->get()->toArray();
        return view('admin.benefitItems.list',
            ['items' => $items,
                'bIPageData' => $benefitInnerPage
            ]);
    }


    public function createBenefitItems($benefitInnerPageId)
    {
        $benefitInnerPage = \App\BenefitInnerPagesEntity::find($benefitInnerPageId)->toArray();
        return view('admin.benefitItems.create', ['benefitInnerPage' => $benefitInnerPage]);
    }

    public function updateOrStoreNewBenefitItems(Request $request)
    {
        $data = $request->all();
        $status = BenefitElementsEntity::updateOrCreateNewItem($data);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'creatingError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'imageFailed':
                return back()->withInput()->withErrors(trans('modelStatusMessages.imageFailed'));
            case 'successMessage':
                return redirect('admin/benefits/items/list/' . $status[1]['inner_benefit_id'])->with('successMessage', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }

    public function destroyBenefitItemById(Request $request)
    {
        $data = $request->all();
        $status = BenefitElementsEntity::destroyItem($data['item_id']);
        return redirect()->back();
//        return dump($status);
    }

    public function showBenefitItem($bItemId)
    {
        $bItem = BenefitElementsEntity::find($bItemId)->toArray();
        $benefitInnerPage = \App\BenefitInnerPagesEntity::find($bItem['inner_benefit_id'])->toArray();
        return view('admin.benefitItems.update', ['bItem' => $bItem,
            'benefitInnerPage' => $benefitInnerPage]);
    }

    public function updateBenefitItem(Request $request)
    {
        $data = $request->all();

        return redirect()->back();
        return dump($data);
    }


}