<?php

namespace App\Http\Controllers;

use App\AboutPageEntity;
use App\BenefitElementsEntity;
use App\BenefitInnerPagesEntity;
use App\BenefitPageEntity;
use App\Classes\GlobalFunctions;
use App\DownloadsItemsEntity;
use App\EventItemsEntity;
use App\HomePageEntity;
use App\Http\Requests;
use App\JobPositionsItemsEntity;
use App\JoinTheTeamPageEntity;
use App\KnowCenterPageEntity;
use App\MethodsEntity;
use App\NewsItemsEntity;
use App\OpinionComponentEntity;
use App\PartnersListEntity;
use App\TeamItemsEntity;
use App\TeamPageEntity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;


class CTeamController extends Controller
{


    public function indexJoinTheTeam()
    {


        $page = JoinTheTeamPageEntity::all()->first()->toArray();

        $pageData['raw'] = $page;

        $pageData['intro'] = GlobalFunctions::getHeaderCompData($page['head_id']);
//        return dump($page['head_id']);
        $pageData['trial'] = GlobalFunctions::getTrialCompData($page['try_id']);
        $pageData['positions'] = JobPositionsItemsEntity::all()->toArray();

        $demoRequestUrls = Config::get('GlobalArrays.demoRequestUrls');


        $trialSeeder = $demoRequestUrls['join_the_team'];

//        TRAL COMPONENT WITHOUT EVERIDE !!!
//        $pageData['trial']['url'] = $trialSeeder['cTrial'] . $page['id'];

        return view('team-join', ['pageData' => $pageData, 'demoSeeder' => $trialSeeder['button']]);
    }

    public function indexJoinApplyToPosition($positionId)
    {


        $page = JoinTheTeamPageEntity::all()->first()->toArray();

        $pageData['raw'] = $page;

        $pageData['intro'] = GlobalFunctions::getHeaderCompData($page['head_id']);

        $pageData['trial'] = GlobalFunctions::getTrialCompData($page['try_id']);
        $pageData['position'] = JobPositionsItemsEntity::find($positionId)->toArray();

        return view('team-job', ['pageData' => $pageData]);
    }

    public function indexTeamList($managers = null)
    {
        $lang = App::getLocale();

        $page = TeamPageEntity::all()->first()->toArray();

        $isManager = false;
        $teamList = [];
        if ($managers == null) {
            $teamList = TeamItemsEntity::where('is_manager', '=', false)->get()->toArray();
        } else {
            $teamList = TeamItemsEntity::where('is_manager', '=', true)->get()->toArray();
            $isManager = true;
        }


        $pageData['raw'] = $page;

        $pageData['intro'] = GlobalFunctions::getHeaderCompData($page['head_id']);
        $pageData['trial'] = GlobalFunctions::getTrialCompData($page['try_id']);
        $pageData['team'] = [];
        $pageData['is_manager'] = $isManager;
        foreach ($teamList as $key => $oneEmployee) {
            $pageData['team'][$key]['name'] = $oneEmployee['name'];
            $pageData['team'][$key]['job'] = $oneEmployee['position_' . $lang];
            $pageData['team'][$key]['description'] = $oneEmployee['description_' . $lang];
            $pageData['team'][$key]['img'] = $oneEmployee['image'];

            $pageData['team'][$key]['twitter'] = $oneEmployee['twitter'];
            $pageData['team'][$key]['linkedin'] = $oneEmployee['linkedin'];
            $pageData['team'][$key]['email'] = $oneEmployee['email'];

        }
//        return dump($pageData);

        return view('team', ['pageData' => $pageData]);
    }


}