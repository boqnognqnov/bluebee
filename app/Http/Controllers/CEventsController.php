<?php

namespace App\Http\Controllers;

use App\BenefitElementsEntity;
use App\BenefitInnerPagesEntity;
use App\BenefitPageEntity;
use App\Classes\GlobalFunctions;
use App\CommentEventEntity;
use App\DownloadsItemsEntity;
use App\EventCategoriesEntity;
use App\EventChildPageEntity;
use App\EventItemsEntity;
use App\EventMainPageEntity;
use App\EventTagsEntity;
use App\HomePageEntity;
use App\Http\Requests;
use App\KnowCenterPageEntity;
use App\MethodsEntity;
use App\NewsCategoriesEntity;
use App\NewsChildPageEntity;
use App\NewsItemsEntity;
use App\NewsMainPageEntity;
use App\NewsTagsEntity;
use App\OpinionComponentEntity;
use App\PartnersListEntity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;


class CEventsController extends Controller
{


    public function index($criterion = null, $val = null)
    {

        $lang = App::getLocale();
        $page = EventMainPageEntity::all()->first()->toArray();

        GlobalFunctions::oldEventsChecker();

        $currentDate = date("Y-m-d");

        $eventList = [];
        if ($criterion == null) {
            $eventList = EventItemsEntity::orderBy('event_date', 'ASC')->where('event_date', '>=', $currentDate . ' 00:00:00')->paginate(5);
        } elseif ($criterion == 'tag') {
            $eventList = EventItemsEntity
                ::join('events_to_tag', 'events_to_tag.event_id', '=', 'events.id')
                ->join('tags_events', 'tags_events.id', '=', 'events_to_tag.tag_id')
                ->where('tags_events.title_' . $lang, 'LIKE', '%' . $val . '%')
                ->orderBy('event_date', 'ASC')
                ->select('events.*')
                ->paginate(5);

        } elseif ($criterion == 'category') {
            $eventList = EventItemsEntity
                ::join('category_events', 'category_events.id', '=', 'events.category_id')
                ->where('category_events.title_' . $lang, 'LIKE', '%' . $val . '%')
                ->orderBy('event_date', 'ASC')
                ->select('events.*')
                ->paginate(5);
        } elseif ($criterion == 'year') {

            $eventList = EventItemsEntity::where('event_date', '<', ($val + 1) . '-01-01 00:00:00')
                ->where('event_date', '>=', ($val) . '-01-01 00:00:00')
                ->orderBy('event_date', 'ASC')
                ->paginate(5);
        }


        $pageData['raw'] = $page;
        $pageData['intro'] = GlobalFunctions::getHeaderCompData($page['head_id']);
        $pageData['newsletter'] = GlobalFunctions::getNewsLetterCompData($page['sign_up_id']);


        $eventUpdateDateList = EventItemsEntity::orderBy('event_date', 'DESC')->get(['event_date']);
//        return dump($eventUpdateDateList);
//        return dump($eventList);
//        foreach ($eventList as $key => $oneEvent) {
//            $eventList[$key]['month'] = $this->getMonth($oneEvent);
//            $eventList[$key]['day'] = $this->getDay($oneEvent);
//        }
        $pageData['eventList'] = $eventList;


        $pageData['sidebar']['categories'] = EventCategoriesEntity
            ::join('events', 'events.category_id', '=', 'category_events.id')
//            ->where('news.title_en', 'NOT LIKE', '')
            ->select(\DB::raw('DISTINCT category_events.*'))
            ->get()->toArray();


        $pageData['sidebar']['tags'] = EventTagsEntity
            ::join('events_to_tag', 'events_to_tag.tag_id', '=', 'tags_events.id')
//            ->where('news.title_en', 'NOT LIKE', '')
            ->select(\DB::raw('DISTINCT tags_events.*'))
            ->get()->toArray();
        
        $pageData['sidebar']['years'] = GlobalFunctions::collectYears($eventUpdateDateList, 'event_date');
        $pageData['sidebar']['itemType'] = 'events';

//                return dump($pageData);

        return view('events', ['pageData' => $pageData]);
    }


    public function indexInnerPage($slug)
    {
        $lang = App::getLocale();
        $eventData = EventItemsEntity::where('slug_' . $lang, 'LIKE', $slug)->get()->first()->toArray();
        $pageData['raw'] = EventChildPageEntity::all()->first()->toArray();


        $eventUpdateDateList = EventItemsEntity::orderBy('created_at', 'DESC')->get(['created_at']);

        $pageData['sidebar']['categories'] = EventCategoriesEntity
            ::join('events', 'events.category_id', '=', 'category_events.id')
//            ->where('news.title_en', 'NOT LIKE', '')
            ->select(\DB::raw('DISTINCT category_events.*'))
            ->get()->toArray();


        $pageData['sidebar']['tags'] = EventTagsEntity
            ::join('events_to_tag', 'events_to_tag.tag_id', '=', 'tags_events.id')
//            ->where('news.title_en', 'NOT LIKE', '')
            ->select(\DB::raw('DISTINCT tags_events.*'))
            ->get()->toArray();


        $pageData['sidebar']['years'] = GlobalFunctions::collectYears($eventUpdateDateList, 'event_date');
        $pageData['sidebar']['itemType'] = 'events';

        $pageData['eventData'] = $eventData;

        $pageData['newsLetter'] = GlobalFunctions::getNewsLetterCompData($pageData['raw']['sign_up_id']);


//        $pageData['intro']['background']['default'] = EventChildPageEntity::$path . $pageData['raw']['head_image'];
//        $pageData['intro']['background']['medium'] = EventChildPageEntity::$pathMedium . $pageData['raw']['head_image'];
//        $pageData['intro']['background']['small'] = EventChildPageEntity::$pathSmall . $pageData['raw']['head_image'];

        $breadcrumbArrData['url'] = 'events/' . $eventData['slug_' . $lang];
        $breadcrumbArrData['title'] = $eventData['title_' . $lang];

        $pageData['intro'] = GlobalFunctions::getHeaderCompData($pageData['raw']['head_id'], $breadcrumbArrData);

        $pageData['newsletter'] = GlobalFunctions::getNewsLetterCompData($pageData['raw']['sign_up_id']);

        $pageData['comments'] = CommentEventEntity::where('event_id', '=', $eventData['id'])->orderBy('created_at', 'DESC')->get()->toArray();

        $pageData['favoriteId'] = $pageData['raw']['favorites_id'];

        $pageData['raw']['userData'] = '';
        if (Auth::check() == true) {
            $pageData['raw']['userData']['name'] = Auth::user()->name;
            $pageData['raw']['userData']['email'] = Auth::user()->email;
        }

        return view('events-single', ['pageData' => $pageData]);
    }

    public function postComment(Request $request)
    {
        $data = $request->all();


        $status = CommentEventEntity::postComment($data);

        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'creatingError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'imageFailed':
                return back()->withInput()->withErrors(trans('modelStatusMessages.imageFailed'));
            case 'successMessage':
                return redirect()->back()->with('successMessage', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }

    }


}