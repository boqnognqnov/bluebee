<?php

namespace App\Http\Controllers;

use App\BenefitElementsEntity;
use App\BenefitInnerPagesEntity;
use App\BenefitPageEntity;
use App\DownloadsItemsEntity;
use App\EventCategoriesEntity;
use App\EventChildPageEntity;
use App\EventItemsEntity;
use App\EventMainPageEntity;
use App\EventTagsEntity;
use App\FavoritesComponentEntity;
use App\FeaturesItemEntity;
use App\Http\Requests;
use App\MethodsEntity;
use App\NewsCategoriesEntity;
use App\NewsChildPageEntity;
use App\NewsEntity;
use App\NewsItems;
use App\NewsItemsEntity;
use App\NewsMainPageEntity;
use App\NewsTagsEntity;
use App\PartnersListEntity;
use App\PartnersPageEntity;
use App\TagToEventsEntity;
use App\TagToNewsEntity;
use Illuminate\Http\Request;


class FavoritesController extends Controller
{

    public function setModelToSession($modelToDropdown)
    {
        $favRecordId = FavoritesComponentEntity::getFavIdByVectorModel($modelToDropdown);
        \Session::put('favSelectedModelTo', $modelToDropdown);
        \Session::put('favId', $favRecordId);
        return redirect()->back();
    }

    public function showTool($atachModelName, $atachModelId)
    {
        //THIS DEFINE PATH TO VIEW BY TYPE OF ITEMS

//        HIDE OLD DATA
        $oldAtachModelId = \Session::get('atachModelID');
        if ($atachModelId !== $oldAtachModelId) {
            \Session::forget('favSelectedModelTo');
            \Session::forget('favId');
        }
        \Session::put('atachModelID', $atachModelId);
        //        HIDE OLD DATA


        switch ($atachModelName) {
            case 'news':
                $name = NewsItemsEntity::find($atachModelId)->title_en;
                return view('admin.news.addToFav', ['atachModelName' => $atachModelName, 'atachModelId' => $atachModelId, 'name' => $name]);
            case 'events':
                $name = EventItemsEntity::find($atachModelId)->title_en;
                return view('admin.events.addToFav', ['atachModelName' => $atachModelName, 'atachModelId' => $atachModelId, 'name' => $name]);
            case 'features_elements':
                $name = FeaturesItemEntity::find($atachModelId)->title_en;
                return view('admin.features.addToFav', ['atachModelName' => $atachModelName, 'atachModelId' => $atachModelId, 'name' => $name]);
            case 'downloads':
                $name = DownloadsItemsEntity::find($atachModelId)->title_en;
                return view('admin.downloads.addToFav', ['atachModelName' => $atachModelName, 'atachModelId' => $atachModelId, 'name' => $name]);
            default:
                return back()->withInput()->withErrors('Server Error !!!');
        }


    }


    public function addFav(Request $request)
    {
        $data = $request->all();
//        \Session::put('favSelectedModelTo', $data['toModel']);
        $favId = FavoritesComponentEntity::getFavIdByVectorModel($data['toModel']);
        $favModel = FavoritesComponentEntity::find($favId);

        $modelName = $data['position'] . '_model_name';
        $modelId = $data['position'] . '_model_id';

        $favModel->$modelName = $data['atachModelName'];
        $favModel->$modelId = $data['atachModelId'];

        $favModel->save();
        return redirect()->back();

    }

    public function addFavByDate(Request $request)
    {
        $data = $request->all();
//        \Session::put('favSelectedModelTo', $data['toModel']);
        $favId = FavoritesComponentEntity::getFavIdByVectorModel($data['toModel']);
        $favModel = FavoritesComponentEntity::find($favId);

        $modelName = $data['position'] . '_model_name';
        $modelId = $data['position'] . '_model_id';

        $favModel->$modelName = $data['atachModelName'];
        $favModel->$modelId = 0;

        $favModel->save();
        return redirect()->back();

    }

    public function addSetSlotRandom(Request $request)
    {
        $data = $request->all();
//        \Session::put('favSelectedModelTo', $data['toModel']);
        $favId = FavoritesComponentEntity::getFavIdByVectorModel($data['toModel']);
        $favModel = FavoritesComponentEntity::find($favId);

        $modelName = $data['position'] . '_model_name';
        $modelId = $data['position'] . '_model_id';

        $favModel->$modelName = $data['atachModelName'];
        $favModel->$modelId = -1;

        $favModel->save();
        return redirect()->back();

    }
}