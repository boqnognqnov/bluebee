<?php

namespace App\Http\Controllers;

use App\BenefitElementsEntity;
use App\BenefitInnerPagesEntity;
use App\BenefitPageEntity;
use App\Classes\GlobalFunctions;
use App\HomePageEntity;
use App\Http\Requests;
use App\MethodsEntity;
use App\OpinionComponentEntity;
use App\PartnersListEntity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;


class CWhyBluebeeController extends Controller
{


    public function index()
    {
//        return dump('sadasd');
        $pageData = [];
        $lang = App::getLocale();

        $page = BenefitPageEntity::all()->first()->toArray();

        $pageData['raw'] = $page;

//        $pageData['intro']['background']['default'] = BenefitPageEntity::$path . $page['head_image'];
//        $pageData['intro']['background']['medium'] = BenefitPageEntity::$pathMedium . $page['head_image'];
//        $pageData['intro']['background']['small'] = BenefitPageEntity::$pathSmall . $page['head_image'];
//        $pageData['intro']['title'] = $page['title_' . $lang];
//        $pageData['intro']['body'] = $page['text_' . $lang];
//        $pageData['intro']['breadcrumb'] = '';
        $pageData['intro'] = GlobalFunctions::getHeaderCompData($page['head_id']);


        $pageData['bodyTexts']['title'] = $page['title_' . $lang];
        $pageData['bodyTexts']['body'] = $page['text_' . $lang];

        $pageIntities = BenefitInnerPagesEntity::all()->toArray();
        foreach ($pageIntities as $key => $oneBenefitItem) {
            $pageData['items'][$key]['title'] = $oneBenefitItem['front_title_' . $lang];
            $pageData['items'][$key]['text'] = $oneBenefitItem['front_text_' . $lang];
            $pageData['items'][$key]['textDescription'] = $oneBenefitItem['front_text_s_' . $lang];

            $pageData['items'][$key]['slug'] = $oneBenefitItem['slug_' . $lang];

            $pageData['items'][$key]['image'] = BenefitInnerPagesEntity::$pathNegative . $oneBenefitItem['front_negative_image'];
            $pageData['items'][$key]['imageBackground'] = $oneBenefitItem['front_background_image'];
        }

        $pageData['quote'] = GlobalFunctions::getOpinionCompData($page['opinion_id']);

        $pageData['trial'] = GlobalFunctions::getTrialCompData($page['try_id']);

        $demoRequestUrls = Config::get('GlobalArrays.demoRequestUrls');

        $trialSeeder = $demoRequestUrls['why_bluebee'];
        $pageData['trial']['url'] = $trialSeeder['cTrial'] . '0';


        return view('why-bluebee', ['pageData' => $pageData, 'demoSeeder' => $trialSeeder['button'] . '0']);
    }


    public function indexInnerPage($slug)
    {
        $lang = App::getLocale();
        $page = BenefitInnerPagesEntity::where('slug_' . $lang, 'LIKE', $slug)->get()->first()->toArray();

//        return dump($page);

        $pageData['raw'] = $page;

        $pageData['intro'] = GlobalFunctions::getHeaderCompData($page['head_id']);
//        return dump($pageData['intro']);

        $items = BenefitElementsEntity::where('inner_benefit_id', '=', $page['id'])->get()->toArray();

//        return dump($items);

        foreach ($items as $key => $item) {
            $pageData['items'][$key + 1]['title'] = $item['title_' . $lang];
            $pageData['items'][$key + 1]['content'] = $item['text_' . $lang];
            $pageData['items'][$key + 1]['image'] = '';
            if (!empty($item['image'])) {
                $pageData['items'][$key + 1]['image'] = BenefitElementsEntity::$path . $item['image'];
            }

        }

        $pageData['quote'] = GlobalFunctions::getOpinionCompData($page['opinion_id']);
        $pageData['trial'] = GlobalFunctions::getTrialCompData($page['try_id']);

        $pageData['favoriteId'] = $page['favorites_id'];

        $demoRequestUrls = Config::get('GlobalArrays.demoRequestUrls');

        $trialSeeder = $demoRequestUrls['why_bluebee'];
        $pageData['trial']['url'] = $trialSeeder['cTrial'] . $page['id'];


        return view('why-bluebee-inner', ['pageData' => $pageData, 'demoSeeder' => $trialSeeder['button'] . $page['id']]);

    }

}