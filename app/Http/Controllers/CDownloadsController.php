<?php

namespace App\Http\Controllers;

use App\BenefitElementsEntity;
use App\BenefitInnerPagesEntity;
use App\BenefitPageEntity;
use App\Classes\GlobalFunctions;
use App\CommentEventEntity;
use App\DownloadsCategoriesEntity;
use App\DownloadsItemsEntity;
use App\DownloadsMainPageEntity;
use App\EventCategoriesEntity;
use App\EventChildPageEntity;
use App\EventItemsEntity;
use App\EventMainPageEntity;
use App\EventTagsEntity;
use App\HistoryDownloadEntity;
use App\HomePageEntity;
use App\Http\Requests;
use App\KnowCenterPageEntity;
use App\MethodsEntity;
use App\NewsCategoriesEntity;
use App\NewsChildPageEntity;
use App\NewsItemsEntity;
use App\NewsMainPageEntity;
use App\NewsTagsEntity;
use App\OpinionComponentEntity;
use App\PartnersListEntity;
use App\UserByCoockieEntity;
use App\UserHistoryEntity;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\App;


class CDownloadsController extends Controller
{


    public function index($criterion = null, $val = null)
    {

        $lang = App::getLocale();
        $page = DownloadsMainPageEntity::all()->first()->toArray();

        $downloadList = [];
        if ($criterion == null) {
//            $downloadList = DownloadsItemsEntity::orderBy('created_at', 'DESC')->paginate(5);
            $downloadList = DownloadsItemsEntity
                ::join('category_downloads', 'category_downloads.id', '=', 'downloads.category_id')
//                ->where('category_downloads.title_en', 'NOT LIKE', 'Datacard')
                ->orderBy('created_at', 'DESC')
                ->select('downloads.*')
                ->paginate(5);


        } elseif ($criterion == 'category') {
            $downloadList = DownloadsItemsEntity
                ::join('category_downloads', 'category_downloads.id', '=', 'downloads.category_id')
                ->where('category_downloads.title_' . $lang, 'LIKE', '%' . $val . '%')
//                ->where('category_downloads.title_en', 'NOT LIKE', 'Datacard')
                ->orderBy('created_at', 'DESC')
                ->select('downloads.*')
                ->paginate(5);
        } elseif ($criterion == 'year') {
//            $downloadList = EventItemsEntity::where('updated_at', '<', ($val + 1) . '-01-01 00:00:00')
//                ->where('updated_at', '>', ($val - 1) . '-01-01 00:00:00')->paginate(5);
        }

        $pageData['currentDataCardId'] = DownloadsCategoriesEntity::where('title_en', 'LIKE', 'Data cards')->get()->first()->id;


        $pageData['raw'] = $page;
        $pageData['intro'] = GlobalFunctions::getHeaderCompData($page['head_id']);
        $pageData['newsletter'] = GlobalFunctions::getNewsLetterCompData($page['sign_up_id']);


        $pageData['downloadList'] = $downloadList;


        $pageData['sidebar']['categories'] = DownloadsCategoriesEntity
            ::join('downloads', 'downloads.category_id', '=', 'category_downloads.id')
//            ->where('news.title_en', 'NOT LIKE', '')
            ->select(\DB::raw('DISTINCT category_downloads.*'))
            ->get()->toArray();


        $pageData['sidebar']['itemType'] = 'downloads';

        return view('downloads', ['pageData' => $pageData]);
    }


    public function indexCurrentPaginate($fileId)
    {

        $allDownloads = DownloadsItemsEntity::orderBy('created_at', 'DESC')->get()->toArray();

        $counter = 1;
        foreach ($allDownloads as $key => $oneDownload) {
            if (($key + 1) % 5 == 0) {
                $counter++;
            }

            if ($oneDownload['id'] == $fileId) {
                Paginator::currentPageResolver(function () use ($counter) {
                    return $counter;
                });

            }
        }

        $page = DownloadsMainPageEntity::all()->first()->toArray();


        $downloadList = DownloadsItemsEntity
            ::join('category_downloads', 'category_downloads.id', '=', 'downloads.category_id')
//                ->where('category_downloads.title_en', 'NOT LIKE', 'Datacard')
            ->orderBy('created_at', 'DESC')
            ->select('downloads.*')
            ->paginate(5);

        $pageData['currentDataCardId'] = DownloadsCategoriesEntity::where('title_en', 'LIKE', 'Data cards')->get()->first()->id;


        $pageData['raw'] = $page;
        $pageData['intro'] = GlobalFunctions::getHeaderCompData($page['head_id']);
        $pageData['newsletter'] = GlobalFunctions::getNewsLetterCompData($page['sign_up_id']);


        $pageData['downloadList'] = $downloadList;



        $pageData['sidebar']['categories'] = DownloadsCategoriesEntity
            ::join('downloads', 'downloads.category_id', '=', 'category_downloads.id')
//            ->where('news.title_en', 'NOT LIKE', '')
            ->select(\DB::raw('DISTINCT category_downloads.*'))
            ->get()->toArray();

        $pageData['sidebar']['itemType'] = 'downloads';

        return view('downloads', ['pageData' => $pageData]);


    }


    private function storeDownloadInfo($fileRec)
    {


        $user = \Auth::user()->toArray();


        $user['fileName'] = $fileRec->title_en;
        try {

            \Mail::send('emails.downloadFileRequest', $user, function ($message) use ($user) {
                $message->from($user['email'], $user['name']);

                $message->to('bozhidarovboyan@gmail.com', 'Download request')->subject('Download request');
//                $message->bcc('kbobokov@gmail.com', 'Download request')->subject('Download request');
//                $message->bcc('ivan.karabaliev@bluebee.com', 'Download request')->subject('Download request');

//                $message->to('bozhidarovboyan@gmail.com', 'Info')->subject('Оставено съобщение от контакт формата на Royal Estate');
//                    $message->bcc('alarma7a@gmail.com', 'Info')->subject('Оставено съобщение от контакт формата на Royal Estate');
            });

        } catch (\Exception $ex) {
        }

        $userByCoockie = UserByCoockieEntity::where('unique_code', 'LIKE', 'fective_code')->get()->first();
        if (!$userByCoockie) {
            $userByCoockie = new UserByCoockieEntity();
            $userByCoockie->unique_code = 'fective_code';
            $userByCoockie->save();
        }

        $historyMiddleTable = UserHistoryEntity::where('user_id', '=', $user['id'])->get()->first();
        if (!$historyMiddleTable) {
            $historyMiddleTable = new UserHistoryEntity();
            $historyMiddleTable->user_id = $user['id'];
            $historyMiddleTable->coockie_id = $userByCoockie->id;
            $historyMiddleTable->save();
        }

        $history = new HistoryDownloadEntity();
        $history->user_download_id = $fileRec->id;
        $history->download_rec_id = $historyMiddleTable->id;

        $history->save();

    }

    public function downloadFile($file_id)
    {

        $fileRec = DownloadsItemsEntity::find($file_id);
        $this->storeDownloadInfo($fileRec);
        $pathToFile = DownloadsItemsEntity::$pathFile . $fileRec->file;
        return response()->download($pathToFile);
    }


}