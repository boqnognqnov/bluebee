<?php

namespace App\Http\Controllers;

use App\BenefitElementsEntity;
use App\BenefitInnerPagesEntity;
use App\BenefitPageEntity;
use App\HomePageEntity;
use App\Http\Requests;
use Illuminate\Http\Request;


class TestController extends Controller
{


    public function index()
    {
        return view('welcome')->with('successMessage', 'Успешен запис');
    }

    public function test()
    {
        return redirect()->back()->with('successMessage', 'Успешен запис');
    }




}