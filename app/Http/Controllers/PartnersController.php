<?php

namespace App\Http\Controllers;

use App\BenefitElementsEntity;
use App\BenefitInnerPagesEntity;
use App\BenefitPageEntity;
use App\Http\Requests;
use App\MethodsEntity;
use App\PartnersListEntity;
use App\PartnersPageEntity;
use Illuminate\Http\Request;


class PartnersController extends Controller
{


    public function index()
    {


        $partnersPage = PartnersPageEntity::all()->first()->toArray();


        return view('admin.partners.page', [
            'partnersPageData' => $partnersPage,
        ]);

    }


    public function updatePage(Request $request)
    {
        $data = $request->all();
        $status = PartnersPageEntity::editPartnersPage($data);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'creatingError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'imageFailed':
                return back()->withInput()->withErrors(trans('modelStatusMessages.imageFailed'));
            case 'successMessage':
                return redirect()->back()->with('successMessage', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }

    }

    public function getPartnerList($partnerId = 0)
    {
        $selectedPartner = [];
        if ($partnerId != 0) {
            $selectedPartner = PartnersListEntity::find($partnerId)->toArray();
        }
        $partnersList = PartnersListEntity::orderBy('position','DESC')->get()->toArray();


        return view('admin.partners.list', [
            'partnersListData' => $partnersList,
            'selectedPartner' => $selectedPartner
        ]);

    }

    public function addOrEditPartner(Request $request)
    {
        $data = $request->all();
//        return dump($data);
        $status = PartnersListEntity::addOrEditPartner($data);
        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'favoriteError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.noEmptyFavPartnersSlot'));
            case 'creatingError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'imageFailed':
                return back()->withInput()->withErrors(trans('modelStatusMessages.imageFailed'));
            case 'successMessage':
                return redirect('admin/partners/list/show')->with('successMessage', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }

    }

    public function destroyPartner(Request $request)
    {
        $data = $request->all();
        $status = PartnersListEntity::destroyPartner($data['partnerId']);
        return redirect()->back();

    }


    public function movePartnerUp($partnerId)
    {

        $max = PartnersListEntity::orderBy('position', 'DESC')->get()->first()->position;

        $currentRecord = PartnersListEntity::find($partnerId);
        $currentPosition = $currentRecord->position;
        if ($max > $currentPosition) {

            $downRecord = PartnersListEntity::where('position', '>', $currentPosition)->orderBy('position', 'ASC')->first();

            if ($downRecord) {
                $currentRecord->position = $downRecord->position;
                $currentRecord->save();
                $downRecord->position = $currentPosition;
                $downRecord->save();
            }
        }

        return redirect()->back();
    }

    public function movePartnerDown($partnerId)
    {
        $min = PartnersListEntity::orderBy('position', 'ASC')->get()->first()->position;

        $currentRecord = PartnersListEntity::find($partnerId);
        $currentPosition = $currentRecord->position;
        if ($min < $currentPosition) {

            $downRecord = PartnersListEntity::where('position', '<', $currentPosition)->orderBy('position', 'DESC')->first();

            if ($downRecord) {
                $currentRecord->position = $downRecord->position;
                $currentRecord->save();
                $downRecord->position = $currentPosition;
                $downRecord->save();
            }

        }

        return redirect()->back();
    }


}