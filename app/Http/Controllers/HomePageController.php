<?php

namespace App\Http\Controllers;

use App\BenefitElementsEntity;
use App\BenefitInnerPagesEntity;
use App\BenefitPageEntity;
use App\HomePageEntity;
use App\Http\Requests;
use Illuminate\Http\Request;


class HomePageController extends Controller
{


    public function index()
    {
        $pageData = HomePageEntity::all()->first()->toArray();
        return view('admin.homePage.show', ['pageData' => $pageData]);
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $status = HomePageEntity::updateHomePage($data);


        switch ($status[0]) {
            case 'validationError':
                return back()->withInput()->withErrors($status[1]);
            case 'creatingError':
                return back()->withInput()->withErrors(trans('modelStatusMessages.creatingError'));
            case 'imageFailed':
                return back()->withInput()->withErrors(trans('modelStatusMessages.imageFailed'));
            case 'successMessage':
                return redirect()->back()->with('successMessage', trans('modelStatusMessages.successMessage'));
            default:
                return back()->withInput()->withErrors(trans('modelStatusMessages.default'));
        }
    }


}