<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::group(['middleware' => ['web']], function () {


    Route::get('password/email', 'Auth\PasswordController@getEmail');
    Route::post('password/email', 'Auth\PasswordController@postEmail');

// Password reset routes...
    Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
    Route::post('password/reset', 'Auth\PasswordController@postReset');

//    Route::get('facebook/getNetworkData/{action?}', 'CSocialLoginController@getFacebookData');
//    Route::get('linkedin/authorize/{oldUrl?}', 'CSocialLoginController@getLinkedinData');
//
//
//    Route::get('facebook/login2', 'CSocialLoginController@facebookLogin');
//    Route::get('linkedin/login', 'CSocialLoginController@linkedinLogin');


    Route::get('facebook/authorize/{oldUrl?}/{action?}', 'Auth\AuthController@redirectToFacebook');
    Route::get('linkedin/authorize/{oldUrl?}/{action?}', 'Auth\AuthController@redirectLinkedinData');


    Route::get('facebook/login', 'Auth\AuthController@facebookLogin');
    Route::get('linkedin/login', 'Auth\AuthController@linkedinLogin');


//    Route::get('auth/twitter/{oldUrl?}/{action?}', 'Auth\AuthController@redirectToTwitter');
//    Route::get('auth/twitter/login/callback', 'Auth\AuthController@twitterCallback');
    Route::get('twitter/authorize/{oldUrl?}/{action?}', 'Auth\AuthController@redirectToTwitter');
    Route::get('twitter/login', 'Auth\AuthController@twitterCallback');


    Route::get('/', 'CHomeController@index');

    Route::get('/home', 'CHomeController@index');

    Route::get('/about', 'CAboutPageController@index');

    Route::get('/features', 'CFeaturesController@index');


    Route::get('/why-bluebee', 'CWhyBluebeeController@index');

    Route::get('/use-cases', 'CCasesController@index');
    Route::get('/use-cases/{case_slug}', 'CCasesController@indexInnerPage');

    Route::get('/knowledge-center', 'CKnowCenterController@index');


    Route::get('/why-bluebee/{benefit_slug}', 'CWhyBluebeeController@indexInnerPage');


    Route::get('/news/list/{criterion?}/{value?}', 'CNewsController@index');
    Route::get('/news/{slug_news}', 'CNewsController@indexInnerPage');
    Route::post('/news/comments/post', 'CNewsController@postComment');

    Route::get('/events/list/{criterion?}/{value?}', 'CEventsController@index');
    Route::get('/events/{slug_event}', 'CEventsController@indexInnerPage');
    Route::post('/events/comments/post', 'CEventsController@postComment');

    Route::get('/downloads/list/{criterion?}/{value?}', 'CDownloadsController@index');

    Route::get('/downloads/paginate/{file_id}', 'CDownloadsController@indexCurrentPaginate');

//    Route::get('/downloads/{download_id}', function () {
//        return view('downloads-single');
//    });

    Route::get('/how-it-works', 'CHowWorksController@index');


    Route::get('/contact', 'CContactsController@indexMainPage');
    Route::get('/contact/{office_id}', 'CContactsController@indexOfficePage');
    Route::post('/contact/post/request', 'CContactsController@contactMail');


    Route::get('/collaborations', 'CCollaborationController@indexMainPage');

    Route::get('/join', 'CTeamController@indexJoinTheTeam');
    Route::get('/join/apply/{position_id}', 'CTeamController@indexJoinApplyToPosition');

    Route::get('/team/{is_manager?}', 'CTeamController@indexTeamList');

//    Route::get('/position', function () {
//        return view('team-job');
//    });

    Route::get('/terms', function () {
        return view('terms');
    });

    Route::get('/privacy', 'PrivacyDisclaimerController@showPrivacyClient');

    Route::get('/disclaimer', 'PrivacyDisclaimerController@showDisclaimerClient');

    Route::get('/log-in', function () {
        return view('login');
    });

    Route::get('/sign-up', function () {
        return view('register');
    });


    Route::get('/demo/{headOrComponent?}/{page?}/{isInner?}', 'CDemoRequestController@indexDemoView');
    Route::post('/demo/request/post', 'CDemoRequestController@postDemoRequest');

    Route::get('/pipelines', 'CPipelinesController@index');
    Route::post('/pipelines/request', 'CPipelinesController@pipeRequest');


    /*
    |--------------------------------------------------------------------------
    | Application Routes
    |--------------------------------------------------------------------------
    |
    | This route group applies the "web" middleware group to every route
    | kernel and includes session state, CSRF protection, and more.
    |
    */
//        Route::auth();


//    Route::group(['namespace' => 'client'], function () {
//    Route::get('/test', 'client\TestController@index');
//    });

//    Route::group(['middleware' => 'registerAdminAuth'], function () {
//
//    });
    Route::post('/api/clientAuthCheck', 'Api\CloginController@authCheck');
    Route::post('/api/clientPostLogin', 'Api\CloginController@postLogin');
    Route::post('/api/clientPostSignUp', 'Api\CloginController@postSignUp');


    Route::group(['middleware' => 'authClient'], function () {
        Route::get('/downloads/additional/download/file/{file_id}', 'AdditionalFilesController@downloadFile');
        Route::get('/downloads/download/file/{file_id}', 'CDownloadsController@downloadFile');

        Route::get('/profile', 'CProfileController@index');
        Route::post('/profile/update_profile', 'CProfileController@updateProfile');
    });

//    Route::post('/downloads/fmodal', 'CDownloadsController@fectiveModal');

    Route::get('auth/login', function () {
        return view('auth.login');
    });


    Route::post('auth/login', 'AdministratorController@postAdminLogin');


//    Route::get('logout', 'Auth\AuthController@getLogout');
    Route::get('logout', 'Auth\AuthController@logout');


    Route::get('/flush', function () {
        \Session::flush();
        \Session::forget('network_action');
        return redirect()->back();
    });

    Route::group(['middleware' => 'auth'], function () {


//        ADMIN LOGIN ROUTES

        Route::get('auth/register', function () {
            return view('auth.register');
        });
        Route::post('auth/register', 'AdministratorController@tttest');


        Route::get('/admin/administrator/page/show', 'AdministratorController@showAdministratorPage');
        Route::get('/admin/administrator/record/new', 'AdministratorController@createAdmin');
        Route::get('/admin/administrator/user/show/{user_id}', 'AdministratorController@showUser');

        Route::post('/admin/administrator/users/update', 'AdministratorController@updateUser');
        Route::post('/admin/administrator/users/store', 'AdministratorController@storeUser');
        Route::post('/admin/administrator/users/approveStatus', 'AdministratorController@changeApprove');

        Route::post('/admin/administrator/record/delete', 'AdministratorController@deleteUser');


        //        ADMIN LOGIN ROUTES

        Route::get('/admin', function () {
            return view('admin.index');
        });


//        BENEFIT MAIN PAGE
        Route::get('/admin/benefits/page', 'BenefitsController@indexPage');
        Route::post('/admin/benefits/mainPage/update', 'BenefitsController@updateMainPage');
//        END BENEFIT MAIN PAGE
//        BENEFIT INNER PAGES
        Route::get('/admin/benefits/page_inner/{benefit_page_id}', 'BenefitsController@showBenefitInnerPage');
        Route::post('/admin/benefits/page_inner/edit', 'BenefitsController@editBenefitInnerPage');
        //     END BENEFIT INNER PAGES
//        BENEFIT INNER ITEMS
        Route::get('/admin/benefits/items/list/{benefit_page_id}', 'BenefitsController@listBenefitItems');
        Route::get('/admin/benefits/items/create/{benefitInnerPageID}', 'BenefitsController@createBenefitItems');
        Route::post('/admin/benefits/items/store', 'BenefitsController@updateOrStoreNewBenefitItems');
        Route::post('/admin/benefits/items/destroy', 'BenefitsController@destroyBenefitItemById');
        Route::get('/admin/benefits/items/show/{benefitItemId}', 'BenefitsController@showBenefitItem');
//        Route::post('/admin/benefits/items/update', 'BenefitsController@updateBenefitItem');
        //       END BENEFIT INNER ITEMS
//        HOW WORK METHODS
        Route::get('/admin/methods/show', 'MethodsController@index');
        Route::post('/admin/methods/update', 'MethodsController@update');
        Route::get('/admin/methods/destroyImage/{image_name?}/{position?}', 'MethodsController@deleteStepImage');
        //   END HOW WORK METHODS
//        PARTNERS
        Route::get('/admin/partners/page/show', 'PartnersController@index');
        Route::post('/admin/partners/page/update', 'PartnersController@updatePage');

        Route::get('/admin/partners/list/show/{partner_id?}', 'PartnersController@getPartnerList');
        Route::post('/admin/partners/list/createUpdate', 'PartnersController@addOrEditPartner');
        Route::post('/admin/partners/list/destroy', 'PartnersController@destroyPartner');


        Route::get('/admin/partners/list/moveDown/{partner_id}', 'PartnersController@movePartnerDown');
        Route::get('/admin/partners/list/moveUp/{partner_id}', 'PartnersController@movePartnerUp');

//        END PARTNERS

//        HOME PAGE ROUTES
        Route::get('/admin/homePage/show', 'HomePageController@index');
        Route::post('/admin/homePage/update', 'HomePageController@update');
//        END HOME PAGE ROUTES
//        CASES ITEMS ROUTES
        Route::get('/admin/cases/destroyImage/{case_id?}', 'CasesController@deleteCaseInnerImage');

        Route::get('/admin/cases/main/showPage', 'CasesController@showMainPage');
        Route::post('/admin/cases/main/updatePage', 'CasesController@updateMainPage');

        Route::get('/admin/cases/inner/showPage/{page_id}', 'CasesController@showInnerPage');
        Route::post('/admin/cases/inner/updatePage', 'CasesController@updateInnerPage');
        Route::post('/admin/cases/inner/updateFront', 'CasesController@updateFrontCaseView');
//        END CASES ITEMS ROUTES
//        NEWS

        Route::get('/admin/news/comments/list', 'NewsController@listCommentNews');
        Route::post('/admin/news/comments/delete', 'NewsController@deleteCommentNews');

        Route::get('/admin/news/destroyImage/{news_id?}', 'NewsController@deleteNewsImage');

        Route::get('/admin/news/categoryList/{category_id?}', 'NewsController@listCategories');
        Route::post('/admin/news/categoryList/addOrUpdate', 'NewsController@addOrEditCategories');
        Route::post('/admin/news/categoryList/destroy', 'NewsController@destroyCategory');


        Route::get('/admin/news/tagList/{tag_id?}', 'NewsController@listTags');
        Route::post('/admin/news/tagList/addOrUpdate', 'NewsController@addOrEditTag');
        Route::post('/admin/news/tagList/destroy', 'NewsController@destroyTag');
        Route::post('/admin/news/tagList/updateTagOfNews', 'NewsController@setNewsTag');
        Route::post('/api/getNewsTags', 'Api\GetNewsTags@getTags');


        Route::get('/admin/news/editMainPage', 'NewsController@editMainPage');
        Route::post('/admin/news/updateMainPage', 'NewsController@updateNewsMainPage');

        Route::get('/admin/news/showChildPage', 'NewsController@showChildNewsPage');
        Route::post('/admin/news/updateChildPage', 'NewsController@updateChildNewsPage');

        Route::get('/admin/news/list', 'NewsController@listNews');
        Route::get('/admin/news/list/create', 'NewsController@createNews');
        Route::get('/admin/news/list/show/{news_id}', 'NewsController@showNews');
        Route::post('/admin/news/list/addOrUpdate', 'NewsController@addOrUpdateNews');
        Route::post('/admin/news/list/destroy', 'NewsController@destroyNews');
//        END OF NEWS
//        EVENTS


        Route::get('/admin/events/comments/list', 'EventsController@listCommentEvents');
        Route::post('/admin/events/comments/delete', 'EventsController@deleteCommentEvent');


        Route::get('/admin/events/destroyImage/{event_id?}', 'EventsController@deleteEventImage');

        Route::get('/admin/events/categoryList/{category_id?}', 'EventsController@listCategories');
        Route::post('/admin/events/categoryList/addOrUpdate', 'EventsController@addOrEditCategories');
        Route::post('/admin/events/categoryList/destroy', 'EventsController@destroyCategory');


        Route::get('/admin/events/tagList/{tag_id?}', 'EventsController@listTags');
        Route::post('/admin/events/tagList/addOrUpdate', 'EventsController@addOrEditTag');
        Route::post('/admin/events/tagList/destroy', 'EventsController@destroyTag');
        Route::post('/admin/events/tagList/updateTagOfNews', 'EventsController@setEventsTag');
        Route::post('/api/getEventTags', 'Api\GetEventTags@getTags');


        Route::get('/admin/events/editMainPage', 'EventsController@editMainPage');
        Route::post('/admin/events/updateMainPage', 'EventsController@updateMainPage');

        Route::get('/admin/events/showChildPage', 'EventsController@showChildPage');
        Route::post('/admin/events/updateChildPage', 'EventsController@updateChildPage');

        Route::get('/admin/events/list', 'EventsController@listEvents');
        Route::get('/admin/events/list/create', 'EventsController@createEvent');
        Route::get('/admin/events/list/show/{event_id}', 'EventsController@showEvents');
        Route::post('/admin/events/list/addOrUpdate', 'EventsController@addOrUpdateEvent');
        Route::post('/admin/events/list/destroy', 'EventsController@destroyEvent');

//        END OF EVENTS
//        DOWNLOADS
        Route::get('/admin/downloads/download/file/{file_id}', 'FilesController@downloadFile');

        Route::get('/admin/downloads/categoryList/{category_id?}', 'FilesController@listCategories');
        Route::post('/admin/downloads/categoryList/addOrUpdate', 'FilesController@addOrEditCategories');
        Route::post('/admin/downloads/categoryList/destroy', 'FilesController@destroyCategory');


        Route::get('/admin/downloads/page/show', 'FilesController@showPage');
        Route::post('/admin/downloads/page/update', 'FilesController@updatePage');

        Route::get('/admin/downloads/list', 'FilesController@listFiles');
        Route::get('/admin/downloads/list/create', 'FilesController@createFile');
        Route::get('/admin/downloads/list/show/{file_id}', 'FilesController@showFileRecord');
        Route::post('/admin/downloads/list/addOrUpdate', 'FilesController@addOrUpdateFileRecord');
        Route::post('/admin/downloads/list/destroy', 'FilesController@destroyFileRecord');


//        END OF DOWNLOADS

//        KNOWLEDGE CENTER
        Route::get('/admin/knowCenter/page/show', 'KnowCenterController@show');
        Route::post('/admin/knowCenter/page/update', 'KnowCenterController@update');

//        END OF KNOWLEDGE CENTER

//        FAVORITES
        Route::get('/admin/favorites/showTool/setDropdownModelToSession/{model_to_name}', 'FavoritesController@setModelToSession');

        Route::get('/admin/favorites/showTool/{atach_model_name}/{atach_model_id}', 'FavoritesController@showTool');
        Route::post('/admin/favorites/add', 'FavoritesController@addFav');
        Route::post('/admin/favorites/setRandomSlot', 'FavoritesController@addSetSlotRandom');
        Route::post('/admin/favorites/setSlotByDate', 'FavoritesController@addFavByDate');

//        END OF FAVORITES
        Route::get('/admin/features/page/show', 'FeaturesController@indexPage');
        Route::post('/admin/features/page/update', 'FeaturesController@updatePage');


        Route::get('/admin/features/items/list', 'FeaturesController@listItems');
        Route::get('/admin/features/items/create', 'FeaturesController@createItem');
        Route::post('/admin/features/items/store', 'FeaturesController@updateOrStoreNewItem');
        Route::post('/admin/features/items/destroy', 'FeaturesController@destroyItemById');
        Route::get('/admin/features/items/show/{item_Id}', 'FeaturesController@showItem');

//        FEATURES
//        END OF FEATURES
//        CONTACTS
        Route::get('/admin/contacts/pages/main/show', 'ContactsController@showMainPage');
        Route::post('/admin/contacts/pages/main/update', 'ContactsController@updateMainPage');

        Route::get('/admin/contacts/pages/child/show', 'ContactsController@showChildPage');
        Route::post('/admin/contacts/pages/child/update', 'ContactsController@updateChildPage');

        Route::get('/admin/contacts/offices/list', 'ContactsController@listOffices');
        Route::get('/admin/contacts/offices/create', 'ContactsController@createOffice');
        Route::get('/admin/contacts/offices/show/{office_id}', 'ContactsController@showOffice');
        Route::post('/admin/contacts/offices/add_update', 'ContactsController@addOrUpdateOffice');
        Route::post('/admin/contacts/offices/destroy', 'ContactsController@destroyOffice');

        Route::post('/admin/contacts/mails/demo', 'ContactsController@updateDemoRequestMails');
        Route::post('/admin/contacts/mails/pipeline', 'ContactsController@updateDataCardRequestMails');


//        CONTACTS

//        END OF CONTACTS

//        TEAM
        Route::get('/admin/team/page/show', 'TeamController@showPage');
        Route::post('/admin/team/page/update', 'TeamController@updatePage');

        Route::get('/admin/team/list', 'TeamController@listEmployee');
        Route::get('/admin/team/list/create', 'TeamController@createEmployee');
        Route::get('/admin/team/list/show/{employee_id}', 'TeamController@showEmployee');
        Route::post('/admin/team/list/addOrUpdate', 'TeamController@addOrUpdateEmployee');
        Route::post('/admin/team/list/destroy', 'TeamController@destroyEmployee');

//        END OF TEAM


//        ABOUT PAGE
        Route::get('/admin/about/page/show', 'AboutController@showPage');
        Route::post('/admin/about/page/update', 'AboutController@updatePage');

//        END OF ABOUT PAGE


//        JOBS

        Route::get('/admin/jobs/page/destroyImage', 'JobsController@deleteJoinTheTeamImage');

        Route::get('/admin/jobs/page/show', 'JobsController@showPage');
        Route::post('/admin/jobs/page/update', 'JobsController@updatePage');

        Route::get('/admin/jobs/items/list', 'JobsController@listItems');
        Route::get('/admin/jobs/items/create', 'JobsController@createItem');
        Route::post('/admin/jobs/items/store', 'JobsController@updateOrStoreNewItem');
        Route::post('/admin/jobs/items/destroy', 'JobsController@destroyItemById');
        Route::get('/admin/jobs/items/show/{item_Id}', 'JobsController@showItem');


        Route::get('/admin/jobs/apply/list', 'JobsController@applyList');

//        JOBS
//        DEMO
//        Route::get('/admin/demo/page/destroyImage', 'DemoController@deleteJoinTheTeamImage');
        Route::get('/admin/demo/page/show', 'DemoController@editMainPage');
        Route::post('/admin/demo/page/update', 'DemoController@updatePage');


//        DEMO

//        PIPELINES

        Route::get('/admin/pipelines/page/destroyImage', 'PipeLinesController@deleteImage');

        Route::get('/admin/pipelines/page/show', 'PipeLinesController@showPage');
        Route::post('/admin/pipelines/page/update', 'PipeLinesController@updatePage');


//        Route::get('/admin/pipelines/items/list', 'PipeLinesController@listItems');
//        Route::get('/admin/pipelines/items/create', 'PipeLinesController@createItem');
//        Route::post('/admin/pipelines/items/store', 'PipeLinesController@updateOrStoreNewItem');
//        Route::post('/admin/pipelines/items/destroy', 'PipeLinesController@destroyItemById');
//        Route::get('/admin/pipelines/items/show/{item_Id}', 'PipeLinesController@showItem');

//        Route::get('/admin/pipelines/download/file/{item_Id}', 'PipeLinesController@downloadFile');


//        PIPELINES

//        ADDITIONAL FILES

        Route::get('/admin/additioanal_files/list', 'AdditionalFilesController@showPage');
        Route::get('/admin/additioanal_files/create', 'AdditionalFilesController@showCreateForm');
        Route::post('/admin/additioanal_files/store', 'AdditionalFilesController@storeFile');
        Route::post('/admin/additioanal_files/delete', 'AdditionalFilesController@deleteFile');


        //        ADDITIONAL FILES

        //       PRIVACY AND DISCLAIMER
        Route::get('/admin/privacy', 'PrivacyDisclaimerController@showPrivacy');
        Route::get('/admin/disclaimer', 'PrivacyDisclaimerController@showDisclaimer');

        Route::post('/admin/privacy/update', 'PrivacyDisclaimerController@updatePrivacy');
        Route::post('/admin/disclaimer/update', 'PrivacyDisclaimerController@updateDisclaimer');
        //       PRIVACY AND DISCLAIMER


    });

//    });
});
