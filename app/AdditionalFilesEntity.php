<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class AdditionalFilesEntity extends Model
{

    protected $table = 'additional_files';

    public static $path = 'uploads' . '/' . 'files' . '/' . 'additionalFiles' . '/';


    public static function editFile($data, $request)
    {
        try {
            $record = AdditionalFilesEntity::findOrNew($data['id']);

            if (isset($data['info'])) {
                $record->info = $data['info'];
            }


            if (isset($data['file_name'])) {

                if (!empty($record->file_name)) {
                    File::delete(self::$path . $record->file_name);
                }


                $file = self::renameFile($data['file_name']);


                $request->file('file_name')->move(self::$path, $file);

                $record->file_name = $file;
            }

            $record->save();


        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('creatingError', $ex);
        }


        return array('successMessage', $record);
    }

    private static function renameFile($file)
    {
        $name = $file->getClientOriginalName();
        $name = str_replace(' ', '_', $name);

        $time = time();
//        $file = $name . '_' . $time . '.' . $file->getClientOriginalExtension();
        $file = $time . '_' . $name;
        return $file;
    }


    public static function destroyRecord($recordId)
    {
        try {
            $record = AdditionalFilesEntity::find($recordId);
            if (!empty($record->file_name)) {
                File::delete(self::$path . $record->file_name);

            }
            $record->delete();

        } catch (\Exception $ex) {
            \Log::error($ex);
            return false;
        }
        return true;
    }


}
