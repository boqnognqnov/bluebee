<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class EventTagsEntity extends Model
{

    protected $table = 'tags_events';


    public static function addOrEditTag($data)
    {
        $langArr = ['en', 'de', 'fr'];


        try {

            $tag = EventTagsEntity::findOrNew($data['id']);


            foreach ($langArr as $oneLang) {
                $title = 'title_' . $oneLang;
                if (isset($data[$title])) {
                    $tag->$title = $data[$title];
                }
            }


            $tag->save();


        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('creatingError', $ex);
        }

        return array('successMessage', $tag);

    }

    public static function destroyTag($tagId)
    {
        try {
            $recordsToDel = TagToEventsEntity::where('tag_id', '=', $tagId)->get();
            foreach ($recordsToDel as $oneRecToDel) {
                $oneRecToDel->delete();
            }
            EventTagsEntity::destroy($tagId);
        } catch (\Exception $ex) {
            \Log::error($ex);
        }

    }

}
