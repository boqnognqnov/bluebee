<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class PartnersListEntity extends Model
{

    protected $table = 'partners';

    public static $path = 'uploads' . '/' . 'images' . '/' . 'components' . '/' . 'partners' . '/' . 'medium' . '/';

    public static $pathMedium = 'uploads' . '/' . 'images' . '/' . 'components' . '/' . 'partners' . '/' . 'medium' . '/';
    public static $pathSmall = 'uploads' . '/' . 'images' . '/' . 'components' . '/' . 'partners' . '/' . 'small' . '/';


    public static function addEditPartnerRules()
    {
        return array(
//            'title_en' => 'required',
            'text_en' => 'required',

//            'title_de' => 'required',
//            'text_de' => 'required',
//
//            'title_fr' => 'required',
//            'text_fr' => 'required',

            'url' => 'required|active_url',
            'image' => 'image',
        );
    }

    public static function addEditPartnerMessages()
    {
        return [
            'title_en.required' => 'The company title EN is not set.',
            'text_en.required' => 'The company text EN is not set.',

            'title_de.required' => 'The company title EN is not set.',
            'text_de.required' => 'The company text EN is not set.',

            'title_fr.required' => 'The company title EN is not set.',
            'text_fr.required' => 'The company text EN is not set.',

            'url.required' => 'The company website url is required',
            'url.active_url' => 'The company website url is not valid',
            'image.image' => 'The file must be image',
            'image.required' => 'The image is not set.',
        ];
    }


    public static function addOrEditPartner($data)
    {
        $rules = self::addEditPartnerRules();
        if ($data['id'] == 0) {
            $rules['image'] = 'required|image';
        }
        $validator = \Validator::make($data, $rules, self::addEditPartnerMessages());


        $is_favorite = false;
        if (isset($data['is_favorite'])) {

            $favPartners = PartnersListEntity::where('is_favorite', '=', true)->get()->toArray();
            $favPartnersCounter = sizeof($favPartners);
            if ($favPartnersCounter >= 5) {
                return array('favoriteError', 'favoriteError');
            }

            $is_favorite = true;
        }

        if ($validator->fails()) {
            return array('validationError', $validator);
        }

        $langArr = ['en', 'de', 'fr'];


        try {

            $partner = PartnersListEntity::findOrNew($data['id']);
            if ($data['id'] == 0) {
                $lastRecord = PartnersListEntity::orderBy('position', 'desc')->first();
                $lastPosition = $lastRecord->position;
                $partner->position = $lastPosition + 1;
            }


            foreach ($langArr as $oneLang) {

                $title = 'title_' . $oneLang;
                $text = 'text_' . $oneLang;


                if (isset($data[$title])) {
                    $partner->$title = $data[$title];
                }

                if (isset($data[$text])) {
                    $partner->$text = $data[$text];
                }
            }

            if (isset($data['url'])) {
                $partner->url = $data['url'];
            }


            $partner->is_favorite = $is_favorite;
            if (isset($data['image'])) {

                if (!empty($partner->image)) {
                    File::delete(self::$path . $partner->image);
                    File::delete(self::$pathMedium . $partner->image);
                    File::delete(self::$pathSmall . $partner->image);
                }

//                $time = time();
//                $fileName = $time . '.' . $data['image']->getClientOriginalExtension();
//                Image::make($data['image']->getRealPath())->save(self::$path . $fileName);

                $time = time();
                $fileName = $time . '.' . $data['image']->getClientOriginalExtension();
                $image = Image::make($data['image']->getRealPath());

//                $image->save(self::$path . $fileName);

                $image->resize(2400, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(self::$path . $fileName);

                $image->resize(1200, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(self::$pathMedium . $fileName);

                $image->resize(600, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(self::$pathSmall . $fileName);

                $partner->image = $fileName;
            }


            $partner->save();


        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('creatingError', $ex);
        }

        return array('successMessage', $partner);

    }

    public static function destroyPartner($partnerId)
    {

        try {

            $partner = PartnersListEntity::find($partnerId);

            if (!empty($partner->image)) {
                File::delete(self::$path . $partner->image);
            }


            $partner->delete();


        } catch (\Exception $ex) {
            \Log::error($ex);
            return false;
        }

        return true;

    }

}
