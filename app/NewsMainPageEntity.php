<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class NewsMainPageEntity extends Model
{

    protected $table = 'page_news';


    public static function editMainNewsPagePage($data)
    {

        try {

            $pageNews = NewsMainPageEntity::all()->first();

            $headIsOn = false;
            if (isset($data['head_is_on'])) {
                $headIsOn = true;

                $headValidStatus = HeaderComponentEntity::validateComponent($data['head_data'], $pageNews->head_id);
                if ($headValidStatus != 'success') {
                    return array('validationError', $headValidStatus);
                }
            }


            $pageNews->head_is_on = $headIsOn;


            $sign_up_is_on = false;
            if (isset($data['sign_up_is_on'])) {
                $sign_up_is_on = true;
            }
            $pageNews->sign_up_is_on = $sign_up_is_on;


            $pageNews->save();

            if (isset($data['head_data'])) {
                HeaderComponentEntity::editHeaderComponent($data['head_data'], $pageNews->head_id);
            }


        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('creatingError', $ex);
        }

        return array('successMessage', $pageNews);

    }

}
