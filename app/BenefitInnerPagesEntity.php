<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class BenefitInnerPagesEntity extends Model
{

    protected $table = 'page_benefits_inner';

    public static $path = 'uploads' . '/' . 'images' . '/' . 'pages' . '/' . 'benefitsFrontImages' . '/';

    public static $pathNegative = 'uploads' . '/' . 'images' . '/' . 'pages' . '/' . 'benefitsFrontImages' . '/' . 'neagtive' . '/';
    public static $pathBackground = 'uploads' . '/' . 'images' . '/' . 'pages' . '/' . 'benefitsFrontImages' . '/' . 'background' . '/' . 'default' . '/';
    public static $pathBackgroundM = 'uploads' . '/' . 'images' . '/' . 'pages' . '/' . 'benefitsFrontImages' . '/' . 'background' . '/' . 'medium' . '/';
    public static $pathBackgroundS = 'uploads' . '/' . 'images' . '/' . 'pages' . '/' . 'benefitsFrontImages' . '/' . 'background' . '/' . 'small' . '/';

    public static function pageRules()
    {
        return array(
            'front_image' => 'image',

            'front_title_en' => 'required',
            'front_text_en' => 'required',

//            'front_title_de' => 'required',
//            'front_text_de' => 'required',
//
//            'front_title_fr' => 'required',
//            'front_text_fr' => 'required',


        );
    }

    public static function pageMessages()
    {
        return [

            'front_image.image' => 'The front image is not valid format',
            'front_image.required' => 'The front image is not set',

//            'front_title_en.required' => 'The title EN is not set.',
            'front_text_en.required' => 'The text EN is not set.',
//
//            'front_title_de.required' => 'The title DE is not set.',
//            'front_text_de.required' => 'The text DE is not set.',
//
//            'front_title_fr.required' => 'The title FR is not set.',
//            'front_text_fr.required' => 'The text FR is not set.',
        ];
    }


    public static function editInnerBenefitPage($data)
    {
        $iBPage = BenefitInnerPagesEntity::find($data['id']);


        $headIsOn = false;
        if (isset($data['head_is_on'])) {
            $headIsOn = true;

            $headValidStatus = HeaderComponentEntity::validateComponent($data['head_data'], $iBPage->head_id);
            if ($headValidStatus != 'success') {
                return array('validationError', $headValidStatus);
            }
        }

        $try_is_on = false;
        if (isset($data['try_is_on'])) {
            $try_is_on = true;

            $tryValidStatus = TryComponentEntity::validateTry($data['try_data'], $iBPage->try_id);
            if ($tryValidStatus != 'success') {
                return array('validationError', $tryValidStatus);
            }
        }


        $rules = self::pageRules();
        if (empty($iBPage->front_image)) {
            $rules['front_image'] = 'required|image';
        }


        $validator = \Validator::make($data, $rules, self::pageMessages());

        if ($validator->fails()) {
            return array('validationError', $validator);
        }

        $langArr = ['en', 'de', 'fr'];


        $favorites_is_on = false;
        if (isset($data['favorites_is_on'])) {
            $favorites_is_on = true;
        }

        $opinion_is_on = false;
        if (isset($data['opinion_is_on'])) {
            $opinion_is_on = true;

            $tryValidStatus = OpinionComponentEntity::validateOpinion($data['opinion_data'], $iBPage->opinion_id);
            if ($tryValidStatus != 'success') {
                return array('validationError', $tryValidStatus);
            }
        }
        $iBPage->opinion_is_on = $opinion_is_on;


        try {


            foreach ($langArr as $oneLang) {

                $title = 'front_title_' . $oneLang;
                $text = 'front_text_' . $oneLang;
                $textDesc = 'front_text_s_' . $oneLang;


                $slug = 'slug_' . $oneLang;


                if (isset($data[$title])) {
                    $iBPage->$title = $data[$title];

                    $iBPage->$slug = str_slug($data[$title]);
                }

                if (isset($data[$text])) {
                    $iBPage->$text = $data[$text];
                }

                if (isset($data[$textDesc])) {
                    $iBPage->$textDesc = $data[$textDesc];
                }

            }
            $iBPage->head_is_on = $headIsOn;
            $iBPage->favorites_is_on = $favorites_is_on;
            $iBPage->try_is_on = $try_is_on;

            if (isset($data['front_image'])) {

                if (!empty($iBPage->front_image)) {
                    File::delete(self::$path . $iBPage->front_image);
                }

                $time = time();
                $fileName = $time . '.' . $data['front_image']->getClientOriginalExtension();
                Image::make($data['front_image']->getRealPath())->save(self::$path . $fileName);
                $iBPage->front_image = $fileName;
            }

            if (isset($data['front_negative_image'])) {

                if (!empty($iBPage->front_negative_image)) {
                    File::delete(self::$pathNegative . $iBPage->front_negative_image);
                }

                $time = time();
                $fileName = $time . '.' . $data['front_negative_image']->getClientOriginalExtension();
                Image::make($data['front_negative_image']->getRealPath())->save(self::$pathNegative . $fileName);
                $iBPage->front_negative_image = $fileName;
            }

            if (isset($data['front_background_image'])) {

                if (!empty($iBPage->front_background_image)) {
                    File::delete(self::$pathBackground . $iBPage->front_background_image);
                    File::delete(self::$pathBackgroundM . $iBPage->front_background_image);
                    File::delete(self::$pathBackgroundS . $iBPage->front_background_image);
                }

//                $time = time();
//                $fileName = $time . '.' . $data['front_background_image']->getClientOriginalExtension();
//                Image::make($data['front_background_image']->getRealPath())->save(self::$pathBackground . $fileName);
//                $iBPage->front_background_image = $fileName;

                $time = time();
                $fileName = $time . '.' . $data['front_background_image']->getClientOriginalExtension();
                $image = Image::make($data['front_background_image']->getRealPath());

//                $image->save(self::$path . $fileName);

                $image->resize(2400, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(self::$pathBackground . $fileName);

                $image->resize(1200, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(self::$pathBackgroundM . $fileName);

                $image->resize(600, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(self::$pathBackgroundS . $fileName);

                $iBPage->front_background_image = $fileName;
            }


            $iBPage->save();

            if (isset($data['try_data'])) {
                TryComponentEntity::editTryComponent($data['try_data'], $iBPage->try_id);
            }

            if (isset($data['head_data'])) {
                HeaderComponentEntity::editHeaderComponent($data['head_data'], $iBPage->head_id);
            }
            if (isset($data['opinion_data'])) {
                OpinionComponentEntity::editOpinionComponent($data['opinion_data'], $iBPage->opinion_id);
            }


        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('creatingError', $ex);
        }

        return array('successMessage', $iBPage);

    }


}
