<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class EventCategoriesEntity extends Model
{

    protected $table = 'category_events';


    public static function addOrEditCategories($data)
    {
        $langArr = ['en', 'de', 'fr'];


        try {

            $category = EventCategoriesEntity::findOrNew($data['id']);


            foreach ($langArr as $oneLang) {
                $title = 'title_' . $oneLang;
                if (isset($data[$title])) {
                    $category->$title = $data[$title];
                }
            }


            $category->save();


        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('creatingError', $ex);
        }

        return array('successMessage', $category);

    }

    public static function destroyCat($catId)
    {
        try {
            $listOfEvents = EventItemsEntity::where('category_id', '=', $catId)->get();
            foreach ($listOfEvents as $oneEvent) {
                if (!empty($oneEvent->image)) {
                    File::delete(NewsItemsEntity::$path . $oneEvent->image);
                }
                $oneEvent->delete();
            }

            $category = EventCategoriesEntity::find($catId);
            $category->delete();
        } catch (\Exception $ex) {
            \Log::error($ex);
        }

    }

}
