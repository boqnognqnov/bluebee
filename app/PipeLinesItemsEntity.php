<?php
//
//namespace App;
//
//use Illuminate\Database\Eloquent\Model;
//use Intervention\Image\ImageManagerStatic as Image;
//use File;
//
//class PipeLinesItemsEntity extends Model
//{
//
//    protected $table = 'pipelines';
//
//    public static $path = 'uploads' . '/' . 'files' . '/' . 'pipeLines' . '/';
//
//
//    public static function addOrEdit($data)
//    {
//        $langArr = ['en', 'de', 'fr'];
//
//
//        try {
//
//            $pipe = PipeLinesItemsEntity::findOrNew($data['id']);
//
//
//            foreach ($langArr as $oneLang) {
//
//                $title = 'title_' . $oneLang;
//                $text = 'text_' . $oneLang;
//
//
//                if (isset($data[$title])) {
//                    $pipe->$title = $data[$title];
//                }
//
//                if (isset($data[$text])) {
//                    $pipe->$text = $data[$text];
//                }
//            }
//
//
//            if (isset($data['pipe_file'])) {
//
//                if (!empty($pipe->pipe_file)) {
//                    File::delete(self::$path . $pipe->pipe_file);
//                }
//
//                $time = time();
//                $fileName = $time . '.' . $data['pipe_file']->getClientOriginalExtension();
//                Image::make($data['pipe_file']->getRealPath())->save(self::$path . $fileName);
//                $pipe->pipe_file = $fileName;
//            }
//
//
//            $pipe->save();
//
//
//        } catch (\Exception $ex) {
//            \Log::error($ex);
//        }
//
//        return $pipe->id;
//
//    }
//
//    public static function destroyPipe($pipe_id)
//    {
//        try {
//            $pipe = PipeLinesItemsEntity::find($pipe_id);
//            if (!empty($pipe->pipe_file)) {
//                File::delete(self::$path . $pipe->pipe_file);
//            }
//            $pipe->delete();
//
//        } catch (\Exception $ex) {
//            \Log::error($ex);
//            return false;
//        }
//        return true;
//    }
//
//}
