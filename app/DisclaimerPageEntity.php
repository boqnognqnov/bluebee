<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class DisclaimerPageEntity extends Model
{

    protected $table = 'page_diclaimer';


    public static function editPageRules()
    {
        return array(
            'text_en' => 'required',
        );
    }

    public static function editPageMessages()
    {
        return [
            'text_en.required' => 'The main section body text is not set in some languages.',
        ];
    }

    public static function editPage($data)
    {
        $validator = \Validator::make($data, self::editPageRules(), self::editPageMessages());

        if ($validator->fails()) {
            return array('validationError', $validator);
        }

        $page = DisclaimerPageEntity::all()->first();

        $langArr = ['en', 'de', 'fr'];


        try {

            foreach ($langArr as $oneLang) {
                $text = 'text_' . $oneLang;
                if (isset($data[$text])) {
                    $page->$text = $data[$text];
                }
            }

            $page->save();

        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('creatingError', $ex);
        }
        return array('successMessage', $page);
    }

}
