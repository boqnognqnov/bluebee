<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class HomePageEntity extends Model
{

    protected $table = 'page_index';
//    public static $pathHeader = 'uploads' . '/' . 'images' . '/' . 'pages' . '/' . 'homePage' . '/';
//    public static $pathHeader = 'uploads/images/pages/homePage/';

    public static $pathHeader = 'uploads' . '/' . 'images' . '/' . 'pages' . '/' . 'homePage' . '/' . 'default' . '/';
    public static $pathMedium = 'uploads' . '/' . 'images' . '/' . 'pages' . '/' . 'homePage' . '/' . 'medium' . '/';
    public static $pathSmall = 'uploads' . '/' . 'images' . '/' . 'pages' . '/' . 'homePage' . '/' . 'small' . '/';


    public static function editPageRules()
    {
        return array(
            'head_image' => 'image',

            'head_title_en' => 'required',
            'head_text_en' => 'required',
//            'head_button_en' => 'required',

//            'head_title_de' => 'required',
//            'head_text_de' => 'required',
//            'head_button_de' => 'required',

//            'head_title_fr' => 'required',
//            'head_text_fr' => 'required',
//            'head_button_fr' => 'required',


        );
    }

    public static function editPageMessages()
    {
        return [

            'head_image.image' => 'Home page intro image is not valid format',
            'head_image.required' => 'Home page intro image is not set',

            'head_title_en.required' => 'Home page intro title EN is required',
            'head_text_en.required' => 'Home page intro text EN is required',
            'head_button_en.required' => 'Home page intro button name EN is required',

            'head_title_de.required' => 'Home page intro title DE is required',
            'head_text_de.required' => 'Home page intro text DE is required',
            'head_button_de.required' => 'Home page intro button name DE is required',

            'head_title_fr.required' => 'Home page intro title FR is required',
            'head_text_fr.required' => 'Home page intro text FR is required',
            'head_button_fr.required' => 'Home page intro button name FR is required',

        ];
    }


    public static function updateHomePage($data)
    {
        $rules = self::editPageRules();
        $page = HomePageEntity::all()->first();
        $head_is_on = false;
        if (isset($data['head_is_on'])) {
            $head_is_on = true;
            if (empty($page->head_image)) {
                $rules['head_image'] = 'required|image';
            }

            $validator = \Validator::make($data, $rules, self::editPageMessages());

            if ($validator->fails()) {
                return array('validationError', $validator);
            }
        }


        $try_is_on = false;
        if (isset($data['try_is_on'])) {
            $try_is_on = true;

            $tryValidStatus = TryComponentEntity::validateTry($data['try_data'], $page->try_id);
            if ($tryValidStatus != 'success') {
                return array('validationError', $tryValidStatus);
            }
        }

        $opinion_is_on = false;
        if (isset($data['opinion_is_on'])) {
            $opinion_is_on = true;

            $tryValidStatus = OpinionComponentEntity::validateOpinion($data['opinion_data'], $page->opinion_id);
            if ($tryValidStatus != 'success') {
                return array('validationError', $tryValidStatus);
            }
        }


        $langArr = ['en', 'de', 'fr'];


        try {


            $page->head_is_on = $head_is_on;

            $how_work_is_on = false;
            if (isset($data['how_work_is_on'])) {
                $how_work_is_on = true;
            }
            $page->how_work_is_on = $how_work_is_on;


            $why_blue_is_on = false;
            if (isset($data['why_blue_is_on'])) {
                $why_blue_is_on = true;
            }
            $page->why_blue_is_on = $why_blue_is_on;


            $partners_is_on = false;
            if (isset($data['partners_is_on'])) {
                $partners_is_on = true;
            }
            $page->partners_is_on = $partners_is_on;


            $page->opinion_is_on = $opinion_is_on;


            $page->try_is_on = $try_is_on;

            if (isset($data['head_button_url'])) {
                $page->head_button_url = $data['head_button_url'];
            }


            foreach ($langArr as $oneLang) {

                $headTitle = 'head_title_' . $oneLang;
                $headText = 'head_text_' . $oneLang;
                $buttonText = 'head_button_' . $oneLang;


                if (isset($data[$headTitle])) {
                    $page->$headTitle = $data[$headTitle];
                }

                if (isset($data[$headText])) {
                    $page->$headText = $data[$headText];
                }

                if (isset($data[$buttonText])) {
                    $page->$buttonText = $data[$buttonText];
                }

            }


            if (isset($data['head_image'])) {

                if (!empty($page->head_image)) {
                    File::delete(self::$pathHeader . $page->head_image);
                    File::delete(self::$pathMedium . $page->head_image);
                    File::delete(self::$pathSmall . $page->head_image);
                }


                $time = time();
                $fileName = $time . '.' . $data['head_image']->getClientOriginalExtension();
                $image = Image::make($data['head_image']->getRealPath());

//                $image->save(self::$path . $fileName);

                $image->resize(2400, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(self::$pathHeader . $fileName);

                $image->resize(1200, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(self::$pathMedium . $fileName);

                $image->resize(600, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(self::$pathSmall . $fileName);

                $page->head_image = $fileName;
            }


            $page->save();

            if (isset($data['opinion_data'])) {
                OpinionComponentEntity::editOpinionComponent($data['opinion_data'], $page->opinion_id);
            }

            if (isset($data['try_data'])) {
                TryComponentEntity::editTryComponent($data['try_data'], $page->try_id);
            }


        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('creatingError', $ex);
        }
        return array('successMessage', $page);
//        return $page->id;

    }

}
