<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class PipeLinesColumnsEntity extends Model
{

    protected $table = 'pipeline_cols';

    public static function editColumnsRules()
    {
        return array(
            'title_en' => 'required',
//            'title_de' => 'required',
//            'title_fr' => 'required',

            'text_en' => 'required',
//            'text_de' => 'required',
//            'text_fr' => 'required',

        );
    }

    public static function editColumnsMessages()
    {
        return [
            'title_en.required' => 'Columns title EN is not set in some languages.',
            'text_en.required' => 'Columns text EN is not set in some languages.',

//            'title_de.required' => 'Columns title DE is not set in some languages.',
//            'text_de.required' => 'Columns text DE is not set in some languages.',
//
//            'title_fr.required' => 'Columns title FR is not set in some languages.',
//            'text_fr.required' => 'Columns text FR is not set in some languages.',
//            'title_en.required' => 'Column 1 title EN is not set.',
//            'text_en.required' => 'Column 1 text EN is not set.',
//
//            'title_de.required' => 'Column 1 title DE is not set.',
//            'text_de.required' => 'Column 1 text DE is not set.',
//
//            'title_fr.required' => 'Column 1 title FR is not set.',
//            'text_fr.required' => 'Column 1 text FR is not set.',

        ];
    }

    public static function validateColumns($data)
    {
        foreach ($data as $colId => $oneCol) {
            $validator = \Validator::make($data, self::editColumnsRules(), self::editColumnsMessages());

            if ($validator->fails()) {
                return $validator;
            }

            return 'success';
        }
    }


    public static function updateAllColumns($data)
    {
        foreach ($data as $colId => $oneCol) {
            self::edit($oneCol, $colId);
        }
    }

    private static function edit($data, $colId)
    {
        $langArr = ['en', 'de', 'fr'];


        try {

            $column = PipeLinesColumnsEntity::find($colId);


            foreach ($langArr as $oneLang) {

                $title = 'title_' . $oneLang;
                $text = 'text_' . $oneLang;


                if (isset($data[$title])) {
                    $column->$title = $data[$title];
                }

                if (isset($data[$text])) {
                    $column->$text = $data[$text];
                }
            }


            $column->save();


        } catch (\Exception $ex) {
            \Log::error($ex);
        }

        return $column->id;

    }


}
