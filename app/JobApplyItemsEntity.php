<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class JobApplyItemsEntity extends Model
{

    protected $table = 'job_apply';

    public static $path = 'uploads' . '/' . 'files' . '/' . 'position_apply' . '/';


    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    public static function addNew($data)
    {


        try {

            $newPosition = new JobApplyItemsEntity();

            $newPosition->name = $data['name'];


            if (isset($data['image'])) {
                $time = time();
                $fileName = $time . '.' . $data['image']->getClientOriginalExtension();
                Image::make($data['image']->getRealPath())->save(self::$path . $fileName);
                $newPosition->image = $fileName;
            }
            if (isset($data['cv'])) {
                $time = time();
                $fileName = $time . '.' . $data['cv']->getClientOriginalExtension();
                Image::make($data['cv']->getRealPath())->save(self::$path . $fileName);
                $newPosition->cv = $fileName;
            }

            if (isset($data['file_1'])) {
                $time = time();
                $fileName = $time . '.' . $data['file_1']->getClientOriginalExtension();
                Image::make($data['file_1']->getRealPath())->save(self::$path . $fileName);
                $newPosition->file_1 = $fileName;
            }

            if (isset($data['file_2'])) {
                $time = time();
                $fileName = $time . '.' . $data['file_2']->getClientOriginalExtension();
                Image::make($data['file_2']->getRealPath())->save(self::$path . $fileName);
                $newPosition->file_2 = $fileName;
            }

            $newPosition->save();


        } catch (\Exception $ex) {
            \Log::error($ex);
        }

        return $newPosition->id;

    }

    public static function changeToRead($position_id)
    {
        $item = JobApplyItemsEntity::find($position_id);
        $item->is_readed = true;
        $item->save();

    }

    public static function destroyPosition($itemId)
    {
        try {
            $item = JobApplyItemsEntity::find($itemId);
            if (!empty($item->image)) {
                File::delete(self::$path . $item->image);
            }

            if (!empty($item->cv)) {
                File::delete(self::$path . $item->cv);
            }

            if (!empty($item->file_1)) {
                File::delete(self::$path . $item->file_1);
            }

            if (!empty($item->file_2)) {
                File::delete(self::$path . $item->file_2);
            }
            $item->delete();

        } catch (\Exception $ex) {
            \Log::error($ex);
            return false;
        }
        return true;
    }

}
