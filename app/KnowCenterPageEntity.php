<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class KnowCenterPageEntity extends Model
{

    protected $table = 'page_knowledge_center';


    public static function editPage($data)
    {

        $page = KnowCenterPageEntity::all()->first();
        $headIsOn = false;
        if (isset($data['head_is_on'])) {
            $headIsOn = true;

            $headValidStatus = HeaderComponentEntity::validateComponent($data['head_data'], $page->head_id);
            if ($headValidStatus != 'success') {
                return array('validationError', $headValidStatus);
            }
        }

        $sign_up_is_on = false;
        if (isset($data['sign_up_is_on'])) {
            $sign_up_is_on = true;
            $newsLetterValidStatus = SignUpComponentEntity::validateSignUp($data['sign_data'], $page->sign_up_id);
            if ($newsLetterValidStatus != 'success') {
                return array('validationError', $newsLetterValidStatus);
            }
        }

        try {


            $page->head_is_on = $headIsOn;


            $page->sign_up_is_on = $sign_up_is_on;


            $page->save();

            if (isset($data['head_data'])) {
                HeaderComponentEntity::editHeaderComponent($data['head_data'], $page->head_id);
            }

            if (isset($data['sign_data'])) {
                SignUpComponentEntity::editSignComponent($data['sign_data'], $page->sign_up_id);
            }


        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('creatingError', $ex);
        }

        return array('successMessage', $page);

    }

}
