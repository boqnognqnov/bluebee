<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class NewsChildPageEntity extends Model
{

    protected $table = 'page_news_inner';


//    public static $path = 'uploads' . '/' . 'images' . '/' . 'pages' . '/' . 'news' . '/' . 'childPage' . '/' . 'default' . '/';
//    public static $pathMedium = 'uploads' . '/' . 'images' . '/' . 'pages' . '/' . 'news' . '/' . 'childPage' . '/' . 'medium' . '/';
//    public static $pathSmall = 'uploads' . '/' . 'images' . '/' . 'pages' . '/' . 'news' . '/' . 'childPage' . '/' . 'small' . '/';


    public static function editPageRules()
    {
        return array(
            'head_image' => 'image',
        );
    }

    public static function editPageMessages()
    {
        return [
            'head_image.image' => 'Intro image is not valid format',
            'head_image.required' => 'Intro image is not set',
        ];
    }


    public static function editChildNewsPagePage($data)
    {


        try {
            $rules = self::editPageRules();
            $pageNews = NewsChildPageEntity::all()->first();

//            $headIsOn = false;
//            if (isset($data['head_is_on'])) {
//                $headIsOn = true;
//                if (empty($pageNews->head_image)) {
//                    $rules['head_image'] = 'required|image';
//                }
//
//            }
            $headIsOn = false;
            if (isset($data['head_is_on'])) {
                $headIsOn = true;

                $headValidStatus = HeaderComponentEntity::validateComponent($data['head_data'], $pageNews->head_id);
                if ($headValidStatus != 'success') {
                    return array('validationError', $headValidStatus);
                }
            }

            $validator = \Validator::make($data, $rules, self::editPageMessages());

            if ($validator->fails()) {
                return array('validationError', $validator);
            }

            $pageNews->head_is_on = $headIsOn;


            $favorites_is_on = false;
            if (isset($data['favorites_is_on'])) {
                $favorites_is_on = true;
            }
            $pageNews->favorites_is_on = $favorites_is_on;

            $sign_up_is_on = false;
            if (isset($data['sign_up_is_on'])) {
                $sign_up_is_on = true;
            }
            $pageNews->sign_up_is_on = $sign_up_is_on;

//            if (isset($data['head_image'])) {
//
//                if (!empty($pageNews->head_image)) {
//                    File::delete(self::$path . $pageNews->head_image);
//                    File::delete(self::$pathMedium . $pageNews->head_image);
//                    File::delete(self::$pathSmall . $pageNews->head_image);
//                }
//
////                $time = time();
////                $fileName = $time . '.' . $data['head_image']->getClientOriginalExtension();
////                Image::make($data['head_image']->getRealPath())->save(self::$path . $fileName);
//
//                $time = time();
//                $fileName = $time . '.' . $data['head_image']->getClientOriginalExtension();
//                $image = Image::make($data['head_image']->getRealPath());
//
////                $image->save(self::$path . $fileName);
//
//                $image->resize(2400, null, function ($constraint) {
//                    $constraint->aspectRatio();
//                })->save(self::$path . $fileName);
//
//                $image->resize(1200, null, function ($constraint) {
//                    $constraint->aspectRatio();
//                })->save(self::$pathMedium . $fileName);
//
//                $image->resize(600, null, function ($constraint) {
//                    $constraint->aspectRatio();
//                })->save(self::$pathSmall . $fileName);
//
//                $pageNews->head_image = $fileName;
//            }


            $pageNews->save();

            if (isset($data['head_data'])) {
                HeaderComponentEntity::editHeaderComponent($data['head_data'], $pageNews->head_id);
            }


        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('creatingError', $ex);
        }

        return array('successMessage', $pageNews);

    }

}
