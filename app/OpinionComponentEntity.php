<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class OpinionComponentEntity extends Model
{

    protected $table = 'component_opinion';
    public static $path = 'uploads' . '/' . 'images' . '/' . 'components' . '/' . 'opinionComponents' . '/';


    public static function editOpinionRules()
    {
        return array(
//            'name' => 'required',

            'text_en' => 'required',
//            'button_en' => 'required',

            'text_de' => 'required',
//            'button_de' => 'required',

            'text_fr' => 'required',
//            'button_fr' => 'required',

            'image' => 'image',

            'url' => 'required',


        );
    }

    public static function editOpinionMessages()
    {
        return [
            'name.required' => 'Opinion component person name is required',

            'text_en.required' => 'Opinion component text EN is required',
            'button_en.required' => 'Opinion component button name EN is required',

            'text_de.required' => 'Opinion component text DE is required',
            'button_de.required' => 'Opinion component button name DE is required',

            'text_fr.required' => 'Opinion component text FR is required',
            'button_fr.required' => 'Opinion component button name FR is required',

            'image.required' => 'Opinion component the image is required',
            'image.image' => 'Opinion component the file must be an image',

        ];
    }

    public static function validateOpinion($data, $id)
    {
        $component = OpinionComponentEntity::find($id);
        $rules = self::editOpinionRules();
        if (empty($component->image)) {
//            $rules['image'] = 'required|image';
        }
        $validator = \Validator::make($data, $rules, self::editOpinionMessages());

        if ($validator->fails()) {
            return $validator;
        }

        return 'success';
    }


    public static function getOpinionObj($opinion_id)
    {
        return OpinionComponentEntity::find($opinion_id)->toArray();
    }

    public static function editOpinionComponent($data, $opinionId)
    {
        $langArr = ['en', 'de', 'fr'];

        try {

            $opinion = OpinionComponentEntity::find($opinionId);


            foreach ($langArr as $oneLang) {

                $text = 'text_' . $oneLang;
                $button = 'button_' . $oneLang;

                if (isset($data['url'])) {
                    $opinion->url = $data['url'];
                }

                if (isset($data[$text])) {
                    $opinion->$text = $data[$text];
                }
                if (isset($data[$button])) {
                    $opinion->$button = $data[$button];
                }
            }

            if (isset($data['name'])) {
                $opinion->name = $data['name'];
            }


            if (isset($data['image'])) {

                if (!empty($opinion->image)) {
                    File::delete(self::$path . $opinion->image);
                }

                $time = time();
                $fileName = $time . '.' . $data['image']->getClientOriginalExtension();
                Image::make($data['image']->getRealPath())->save(self::$path . $fileName);
                $opinion->image = $fileName;
            }


            $opinion->save();

        } catch (\Exception $ex) {
            \Log::error($ex);
        }

        return $opinion->id;

    }

}
