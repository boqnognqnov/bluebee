<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\ImageManagerStatic as Image;
use File;
use Illuminate\Support\Facades\Config;

class CommentNewsEntity extends Model
{

    protected
        $table = 'comment_news';

    public
    static $path = 'uploads' . '/' . 'images' . '/' . 'comments' . '/' . 'news' . '/';


    public
    static function postRules()
    {
        return array(
//            'g-recaptcha-response' => 'required|captcha',
            'name' => 'required',
            'message' => 'required',
            'email' => 'required',
        );
    }

    public static function postMessages()
    {
        return [
            'name.required' => 'The name is required.',
            'message.required' => 'The message is required.',
            'email.required' => 'The email is required.',
        ];
    }

    public static function getNewsTitle($newsID)
    {
        $news = NewsItemsEntity::find($newsID)->toArray();
        $newsTitle = $news['title_' . App::getLocale()];
        return $newsTitle;
    }

   

    public static function postComment($data)
    {
        $validator = \Validator::make($data, self::postRules(), self::postMessages());
//
        if ($validator->fails()) {
            return array('validationError', $validator);
        }

        try {
            $comment = new CommentNewsEntity();
            $comment->name = $data['name'];
            $comment->text = $data['message'];
            $comment->email = $data['email'];
            $comment->user_id = Auth::user()->id;

//            $comment->image = '';

//            $blueBeeDefaultAvatar = Config::get('GlobalArrays.defaultAvatar');

//            if (Auth::check() == true) {
//                if (!empty(Auth::user()->image_avatar)) {
//                    $comment->image = Auth::user()->image_avatar;
//                } else {
//                    $comment->image = $blueBeeDefaultAvatar;
//                }
//            } else {
//                $comment->image = $blueBeeDefaultAvatar;
//            }


            $comment->news_id = $data['news_id'];


            $comment->save();

//
        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('creatingError', $ex);
        }

        return array('successMessage', $comment);


    }

}
