<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class BenefitPageEntity extends Model
{

    protected $table = 'page_benefits';


//    public static $path = 'uploads' . '/' . 'images' . '/' . 'components' . '/' . 'headerComponents' . '/' . 'default' . '/';
//    public static $pathMedium = 'uploads' . '/' . 'images' . '/' . 'components' . '/' . 'headerComponents' . '/' . 'medium' . '/';
//    public static $pathSmall = 'uploads' . '/' . 'images' . '/' . 'components' . '/' . 'headerComponents' . '/' . 'small' . '/';


    public static function pageRules()
    {
        return array(
            'head_image' => 'image',

//            'title_en' => 'required',
            'text_en' => 'required',

//            'title_de' => 'required',
//            'text_de' => 'required',
//
//            'title_fr' => 'required',
//            'text_fr' => 'required',

        );
    }

    public static function pageMessages()
    {
        return [

            'head_image.image' => 'The intro background image is not valid format',
            'head_image.required' => 'The intro background image is not set',

            'title_en.required' => 'The title EN is not set.',
            'text_en.required' => 'The text EN is not set.',

//            'title_de.required' => 'The title DE is not set.',
//            'text_de.required' => 'The text DE is not set.',
//
//            'title_fr.required' => 'The title FR is not set.',
//            'text_fr.required' => 'The text FR is not set.',
        ];
    }


    public static function editPage($data)
    {
        $langArr = ['en', 'de', 'fr'];

        $rules = self::pageRules();

        $benefit = BenefitPageEntity::all()->first();

        $headIsOn = false;
        if (isset($data['head_is_on'])) {
            $headIsOn = true;

            $headValidStatus = HeaderComponentEntity::validateComponent($data['head_data'], $benefit->head_id);
            if ($headValidStatus != 'success') {
                return array('validationError', $headValidStatus);
            }
        }


        $benefit->head_is_on = $headIsOn;

        $validator = \Validator::make($data, $rules, self::pageMessages());

        if ($validator->fails()) {
            return array('validationError', $validator);
        }


        $try_is_on = false;
        if (isset($data['try_is_on'])) {
            $try_is_on = true;

            $tryValidStatus = TryComponentEntity::validateTry($data['try_data'], $benefit->try_id);
            if ($tryValidStatus != 'success') {
                return array('validationError', $tryValidStatus);
            }
        }


        $opinion_is_on = false;
        if (isset($data['opinion_is_on'])) {
            $opinion_is_on = true;

            $tryValidStatus = OpinionComponentEntity::validateOpinion($data['opinion_data'], $benefit->opinion_id);
            if ($tryValidStatus != 'success') {
                return array('validationError', $tryValidStatus);
            }
        }
        try {

            foreach ($langArr as $oneLang) {

                $title = 'title_' . $oneLang;
                $text = 'text_' . $oneLang;

                if (isset($data[$title])) {
                    $benefit->$title = $data[$title];
                }
                if (isset($data[$text])) {
                    $benefit->$text = $data[$text];
                }
            }


            $benefit->try_is_on = $try_is_on;
            $benefit->opinion_is_on = $opinion_is_on;

            $benefit->save();

            if (isset($data['try_data'])) {
                TryComponentEntity::editTryComponent($data['try_data'], $benefit->try_id);
            }
            if (isset($data['opinion_data'])) {
                OpinionComponentEntity::editOpinionComponent($data['opinion_data'], $benefit->opinion_id);
            }
            if (isset($data['head_data'])) {
                HeaderComponentEntity::editHeaderComponent($data['head_data'], $benefit->head_id);
            }

        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('creatingError', $ex);
        }

        return array('successMessage', $benefit);

    }
}
