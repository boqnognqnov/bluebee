<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class NewsCategoriesEntity extends Model
{

    protected $table = 'category_news';


    public static function addOrEditCategories($data)
    {
        $langArr = ['en', 'de', 'fr'];


        try {

            $category = NewsCategoriesEntity::findOrNew($data['id']);


            foreach ($langArr as $oneLang) {
                $title = 'title_' . $oneLang;
                if (isset($data[$title])) {
                    $category->$title = $data[$title];
                }
            }


            $category->save();


        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('creatingError', $ex);
        }

        return array('successMessage', $category);

    }

    public static function destroyCat($catId)
    {
        try {
            $listOfNews = NewsItemsEntity::where('category_id', '=', $catId)->get();
            foreach ($listOfNews as $oneNews) {
                if (!empty($oneNews->image)) {
                    File::delete(NewsItemsEntity::$path . $oneNews->image);
                }
                $oneNews->delete();
            }

            $category = NewsCategoriesEntity::find($catId);
            $category->delete();
        } catch (\Exception $ex) {
            \Log::error($ex);
        }

    }

}
