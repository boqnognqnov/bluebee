<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class ContactsChildPageEntity extends Model
{

    protected $table = 'page_contacts_inner';


    public static function editPage($data)
    {

        try {

            $page = ContactsChildPageEntity::all()->first();

            $headIsOn = false;
            if (isset($data['head_is_on'])) {
                $headIsOn = true;

                $headValidStatus = HeaderComponentEntity::validateComponent($data['head_data'], $page->head_id);
                if ($headValidStatus != 'success') {
                    return array('validationError', $headValidStatus);
                }
            }
            $page->head_is_on = $headIsOn;


            $try_is_on = false;
            if (isset($data['try_is_on'])) {
                $try_is_on = true;

                $tryValidStatus = TryComponentEntity::validateTry($data['try_data'], $page->try_id);
                if ($tryValidStatus != 'success') {
                    return array('validationError', $tryValidStatus);
                }
            }
            $page->try_is_on = $try_is_on;

            $contact_form_is_on = false;
            if (isset($data['contact_form_is_on'])) {
                $contact_form_is_on = true;

            }

            $page->contact_form_is_on = $contact_form_is_on;


            $page->save();

            if (isset($data['head_data'])) {
                HeaderComponentEntity::editHeaderComponent($data['head_data'], $page->head_id);
            }

            if (isset($data['try_data'])) {
                TryComponentEntity::editTryComponent($data['try_data'], $page->try_id);
            }


        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('creatingError', $ex);
        }

        return array('successMessage', $page);

    }

}
