<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class PipeLinesPageEntity extends Model
{

    protected $table = 'page_pipelines';

    public static $path = 'uploads' . '/' . 'images' . '/' . 'pages' . '/' . 'pipelinesPage' . '/' . 'innerPage' . '/' . 'default' . '/';
    public static $pathMedium = 'uploads' . '/' . 'images' . '/' . 'pages' . '/' . 'pipelinesPage' . '/' . 'innerPage' . '/' . 'medium' . '/';
    public static $pathSmall = 'uploads' . '/' . 'images' . '/' . 'pages' . '/' . 'pipelinesPage' . '/' . 'innerPage' . '/' . 'small' . '/';


    public static function editPageRules()
    {
        return array(
            'head_image' => 'image',

//            'pipe_text_en' => 'required',
//            'pipe_text_de' => 'required',
//            'pipe_text_fr' => 'required',
//
//            's_pipe_text_en' => 'required',
//            's_pipe_text_de' => 'required',
//            's_pipe_text_fr' => 'required',
        );
    }

    public static function editPageMessages()
    {
        return [

            'head_image.image' => 'Home page intro image is not valid format',
            'head_image.required' => 'Home page intro image is not set',

            'pipe_text_en.required' => 'The text EN in first part is not set.',
            'pipe_text_de.required' => 'The text DE in first part is not set.',
            'pipe_text_fr.required' => 'The text FR in first part is not set.',

            's_pipe_text_en.required' => 'The text EN in second part is not set.',
            's_pipe_text_de.required' => 'The text DE in second part is not set.',
            's_pipe_text_fr.required' => 'The text FR in second part is not set.',

        ];
    }


    public static function editPage($data)
    {

        $langArr = ['en', 'de', 'fr'];
        $rules = self::editPageRules();
        try {

            $page = PipeLinesPageEntity::all()->first();

            $headIsOn = false;
            if (isset($data['head_is_on'])) {
                $headIsOn = true;

                $headValidStatus = HeaderComponentEntity::validateComponent($data['head_data'], $page->head_id);
                if ($headValidStatus != 'success') {
                    return array('validationError', $headValidStatus);
                }
            }

            $validator = \Validator::make($data, $rules, self::editPageMessages());

            if ($validator->fails()) {
                return array('validationError', $validator);
            }
            $page->head_is_on = $headIsOn;


            $try_is_on = false;
            if (isset($data['try_is_on'])) {
                $try_is_on = true;

                $tryValidStatus = TryComponentEntity::validateTry($data['try_data'], $page->try_id);
                if ($tryValidStatus != 'success') {
                    return array('validationError', $tryValidStatus);
                }
            }
            $page->try_is_on = $try_is_on;


            foreach ($langArr as $oneLang) {

                $text = 'pipe_text_' . $oneLang;
                $secondText = 's_pipe_text_' . $oneLang;


                if (isset($data[$text])) {
                    $page->$text = $data[$text];
                }

                if (isset($data[$secondText])) {
                    $page->$secondText = $data[$secondText];
                }
            }

            if (isset($data['image'])) {

                if (!empty($page->image)) {
                    File::delete(self::$path . $page->image);
                    File::delete(self::$pathMedium . $page->image);
                    File::delete(self::$pathSmall . $page->image);
                }

//                $time = time();
//                $fileName = $time . '.' . $data['image']->getClientOriginalExtension();
//                Image::make($data['image']->getRealPath())->save(self::$path . $fileName);

                $time = time();
                $fileName = $time . '.' . $data['image']->getClientOriginalExtension();
                $image = Image::make($data['image']->getRealPath());

//                $image->save(self::$path . $fileName);

                $image->resize(2400, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(self::$path . $fileName);

                $image->resize(1200, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(self::$pathMedium . $fileName);

                $image->resize(600, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(self::$pathSmall . $fileName);

                $page->image = $fileName;
            }


            $page->save();

            if (isset($data['head_data'])) {
                HeaderComponentEntity::editHeaderComponent($data['head_data'], $page->head_id);
            }


            if (isset($data['try_data'])) {
                TryComponentEntity::editTryComponent($data['try_data'], $page->try_id);
            }

            if (isset($data['pipeCol'])) {
                foreach ($data['pipeCol'] as $oneCol) {
                    $validStatus = PipeLinesColumnsEntity::validateColumns($oneCol);
                    if ($validStatus != 'success') {
                        return array('validationError', $validStatus);
                    }
                }
                PipeLinesColumnsEntity::updateAllColumns($data['pipeCol']);
            }


        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('creatingError', $ex);
        }

        return array('successMessage', $page);

    }

    public static function deleteImage()
    {
        $page = PipeLinesPageEntity::all()->first();
        File::delete(self::$path . $page->image);
        File::delete(self::$pathMedium . $page->image);
        File::delete(self::$pathSmall . $page->image);
        $page->image = '';
        $page->save();
    }

}
