<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Hash;
use Intervention\Image\ImageManagerStatic as Image;
use File;
use Illuminate\Support\Facades\Config;

class User extends Authenticatable
{

    public static $avatar_path = 'uploads' . '/' . 'images' . '/' . 'avatars' . '/';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public static function userRules()
    {
        return array(

            'name' => 'required',
            'phone' => 'required',
            'email' => 'required|email|unique:users',

        );
    }

    public static function userMessages()
    {
        return [
            'password.required' => 'The password is not set.',
            'name.required' => 'The User name is not set.',

            'phone.required' => 'The phone is not set.',
            'email.required' => 'The email is not set.',
        ];
    }


    public static function addUser($request, $data)
    {
        $rules = self::userRules();
        $rules['password'] = 'required';


        $validator = \Validator::make($data, $rules, self::userMessages());
        if (!isset($data['is_ajax'])) {
            if ($validator->fails()) {
                return array('validationError', $validator);
            }
        }

        try {

            $request->merge(['password' => Hash::make($request->password)]);
            $user = User::create($request->all());

            $user->remember_token = csrf_token();

            $user->is_admin = false;
            $user->is_approve = false;
            if (isset($data['is_admin'])) {
                $user->is_admin = true;
                $user->is_approve = true;
            }
            if (isset($data['is_approve'])) {
                $user->is_approve = true;
            }
            if (isset($data['phone'])) {
                $user->phone = $data['phone'];
            }

            if (isset($data['is_ajax'])) {
                $user->account_type = 'default';
            }


            $user->save();
        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('creatingError', $ex);
        }

        return array('successMessage', $user);
    }

    public static function updateProfile()
    {
        return array(
            'name' => 'required',
//            'email' => 'required|email|unique:users',
            'email' => 'required|email',
        );
    }

    public static function updateUserAdmin($request)
    {


        $data = $request->all();

        $user = User::find($data['id']);
        $rules = self::updateProfile();

        if ($data['old_password'] != '') {
            $defPass = Config::get('GlobalArrays.social_def_pass');
            if ($user->password != Hash::make($defPass)) {
                if (!password_verify($data['old_password'], $user->password)) {
                    return array('validationError', 'The old password is wrong');
                }
            }

        }
        if ($data['new_password'] != '' || $data['re_new_password'] != '') {
            $rules['old_password'] = 'required';
            $rules['new_password'] = 'required';
            $rules['re_new_password'] = 'required|same:new_password';

        }

        if ($user->email != $data['email']) {
            $rules['email'] = 'required|email|unique:users';
        }

        $validator = \Validator::make($data, $rules);
        if ($validator->fails()) {
            return array('validationError', $validator);
        }

//        try {


        if (empty($user->remember_token)) {
            $user->remember_token = csrf_token();
        }


        if (isset($data['name'])) {
            $user->name = $data['name'];
        }

        if (isset($data['email'])) {
            $user->email = $data['email'];
        }
        if (isset($data['phone'])) {
            $user->phone = $data['phone'];
        }

        if (isset($data['linkedin_url'])) {
            $user->linkedin_url = $data['linkedin_url'];
        }
        if (isset($data['facebook_url'])) {
            $user->facebook_url = $data['facebook_url'];
        }
        if (isset($data['twitter_url'])) {
            $user->twitter_url = $data['twitter_url'];
        }

        if (isset($data['job_position'])) {
            $user->job_position = $data['job_position'];
        }
        if (isset($data['company_name'])) {
            $user->company_name = $data['company_name'];
        }
        if (isset($data['country'])) {
            $user->country = $data['country'];
        }

        $user->is_admin = false;
        $user->is_approve = false;
        if (isset($data['is_admin'])) {
            $user->is_admin = true;
            $user->is_approve = true;
        }
        if (isset($data['is_approve'])) {
            $user->is_approve = true;
        }
        if (!isset($data['old_password'])) {
            $data['old_password'] = '';
        }
        if (!isset($data['new_password'])) {
            $data['new_password'] = '';
        }

        if ($data['new_password'] != '') {
            $user->password = Hash::make($data['new_password']);
        }

        if (isset($data['image_avatar'])) {
            if (!empty($user->image_avatar)) {
                File::delete(self::$avatar_path . $user->image_avatar);
            }


            $time = time();
            $fileName = $time . '.' . $data['image_avatar']->getClientOriginalExtension();
            $image = Image::make($data['image_avatar']->getRealPath());

            $image->save(self::$avatar_path . $fileName);


            $user->image_avatar = $fileName;
        }
        $user->save();

//        } catch (\Exception $ex) {
//            \Log::error($ex);
//            return array('creatingError', $ex);
//        }

        return array('successMessage', $user);
    }


    public static function updateUserClient($request)
    {

        $data = $request->all();

        $user = User::find($data['id']);
        $rules = self::updateProfile();

        if ($data['old_password'] != '') {
            $defPass = Config::get('GlobalArrays.social_def_pass');
            if ($user->password != Hash::make($defPass)) {
                if (!password_verify($data['old_password'], $user->password)) {
                    return array('validationError', 'The old password is wrong');
                }
            }

        }
        if ($data['new_password'] != '' || $data['re_new_password'] != '') {
            $rules['old_password'] = 'required';
            $rules['new_password'] = 'required';
            $rules['re_new_password'] = 'required|same:new_password';

//            }

            if ($user->email != $data['email']) {
                $rules['email'] = 'required|email|unique:users';
            }

            $validator = \Validator::make($data, $rules);
            if ($validator->fails()) {
                return array('validationError', $validator);
            }
        }

        try {


            if (empty($user->remember_token)) {
                $user->remember_token = csrf_token();
            }

            if (isset($data['name'])) {
                $user->name = $data['name'];
            }

            if (isset($data['email'])) {
                $user->email = $data['email'];
            }
            if (isset($data['phone'])) {
                $user->phone = $data['phone'];
            }

            if (isset($data['linkedin_url'])) {
                $user->linkedin_url = $data['linkedin_url'];
            }
            if (isset($data['facebook_url'])) {
                $user->facebook_url = $data['facebook_url'];
            }
            if (isset($data['twitter_url'])) {
                $user->twitter_url = $data['twitter_url'];
            }

            if (isset($data['job_position'])) {
                $user->job_position = $data['job_position'];
            }
            if (isset($data['company_name'])) {
                $user->company_name = $data['company_name'];
            }
            if (isset($data['country'])) {
                $user->country = $data['country'];
            }


            if (!isset($data['old_password'])) {
                $data['old_password'] = '';
            }
            if (!isset($data['new_password'])) {
                $data['new_password'] = '';
            }

            if ($data['new_password'] != '') {
                $user->password = Hash::make($data['new_password']);

                $user->account_type = 'default';
            }

            if (isset($data['image_avatar'])) {
                if (!empty($user->image_avatar)) {
                    File::delete(self::$avatar_path . $user->image_avatar);
                }


                $time = time();
                $fileName = $time . '.' . $data['image_avatar']->getClientOriginalExtension();
                $image = Image::make($data['image_avatar']->getRealPath());

                $image->save(self::$avatar_path . $fileName);


                $user->image_avatar = $fileName;
            }
            $user->save();

        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('creatingError', $ex);
        }

        return array('successMessage', $user);
    }


    public
    static function destroyUser($userId)
    {
        try {
            $historyTable = UserHistoryEntity::where('user_id', '=', $userId)->get()->first();
            if ($historyTable) {
                $historyTable->delete();
            }


            $user = User::find($userId);
            if (!empty($user->image_avatar)) {
                File::delete(self::$avatar_path . $user->image_avatar);
            }
            $user->delete();

        } catch (\Exception $ex) {
            \Log::error($ex);
            return false;
        }
        return true;

    }


}
