<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class DownloadsItemsEntity extends Model
{

    protected $table = 'downloads';

    public static $pathFrontImage = 'uploads' . '/' . 'images' . '/' . 'pages' . '/' . 'downloads' . '/' . 'list' . '/' . 'default' . '/';

    public static $pathFrontImageMedium = 'uploads' . '/' . 'images' . '/' . 'pages' . '/' . 'downloads' . '/' . 'list' . '/' . 'medium' . '/';
    public static $pathFrontImageSmall = 'uploads' . '/' . 'images' . '/' . 'pages' . '/' . 'downloads' . '/' . 'list' . '/' . 'small' . '/';


    public static $pathFile = 'uploads' . '/' . 'files' . '/';

    public static function itemRules()
    {
        return array(
            'image' => 'image',

            'title_en' => 'required',
            'text_en' => 'required',
//            'title_de' => 'required',
//            'text_de' => 'required',
//            'title_fr' => 'required',
//            'text_fr' => 'required',

        );
    }

    public static function itemMessages()
    {
        return [
            'image.image' => 'Download image is not valid format',
            'image.required' => 'Download image is not set',

            'file.required' => 'Download file is not set',

            'title_en.required' => 'Download title EN is required',
            'text_en.required' => 'Download text EN is required',
            'title_de.required' => 'Download title DE is required',
            'text_de.required' => 'Download text DE is required',
            'title_fr.required' => 'Download title FR is required',
            'text_fr.required' => 'Download text FR is required',
        ];
    }


    public static function getCategory($catId)
    {
        $categoryName = DownloadsCategoriesEntity::find($catId)->title_en;
        return $categoryName;
    }

    public static function getFileObj($fileId)
    {
        $file = DownloadsItemsEntity::find($fileId)->toArray();
        return $file;
    }

    public static function addOrEditRecord($data, $request)
    {
        $langArr = ['en', 'de', 'fr'];


        try {

            $record = DownloadsItemsEntity::findOrNew($data['id']);

            $rules = self::itemRules();
            if (empty($record->image)) {
                $rules['image'] = 'required|image';
            }

            if (empty($record->file)) {
                $rules['file'] = 'required';
            }
            $validator = \Validator::make($data, $rules, self::itemMessages());

            if ($validator->fails()) {
                return array('validationError', $validator);
            }


            foreach ($langArr as $oneLang) {

                $title = 'title_' . $oneLang;
                $text = 'text_' . $oneLang;


                if (isset($data[$title])) {
                    $record->$title = $data[$title];
                }

                if (isset($data[$text])) {
                    $record->$text = $data[$text];
                }
            }

            $record->category_id = $data['category_id'];

            if (isset($data['image'])) {
                if (!empty($record->image)) {
                    File::delete(self::$pathFrontImage . $record->image);
                    File::delete(self::$pathFrontImageMedium . $record->image);
                    File::delete(self::$pathFrontImageSmall . $record->image);
                }
//                $time = time();
//                $fileName = $time . '.' . $data['image']->getClientOriginalExtension();
//                Image::make($data['image']->getRealPath())->save(self::$pathFrontImage . $fileName);

                $time = time();
                $fileName = $time . '.' . $data['image']->getClientOriginalExtension();
                $image = Image::make($data['image']->getRealPath());

//                $image->save(self::$path . $fileName);

                $image->resize(2400, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(self::$pathFrontImage . $fileName);

                $image->resize(1200, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(self::$pathFrontImageMedium . $fileName);

                $image->resize(600, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(self::$pathFrontImageSmall . $fileName);


                $record->image = $fileName;
            }

            if (isset($data['file'])) {

                if (!empty($record->file)) {
                    File::delete(self::$pathFile . $record->file);
                }


                $file = self::renameFile($data['file']);


                $request->file('file')->move(self::$pathFile, $file);

                $record->file = $file;
            }

            $record->save();


        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('creatingError', $ex);
        }

        return array('successMessage', $record);

    }

    private static function renameFile($file)
    {
        $name = $file->getClientOriginalName();
        $name = str_replace(' ', '_', $name);

        $time = time();
//        $file = $name . '_' . $time . '.' . $file->getClientOriginalExtension();
        $file = $time . '_' . $name;
        return $file;
    }

    public static function destroyRecord($recordId)
    {
        try {
            $record = DownloadsItemsEntity::find($recordId);
            if (!empty($record->image)) {
                File::delete(self::$pathFrontImage . $record->image);
                File::delete(self::$pathFrontImageMedium . $record->image);
                File::delete(self::$pathFrontImageSmall . $record->image);
            }
            if (!empty($record->file)) {
                File::delete(self::$pathFile . $record->file);
            }
            $record->delete();

        } catch (\Exception $ex) {
            \Log::error($ex);
            return false;
        }
        return true;
    }

}
