<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class ContactsMainPageEntity extends Model
{

    protected $table = 'page_contact';


    public static function editPage($data)
    {


        try {


            $page = ContactsMainPageEntity::all()->first();

            $headIsOn = false;
            if (isset($data['head_is_on'])) {
                $headIsOn = true;

                $headValidStatus = HeaderComponentEntity::validateComponent($data['head_data'], $page->head_id);
                if ($headValidStatus != 'success') {
                    return array('validationError', $headValidStatus);
                }
            }
            $page->head_is_on = $headIsOn;


            $try_is_on = false;
            if (isset($data['try_is_on'])) {
                $try_is_on = true;

                $tryValidStatus = TryComponentEntity::validateTry($data['try_data'], $page->try_id);
                if ($tryValidStatus != 'success') {
                    return array('validationError', $tryValidStatus);
                }
            }
            $page->try_is_on = $try_is_on;


            $page->save();

            if (isset($data['head_data'])) {
                HeaderComponentEntity::editHeaderComponent($data['head_data'], $page->head_id);
            }

            if (isset($data['try_data'])) {
                TryComponentEntity::editTryComponent($data['try_data'], $page->try_id);
            }


        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('creatingError', $ex);
        }

        return array('successMessage', $page);

    }


    public static function updateDemoMails($demoMailsList)
    {
        foreach ($demoMailsList as $key => $oneEmail) {
            if (!filter_var($oneEmail, FILTER_VALIDATE_EMAIL)) {
                return array('emailIsNotValid', 'The email from Demo request on slot ' . $key . ' is not valid');
            }
        }
        try {
            $page = ContactsMainPageEntity::all()->first();
            $page->demo_requests_emails1 = $demoMailsList[1];
            $page->demo_requests_emails2 = $demoMailsList[2];
            $page->demo_requests_emails3 = $demoMailsList[3];
            $page->save();
        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('creatingError', $ex);
        }
        return array('successMessage', $page);

    }

    public static function updateDataCardsMails($datacardsMailsList)
    {

        foreach ($datacardsMailsList as $key => $oneEmail) {
            if (!filter_var($oneEmail, FILTER_VALIDATE_EMAIL)) {
                return array('emailIsNotValid', 'The email from Datacard request on slot ' . $key . ' is not valid');
            }
        }


        try {
            $page = ContactsMainPageEntity::all()->first();
            $page->pipelines_requests_emails1 = $datacardsMailsList[1];
            $page->pipelines_requests_emails2 = $datacardsMailsList[2];
            $page->pipelines_requests_emails3 = $datacardsMailsList[3];
            $page->save();
        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('creatingError', $ex);
        }
        return array('successMessage', $page);

    }


}
