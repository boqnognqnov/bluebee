<?php

namespace App;

use App\Classes\GlobalFunctions;
use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class EventItemsEntity extends Model
{

    protected $table = 'events';

    public static $path = 'uploads' . '/' . 'images' . '/' . 'pages' . '/' . 'events' . '/' . 'list' . '/' . 'default' . '/';
    public static $pathMedium = 'uploads' . '/' . 'images' . '/' . 'pages' . '/' . 'events' . '/' . 'list' . '/' . 'medium' . '/';
    public static $pathSmall = 'uploads' . '/' . 'images' . '/' . 'pages' . '/' . 'events' . '/' . 'list' . '/' . 'small' . '/';


    public static function itemRules()
    {
        return array(
            'image' => 'image',

            'title_en' => 'required',
            'text_en' => 'required',
//            'title_de' => 'required',
//            'text_de' => 'required',
//            'title_fr' => 'required',
//            'text_fr' => 'required',
        );
    }

    public static function itemMessages()
    {
        return [
            'image.image' => 'Event image is not valid format',
            'image.required' => 'Event image is not set',


            'title_en.required' => 'Event title EN is not set',
            'text_en.required' => 'Event text EN is not set',
            'title_de.required' => 'Event title DE is not set',
            'text_de.required' => 'Event text DE is not set',
            'title_fr.required' => 'Event title FR is not set',
            'text_fr.required' => 'Event text FR is not set',
        ];
    }


    public static function getCategory($catId)
    {
        $categoryName = EventCategoriesEntity::find($catId)->title_en;
        return $categoryName;
    }


    public static function addOrEditEvent($data)
    {
        $langArr = ['en', 'de', 'fr'];

        $rules = self::itemRules();
        try {

            $event = EventItemsEntity::findOrNew($data['id']);
            if (empty($event->image)) {
//                $rules['image'] = 'required|image';
            }

            $validator = \Validator::make($data, $rules, self::itemMessages());

            if ($validator->fails()) {
                return array('validationError', $validator);
            }
//            $event['created_at'] = GlobalFunctions::generateDateTimeToStr($event['created_at']);
//            $event['event_date'] = GlobalFunctions::generateDateTimeToStr($event['event_date']);

            $event->created_at = GlobalFunctions::changeDateFormat($data['created_at']);

            $event->event_date = GlobalFunctions::changeDateFormat($data['event_date']);

            if ($data['created_at'] == '00/00/0000') {
                $event->created_at = new \DateTime();
            }

            if ($data['event_date'] == '00/00/0000') {
                $event->event_date = new \DateTime();
            }


            foreach ($langArr as $oneLang) {

                $title = 'title_' . $oneLang;
                $text = 'text_' . $oneLang;
                $slug = 'slug_' . $oneLang;


                if (isset($data[$title])) {
                    $event->$title = $data[$title];
                    $event->$slug = str_slug($data[$title]);
                }

                if (isset($data[$text])) {
                    $event->$text = $data[$text];
                }
            }
            $event->category_id = $data['category_id'];


            if (isset($data['image'])) {

                if (!empty($event->image)) {
                    File::delete(self::$path . $event->image);
                    File::delete(self::$pathMedium . $event->image);
                    File::delete(self::$pathSmall . $event->image);
                }

//                $time = time();
//                $fileName = $time . '.' . $data['image']->getClientOriginalExtension();
//                Image::make($data['image']->getRealPath())->save(self::$path . $fileName);

                $time = time();
                $fileName = $time . '.' . $data['image']->getClientOriginalExtension();
                $image = Image::make($data['image']->getRealPath());

//                $image->save(self::$path . $fileName);

                $image->resize(2400, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(self::$path . $fileName);

                $image->resize(1200, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(self::$pathMedium . $fileName);

                $image->resize(600, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(self::$pathSmall . $fileName);

                $event->image = $fileName;
            }

            $event->save();


        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('creatingError', $ex);
        }

        return array('successMessage', $event);

    }

    public static function destroyEvent($eventId)
    {
        try {

            $eventsToTagList = EventsToTagEntity::where('event_id', '=', $eventId)->get();
            foreach ($eventsToTagList as $oneRec) {
                $oneRec->delete();
            }
            $event = EventItemsEntity::find($eventId);
            if (!empty($event->image)) {
                File::delete(self::$path . $event->image);
                File::delete(self::$pathMedium . $event->image);
                File::delete(self::$pathSmall . $event->image);
            }
            $event->delete();

        } catch (\Exception $ex) {
            \Log::error($ex);
            return false;
        }
        return true;
    }

    public static function deleteImage($eventId)
    {
        $event = EventItemsEntity::find($eventId);

        File::delete(self::$path . $event->image);
        File::delete(self::$pathMedium . $event->image);
        File::delete(self::$pathSmall . $event->image);

        $event->image = '';
        $event->save();


    }

}
