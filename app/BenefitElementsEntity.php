<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class BenefitElementsEntity extends Model
{

    protected $table = 'benefits_in_elements';
    public static $path = 'uploads' . '/' . 'images' . '/' . 'pages' . '/' . 'benefitItemsImages' . '/';


    public static function itemRules()
    {
        return array(
            'image' => 'image',

//            'title_en' => 'required',
            'text_en' => 'required',

//            'title_de' => 'required',
//            'text_de' => 'required',
//
//            'title_fr' => 'required',
//            'text_fr' => 'required',

        );
    }

    public static function itemMessages()
    {
        return [

            'image.image' => 'The front image is not valid format',
//            'image.required' => 'The front image is not set',

            'title_en.required' => 'The title EN is not set.',
            'text_en.required' => 'The text EN is not set.',

            'title_de.required' => 'The title DE is not set.',
            'text_de.required' => 'The text DE is not set.',

            'title_fr.required' => 'The title FR is not set.',
            'text_fr.required' => 'The text FR is not set.',
        ];
    }

    public static function updateOrCreateNewItem($data)
    {

        $validator = \Validator::make($data, self::itemRules(), self::itemMessages());

        if ($validator->fails()) {
            return array('validationError', $validator);
        }

        $langArr = ['en', 'de', 'fr'];


        try {

            $item = BenefitElementsEntity::findOrNew($data['itemId']);

            foreach ($langArr as $oneLang) {

                $title = 'title_' . $oneLang;
                $text = 'text_' . $oneLang;


                if (isset($data[$title])) {
                    $item->$title = $data[$title];
                }

                if (isset($data[$text])) {
                    $item->$text = $data[$text];
                }

            }
            $item->inner_benefit_id = $data['innerPageId'];

            if (isset($data['image'])) {

                if (!empty($item->image)) {
                    File::delete(self::$path . $item->image);
                }

                $time = time();
                $fileName = $time . '.' . $data['image']->getClientOriginalExtension();
                Image::make($data['image']->getRealPath())->save(self::$path . $fileName);
                $item->image = $fileName;
            }


            $item->save();


        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('creatingError', $ex);
        }

        return array('successMessage', $item);

    }

    public static function destroyItem($itemId)
    {
        try {
            $item = BenefitElementsEntity::find($itemId);
            if (!empty($item->image)) {
                File::delete(self::$path . $item->image);
            }

            $item->delete();

        } catch (\Exception $ex) {
            \Log::error($ex);
            return 'error';
        }

        return 'success';

    }
}
