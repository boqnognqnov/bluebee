<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class OfficesItemsEntity extends Model
{

    protected $table = 'offices';

    public static $path = 'uploads' . '/' . 'images' . '/' . 'pages' . '/' . 'offices' . '/' . 'list' . '/' . 'default' . '/';
    public static $pathMedium = 'uploads' . '/' . 'images' . '/' . 'pages' . '/' . 'offices' . '/' . 'list' . '/' . 'medium' . '/';
    public static $pathSmall = 'uploads' . '/' . 'images' . '/' . 'pages' . '/' . 'offices' . '/' . 'list' . '/' . 'small' . '/';


    public static function addEditOfficeRules()
    {
        return array(
            'image' => 'image',

            'title_en' => 'required',
            'text_en' => 'required',

//            'title_de' => 'required',
//            'text_de' => 'required',
//
//            'title_fr' => 'required',
//            'text_fr' => 'required',

            'address_en' => 'required',
//            'address_de' => 'required',
//            'address_fr' => 'required',

            'phone' => 'required',
            'email' => 'required',
            'coordinates' => 'required',
        );
    }

    public static function addEditOfficeMessages()
    {
        return [
            'image.image' => 'image',

            'title_en.required' => 'Office title EN is not set.',
            'text_en.required' => 'Office text EN is not set.',

            'title_de.required' => 'Office title DE is not set.',
            'text_de.required' => 'Office text DE is not set.',

            'title_fr.required' => 'Office title FR is not set.',
            'text_fr.required' => 'Office text FR is not set.',

            'address_en.required' => 'Office address En is not set.',
            'address_de.required' => 'Office address En is not set.',
            'address_fr.required' => 'Office address En is not set.',

            'phone.required' => 'The phone is not set.',
            'email.required' => 'The email is not set.',
            'coordinates.required' => 'The map coordinates is not set.',

        ];
    }


    public static function addOrEdit($data)
    {

        $validator = \Validator::make($data, self::addEditOfficeRules(), self::addEditOfficeMessages());

        if ($validator->fails()) {
            return array('validationError', $validator);
        }

        $langArr = ['en', 'de', 'fr'];


        try {

            $office = OfficesItemsEntity::findOrNew($data['id']);


            foreach ($langArr as $oneLang) {

                $title = 'title_' . $oneLang;
                $text = 'text_' . $oneLang;

                $address = 'address_' . $oneLang;


                if (isset($data[$title])) {
                    $office->$title = $data[$title];
                }

                if (isset($data[$text])) {
                    $office->$text = $data[$text];
                }

                if (isset($data[$address])) {
                    $office->$address = $data[$address];
                }
            }

            $office->phone = $data['phone'];
            $office->email = $data['email'];
            $office->coordinates = $data['coordinates'];


            if (isset($data['image'])) {

                if (!empty($office->image)) {

                    File::delete(self::$path . $office->image);
                    File::delete(self::$pathMedium . $office->image);
                    File::delete(self::$pathSmall . $office->image);
                }

//                $time = time();
//                $fileName = $time . '.' . $data['image']->getClientOriginalExtension();
//                Image::make($data['image']->getRealPath())->save(self::$path . $fileName);

                $time = time();
                $fileName = $time . '.' . $data['image']->getClientOriginalExtension();
                $image = Image::make($data['image']->getRealPath());

//                $image->save(self::$path . $fileName);

                $image->resize(2400, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(self::$path . $fileName);

                $image->resize(1200, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(self::$pathMedium . $fileName);

                $image->resize(600, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(self::$pathSmall . $fileName);

                $office->image = $fileName;
            }

            $office->save();


        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('creatingError', $ex);
        }

        return array('successMessage', $office);

    }

    public static function destroyOffice($officeId)
    {
        try {
            $office = OfficesItemsEntity::find($officeId);
            if (!empty($office->image)) {
                File::delete(self::$path . $office->image);
            }
            $office->delete();

        } catch (\Exception $ex) {
            \Log::error($ex);
            return false;
        }
        return true;
    }

}
