<?php

namespace App;

use App\Classes\GlobalFunctions;
use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class NewsItemsEntity extends Model
{

    protected $table = 'news';

//    public static $path = 'uploads' . '/' . 'images' . '/' . 'pages' . '/' . 'news' . '/' . 'list' . '/';

    public static $path = 'uploads' . '/' . 'images' . '/' . 'pages' . '/' . 'news' . '/' . 'list' . '/' . 'default' . '/';

    public static $pathMedium = 'uploads' . '/' . 'images' . '/' . 'pages' . '/' . 'news' . '/' . 'list' . '/' . 'medium' . '/';
    public static $pathSmall = 'uploads' . '/' . 'images' . '/' . 'pages' . '/' . 'news' . '/' . 'list' . '/' . 'small' . '/';


    public static function newsRules()
    {
        return array(
            'image' => 'image',

            'title_en' => 'required',
            'text_en' => 'required',
//            'title_de' => 'required',
//            'text_de' => 'required',
//            'title_fr' => 'required',
//            'text_fr' => 'required',

        );
    }

    public static function newsMessages()
    {
        return [
            'image.image' => 'Intro image is not valid format',
            'image.required' => 'Intro image is not set',

            'title_en.required' => 'News title EN is required',
            'text_en.required' => 'News text EN is required',
            'title_de.required' => 'News title DE is required',
            'text_de.required' => 'News text DE is required',
            'title_fr.required' => 'News title FR is required',
            'text_fr.required' => 'News text FR is required',
        ];
    }


    public static function getCategory($catId)
    {
        $categoryName = NewsCategoriesEntity::find($catId)->title_en;
        return $categoryName;
    }


    public static function addOrEditNews($data)
    {
        $langArr = ['en', 'de', 'fr'];

        $news = NewsItemsEntity::findOrNew($data['id']);

        $rules = self::newsRules();

        if ($data['id'] == 0) {
//            $rules['image'] = 'required|image';
        }

        if (empty($news->image)) {
//            $rules['image'] = 'required|image';
        }
        $validator = \Validator::make($data, $rules, self::newsMessages());

        if ($validator->fails()) {
            return array('validationError', $validator);
        }

        $news->created_at = GlobalFunctions::changeDateFormat($data['created_at']);

        if ($data['created_at'] == '00/00/0000') {
            $news->created_at = new \DateTime();
        }


        try {


            foreach ($langArr as $oneLang) {

                $title = 'title_' . $oneLang;
                $text = 'text_' . $oneLang;
                $slug = 'slug_' . $oneLang;



                if (isset($data[$title])) {
                    $news->$title = $data[$title];
                    $news->$slug = str_slug($data[$title]);

                }

                if (isset($data[$text])) {
                    $news->$text = $data[$text];
                }
            }
            $news->category_id = $data['category_id'];


            if (isset($data['image'])) {

                if (!empty($news->image)) {
                    File::delete(self::$path . $news->image);
                    File::delete(self::$pathMedium . $news->image);
                    File::delete(self::$pathSmall . $news->image);
                }

//                $time = time();
//                $fileName = $time . '.' . $data['image']->getClientOriginalExtension();
//                Image::make($data['image']->getRealPath())->save(self::$path . $fileName);

                $time = time();
                $fileName = $time . '.' . $data['image']->getClientOriginalExtension();
                $image = Image::make($data['image']->getRealPath());

//                $image->save(self::$path . $fileName);

                $image->resize(2400, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(self::$path . $fileName);

                $image->resize(1200, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(self::$pathMedium . $fileName);

                $image->resize(600, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(self::$pathSmall . $fileName);

                $news->image = $fileName;
            }

            $news->save();


        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('creatingError', $ex);
        }

        return array('successMessage', $news);

    }

    public static function destroyNews($newsId)
    {
        try {
            $newsToTagList = NewsToTagEntity::where('news_id', '=', $newsId)->get();
            foreach ($newsToTagList as $oneRec) {
                $oneRec->delete();
            }
            $news = NewsItemsEntity::find($newsId);
            if (!empty($news->image)) {
                File::delete(self::$path . $news->image);
                File::delete(self::$pathMedium . $news->image);
                File::delete(self::$pathSmall . $news->image);
            }
            $news->delete();

        } catch (\Exception $ex) {
            \Log::error($ex);
            return false;
        }
        return true;
    }

    public static function deleteImage($newsId)
    {
        $news = NewsItemsEntity::find($newsId);

        File::delete(self::$path . $news->image);
        File::delete(self::$pathMedium . $news->image);
        File::delete(self::$pathSmall . $news->image);

        $news->image = '';
        $news->save();


    }

}
