<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class TryComponentEntity extends Model
{

    protected $table = 'component_try';

    public static $path = 'uploads' . '/' . 'images' . '/' . 'components' . '/' . 'tryComponents' . '/' . 'default' . '/';
    public static $pathMedium = 'uploads' . '/' . 'images' . '/' . 'components' . '/' . 'tryComponents' . '/' . 'medium' . '/';
    public static $pathSmall = 'uploads' . '/' . 'images' . '/' . 'components' . '/' . 'tryComponents' . '/' . 'small' . '/';

    public static function editTryRules()
    {
        return array(
            'title_en' => 'required',
            'text_en' => 'required',
            'button_en' => 'required',

//            'title_de' => 'required',
//            'text_de' => 'required',
//            'button_de' => 'required',
//
//            'title_fr' => 'required',
//            'text_fr' => 'required',
//            'button_fr' => 'required',

            'url' => 'required',
            'image' => 'image',


        );
    }

    public static function editTryMessages()
    {
        return [
            'title_en.required' => 'Trial CTA component title EN is required',
            'text_en.required' => 'Trial CTA component text EN is required',
            'button_en.required' => 'Trial CTA component button name EN is required',

            'title_de.required' => 'Trial CTA component title DE is required',
            'text_de.required' => 'Trial CTA component text DE is required',
            'button_de.required' => 'Trial CTA component button name DE is required',

            'title_fr.required' => 'Trial CTA component title FR is required',
            'text_fr.required' => 'Trial CTA component text FR is required',
            'button_fr.required' => 'Trial CTA component button name FR is required',

            'url.required' => 'Trial CTA component button url is required',
            'image.required' => 'Trial CTA component the image is required',
            'image.image' => 'Trial CTA component the file must be an image',

        ];
    }

    public static function validateTry($data, $tryId)
    {
        $component = TryComponentEntity::find($tryId);
        $rules = self::editTryRules();
        if (empty($component->image)) {
            $rules['image'] = 'required|image';
        }
        $validator = \Validator::make($data, $rules, self::editTryMessages());

        if ($validator->fails()) {
            return $validator;
        }

        return 'success';
    }


    public static function getTryObj($try_id)
    {
        return TryComponentEntity::find($try_id)->toArray();
    }

    public static function editTryComponent($data, $tryId)
    {
        $langArr = ['en', 'de', 'fr'];

        try {

            $try = TryComponentEntity::find($tryId);


            foreach ($langArr as $oneLang) {
                $title = 'title_' . $oneLang;
                $text = 'text_' . $oneLang;
                $button = 'button_' . $oneLang;
                if (isset($data[$title])) {
                    $try->$title = $data[$title];
                }
                if (isset($data[$text])) {
                    $try->$text = $data[$text];
                }
                if (isset($data[$button])) {
                    $try->$button = $data[$button];
                }
            }

            if (isset($data['url'])) {
                $try->url = $data['url'];
            }


            if (isset($data['image'])) {

                if (!empty($try->image)) {
                    File::delete(self::$path . $try->image);
                    File::delete(self::$pathMedium . $try->image);
                    File::delete(self::$pathSmall . $try->image);
                }

//                $time = time();
//                $fileName = $time . '.' . $data['image']->getClientOriginalExtension();
//                Image::make($data['image']->getRealPath())->save(self::$path . $fileName);


                $time = time();
                $fileName = $time . '.' . $data['image']->getClientOriginalExtension();
                $image = Image::make($data['image']->getRealPath());

//                $image->save(self::$path . $fileName);

                $image->resize(2400, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(self::$path . $fileName);

                $image->resize(1200, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(self::$pathMedium . $fileName);

                $image->resize(600, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(self::$pathSmall . $fileName);

                $try->image = $fileName;
            }


            $try->save();

        } catch (\Exception $ex) {
            \Log::error($ex);
        }

        return $try->id;

    }


}
