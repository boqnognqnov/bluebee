<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class DemoPageEntity extends Model
{

    protected $table = 'page_demo';


    public static function editPage($data)
    {
        $page = DemoPageEntity::all()->first();

        $headIsOn = false;
        if (isset($data['head_is_on'])) {
            $headIsOn = true;

            $headValidStatus = HeaderComponentEntity::validateComponent($data['head_data'], $page->head_id);
            if ($headValidStatus != 'success') {
                return array('validationError', $headValidStatus);
            }
        }


        try {


            $page->head_is_on = $headIsOn;


            $page->save();

            if (isset($data['head_data'])) {
                HeaderComponentEntity::editHeaderComponent($data['head_data'], $page->head_id);
            }

        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('creatingError', $ex);
        }

        return array('successMessage', $page);

    }
}
