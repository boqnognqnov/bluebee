<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class HeaderComponentEntity extends Model
{

    protected $table = 'component_header';

//    public static $path = 'uploads' . '/' . 'images' . '/' . 'components' . '/' . 'headerComponents' . '/';

    public static $path = 'uploads' . '/' . 'images' . '/' . 'components' . '/' . 'headerComponents' . '/' . 'default' . '/';
    public static $pathMedium = 'uploads' . '/' . 'images' . '/' . 'components' . '/' . 'headerComponents' . '/' . 'medium' . '/';
    public static $pathSmall = 'uploads' . '/' . 'images' . '/' . 'components' . '/' . 'headerComponents' . '/' . 'small' . '/';


    public static function editRules()
    {
        return array(
            'title_en' => 'required',
//            'text_en' => 'required',

//            'title_de' => 'required',
//            'text_de' => 'required',
//
//            'title_fr' => 'required',
//            'text_fr' => 'required',

            'image' => 'image',


        );
    }

    public static function editMessages()
    {
        return [
            'title_en.required' => 'Intro component title EN is required',
            'text_en.required' => 'Intro component text EN is required',

            'title_de.required' => 'Intro component title DE is required',
            'text_de.required' => 'Intro component text DE is required',

            'title_fr.required' => 'Intro component title FR is required',
            'text_fr.required' => 'Intro component text FR is required',

            'image.required' => 'Intro component the image is required',
            'image.image' => 'Intro component the file must be an image',

        ];
    }

    public static function validateComponent($data, $id)
    {
        $component = HeaderComponentEntity::find($id);
        $rules = self::editRules();
        if (empty($component->image)) {
            $rules['image'] = 'required|image';
        }
        $validator = \Validator::make($data, $rules, self::editMessages());

        if ($validator->fails()) {
            return $validator;
        }

        return 'success';
    }


    public static function getHeaderObj($header_id)
    {
        return HeaderComponentEntity::find($header_id)->toArray();
    }


    public static function editHeaderComponent($data, $headerId)
    {
        $langArr = ['en', 'de', 'fr'];

        try {

            $header = HeaderComponentEntity::find($headerId);

            foreach ($langArr as $oneLang) {

                $title = 'title_' . $oneLang;
                $text = 'text_' . $oneLang;


                if (isset($data[$title])) {
                    $header->$title = $data[$title];
                }

                if (isset($data[$text])) {
                    $header->$text = $data[$text];
                }

            }


            if (isset($data['image'])) {

                if (!empty($header->image)) {
                    File::delete(self::$path . $header->image);
                    File::delete(self::$pathMedium . $header->image);
                    File::delete(self::$pathSmall . $header->image);
                }

                $time = time();
                $fileName = $time . '.' . $data['image']->getClientOriginalExtension();
                $image = Image::make($data['image']->getRealPath());

//                $image->save(self::$path . $fileName);

                $image->resize(2400, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(self::$path . $fileName);

                $image->resize(1200, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(self::$pathMedium . $fileName);

                $image->resize(600, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(self::$pathSmall . $fileName);

                $header->image = $fileName;
            }


            $header->save();

        } catch (\Exception $ex) {
            \Log::error($ex);
        }

        return $header->id;

    }

}
