<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class DownloadsCategoriesEntity extends Model
{

    protected $table = 'category_downloads';


    public static function addOrEditCategories($data)
    {
        $langArr = ['en', 'de', 'fr'];


        try {

            $category = DownloadsCategoriesEntity::findOrNew($data['id']);


            foreach ($langArr as $oneLang) {
                $title = 'title_' . $oneLang;
                if (isset($data[$title])) {
                    $category->$title = $data[$title];
                }
            }


            $category->save();


        } catch (\Exception $ex) {
            \Log::error($ex);
        }

        return $category->id;

    }

    public static function destroyCat($catId)
    {
        try {
            $listOfFiles = DownloadsItemsEntity::where('category_id', '=', $catId)->get();
            foreach ($listOfFiles as $oneFile) {
                if (!empty($oneFile->image)) {
                    File::delete(DownloadsItemsEntity::$pathFrontImage . $oneFile->image);
                }
                $oneFile->delete();
            }

            $category = DownloadsCategoriesEntity::find($catId);
            $category->delete();
        } catch (\Exception $ex) {
            \Log::error($ex);
        }

    }

}
