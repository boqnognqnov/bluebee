<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class CasesMainPageEntity extends Model
{

    protected $table = 'page_cases';


    public static function editPage($data)
    {
        $langArr = ['en', 'de', 'fr'];

//        $head_is_on = false;
//        if (isset($data['head_is_on'])) {
//            $head_is_on = true;
//        }

//        $opinion_is_on = false;
//        if (isset($data['opinion_is_on'])) {
//            $opinion_is_on = true;
//        }


//        $try_is_on = false;
//        if (isset($data['try_is_on'])) {
//            $try_is_on = true;
//        }


        try {

            $page = CasesMainPageEntity::all()->first();

            $opinion_is_on = false;
            if (isset($data['opinion_is_on'])) {
                $opinion_is_on = true;

                $tryValidStatus = OpinionComponentEntity::validateOpinion($data['opinion_data'], $page->opinion_id);
                if ($tryValidStatus != 'success') {
                    return array('validationError', $tryValidStatus);
                }
            }

            $head_is_on = false;
            if (isset($data['head_is_on'])) {
                $head_is_on = true;

                $headValidStatus = HeaderComponentEntity::validateComponent($data['head_data'], $page->head_id);
                if ($headValidStatus != 'success') {
                    return array('validationError', $headValidStatus);
                }
            }

            $try_is_on = false;
            if (isset($data['try_is_on'])) {
                $try_is_on = true;

                $tryValidStatus = TryComponentEntity::validateTry($data['try_data'], $page->try_id);
                if ($tryValidStatus != 'success') {
                    return array('validationError', $tryValidStatus);
                }
            }


            foreach ($langArr as $oneLang) {

                $title = 'title_' . $oneLang;
                $text = 'text_' . $oneLang;

                if (isset($data[$title])) {
                    $page->$title = $data[$title];
                }
                if (isset($data[$text])) {
                    $page->$text = $data[$text];
                }
            }


            $page->head_is_on = $head_is_on;
            $page->try_is_on = $try_is_on;
            $page->opinion_is_on = $opinion_is_on;

            $page->save();

            if (isset($data['head_data'])) {
                HeaderComponentEntity::editHeaderComponent($data['head_data'], $page->head_id);
            }

            if (isset($data['try_data'])) {
                TryComponentEntity::editTryComponent($data['try_data'], $page->try_id);
            }
            if (isset($data['opinion_data'])) {
                OpinionComponentEntity::editOpinionComponent($data['opinion_data'], $page->opinion_id);
            }

        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('creatingError', $ex);
        }

        return array('successMessage', $page);

    }
}
