<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class JoinTheTeamPageEntity extends Model
{

    protected $table = 'page_join_team';

    public static $path = 'uploads' . '/' . 'images' . '/' . 'pages' . '/' . 'joinTheTeam' . '/' . 'default' . '/';
    public static $pathMedium = 'uploads' . '/' . 'images' . '/' . 'pages' . '/' . 'joinTheTeam' . '/' . 'medium' . '/';
    public static $pathSmall = 'uploads' . '/' . 'images' . '/' . 'pages' . '/' . 'joinTheTeam' . '/' . 'small' . '/';


    public static function editPageRules()
    {
        return array(
            'image' => 'image',

//            'title_en' => 'required',
            'text_en' => 'required',

        );
    }

    public static function editPageMessages()
    {
        return [

            'image.image' => 'Home page intro image is not valid format',
            'image.required' => 'Home page intro image is not set',

            'title_en.required' => 'Home page intro title EN is required',
            'text_en.required' => 'Home page intro text EN is required',

        ];
    }


    public static function editPage($data)
    {
        $langArr = ['en', 'de', 'fr'];

        try {

            $validator = \Validator::make($data, self::editPageRules(), self::editPageMessages());

            if ($validator->fails()) {
                return array('validationError', $validator);
            }

            $page = JoinTheTeamPageEntity::all()->first();

            $headIsOn = false;
            if (isset($data['head_is_on'])) {
                $headIsOn = true;

                $headValidStatus = HeaderComponentEntity::validateComponent($data['head_data'], $page->head_id);
                if ($headValidStatus != 'success') {
                    return array('validationError', $headValidStatus);
                }
            }

            $try_is_on = false;
            if (isset($data['try_is_on'])) {
                $try_is_on = true;
            }


            if (isset($data['image'])) {

                if (!empty($page->image)) {
                    File::delete(self::$path . $page->image);
                    File::delete(self::$pathMedium . $page->image);
                    File::delete(self::$pathSmall . $page->image);
                }


                $time = time();
                $fileName = $time . '.' . $data['image']->getClientOriginalExtension();
                $image = Image::make($data['image']->getRealPath());

//                $image->save(self::$path . $fileName);

                $image->resize(2400, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(self::$path . $fileName);

                $image->resize(1200, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(self::$pathMedium . $fileName);

                $image->resize(600, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(self::$pathSmall . $fileName);

                $page->image = $fileName;
            }

            $tryValidStatus = TryComponentEntity::validateTry($data['try_data'], $page->try_id);
            if ($tryValidStatus != 'success') {
                return array('validationError', $tryValidStatus);
            }


            $page->try_is_on = $try_is_on;

            $page->head_is_on = $headIsOn;


            foreach ($langArr as $oneLang) {

                $title = 'title_' . $oneLang;
                $text = 'text_' . $oneLang;

                $text_second = 'text_s_' . $oneLang;


                if (isset($data[$title])) {
                    $page->$title = $data[$title];
                }

                if (isset($data[$text])) {
                    $page->$text = $data[$text];
                }

                if (isset($data[$text_second])) {
                    $page->$text_second = $data[$text_second];
                }

            }


            $page->save();

            if (isset($data['head_data'])) {
                HeaderComponentEntity::editHeaderComponent($data['head_data'], $page->head_id);
            }

            if (isset($data['try_data'])) {
                TryComponentEntity::editTryComponent($data['try_data'], $page->try_id);
            }


        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('creatingError', $ex);
        }

        return array('successMessage', $page);

    }

    public static function deleteInnerImage()
    {
        $page = JoinTheTeamPageEntity::all()->first();
        File::delete(self::$path . $page->image);
        File::delete(self::$pathMedium . $page->image);
        File::delete(self::$pathSmall . $page->image);
        $page->image = '';
        $page->save();

    }

}
