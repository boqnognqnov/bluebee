<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\ImageManagerStatic as Image;
use File;
use Illuminate\Support\Facades\Config;

class CommentEventEntity extends Model
{

    protected
        $table = 'comment_events';

    public
    static $path = 'uploads' . '/' . 'images' . '/' . 'comments' . '/' . 'events' . '/';

    public static function getEventTitle($eventID)
    {
        $event = EventItemsEntity::find($eventID)->toArray();
        $eventTitle = $event['title_' . App::getLocale()];
        return $eventTitle;
    }


    public
    static function postRules()
    {
        return array(
//            'g-recaptcha-response' => 'required|captcha',
            'name' => 'required',
            'message' => 'required',
            'email' => 'required',
        );
    }

    public static function postMessages()
    {
        return [
            'name.required' => 'The name is required.',
            'message.required' => 'The message is required.',
            'email.required' => 'The email is required.',
        ];
    }

    public static function postComment($data)
    {
        $validator = \Validator::make($data, self::postRules(), self::postMessages());
//
        if ($validator->fails()) {
            return array('validationError', $validator);
        }
        $blueBeeDefaultAvatar = Config::get('GlobalArrays.defaultAvatar');
        try {
            $comment = new CommentEventEntity();
            $comment->name = $data['name'];
            $comment->text = $data['message'];
            $comment->email = $data['email'];
            $comment->user_id = Auth::user()->id;
            
//            $comment->image = '';
//
//            if (Auth::check() == true) {
//                if (!empty(Auth::user()->image_avatar)) {
//                    $comment->image = Auth::user()->image_avatar;
//                } else {
//                    $comment->image = $blueBeeDefaultAvatar;
//                }
//            } else {
//                $comment->image = $blueBeeDefaultAvatar;
//            }


            $comment->event_id = $data['event_id'];


            $comment->save();


        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('creatingError', $ex);
        }

        return array('successMessage', $comment);


    }

}
