<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class FeaturesItemEntity extends Model
{

    protected $table = 'features_elements';
    public static $path = 'uploads' . '/' . 'images' . '/' . 'pages' . '/' . 'featuresItems' . '/';

    public static function itemRules()
    {
        return array(
            'image' => 'image',

            'title_en' => 'required',
            'text_en' => 'required',
//            'title_de' => 'required',
//            'text_de' => 'required',
//            'title_fr' => 'required',
//            'text_fr' => 'required',
        );
    }

    public static function itemMessages()
    {
        return [
            'image.image' => 'Features image is not valid format',
            'image.required' => 'Features image is not set',


            'title_en.required' => 'Features title EN is not set',
            'text_en.required' => 'Features text EN is not set',
            'title_de.required' => 'Features title DE is not set',
            'text_de.required' => 'Features text DE is not set',
            'title_fr.required' => 'Features title FR is not set',
            'text_fr.required' => 'Features text FR is not set',
        ];
    }


    public static function updateOrCreateNewItem($data)
    {
        $langArr = ['en', 'de', 'fr'];
        $rules = self::itemRules();


        try {

            $item = FeaturesItemEntity::findOrNew($data['id']);
//            if (empty($item->image)) {
//                $rules['image'] = 'required|image';
//            }


            $validator = \Validator::make($data, $rules, self::itemMessages());

            if ($validator->fails()) {
                return array('validationError', $validator);
            }


            foreach ($langArr as $oneLang) {

                $title = 'title_' . $oneLang;
                $text = 'text_' . $oneLang;


                if (isset($data[$title])) {
                    $item->$title = $data[$title];
                }

                if (isset($data[$text])) {
                    $item->$text = $data[$text];
                }

            }

            if (isset($data['image'])) {

                if (!empty($item->image)) {
                    File::delete(self::$path . $item->image);
                }

                $time = time();
                $fileName = $time . '.' . $data['image']->getClientOriginalExtension();
                Image::make($data['image']->getRealPath())->save(self::$path . $fileName);
                $item->image = $fileName;
            }


            $item->save();


        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('creatingError', $ex);
        }

        return array('successMessage', $item);

    }

    public static function destroyItem($itemId)
    {
        try {
            $item = FeaturesItemEntity::find($itemId);
            if (!empty($item->image)) {
                File::delete(self::$path . $item->image);
            }

            $item->delete();

        } catch (\Exception $ex) {
            \Log::error($ex);
            return 'error';
        }

        return 'success';

    }
}
