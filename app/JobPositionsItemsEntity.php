<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class JobPositionsItemsEntity extends Model
{

    protected $table = 'positions_to_apply';

    public static $path = 'uploads' . '/' . 'images' . '/' . 'pages' . '/' . 'openJobPositions' . '/' . 'default' . '/';
    public static $pathMedium = 'uploads' . '/' . 'images' . '/' . 'pages' . '/' . 'openJobPositions' . '/' . 'medium' . '/';
    public static $pathSmall = 'uploads' . '/' . 'images' . '/' . 'pages' . '/' . 'openJobPositions' . '/' . 'small' . '/';

    public static function jobPositionRules()
    {
        return array(
            'title_en' => 'required',
            'requirements_en' => 'required',
        );
    }

    public static function jobPositionMessages()
    {
        return [
            'title_en.required' => 'The title of position is required.',
            'requirements_en.required' => 'This position requirements are not set.',

        ];
    }

    public static function addOrEdit($data)
    {

        $validator = \Validator::make($data, self::jobPositionRules(), self::jobPositionMessages());

        if ($validator->fails()) {
            return array('validationError', $validator);
        }
        $langArr = ['en', 'de', 'fr'];


        try {

            $item = JobPositionsItemsEntity::findOrNew($data['id']);


            foreach ($langArr as $oneLang) {

                $title = 'title_' . $oneLang;
                $requirements = 'requirements_' . $oneLang;


                if (isset($data[$title])) {
                    $item->$title = $data[$title];
                }

                if (isset($data[$requirements])) {
                    $item->$requirements = $data[$requirements];
                }
            }

            if (isset($data['image'])) {

                if (!empty($item->image)) {
                    File::delete(self::$path . $item->image);
                    File::delete(self::$pathMedium . $item->image);
                    File::delete(self::$pathSmall . $item->image);
                }

//                $time = time();
//                $fileName = $time . '.' . $data['image']->getClientOriginalExtension();
//                Image::make($data['image']->getRealPath())->save(self::$path . $fileName);

                $time = time();
                $fileName = $time . '.' . $data['image']->getClientOriginalExtension();
                $image = Image::make($data['image']->getRealPath());

//                $image->save(self::$path . $fileName);

                $image->resize(2400, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(self::$path . $fileName);

                $image->resize(1200, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(self::$pathMedium . $fileName);

                $image->resize(600, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(self::$pathSmall . $fileName);

                $item->image = $fileName;
            }


            $item->save();


        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('creatingError', $ex);
        }

        return array('successMessage', $item);

    }

    public static function destroyItem($itemId)
    {
        try {
            $item = JobPositionsItemsEntity::find($itemId);
            $item->delete();

        } catch (\Exception $ex) {
            \Log::error($ex);
            return false;
        }
        return true;
    }

}
