<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class AboutPageEntity extends Model
{

    protected $table = 'page_about';

    public static $path = 'uploads' . '/' . 'images' . '/' . 'pages' . '/' . 'aboutPage' . '/' . 'default' . '/';

    public static $pathMedium = 'uploads' . '/' . 'images' . '/' . 'pages' . '/' . 'aboutPage' . '/' . 'medium' . '/';
    public static $pathSmall = 'uploads' . '/' . 'images' . '/' . 'pages' . '/' . 'aboutPage' . '/' . 'small' . '/';


    public static function editPageRules()
    {
        return array(
            'intro_image' => 'image',

//            'intro_title_en' => 'required',
//            'intro_title_de' => 'required',
//            'intro_title_fr' => 'required',

            'intro_text_en' => 'required',
//            'intro_text_de' => 'required',
//            'intro_text_fr' => 'required',


//            'services_title_en' => 'required',
//            'services_title_de' => 'required',
//            'services_title_fr' => 'required',

//            'services_text_en' => 'required',
//            'services_text_de' => 'required',
//            'services_text_fr' => 'required',

//            'intro_button_ref' => 'required',

//            'intro_button_en' => 'required',
//            'intro_button_de' => 'required',
//            'intro_button_fr' => 'required',

//            'achievement_title_en' => 'required',
//            'achievement_title_de' => 'required',
//            'achievement_title_fr' => 'required',
//
//            'achievement_text_en' => 'required',
//            'achievement_text_de' => 'required',
//            'achievement_text_fr' => 'required',

            'team_image' => 'image',

            'team_title_en' => 'required',
//            'team_title_de' => 'required',
//            'team_title_fr' => 'required',

            'team_text_en' => 'required',
//            'team_text_de' => 'required',
//            'team_text_fr' => 'required',

            'team_button_en' => 'required',
//            'team_button_de' => 'required',
//            'team_button_fr' => 'required',


        );
    }

    public static function editPageMessages()
    {
        return [
            'intro_image.image' => 'The main section image is not valid format',

            'intro_title_en.required' => 'The main section body title is not set in some languages.',
            'intro_title_de.required' => 'The main section body title is not set in some languages.',
            'intro_title_fr.required' => 'The main section body title is not set in some languages.',


            'intro_text_en.required' => 'The main section body text is not set in some languages.',
            'intro_text_de.required' => 'The main section body text is not set in some languages.',
            'intro_text_fr.required' => 'The main section body text is not set in some languages.',

            'intro_button_ref.required' => 'The main section button url is not set.',

            'services_title_en.required' => 'The service section title is not set in some languages.',
            'services_title_de.required' => 'The service section title is not set in some languages.',
            'services_title_fr.required' => 'The service section title is not set in some languages.',

            'services_text_en.required' => 'The service section text is not set in some languages.',
            'services_text_de.required' => 'The service section text is not set in some languages.',
            'services_text_fr.required' => 'The service section text is not set in some languages.',

            'intro_button_en.required' => 'The main section button name is not set in some languages.',
            'intro_button_de.required' => 'The main section button name is not set in some languages.',
            'intro_button_fr.required' => 'The main section button name is not set in some languages.',

            'achievement_title_en.required' => 'The achievement section title is not set in some languages.',
            'achievement_title_de.required' => 'The achievement section title is not set in some languages.',
            'achievement_title_fr.required' => 'The achievement section title is not set in some languages.',

            'achievement_text_en.required' => 'The achievement section text is not set in some languages.',
            'achievement_text_de.required' => 'The achievement section text is not set in some languages.',
            'achievement_text_fr.required' => 'The achievement section text is not set in some languages.',

            'team_image' => 'The team section image is not valid format',

            'team_title_en.required' => 'The team section title is not set in some languages.',
            'team_title_de.required' => 'The team section title is not set in some languages.',
            'team_title_fr.required' => 'The team section title is not set in some languages.',

            'team_text_en.required' => 'The team section text is not set in some languages.',
            'team_text_de.required' => 'The team section text is not set in some languages.',
            'team_text_fr.required' => 'The team section text is not set in some languages.',

            'team_button_en.required' => 'The team section button name is not set in some languages.',
            'team_button_de.required' => 'The team section button name is not set in some languages.',
            'team_button_fr.required' => 'The team section button name is not set in some languages.',

        ];
    }

    public static function editPage($data)
    {
        $validator = \Validator::make($data, self::editPageRules(), self::editPageMessages());

        if ($validator->fails()) {
            return array('validationError', $validator);
        }

        $page = AboutPageEntity::all()->first();

        $langArr = ['en', 'de', 'fr'];

        $headIsOn = false;
        if (isset($data['head_is_on'])) {
            $headIsOn = true;

            $headValidStatus = HeaderComponentEntity::validateComponent($data['head_data'], $page->head_id);
            if ($headValidStatus != 'success') {
                return array('validationError', $headValidStatus);
            }
        }

        $try_is_on = false;
        if (isset($data['try_is_on'])) {
            $try_is_on = true;

            $tryValidStatus = TryComponentEntity::validateTry($data['try_data'], $page->try_id);
            if ($tryValidStatus != 'success') {
                return array('validationError', $tryValidStatus);
            }
        }


//        try {


            foreach ($langArr as $oneLang) {
                $about_title = 'intro_title_' . $oneLang;
                $about_text = 'intro_text_' . $oneLang;
                $about_s_title = 'services_title_' . $oneLang;
                $about_s_text = 'services_text_' . $oneLang;
                $about_button = 'intro_button_' . $oneLang;
                $s_title = 'achievement_title_' . $oneLang;
                $s_text = 'achievement_text_' . $oneLang;
                $team_title = 'team_title_' . $oneLang;
                $team_text = 'team_text_' . $oneLang;
                $team_button = 'team_button_' . $oneLang;

                if (isset($data[$about_title])) {
                    $page->$about_title = $data[$about_title];
                }
                if (isset($data[$about_text])) {
                    $page->$about_text = $data[$about_text];
                }
                if (isset($data[$about_s_title])) {
                    $page->$about_s_title = $data[$about_s_title];
                }
                if (isset($data[$about_s_text])) {
                    $page->$about_s_text = $data[$about_s_text];
                }
                if (isset($data[$about_button])) {
                    $page->$about_button = $data[$about_button];
                }
                if (isset($data[$s_title])) {
                    $page->$s_title = $data[$s_title];
                }
                if (isset($data[$s_text])) {
                    $page->$s_text = $data[$s_text];
                }
                if (isset($data[$team_title])) {
                    $page->$team_title = $data[$team_title];
                }
                if (isset($data[$team_text])) {
                    $page->$team_text = $data[$team_text];
                }
                if (isset($data[$team_button])) {
                    $page->$team_button = $data[$team_button];
                }
            }

            $page->intro_button_ref = $data['intro_button_ref'];

            if (isset($data['intro_image'])) {

                if (!empty($page->intro_image)) {
                    File::delete(self::$path . $page->intro_image);
                    File::delete(self::$pathMedium . $page->intro_image);
                    File::delete(self::$pathSmall . $page->intro_image);
                }

//                $time = time();
//                $fileName = $time . '.' . $data['intro_image']->getClientOriginalName();
//                Image::make($data['intro_image']->getRealPath())->save(self::$path . $fileName);

                $time = time();
                $fileName = $time . '.' . $data['intro_image']->getClientOriginalExtension();
                $image = Image::make($data['intro_image']->getRealPath());

//                $image->save(self::$path . $fileName);

                $image->resize(2400, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(self::$path . $fileName);

                $image->resize(1200, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(self::$pathMedium . $fileName);

                $image->resize(600, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(self::$pathSmall . $fileName);

                $page->intro_image = $fileName;
            }

            if (isset($data['team_image'])) {

                if (!empty($page->team_image)) {
                    File::delete(self::$path . $page->team_image);
                    File::delete(self::$pathMedium . $page->team_image);
                    File::delete(self::$pathSmall . $page->team_image);
                }

//                $time = time();
//                $fileName = $time . '.' . $data['team_image']->getClientOriginalName();
//                Image::make($data['team_image']->getRealPath())->save(self::$path . $fileName);
                $time = time();
                $fileName = $time . '.' . $data['team_image']->getClientOriginalExtension();
                $image = Image::make($data['team_image']->getRealPath());

//                $image->save(self::$path . $fileName);

                $image->resize(2400, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(self::$path . $fileName);

                $image->resize(1200, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(self::$pathMedium . $fileName);

                $image->resize(600, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(self::$pathSmall . $fileName);
                $page->team_image = $fileName;
            }


            $page->head_is_on = $headIsOn;

            $partners_is_on = false;
            if (isset($data['partners_is_on'])) {
                $partners_is_on = true;
            }
            $page->partners_is_on = $partners_is_on;


//            $try_is_on = false;
//            if (isset($data['try_is_on'])) {
//                $try_is_on = true;
//            }
            $page->try_is_on = $try_is_on;


            $page->save();

            if (isset($data['head_data'])) {
                HeaderComponentEntity::editHeaderComponent($data['head_data'], $page->head_id);
            }

            if (isset($data['try_data'])) {
                TryComponentEntity::editTryComponent($data['try_data'], $page->try_id);
            }

//
//        } catch (\Exception $ex) {
//            \Log::error($ex);
//            return array('creatingError', $ex);
//        }
        return array('successMessage', $page);
//        return $page->id;

    }

}
