<?php

namespace App\Classes;

use App;
use App\Complex;
use App\ComplexRooms;
use App\EarlyBooking;
use App\Http\Controllers\Api\ReservationsApi;
use App\Reservations;
use App\ReservedRooms;
use App\RoomOccupancy;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use Intervention\Image\ImageManagerStatic as Image;
use File;
use Log;
use App\BenefitInnerPagesEntity;
use Illuminate\Support\Facades\Input;


class GlobalFunctions
{

    public static function getHeaderCompData($id, $breadcrumbArrData = null)
    {
        $headData = App\HeaderComponentEntity::find($id)->toArray();
        $lang = App::getLocale();
        $data = [];
        $data['background']['default'] = App\HeaderComponentEntity::$path . $headData['image'];
        $data['background']['medium'] = App\HeaderComponentEntity::$pathMedium . $headData['image'];
        $data['background']['small'] = App\HeaderComponentEntity::$pathSmall . $headData['image'];

        $data['title'] = $headData['title_' . $lang];
        $data['body'] = $headData['text_' . $lang];
        $data['breadcrumb'] = self::collectBreadcrumbData($id, $breadcrumbArrData);

        return $data;
    }

    private static function collectBreadcrumbData($headerId, $breadcrumbArrData = null)
    {
        $breadcrumb = [];

//        WHY BLUEBEE
        if ($headerId == 1 || $headerId == 4 || ($headerId > 1 && $headerId < 4) || $headerId == 23) {
            $breadcrumb[0]['url'] = 'why-bluebee';
            $whyBlueBeeMainPage = App\BenefitPageEntity::all()->first()->toArray();
            $benefitMainPageName = $whyBlueBeeMainPage['title_' . App::getLocale()];
            if (empty($benefitMainPageName)) {
                $benefitMainPageName = 'Why bluebee';
            }
            $breadcrumb[0]['name'] = $benefitMainPageName;

            if ($headerId == 23) {
                return $breadcrumb;
            }

            $whyBlueBeeInner = BenefitInnerPagesEntity::where('head_id', '=', $headerId)->get()->first()->toArray();
            $breadcrumb[1]['url'] = 'why-bluebee/' . $whyBlueBeeInner['slug_' . App::getLocale()];
            $breadcrumb[1]['name'] = $whyBlueBeeInner['front_title_' . App::getLocale()];
        }
//        WHY BLUEBEE
//        CASES
        if ($headerId == 6 || $headerId == 10 || ($headerId > 6 && $headerId < 10)) {
            $breadcrumb[0]['url'] = 'use-cases';
            $caseMainPage = App\CasesMainPageEntity::all()->first()->toArray();
            $mainCasePageName = $caseMainPage['title_' . App::getLocale()];
            if (empty($mainCasePageName)) {
                $mainCasePageName = 'Use cases';
            }
            $breadcrumb[0]['name'] = $mainCasePageName;
            if ($headerId == 6) {
                return $breadcrumb;
            }
            $caseInnerPage = App\CasesInnerPageEntity::where('head_id', '=', $headerId)->get()->first()->toArray();
            $breadcrumb[1]['url'] = 'use-cases/' . $caseInnerPage['slug_' . App::getLocale()];
            $breadcrumb[1]['name'] = $caseInnerPage['front_title_' . App::getLocale()];


        }
//        CASES
//        FEATURES
        if ($headerId == 15) {
            $breadcrumb[0]['url'] = 'features';
            $breadcrumb[0]['name'] = 'Features';
        }

//        FEATURES
//        ABOUT PAGE
        if ($headerId == 19) {
            $breadcrumb[0]['url'] = 'about';
            $breadcrumb[0]['name'] = 'About';
        }
        //        ABOUT PAGE

        //        ABOUT PAGE
        if ($headerId == 18) {
            $breadcrumb[0]['url'] = 'about';
            $breadcrumb[0]['name'] = 'About';
        }
        //        ABOUT PAGE

//        JOIN THE TEAM
        if ($headerId == 20) {
            $breadcrumb[0]['url'] = 'join';
            $breadcrumb[0]['name'] = 'Join the team';
        }

//        JOIN THE TEAM
//        PIPELINES
        if ($headerId == 21) {
            $breadcrumb[0]['url'] = 'pipelines';
            $breadcrumb[0]['name'] = 'Pipelines';
        }
//        PIPELINES
//        HOW DOES WORK
        if ($headerId == 24) {
            $breadcrumb[0]['url'] = 'how-it-works';
            $breadcrumb[0]['name'] = 'How does it work';
        }
//        HOW DOES IT WORK
//        KNOWLEDGE CENTER
        if ($headerId == 11 || $headerId == 12 || $headerId == 13 || $headerId == 14 || $headerId == 25 || $headerId == 26) {
            $breadcrumb[0]['url'] = 'knowledge-center';
            $breadcrumb[0]['name'] = 'Knowledge centre';
            if ($headerId == 14) {
                return $breadcrumb;
            }
            if ($headerId == 11 || $headerId == 25) {
                $breadcrumb[1]['url'] = 'news/list';
                $breadcrumb[1]['name'] = 'News';
                if ($headerId == 11) {
                    return $breadcrumb;
                }
                $breadcrumb[2]['url'] = $breadcrumbArrData['url'];
                $breadcrumb[2]['name'] = $breadcrumbArrData['title'];

            }
            if ($headerId == 12 || $headerId == 26) {
                $breadcrumb[1]['url'] = 'events/list';
                $breadcrumb[1]['name'] = 'Events';
                if ($headerId == 12) {
                    return $breadcrumb;
                }
                $breadcrumb[2]['url'] = $breadcrumbArrData['url'];
                $breadcrumb[2]['name'] = $breadcrumbArrData['title'];
            }
            if ($headerId == 13) {
                $breadcrumb[1]['url'] = 'downloads/list';
                $breadcrumb[1]['name'] = 'Downloads';
            }
        }

//        KNOWLEDGE CENTER
        return $breadcrumb;
    }

    public static function getTrialCompData($id)
    {
        $lang = App::getLocale();
        $componnet = App\TryComponentEntity::find($id)->toArray();
        $data['title'] = $componnet['title_' . $lang];
        $data['text'] = $componnet['text_' . $lang];
        $data['button'] = $componnet['button_' . $lang];

        $data['image']['default'] = App\TryComponentEntity::$path . $componnet['image'];
        $data['image']['medium'] = App\TryComponentEntity::$pathMedium . $componnet['image'];
        $data['image']['small'] = App\TryComponentEntity::$pathSmall . $componnet['image'];

        $data['url'] = $componnet['url'];


        return $data;
    }

    public static function getOpinionCompData($id)
    {
        $data = [];
        $lang = App::getLocale();
        $componnet = App\OpinionComponentEntity::find($id)->toArray();
        $data['name'] = $componnet['name'];
        $data['text'] = $componnet['text_' . $lang];
        $data['button'] = $componnet['button_' . $lang];
        $data['url'] = $componnet['url'];

        return $data;
    }

//    public static function getFavoriteCompData($id)
//    {
//        $data = [];
////        return ['image' => $imagePaths, 'url' => $url, 'title' => $titleToReturn, 'text' => $textToReturn];
//
//
//        return $data;
//    }

    public static function getPartnersCompData()
    {
        $data = [];

        $partnersData = App\PartnersListEntity::orderBy('position', 'DESC')->where('is_favorite', '=', true)->get()->toArray();
        foreach ($partnersData as $key => $onePartner) {
//            $pageData['partners'][$key]['title'] = $onePartner['title_' . $lang];
            $data[$key]['image'] = App\PartnersListEntity::$path . $onePartner['image'];
            $data[$key]['url'] = $onePartner['url'];
        }
        return $data;
    }

    public static function getBenefitsCompData()
    {
        $lang = App::getLocale();
        $data = [];
        $benefitItems = App\BenefitInnerPagesEntity::all()->toArray();


        foreach ($benefitItems as $key => $oneBenefitTitle) {
            $data[$key]['title'] = $oneBenefitTitle['front_title_' . $lang];
            $data[$key]['desc'] = $oneBenefitTitle['front_text_' . $lang];

            $imgPath = BenefitInnerPagesEntity::$path . $oneBenefitTitle['front_image'];
            $data[$key]['icon'] = $imgPath;
            $data[$key]['imgClass'] = GlobalFunctions::checkImgWidthHeight($imgPath);

            $data[$key]['url'] = $oneBenefitTitle['slug_' . $lang];
            $data[$key]['buttonText'] = 'More info';

        }
        return $data;
    }

    public static function getMethodsCompData($arrayToAppend, $withText = false)
    {
        $lang = App::getLocale();

        $methods = App\MethodsEntity::all()->first()->toArray();

        $arrayToAppend['main']['page_title'] = $methods['title_' . $lang];
        $arrayToAppend['main']['page_desc'] = $methods['text_' . $lang];

        if ($withText != true) {
            $arrayToAppend['steps'][1]['title'] = $methods['one_title_' . $lang];
            $arrayToAppend['steps'][2]['title'] = $methods['second_title_' . $lang];
            $arrayToAppend['steps'][3]['title'] = $methods['third_title_' . $lang];
            $arrayToAppend['steps'][4]['title'] = $methods['fourth_title_' . $lang];

        } else if ($withText == true) {

            $arrayToAppend['steps'][1]['title'] = $methods['one_title_short_' . $lang];
            $arrayToAppend['steps'][2]['title'] = $methods['second_title_short_' . $lang];
            $arrayToAppend['steps'][3]['title'] = $methods['third_title_short_' . $lang];
            $arrayToAppend['steps'][4]['title'] = $methods['fourth_title_short_' . $lang];

            $arrayToAppend['steps'][1]['desc'] = $methods['one_text_short_' . $lang];
            $arrayToAppend['steps'][2]['desc'] = $methods['second_text_short_' . $lang];
            $arrayToAppend['steps'][3]['desc'] = $methods['third_text_short_' . $lang];
            $arrayToAppend['steps'][4]['desc'] = $methods['fourth_text_short_' . $lang];
        }


        return $arrayToAppend;
    }

    public static function getNewsLetterCompData($component_id)
    {
        $lang = App::getLocale();
        $component = App\SignUpComponentEntity::find($component_id)->toArray();
        $data = [];
        $data['text'] = $component['text_' . $lang];
        $data['button'] = $component['button_' . $lang];
        $data['background']['default'] = App\SignUpComponentEntity::$path . $component['image'];
        $data['background']['medium'] = App\HeaderComponentEntity::$pathMedium . $component['image'];
        $data['background']['small'] = App\HeaderComponentEntity::$pathSmall . $component['image'];


        return $data;

    }

    public static function checkImgWidthHeight($imgPath)
    {
//DEFINE IMAGE PROPORTION
        $image = '';
        try {
//            $image = Image::make($imgPath);
            $image = Image::make(imagecreatefromjpeg($imgPath));

        } catch (\Exception $ex) {
            \Log::error($ex);
        }


        $height = 1;
        $width = 0;
        if ($image != null) {
            $height = $image->height();
            $width = $image->width();
        }

        if ($height > $width) {
            $className = 'portrait';
        } else {
            $className = 'landscape';
        }

        return $className;
    }

    public static function collectYears($records, $stampColumn)
    {

        $yearList = [];

        foreach ($records as $oneRecord) {

            if ($oneRecord == 'created_at') {
                $tempYear = $oneRecord[$stampColumn]->toDateTimeString();
            } else {
                $tempYear = $oneRecord[$stampColumn];
            }

            $tempYear = explode('-', $tempYear);
            $tempYear = $tempYear[0];
            if (in_array($tempYear, $yearList) == false) {
                array_push($yearList, $tempYear);
            }


        }
        return $yearList;

    }

    public static function changeDateFormat($date)
    {
        $dateArr = explode('/', $date);
        $dateStr = $dateArr[2] . '-' . $dateArr[0] . '-' . $dateArr[1];
        return new \DateTime($dateStr);
    }

    public static function generateDateTimeToStr($date)
    {
//    EXAMPLE:    2016-01-20 18:28:03 to 01/20/2016

        $dateArr = explode('-', $date);
        $day = explode(' ', $dateArr[2]);
        $day = $day[0];

        $date = $dateArr[1] . '/' . $day . '/' . $dateArr[0];

        return $date;
    }

    public static function getMonthByDateTime($datetime)
    {
        $monthRaw = explode('-', $datetime);
        $month = explode('-', $monthRaw[1]);

        $monthArr = Config::get('GlobalArrays.month');
        return $monthArr[$month[0]];
    }

    public static function getDayByDateTime($datetime)
    {
        $dayRaw = explode('-', $datetime);
        $day = explode(' ', $dayRaw[2]);
        return $day[0];
    }

    public static function getStampToStrFullMonth($datetime)
    {
//    EXAMPLE:    2016-01-20 18:28:03 to 01/20/2016

        $dateArr = explode('-', $datetime);
        $day = explode(' ', $dateArr[2]);
        $day = $day[0];
        $monthNamedArr = Config::get('GlobalArrays.monthNamed');
//        $date = $monthNamedArr[$dateArr[1]] . ' ' . $day . ', ' . $dateArr[0];
        $date = $day . ' ' . $monthNamedArr[$dateArr[1]] . ', ' . $dateArr[0];

        return $date;
    }

    public static function getTimestamToTimeOfDayStr($datetime)
    {
//    EXAMPLE:   10:09 am

        $datetime = date('h:i A', strtotime($datetime));
        return $datetime;
    }


    public static function oldEventsChecker()
    {


        $currentDate = date("Y-m-d") . ' 00:00:00';
        $eventsToCheck = App\EventItemsEntity::where('event_date', '<', $currentDate)->get();


        $oldEventCategory = App\EventCategoriesEntity::where('title_en', 'LIKE', 'Past events')->get()->first();
        if (!$oldEventCategory) {
            $oldEventCategory = new App\EventCategoriesEntity();
            $oldEventCategory->title_en = 'Past events';
            $oldEventCategory->save();
        }

        foreach ($eventsToCheck as $oneEvent) {
            self::archiveOldEvents($oneEvent, $oldEventCategory);
        }
    }

    public static function archiveOldEvents($eventToArchive, $oldEventCategory)
    {

        $eventToArchive->category_id = $oldEventCategory->id;
        $eventToArchive->save();


    }

    public static function getUserAvatar($userId)
    {
        $user = User::find($userId);

        if ($user == null) {
            $blueBeeDefaultAvatar = Config::get('GlobalArrays.defaultAvatar');
            $avatar = $blueBeeDefaultAvatar;
        } else {
            $avatar = $user->image_avatar;
        }

        if (empty($avatar)) {
            $blueBeeDefaultAvatar = Config::get('GlobalArrays.defaultAvatar');
            $avatar = $blueBeeDefaultAvatar;
        }
        return $avatar;
    }

}