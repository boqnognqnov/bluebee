<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Intervention\Image\ImageManagerStatic as Image;
use File;
use Illuminate\Support\Facades\Config;

class FavoritesComponentEntity extends Model
{

    protected $table = 'component_favorite';

//WORKING

    public static function getDropDownForModel()
    {
        //THIS FUNCTION DEFINE MODEL ON PAGE WHO WILL HOLD FAV COMPONENT
        $dropDown = [];
        $dropDown['page_features'] = 'Features page';
        $dropDown['page_news_inner'] = 'News page';
        $dropDown['page_events_inner'] = 'Events page';
        $dropDown['page_benefits_inner-1'] = 'Why BlueBee ' . BenefitInnerPagesEntity::find(1)->front_title_en;
        $dropDown['page_benefits_inner-2'] = 'Why BlueBee ' . BenefitInnerPagesEntity::find(2)->front_title_en;
        $dropDown['page_benefits_inner-3'] = 'Why BlueBee ' . BenefitInnerPagesEntity::find(3)->front_title_en;
        $dropDown['page_benefits_inner-4'] = 'Why BlueBee ' . BenefitInnerPagesEntity::find(4)->front_title_en;

        $dropDown['page_cases_inner-1'] = 'Case ' . CasesInnerPageEntity::find(1)->front_title_en;
        $dropDown['page_cases_inner-2'] = 'Case ' . CasesInnerPageEntity::find(2)->front_title_en;
        $dropDown['page_cases_inner-3'] = 'Case ' . CasesInnerPageEntity::find(3)->front_title_en;
        $dropDown['page_cases_inner-4'] = 'Case ' . CasesInnerPageEntity::find(4)->front_title_en;
        return $dropDown;
    }


    public static function getFavIdByVectorModel($dropDownKey)
    {
        //THIS FUNCTION DEFINE MODEL ON PAGE WHO HOLD FAV COMPONENT
        if ($dropDownKey == 'page_features') {
            $id = FeaturesPageEntity::all()->first()->favorites_id;
        } elseif ($dropDownKey == 'page_news_inner') {
            $id = NewsChildPageEntity::all()->first()->favorites_id;
        } elseif ($dropDownKey == 'page_events_inner') {
            $id = EventChildPageEntity::all()->first()->favorites_id;
        } elseif (strpos($dropDownKey, 'page_benefits_inner') !== false) {
            $modelId = substr($dropDownKey, strpos($dropDownKey, "-") + 1);
            $id = BenefitInnerPagesEntity::find($modelId)->favorites_id;
        } elseif (strpos($dropDownKey, 'page_cases_inner') !== false) {
            $modelId = substr($dropDownKey, strpos($dropDownKey, "-") + 1);
            $id = CasesInnerPageEntity::find($modelId)->favorites_id;
        }
        return $id;
        //THIS FUNCTION DEFINE MODEL ON PAGE WHO HOLD FAV COMPONENT
    }


    public static function viewFavDataByPosition($position, $favObj_id)
    {
        $favObj = FavoritesComponentEntity::find($favObj_id)->toArray();
        if ($position == 'first') {
            $modelName = $favObj['f_model_name'];
            $modelId = $favObj['f_model_id'];
        } elseif ($position == 'second') {
            $modelName = $favObj['s_model_name'];
            $modelId = $favObj['s_model_id'];
        } else {
            $modelName = $favObj['t_model_name'];
            $modelId = $favObj['t_model_id'];
        }

        $dataToReturn = self::collectFavoriteData($modelName, $modelId);

        return $dataToReturn;
    }

    private static function collectFavoriteData($modelName, $modelId)
    {
        $blueBeeDefaultPath = Config::get('GlobalArrays.defaultImagePath');
//        CASE RANDOM ARTICLE
        $imagePaths['default'] = '';
        $imagePaths['medium'] = '';
        $imagePaths['small'] = '';
        $url = '';
        $slug = 'slug_' . App::getLocale();

        $titleToReturn = '';
        $textToReturn = '';
        $title = 'title_' . App::getLocale();
        $text = 'text_' . App::getLocale();

        $slotStatus = 'Custom item';
        //        CASE RANDOM ARTICLE


        if ($modelName == 'news') {

            $newsPage = NewsItemsEntity::find($modelId);
            if ($modelId == 0 || $modelId == -1) {
                $data = self::collectArticleByDate('news', $modelId);
                return $data;
            } else {
                $image = $newsPage->image;
                $imagePaths['default'] = NewsItemsEntity::$path . '' . $image;
                $imagePaths['medium'] = NewsItemsEntity::$pathMedium . '' . $image;
                $imagePaths['small'] = NewsItemsEntity::$pathSmall . '' . $image;

                if (empty($newsPage->image)) {
                    $imagePaths['default'] = $blueBeeDefaultPath;
                    $imagePaths['medium'] = $blueBeeDefaultPath;
                    $imagePaths['small'] = $blueBeeDefaultPath;
                }

                $url = $newsPage->$slug;
                $url = 'news/' . $url;

                $titleToReturn = $newsPage->$title;
                $textToReturn = $newsPage->$text;
            }
        } elseif ($modelName == 'events') {

            $eventPage = EventItemsEntity::find($modelId);
            if ($modelId == 0 || $modelId == -1) {
                $data = self::collectArticleByDate('events', $modelId);
                return $data;
            } else {

                $image = $eventPage->image;
                $imagePaths['default'] = EventItemsEntity::$path . '' . $image;
                $imagePaths['medium'] = EventItemsEntity::$pathMedium . '' . $image;
                $imagePaths['small'] = EventItemsEntity::$pathSmall . '' . $image;

                if (empty($image)) {
                    $imagePaths['default'] = $blueBeeDefaultPath;
                    $imagePaths['medium'] = $blueBeeDefaultPath;
                    $imagePaths['small'] = $blueBeeDefaultPath;
                }


                $url = $eventPage->$slug;
                $url = 'events/' . $url;

                $titleToReturn = $eventPage->$title;
                $textToReturn = $eventPage->$text;
            }
        } elseif ($modelName == 'features_elements') {

            $featureEntity = FeaturesItemEntity::find($modelId);
            if ($modelId == 0 || $modelId == -1) {
                $data = self::collectArticleByDate('features_elements', $modelId);
                return $data;
            } else {
                $image = $featureEntity->image;
                $imagePaths['default'] = FeaturesItemEntity::$path . '' . $image;
                $imagePaths['medium'] = FeaturesItemEntity::$path . '' . $image;
                $imagePaths['small'] = FeaturesItemEntity::$path . '' . $image;

                if (empty($image)) {
                    $imagePaths['default'] = $blueBeeDefaultPath;
                    $imagePaths['medium'] = $blueBeeDefaultPath;
                    $imagePaths['small'] = $blueBeeDefaultPath;
                }


//            $url = $featureEntity->$slug;
//            $url = 'events/' . $url;
                $url = 'features';

                $titleToReturn = $featureEntity->$title;
                $textToReturn = $featureEntity->$text;
            }
        } elseif ($modelName == 'downloads') {

            $fileItem = DownloadsItemsEntity::find($modelId);
            if ($modelId == 0 || $modelId == -1) {
                $data = self::collectArticleByDate('downloads', $modelId);
                return $data;
            } else {

                $imagePaths['default'] = DownloadsItemsEntity::$pathFrontImage . $fileItem->image;
                $imagePaths['medium'] = DownloadsItemsEntity::$pathFrontImageMedium . $fileItem->image;
                $imagePaths['small'] = DownloadsItemsEntity::$pathFrontImageSmall . $fileItem->image;

                if (empty($fileItem->image)) {
                    $imagePaths['default'] = $blueBeeDefaultPath;
                    $imagePaths['medium'] = $blueBeeDefaultPath;
                    $imagePaths['small'] = $blueBeeDefaultPath;
                }

                $currentDataCartId = DownloadsCategoriesEntity::where('title_en', 'LIKE', 'Data cards')->get()->first()->id;

                if ($currentDataCartId != $fileItem['category_id']) {
                    $url = 'downloads/paginate/' . $fileItem['id'] . '#file_item_' . $fileItem['id'];
                } else {
                    $url = 'pipelines#pipeid' . $fileItem['id'];
                }


                $titleToReturn = $fileItem->$title;
                $textToReturn = $fileItem->$text;
            }
        }

        return ['image' => $imagePaths, 'url' => $url, 'title' => $titleToReturn, 'text' => $textToReturn, 'slotStatus' => $slotStatus];
    }

    private static function collectArticleByDate($itemType, $modelId)
    {
        $blueBeeDefaultPath = Config::get('GlobalArrays.defaultImagePath');
        $data = [];
        $slug = 'slug_' . App::getLocale();
        $title = 'title_' . App::getLocale();
        $text = 'text_' . App::getLocale();

        $data['slotStatus'] = 'Latest item';


        if ($itemType == 'news') {
            if ($modelId == 0) {
                $news = NewsItemsEntity::orderBy('created_at', 'DESC')->get()->first();
            } else {
                $data['slotStatus'] = 'Random News';
                $news = NewsItemsEntity::orderBy(\DB::raw('RAND()'))->get()->first();
            }
            $data['image']['default'] = NewsItemsEntity::$path . $news->image;
            $data['image']['medium'] = NewsItemsEntity::$pathMedium . $news->image;
            $data['image']['small'] = NewsItemsEntity::$pathSmall . $news->image;

            if (empty($news->image)) {
                $data['image']['default'] = $blueBeeDefaultPath;
                $data['image']['medium'] = $blueBeeDefaultPath;
                $data['image']['small'] = $blueBeeDefaultPath;
            }

            $url = $news->$slug;
            $data['url'] = 'news/' . $url;

            $data['title'] = $news->$title;
            $data['text'] = $news->$text;

        } else if ($itemType == 'events') {
            if ($modelId == 0) {
                $event = EventItemsEntity::orderBy('created_at', 'DESC')->get()->first();
            } else {
                $data['slotStatus'] = 'Random Events';
                $event = EventItemsEntity::orderBy(\DB::raw('RAND()'))->get()->first();
            }
            $data['image']['default'] = EventItemsEntity::$path . $event->image;
            $data['image']['medium'] = EventItemsEntity::$pathMedium . $event->image;
            $data['image']['small'] = EventItemsEntity::$pathSmall . $event->image;

            if (empty($event->image)) {
                $data['image']['default'] = $blueBeeDefaultPath;
                $data['image']['medium'] = $blueBeeDefaultPath;
                $data['image']['small'] = $blueBeeDefaultPath;
            }

            $url = $event->$slug;
            $data['url'] = 'events/' . $url;

            $data['title'] = $event->$title;
            $data['text'] = $event->$text;
        } else if ($itemType == 'features_elements') {
            if ($modelId == 0) {
                $feature = FeaturesItemEntity::orderBy('created_at', 'DESC')->get()->first();
            } else {
                $data['slotStatus'] = 'Random Features';
                $feature = FeaturesItemEntity::orderBy(\DB::raw('RAND()'))->get()->first();
            }
            $data['image']['default'] = FeaturesItemEntity::$path . $feature->image;
            $data['image']['medium'] = FeaturesItemEntity::$path . $feature->image;
            $data['image']['small'] = FeaturesItemEntity::$path . $feature->image;


            if (empty($feature->image)) {
                $data['image']['default'] = $blueBeeDefaultPath;
                $data['image']['medium'] = $blueBeeDefaultPath;
                $data['image']['small'] = $blueBeeDefaultPath;
            }

            $url = $feature->$slug;
            $data['url'] = 'features/' . $url;

            $data['title'] = $feature->$title;
            $data['text'] = $feature->$text;
        } else if ($itemType == 'downloads') {
            if ($modelId == 0) {
                $download = DownloadsItemsEntity::orderBy('created_at', 'DESC')->get()->first();
            } else {
                $data['slotStatus'] = 'Random Downloads';
                $download = DownloadsItemsEntity::orderBy(\DB::raw('RAND()'))->get()->first();
            }
            $data['image']['default'] = DownloadsItemsEntity::$pathFrontImage . $download->image;
            $data['image']['medium'] = DownloadsItemsEntity::$pathFrontImageMedium . $download->image;
            $data['image']['small'] = DownloadsItemsEntity::$pathFrontImageSmall . $download->image;

            if (empty($download->image)) {
                $data['image']['default'] = $blueBeeDefaultPath;
                $data['image']['medium'] = $blueBeeDefaultPath;
                $data['image']['small'] = $blueBeeDefaultPath;
            }

            $currentDataCartId = DownloadsCategoriesEntity::where('title_en', 'LIKE', 'Data cards')->get()->first()->id;
            if ($currentDataCartId != $download['category_id']) {
                $data['url'] = 'downloads/paginate/' . $download['id'] . '#file_item_' . $download['id'];
            } else {
                $data['url'] = 'pipelines#pipeid' . $download['id'];
            }

            $data['title'] = $download->$title;
            $data['text'] = $download->$text;
        }

        return $data;
        //WORKING

    }
}
