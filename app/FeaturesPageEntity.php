<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class FeaturesPageEntity extends Model
{

    protected $table = 'page_features';


    public static function editPage($data)
    {
        $page = FeaturesPageEntity::all()->first();

        $headIsOn = false;
        if (isset($data['head_is_on'])) {
            $headIsOn = true;

            $headValidStatus = HeaderComponentEntity::validateComponent($data['head_data'], $page->head_id);
            if ($headValidStatus != 'success') {
                return array('validationError', $headValidStatus);
            }
        }

        $try_is_on = false;
        if (isset($data['try_is_on'])) {
            $try_is_on = true;

            $tryValidStatus = TryComponentEntity::validateTry($data['try_data'], $page->try_id);
            if ($tryValidStatus != 'success') {
                return array('validationError', $tryValidStatus);
            }
        }

        $favorites_is_on = false;
        if (isset($data['favorites_is_on'])) {
            $favorites_is_on = true;
        }

        try {


            $page->head_is_on = $headIsOn;
            $page->try_is_on = $try_is_on;
            $page->favorites_is_on = $favorites_is_on;

            $page->save();

            if (isset($data['try_data'])) {
                TryComponentEntity::editTryComponent($data['try_data'], $page->try_id);
            }
            if (isset($data['head_data'])) {
                HeaderComponentEntity::editHeaderComponent($data['head_data'], $page->head_id);
            }

        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('creatingError', $ex);
        }

        return array('successMessage', $page);

    }
}
