<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class DownloadsMainPageEntity extends Model
{

    protected $table = 'page_downloads';


    public static function editMainPage($data)
    {

        try {

            $page = DownloadsMainPageEntity::all()->first();

            $headIsOn = false;
            if (isset($data['head_is_on'])) {
                $headIsOn = true;

                $headValidStatus = HeaderComponentEntity::validateComponent($data['head_data'], $page->head_id);
                if ($headValidStatus != 'success') {
                    return array('validationError', $headValidStatus);
                }
            }

            $page->head_is_on = $headIsOn;


            $sign_up_is_on = false;
            if (isset($data['sign_up_is_on'])) {
                $sign_up_is_on = true;
            }
            $page->sign_up_is_on = $sign_up_is_on;


            $page->save();

            if (isset($data['head_data'])) {
                HeaderComponentEntity::editHeaderComponent($data['head_data'], $page->head_id);
            }


        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('creatingError', $ex);
        }


        return array('successMessage', $page);

    }

}
