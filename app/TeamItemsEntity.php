<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class TeamItemsEntity extends Model
{

    protected $table = 'team';

    public static $path = 'uploads' . '/' . 'images' . '/' . 'pages' . '/' . 'team' . '/' . 'list' . '/' . 'default' . '/';
    public static $pathMedium = 'uploads' . '/' . 'images' . '/' . 'pages' . '/' . 'team' . '/' . 'list' . '/' . 'medium' . '/';
    public static $pathSmall = 'uploads' . '/' . 'images' . '/' . 'pages' . '/' . 'team' . '/' . 'list' . '/' . 'small' . '/';


    public static function editItemRules()
    {
        return array(
            'name' => 'required',

            'position_en' => 'required',
            'description_en' => 'required',

//            'position_de' => 'required',
//            'description_de' => 'required',
//
//            'position_fr' => 'required',
//            'description_fr' => 'required',


        );
    }

    public static function editItemMessages()
    {
        return [
            'name.required' => 'The name of person is not set.',

            'position_en.required' => 'Person position EN is required',
            'description_en.required' => 'Description of this position EN is required',

            'position_de.required' => 'Person position DE is required',
            'description_de.required' => 'Description of this position DE is required',

            'position_fr.required' => 'Person position FR is required',
            'description_fr.required' => 'Description of this position FR is required',

        ];
    }


    public static function addOrEdit($data)
    {

        $validator = \Validator::make($data, self::editItemRules(), self::editItemMessages());

        if ($validator->fails()) {
            return array('validationError', $validator);
        }
        $langArr = ['en', 'de', 'fr'];


        try {

            $employee = TeamItemsEntity::findOrNew($data['id']);

            if (isset($data['twitter'])) {
                $employee->twitter = $data['twitter'];
            }
            if (isset($data['linkedin'])) {
                $employee->linkedin = $data['linkedin'];
            }
            if (isset($data['email'])) {
                $employee->email = $data['email'];
            }


            foreach ($langArr as $oneLang) {

                $description = 'description_' . $oneLang;
                $position = 'position_' . $oneLang;


                if (isset($data[$description])) {
                    $employee->$description = $data[$description];
                }

                if (isset($data[$position])) {
                    $employee->$position = $data[$position];
                }
            }
            $employee->name = $data['name'];


            if (isset($data['image'])) {

                if (!empty($employee->image)) {
                    File::delete(self::$path . $employee->image);
                    File::delete(self::$pathMedium . $employee->image);
                    File::delete(self::$pathSmall . $employee->image);
                }

//                $time = time();
//                $fileName = $time . '.' . $data['image']->getClientOriginalExtension();
//                Image::make($data['image']->getRealPath())->save(self::$path . $fileName);

                $time = time();
                $fileName = $time . '.' . $data['image']->getClientOriginalExtension();
                $image = Image::make($data['image']->getRealPath());

//                $image->save(self::$path . $fileName);

                $image->resize(2400, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(self::$path . $fileName);

                $image->resize(1200, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(self::$pathMedium . $fileName);

                $image->resize(600, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(self::$pathSmall . $fileName);

                $employee->image = $fileName;
            }

            $isManager = false;
            if (isset($data['is_manager'])) {
                $isManager = true;
            }
            $employee->is_manager = $isManager;

            $isManager = false;
            if (isset($data['is_manager'])) {
                $isManager = true;
            }
            $employee->is_manager = $isManager;

            $employee->save();


        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('creatingError', $ex);
        }

        return array('successMessage', $employee);

    }

    public static function destroyEmployye($employee_id)
    {
        try {
            $employee = TeamItemsEntity::find($employee_id);
            if (!empty($employee->image)) {
                File::delete(self::$path . $employee->image);
            }
            $employee->delete();

        } catch (\Exception $ex) {
            \Log::error($ex);
            return false;
        }
        return true;
    }

}
