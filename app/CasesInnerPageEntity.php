<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManagerStatic as Image;
use File;

class CasesInnerPageEntity extends Model
{

    protected $table = 'page_case_inner';

//    public static $pathMainCase = 'uploads' . '/' . 'images' . '/' . 'pages' . '/' . 'cases' . '/';
//    public static $pathStepsInnerImg = 'uploads' . '/' . 'images' . '/' . 'pages' . '/' . 'cases' . '/' . 'steps' . '/';

    public static $pathStepsInnerImg = 'uploads' . '/' . 'images' . '/' . 'pages' . '/' . 'cases' . '/' . 'steps' . '/' . 'default' . '/';
    public static $pathStepsInnerImgMed = 'uploads' . '/' . 'images' . '/' . 'pages' . '/' . 'cases' . '/' . 'steps' . '/' . 'medium' . '/';
    public static $pathStepsInnerImgSmall = 'uploads' . '/' . 'images' . '/' . 'pages' . '/' . 'cases' . '/' . 'steps' . '/' . 'small' . '/';


    public static $pathStepsFront = 'uploads' . '/' . 'images' . '/' . 'pages' . '/' . 'cases' . '/' . 'steps' . '/' . 'frontImages' . '/';


    public static function editPage($data)
    {
        $langArr = ['en', 'de', 'fr'];
//
//        $head_is_on = false;
//        if (isset($data['head_is_on'])) {
//            $head_is_on = true;
//        }


//        $try_is_on = false;
//        if (isset($data['try_is_on'])) {
//            $try_is_on = true;
//        }

        $favorites_is_on = false;
        if (isset($data['favorites_is_on'])) {
            $favorites_is_on = true;
        }


        try {

            $page = CasesInnerPageEntity::find($data['id']);
            $head_is_on = false;
            if (isset($data['head_is_on'])) {
                $head_is_on = true;

                $headValidStatus = HeaderComponentEntity::validateComponent($data['head_data'], $page->head_id);
                if ($headValidStatus != 'success') {
                    return array('validationError', $headValidStatus);
                }
            }

            $try_is_on = false;
            if (isset($data['try_is_on'])) {
                $try_is_on = true;

                $tryValidStatus = TryComponentEntity::validateTry($data['try_data'], $page->try_id);
                if ($tryValidStatus != 'success') {
                    return array('validationError', $tryValidStatus);
                }
            }

            $opinion_is_on = false;
            if (isset($data['opinion_is_on'])) {
                $opinion_is_on = true;

                $tryValidStatus = OpinionComponentEntity::validateOpinion($data['opinion_data'], $page->opinion_id);
                if ($tryValidStatus != 'success') {
                    return array('validationError', $tryValidStatus);
                }
            }


            foreach ($langArr as $oneLang) {

                $caseText = 'case_text_' . $oneLang;
                $caseSecondTitle = 'case_second_title_' . $oneLang;
                $caseSecondText = 'case_second_text_' . $oneLang;


                if (isset($data[$caseText])) {
                    $page->$caseText = $data[$caseText];
                }
                if (isset($data[$caseSecondTitle])) {
                    $page->$caseSecondTitle = $data[$caseSecondTitle];
                }
                if (isset($data[$caseSecondText])) {
                    $page->$caseSecondText = $data[$caseSecondText];
                }
                if (isset($data[$caseSecondTitle])) {
                    $page->$caseSecondTitle = $data[$caseSecondTitle];
                }

            }

            if (isset($data['case_image'])) {

                if (!empty($page->case_image)) {
                    File::delete(self::$pathStepsInnerImg . $page->case_image);
                    File::delete(self::$pathStepsInnerImgMed . $page->case_image);
                    File::delete(self::$pathStepsInnerImgSmall . $page->case_image);
                }

//                $time = time();
//                $fileName = $time . '.' . $data['case_image']->getClientOriginalExtension();
//                Image::make($data['case_image']->getRealPath())->save(self::$pathStepsInnerImg . $fileName);

                $time = time();
                $fileName = $time . '.' . $data['case_image']->getClientOriginalExtension();
                $image = Image::make($data['case_image']->getRealPath());

//                $image->save(self::$path . $fileName);

                $image->resize(2400, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(self::$pathStepsInnerImg . $fileName);

                $image->resize(1200, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(self::$pathStepsInnerImgMed . $fileName);

                $image->resize(600, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(self::$pathStepsInnerImgSmall . $fileName);


                $page->case_image = $fileName;
            }


            $page->head_is_on = $head_is_on;

            $page->try_is_on = $try_is_on;
            $page->favorites_is_on = $favorites_is_on;
            $page->opinion_is_on = $opinion_is_on;


            $page->save();

            if (isset($data['head_data'])) {
                HeaderComponentEntity::editHeaderComponent($data['head_data'], $page->head_id);
            }

            if (isset($data['try_data'])) {
                TryComponentEntity::editTryComponent($data['try_data'], $page->try_id);
            }
            
            if (isset($data['opinion_data'])) {
                OpinionComponentEntity::editOpinionComponent($data['opinion_data'], $page->opinion_id);
            }


        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('creatingError', $ex);
        }

        return array('successMessage', $page);

    }


    public static function editFrontSIde($data)
    {
        $langArr = ['en', 'de', 'fr'];


        try {

            $page = CasesInnerPageEntity::find($data['id']);

            foreach ($langArr as $oneLang) {


                $frontTitle = 'front_title_' . $oneLang;
                $frontText = 'front_text_' . $oneLang;
                $slug = 'slug_' . $oneLang;


                if (isset($data[$frontTitle])) {
                    $page->$frontTitle = $data[$frontTitle];
                    $page->$slug = str_slug($data[$frontTitle]);
                }

                if (isset($data[$frontText])) {
                    $page->$frontText = $data[$frontText];
                }

            }


            if (isset($data['front_image'])) {

                if (!empty($page->front_image)) {
                    File::delete(self::$pathStepsFront . $page->front_image);
                }

                $time = time();
                $fileName = $time . '.' . $data['front_image']->getClientOriginalExtension();
                Image::make($data['front_image']->getRealPath())->save(self::$pathStepsFront . $fileName);
                $page->front_image = $fileName;
            }


            $page->save();


        } catch (\Exception $ex) {
            \Log::error($ex);
            return array('creatingError', $ex);
        }

        return array('successMessage', $page);

    }

    public static function deleteInnerImage($case_id)
    {
        $page = CasesInnerPageEntity::find($case_id);

        File::delete(self::$pathStepsInnerImg . $page->case_image);
        File::delete(self::$pathStepsInnerImgMed . $page->case_image);
        File::delete(self::$pathStepsInnerImgSmall . $page->case_image);

        $page->case_image = '';

        $page->save();


    }
}
