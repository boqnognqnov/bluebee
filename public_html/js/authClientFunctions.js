/**
 * Created by B_OCS on 3/29/2016.
 */


$(document).ready(function () {
    // authRequest();
});


function loginRequest() {
    var data = {};
    data['email'] = $('#loginModal input[name="email"]').val()
    data['password'] = $('#loginModal input[name="password"]').val()


    $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('input[name="rand"]').val()}});
    $.ajax({
        url: "/api/clientPostLogin",
        type: "POST",
        dataType: "json",
        data: data
    }).done(function (result) {
        // console.log(result['status']);
        if (result['status'] == 'success') {
            location.reload();
        } else {
            var alertDiv = $('#loginModal #loginModalAlertDIv');
            alertDiv.empty();
            alertDiv.addClass('c-alert-box c-alert-box--error');
            var messages = result['messages'];
            // console.log(result['messages']);
            if (messages['email'] != '') {
                alertDiv.append('<p class="u-ms-1">' + messages['email'] +
                    '</p>');
            }
            if (messages['password'] != '') {
                alertDiv.append('<p class="u-ms-1">' + messages['password'] +
                    '</p>');
            }
            if (messages['mainError'] != '') {
                alertDiv.append('<p class="u-ms-1">' + messages['mainError'] +
                    '</p>');
            }
        }
    }).fail(function (result) {
//            alert('error');
    })


}

function signUpRequest() {

    var data = {};
    data['name'] = $('#registrationModal input[name="name"]').val()
    data['email'] = $('#registrationModal input[name="email"]').val()
    data['company'] = $('#registrationModal input[name="company"]').val()
    data['password'] = $('#registrationModal input[name="password"]').val()
    data['re_password'] = $('#registrationModal input[name="re_password"]').val()


    $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('input[name="rand"]').val()}});
    $.ajax({
        url: "/api/clientPostSignUp",
        type: "POST",
        dataType: "json",
        data: data
    }).done(function (result) {
        if (result['status'] == 'success') {
            location.reload();
        } else {
            console.log(result['messages']);
            var alertDiv = $('#registrationModal .registration_alert_div');
            alertDiv.empty();
            alertDiv.addClass('c-alert-box c-alert-box--error');

            $.each(result['messages'], function (index, value) {
                alertDiv.append('<p class="u-ms-1">' + value + '<br>' +
                    '</p>');
            });


        }
    }).fail(function (result) {
//            alert('error');
    })


}



