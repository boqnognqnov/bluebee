$(document).ready(function () {
  $('.news-slider').slick({
    prevArrow: '.c-news-prev',
    nextArrow: '.c-news-next',
    infinite: false,
    speed: 300,
    slide: '.o-grid__item',
    slidesToShow: 3,
    slidesToScroll: 3,
    responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    ]
  });
  $('.events-slider').slick({
    prevArrow: '.c-events-prev',
    nextArrow: '.c-events-next',
    infinite: false,
    speed: 300,
    slide: '.o-grid__item',
    slidesToShow: 3,
    slidesToScroll: 3,
    responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    ]
  });
  $('.downloads-slider').slick({
    prevArrow: '.c-downloads-prev',
    nextArrow: '.c-downloads-next',
    infinite: false,
    speed: 300,
    slide: '.o-grid__item',
    slidesToShow: 3,
    slidesToScroll: 3,
    responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    ]
  });
});

// On edge hit
$('.events-slider').on('init', function (event, slick, direction) {
  console.log('slider has loaded');
  $('.slick-slide').each(function(){
    //(this).height('unset');
  });
});