$(document).ready(function () {
    var element = document.getElementById('.js-truncate');
    var ellipsis = new Ellipsis(element);

    ellipsis.calc();
    ellipsis.set();
});