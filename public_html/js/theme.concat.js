var chopstick =
{
    // init, something like a constructor
    init: function()
    {
        chopstick.loadObject(chopstick.mobileNav, 'chopstick.mobileNav');
        chopstick.loadObject(chopstick.hide, 'chopstick.hide');
        chopstick.loadObject(chopstick.toggle, 'chopstick.toggle');
        chopstick.loadObject(chopstick.blazy, 'chopstick.blazy');
        chopstick.loadObject(chopstick.matchHeight, 'chopstick.matchHeight');

        console.log("javascript is locked and loaded!") // for testing purposes. Check your console. Delete after you finished reading this. :-)
    },

    /**
     * This function will load an object by a given name
     *
     * If the object doesn't exist no error will be thrown
     * But if object exists but doesn't have an init method it will throw an error
     *
     * If everything is ok we'll initiate the given object
     */
    loadObject: function(obj, name)
    {
        // create object based on a name
        // var objName = window[objName];

        // check if object exists
        if(typeof obj != 'undefined') {

            // check if object has a method init
            if (typeof obj.init == 'undefined') {
                // we will throw an error so the designer / developer know there's a problem
                throw new Error('ERROR: "' + name + '" does not have an init function');

            } else {
                // everything is fine so initiate
                obj.init();
            }
        }
    }
};

var blazySettings
chopstick.blazy =
{
    settings:
    {
        blazySelector: ".js-blazy"
    },

    init: function()
    {
        blazySettings = chopstick.blazy.settings;
        chopstick.bLazy = new Blazy({
            selector: blazySettings.blazySelector
            , successClass: "p-blazy--loaded"
            , breakpoints: [{
                width: 320 // max-width
                , src: 'data-src-small'
            }
            , {
                width: 768 // max-width
                , src: 'data-src-medium'
            }]
            , error: function(ele, msg){
                if(msg === 'missing'){
                    // Data-src is missing
                    //console.log(ele);
                    ele.remove();
                    console.log('element ' + ele + ' failed to load because ' + msg);
                }
                else if(msg === 'invalid'){
                    // Data-src is invalid
                    //console.log(ele);
                    ele.remove();
                    console.log('element ' + ele + ' failed to load because ' + msg);
                }  
            }
        });

    }
};

var hideSettings
chopstick.hide =
{
    settings:
    {
        hideToggle: $('.js-hide')
    },

    init: function()
    {
        // Initialize hide settings
        hideSettings = chopstick.hide.settings;
        // Bind hide events
        chopstick.hide.bindUIEvents();
    },

    bindUIEvents: function()
    {
        // Bind show hide event
        hideSettings.hideToggle.on('touchstart click', function(e){
            var trigger = $(this);
            // Check if action needs to be prevented
            if (trigger.data("action") == "none") {
                e.preventDefault();
            }
            chopstick.hide.showHide(trigger.data("target-selector"));
        });
    },

    showHide: function(targets)
    {
        //  add the 'is-hidden' class
        $(targets).addClass('is-hidden');
    }
};

var matchHeightSettings
chopstick.matchHeight =
{
    settings:
    {
        matchHeightItem: $('.js-match-height')
    },

    init: function()
    {
        matchHeightSettings = chopstick.matchHeight.settings;
        chopstick.matchHeight.matchHeightContent();
    },

    matchHeightContent: function ()
    {
        matchHeightSettings.matchHeightItem.matchHeight();
    }
};

var mobileNavSettings
chopstick.mobileNav =
{
    settings:
    {
        navigation: $('.js-nav'),
        trigger: $('.js-nav-trigger')
    },

    init: function()
    {
        // Initialize mobile nav settings
        mobileNavSettings = chopstick.mobileNav.settings;
        // Bind toggle events
        chopstick.mobileNav.bindUIEvents();
    },

    bindUIEvents: function()
    {
        mobileNavSettings.trigger.on('click', function() {
            chopstick.mobileNav.toggleNavigation();
        });
    },

    // build mobile nav
    toggleNavigation: function()
    {
        mobileNavSettings.navigation.toggleClass('is-visible');
        mobileNavSettings.trigger.toggleClass('is-active');
    }
};

var toggleSettings
chopstick.toggle =
{
    settings:
    {
        showHideToggle: $('.js-show-hide')
    },

    init: function()
    {
        // Initialize toggle settings
        toggleSettings = chopstick.toggle.settings;
        // Bind toggle events
        chopstick.toggle.bindUIEvents();
    },

    bindUIEvents: function()
    {
        // Bind show hide event
        toggleSettings.showHideToggle.on('touchstart click', function(e){
            var trigger = $(this);
            // Check if action needs to be prevented
            if (trigger.data("action") == "none") {
                e.preventDefault();
            }
            chopstick.toggle.showHide(trigger.data("target-selector"));
            trigger.toggleClass('is-toggled');
        });
    },

    showHide: function(targets)
    {
        //  Toggle the 'is-hidden' class
        $(targets).toggleClass('is-hidden');
    }
};

$(chopstick.init);
