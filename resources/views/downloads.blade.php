@extends( 'master' )

@section( 'content' )
    @if( $pageData['raw']['head_is_on']==true)
        @include('components.intro', $pageData['intro'])
    @endif
    @if(Session::has('fectiveSuccess'))
        <input name="fectId" type="hidden" value="{!! Session::get('fectiveSuccess') !!}">
    @endif
    <div class="c-row c-row-md u-bgcolor-neutral-xxx-light" id="list-view">
        <div class="c-row u-bgcolor-neutral-xxx-light">
            <div class="o-container u-z-beta">
                <div class="o-grid o-grid--gutter">
                    <div class="o-grid__item u-1-of-5-bp4 u-push-1-of-5-bp4"><a
                                class="c-button c-button--ghost-alpha c-button--lg c-button--block"
                                href="{!! url('/news/list') !!}#list-view" role="button">News</a></div>
                    <div class="o-grid__item u-1-of-5-bp4 u-push-1-of-5-bp4"><a
                                class="c-button c-button--ghost-beta c-button--lg c-button--block"
                                href="{!! url('/events/list') !!}#list-view" role="button">Events</a></div>
                    <div class="o-grid__item u-1-of-5-bp4 u-push-1-of-5-bp4"><a
                                class="c-button c-button--gamma c-button--lg c-button--block"
                                href="{!! url('/downloads/list') !!}#list-view" role="button">Downloads</a></div>
                </div>
            </div>
        </div>
    </div>
    <div class="c-row c-row--md u-bgcolor-neutral-xxx-light">
        {{--<div class="c-background-visual c-background-visual--grey c-background-visual--br u-opacity-50 u-z-alpha"></div>--}}
        <div class="o-container u-z-beta">
            <div class="o-grid o-grid--gutter">
                <div class="o-grid__item u-4-of-6-bp3">
                    @foreach($pageData['downloadList'] as $oneDownload)
                        @include('components.single-download',['oneDownload'=>$oneDownload,'currentDataCardId'=>$pageData['currentDataCardId']])
                    @endforeach

                    {{--@include('components.pagination')--}}
                    <div class="pagination-wrapper">{!! $pageData['downloadList'] -> render() !!}</div>
                </div>
                <div class="o-grid__item o-sidebar u-2-of-6-bp3">
                    @include('sections.sidebar-downloads',['sidebar'=>$pageData['sidebar']])
                </div>
            </div>
        </div>
    </div>
    @if($pageData['raw']['sign_up_is_on']==true)
        @include('components.cta-newsletter',$pageData['newsletter'])
    @endif


@endsection

@section( 'view-scripts' )

    <script>

        $(document).ready(function () {
            @if($errors -> any() )
            $('#fectModal').modal();
            @endif


                         $('.fModalClass').on('click', function (event) {

//
                var id = $(this).attr('data_file_id');
                $('.fectMod input[name="file_id"]').val(id);
            });

            @if(Session::has('fectiveSuccess'))

                    window.location.href = "../../../downloads/download/file/" + $('input[name = "fectId"]').val();

            @endif


        });


    </script>

@endsection
