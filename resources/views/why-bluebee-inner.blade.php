@extends( 'master' )

@section( 'content' )
    @if($pageData['raw']['head_is_on']==true)
        @include('components.intro', $pageData['intro'])
    @endif
    @if(isset($pageData['items']))
        @foreach($pageData['items'] as $key=>$item)
            @if($key%2!=0 && !empty($item['image']))
                @include('components.benefit-media-left', $item)

            @elseif($key%2==0 && !empty($item['image']))
                @include('components.benefit-media-right', $item)

            @else
                @include('components.featuresItem', $item)

            @endif
        @endforeach
    @endif

    @if($pageData['raw']['favorites_is_on']==true)
        <div class="c-row u-bgcolor-neutral-xxx-light">
            <div class="o-container u-z-beta">
                @include('components.cards-with-label',['favId'=>$pageData['favoriteId']])
            </div>
        </div>
    @endif
    @if($pageData['raw']['opinion_is_on']==true)
        @include('components.quote',$pageData['quote'])
    @endif
    @if($pageData['raw']['try_is_on']==true)
        @include('components.cta-trial',$pageData['trial'])
    @endif
@endsection

@section( 'view-scripts' )

@endsection