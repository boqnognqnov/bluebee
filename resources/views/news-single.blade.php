@extends( 'master' )

@section( 'content' )
    @if($pageData['raw']['head_is_on']==true)
        {{--@include('components.intro', ['title'=>'News','body'=>'','button'=>'','breadcrumb'=>'','background'=>['default'=>'/uploads/images/pages/news/childPage/default/1459107617.jpg','small'=>'/uploads/images/pages/news/childPage/default/1459107617.jpg','medium'=>'/uploads/images/pages/news/childPage/default/1459107617.jpg']])--}}
        @include('components.intro',  $pageData['intro'])
    @endif
    <div class="c-row c-row-md u-bgcolor-neutral-xxx-light">
        <div class="c-row u-bgcolor-neutral-xxx-light">
            <div class="o-container">
                <div class="o-grid o-grid--gutter">
                    <div class="o-grid__item u-1-of-5-bp4 u-push-1-of-5-bp4"><a
                                class="c-button c-button--ghost-alpha c-button--lg c-button--block"
                                href="{!! url('news/list') !!}" role="button">News</a></div>
                    <div class="o-grid__item u-1-of-5-bp4 u-push-1-of-5-bp4"><a
                                class="c-button c-button--ghost-beta c-button--lg c-button--block"
                                href="{!! url('events/list') !!}"
                                role="button">Events</a></div>
                    <div class="o-grid__item u-1-of-5-bp4 u-push-1-of-5-bp4"><a
                                class="c-button c-button--ghost-gamma c-button--lg c-button--block"
                                href="{!! url('downloads/list') !!}"
                                role="button">Downloads</a></div>
                </div>
            </div>
        </div>
    </div>
    <div class="c-row c-row--md u-bgcolor-neutral-xxx-light">
        {{--<div class="c-background-visual c-background-visual--grey c-background-visual--br u-opacity-50 u-z-alpha"></div>--}}
        <div class="o-container u-z-beta">
            <div class="o-grid o-grid--gutter">
                <div class="o-grid__item u-4-of-6-bp3">
                    <div class="c-main-section s-content">
                        <div class="u-align-horizontal">
                            <p class="u-ms-2 u-text-uppercase u-fw-beta u-mb-beta u-color-neutral-light">
                                {{--January 4, 2016--}}
                                {!! \App\Classes\GlobalFunctions::getStampToStrFullMonth($pageData['newsData']['created_at']) !!}
                                {{--{!! \App\Classes\GlobalFunctions::getStampToStrFullMonth($oneNews['created_at']) !!}--}}
                            </p>

                            <h1>
                                {!! $pageData['newsData']['title_'.\App::getLocale()] !!}
                            </h1>
                        </div>
                        <!-- change width with utility classes u-max-width-... -->
                        <div class="u-align-horizontal">
                            @if(!empty($pageData['newsData']['image']))
                                <p class="u-mb-jota">
                                    <img class="p-lazy p-lazy--preview js-blazy"
                                         src="{!! asset(\App\NewsItemsEntity::$path.$pageData['newsData']['image']) !!}"
                                         data-src="{!! asset(\App\NewsItemsEntity::$pathMedium.$pageData['newsData']['image']) !!}"
                                         data-src-small="{!! asset(\App\NewsItemsEntity::$pathSmall.$pageData['newsData']['image']) !!}"
                                         alt="alt text"
                                    />
                                </p>
                            @endif

                            <p>
                                {!! $pageData['newsData']['text_'.\App::getLocale()] !!}
                            </p>
                        </div>
                        <div class="u-align-horizontal">
                            <div class="o-grid o-grid--gutter">
                                <div class="o-grid__item u-6-of-6-bp3">
                                    <ul class="o-list o-list--horizontal c-social-links c-social-links__inv">
                                        <li class="o-list__item c-social-links__item"><span
                                                    class="u-ms-2 u-color-neutral-base u-fw-beta share-help-text">share via</span>
                                        </li>
                                        <li class="o-list__item c-social-links__item"><a
                                                    class="c-social-links__link c-button--ghost-neutral"
                                                    href="https://www.facebook.com/sharer/sharer.php?u={!! Request::url() !!}"
                                                    target="_blank"><span aria-hidden="true"
                                                                          class="c-icon c-icon--facebook u-color-neutral-base"></span><span
                                                        class="is-accessible-hidden"> Facebook</span></a></li>
                                        <li class="o-list__item c-social-links__item"><a
                                                    class="c-social-links__link c-button--ghost-neutral"
                                                    href="https://twitter.com/intent/tweet?text={!! $pageData['newsData']['title_'.\App::getLocale()] !!}&url={!! Request::url() !!}&via=bluebeegenomics"
                                                    target="_blank"><span aria-hidden="true"
                                                                          class="c-icon c-icon--twitter u-color-neutral-base"></span><span
                                                        class="is-accessible-hidden"> Twitter</span></a></li>
                                        <li class="o-list__item c-social-links__item"><a
                                                    class="c-social-links__link c-button--ghost-neutral"
                                                    href="https://www.linkedin.com/shareArticle?mini=true&url={!! Request::url() !!}"
                                                    target="_blank"><span aria-hidden="true"
                                                                          class="c-icon c-icon--linkedin u-color-neutral-base"></span><span
                                                        class="is-accessible-hidden"> LinkedIn</span></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if(sizeof($pageData['comments'])>0)
                        <div class="u-align-horizontal u-mt-delta">
                            <h3 class="o-comments__heading u-color-alpha-dark">Comments</h3>
                            @foreach($pageData['comments'] as $oneComment)
                                <article class="o-media u-mt-delta u-pt-delta">
                                    <div class="o-grid o-grid--gutter">
                                        <div class="o-media__img o-grid__item u-1-of-5-bp3">
                                            <div class="u-circle">
                                                <img src="{!! asset(\App\User::$avatar_path.\App\Classes\GlobalFunctions::getUserAvatar($oneComment['user_id'])) !!}">
                                            </div>
                                        </div>
                                        <div class="o-media__body o-grid__item u-4-of-6-bp3">
                                            <h3 class="o-comments__name u-color-beta-base u-fw-gamma u-ms1 u-no-letter-spacing">
                                                {!! $oneComment['name'] !!}</h3>

                                            <p class="u-ms-1 u-fw-gamma u-color-neutral-base">
                                                {!! $oneComment['text'] !!}
                                            </p>
                                            <ul class="o-list o-list--horizontal c-social-links u-align-left">
                                                <li class="o-list__item c-social-links__item"><a
                                                            class="c-social-links__link c-button--ghost-neutral"
                                                            href="https://www.linkedin.com/shareArticle?mini=true&url={!! Request::url() !!}"><span
                                                                aria-hidden="true"
                                                                class="c-icon c-icon--facebook"></span><span
                                                                class="is-accessible-hidden"> Facebook</span></a></li>
                                                <li class="o-list__item c-social-links__item"><a
                                                            class="c-social-links__link c-button--ghost-neutral"
                                                            href="https://twitter.com/intent/tweet?text=via+%40bluebeegenomics%3a&url={!! Request::url() !!}"><span
                                                                aria-hidden="true"
                                                                class="c-icon c-icon--twitter"></span><span
                                                                class="is-accessible-hidden"> Twitter</span></a></li>
                                                <li class="o-list__item c-social-links__item"><a
                                                            class="c-social-links__link c-button--ghost-neutral"
                                                            href="https://www.facebook.com/sharer/sharer.php?u={!! Request::url() !!}"><span
                                                                aria-hidden="true"
                                                                class="c-icon c-icon--linkedin"></span><span
                                                                class="is-accessible-hidden"> LinkedIn</span></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </article>
                            @endforeach
                        </div>
                    @endif
                </div>
                <div class="o-grid__item o-sidebar u-2-of-6-bp3">
                    @include('sections.sidebar',['sidebar'=>$pageData['sidebar']])
                </div>
            </div>
        </div>
    </div>
    @if(\Auth::user())
        <div class="c-row c-row--md u-bgcolor-neutral-xx-light">
            <div class="o-container u-z-beta">
                <div class="o-grid o-grid--gutter">
                    <div class="o-grid__item u-4-of-6-bp3">
                        <div class="c-main-section">
                            {{--<div class="u-mb-jota">
                                <div class="o-container">
                                    <div class="o-grid o-grid--gutter">
                                        <div class="o-grid__item u-1-of-2-bp3">
                                            <div class="c-main-section">
                                                <a href="#"
                                                   class="o-postnav u-color-gamma-x-dark o-link__noline u-ms-3 u-fw-gamma u-text-uppercase u-no-letter-spacing">Previous
                                                    post</a>
                                            </div>
                                        </div>
                                        <div class="o-grid__item u-1-of-2-bp3">
                                            <div class="c-main-section u-align-right">
                                                <a href="#"
                                                   class="o-postnav u-color-gamma-x-dark o-link__noline u-ms-3 u-fw-gamma u-text-uppercase u-no-letter-spacing">Next
                                                    post</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>--}}
                            <div class="u-mt-jota u-mb-jota">
                                <div class="c-main-section u-align-center">
                                    <h4 class="o-postnav u-color-neutral-dark u-fw-gamma u-text-uppercase u-no-letter-spacing">
                                        Leave a comment</h4>
                                </div>
                            </div>

                            @if(Session::has('successMessage'))
                                <div class="c-alert-box c-alert-box--success">
                                    <p class="u-ms-1">
                                        <span>{!! Session::get('successMessage') !!}</span>
                                    </p>
                                </div>
                            @endif

                            @if($errors -> any() )
                                <div class="c-alert-box c-alert-box--error">
                                    <p class="u-ms-1">
                                        @foreach ($errors->all() as $error)
                                            <span>{{ $error }}</span>
                                        @endforeach
                                    </p>
                                </div>
                            @endif

                            {!! Form::open(array('action'=>'CNewsController@postComment','method'=>'post')) !!}
                            {!! Form::hidden('news_id',$pageData['newsData']['id']) !!}
                            <div class="u-mt-gamma">
                                <label for="message" class="c-label">Message:<sup>*</sup></label>
                            <textarea id="message" name="message" class="c-input-text c-input-text--md"
                                      rows="5"></textarea>
                            </div>
                            <div class="u-mt-gamma">
                                <div class="o-container">
                                    <div class="o-grid o-grid--gutter">
                                        <div class="o-grid__item u-1-of-2-bp3">
                                            <div class="c-main-section">
                                                <label for="name" class="c-label">Name:<sup>*</sup></label>
                                                {{--<input class="c-input-text c-input-text--md" type="text" name="name"--}}
                                                {{--id="name" placeholder="Full Name"--}}
                                                {{--value="{!! @$pageData['raw']['userData']['name'] !!}"--}}
                                                {{--@if(!empty(@$pageData['raw']['userData']['name']))disabled @endif>--}}

                                                @if(!empty(@$pageData['raw']['userData']['name']))
                                                    {!! Form::text('name',@$pageData['raw']['userData']['name'],['placeholder'=>'Full Name','class'=>'c-input-text c-input-text--md','id'=>'name','readonly'=>'readonly']) !!}
                                                @else
                                                    {!! Form::text('name',@$pageData['raw']['userData']['name'],['placeholder'=>'Full Name','class'=>'c-input-text c-input-text--md','id'=>'name']) !!}
                                                @endif


                                            </div>
                                        </div>
                                        <div class="o-grid__item u-1-of-2-bp3">
                                            <div class="c-main-section">
                                                <label for="email" class="c-label">E-mail:<sup>*</sup></label>
                                                {{--<input class="c-input-text c-input-text--md" type="email" name="email"--}}
                                                {{--id="email" placeholder="email&#64;address.com"--}}
                                                {{--value="{!! @$pageData['raw']['userData']['email'] !!}"--}}
                                                {{--@if(!empty(@$pageData['raw']['userData']['email']))disabled @endif>--}}

                                                @if(!empty(@$pageData['raw']['userData']['email']))
                                                    {!! Form::text('email',@$pageData['raw']['userData']['email'],['placeholder'=>'email&#64;address.com','class'=>'c-input-text c-input-text--md','id'=>'email','readonly'=>'readonly']) !!}
                                                @else
                                                    {!! Form::text('email',@$pageData['raw']['userData']['email'],['placeholder'=>'email&#64;address.com','class'=>'c-input-text c-input-text--md','id'=>'email']) !!}
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="u-mt-jota">
                                <input class="c-button c-button--lg c-button--gamma" value="Post a comment"
                                       type="submit">
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    @if($pageData['raw']['favorites_is_on']==true)
        <div class="c-row c-row--sm u-bgcolor-neutral-xxxx-light">
            <div class="c-background-visual c-background-visual--grey c-background-visual--br u-opacity-50 u-z-alpha"></div>
            <div class="o-container u-z-beta">
                <div class="o-grid o-grid--gutter">
                    <div class="o-grid__item u-6-of-6-bp3">
                        <div class="c-main-section s-content">
                            <div class="u-align-horizontal">
                                <h3 class="o-related__heading u-color-alpha-dark u-mb-jota">Related articles</h3>


                                <div class="c-row u-bgcolor-neutral-xxx-light">
                                    <div class="o-container u-z-beta">
                                        @include('components.cards-with-label',['favId'=>$pageData['favoriteId']])
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    @if($pageData['raw']['sign_up_is_on']==true)
        @include('components.cta-newsletter',$pageData['newsletter'])
    @endif
@endsection

@section( 'view-scripts' )

@endsection


@section( 'title_section',              strip_tags ($pageData['newsData']['title_'.\App::getLocale()] ) . ' | News | Bluebee')
@section( 'facebook_title',             strip_tags ($pageData['newsData']['title_'.\App::getLocale()] ))

@section( 'facebook_description',trim(preg_replace('/\s\s+/', ' ', html_entity_decode(strip_tags ($pageData['newsData']['text_'.\App::getLocale()] )))))


@section( 'meta_description_section',  strip_tags ( $pageData['newsData']['text_'.\App::getLocale()] ))
@section( 'facebook_image',             asset(\App\NewsItemsEntity::$pathMedium.$pageData['newsData']['image']) )
@section( 'facebook_url',               url('news/'.$pageData['newsData']['slug_'.\App::getLocale()]))

{{--@section( 'meta_keywords_section',      $productsData -> keywords )--}}
