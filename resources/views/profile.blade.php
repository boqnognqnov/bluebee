@extends( 'master' )

@section( 'content' )
    @include('components.intro', ['title'=>'My profile','body'=>'','button'=>'','background'=>['default'=>'img/photos/intro/2400/lab-4.jpg','small'=>'img/photos/intro/600/lab-4.jpg','medium'=>'img/photos/intro/1200/lab-4.jpg']])
    <div class="c-row c-row--md c-row--border-top u-bgcolor-neutral-xxx-light u-pb-jota u-mb-neg-beta">
        <div class="u-align-horizontal u-max-width-md c-main-section s-content">

            <h2>
                Personal information
            </h2>
            @if(Session::has('successMessage'))
                <div class="c-alert-box c-alert-box--success">
                    <p class="u-ms-1">
                        <span>{!! Session::get('successMessage') !!}</span>
                    </p>
                </div>
            @endif

            @if($errors -> any() )
                <div class="c-alert-box c-alert-box--error">
                    <p class="u-ms-1">
                        @foreach ($errors->all() as $error)
                            <span>{{ $error }}</span>
                        @endforeach
                    </p>
                </div>
            @endif
            <fieldset>
                {!! Form::open(array('action'=>'CProfileController@updateProfile','method'=>'post','files'=>true)) !!}

                {!! Form::hidden('id',@$userData['id']) !!}
                {!! Form::hidden('req_client','req_client') !!}

                {{--{!!  Form::open(array('url' => 'profile/update_profile', 'method' => 'get')) !!}--}}


                <div class="o-grid o-grid--gutter">
                    <div class="o-grid__item u-1-of-2-bp3">
                        <p>
                          <label for="pf-name" class="c-label">Name<abbr title="Required field">*</abbr></label>
                          {{--<input type="text" id="pf-name" class="c-input-text c-input-text--md" placeholder=""/>--}}
                          {!! Form::text('name',@$userData['name'],['class'=>'c-input-text c-input-text--md','id'=>'pf-name']) !!}
                        </p>
                        <p>
                          <label for="pf-email" class="c-label">Email address<abbr
                            title="Required field">*</abbr></label>

                            @if($userData['account_type']=='linkedin' || $userData['account_type']=='facebook')
                              {!! Form::text('email',@$userData['email'],['class'=>'c-input-text c-input-text--md','id'=>'pf-email','readonly']) !!}
                            @else
                              @if(is_numeric($userData['email']))
                                {!! Form::text('email',null,['class'=>'c-input-text c-input-text--md','id'=>'pf-email']) !!}
                              @else
                                {!! Form::text('email',@$userData['email'],['class'=>'c-input-text c-input-text--md','id'=>'pf-email']) !!}
                              @endif

                            @endif
                          </p>
                          <p>
                            <label for="pf-country" class="c-label">Country<abbr title="Required field">*</abbr></label>
                            {!! Form::text('country',@$userData['country'],['class'=>'c-input-text c-input-text--md','id'=>'pf-country']) !!}
                          </p>
                    </div>
                    <div class="o-grid__item u-1-of-2-bp3">
                        <p>
                          <label for="pf-fburl" class="c-label">Facebook URL</label>
                          {!! Form::text('facebook_url',@$userData['facebook_url'],['class'=>'c-input-text c-input-text--md','id'=>'pf-fburl']) !!}
                        </p>
                        <p>
                          <label for="pf-twurl" class="c-label">Twitter URL</label>
                          {!! Form::text('twitter_url',@$userData['twitter_url'],['class'=>'c-input-text c-input-text--md','id'=>'pf-twurl']) !!}
                        </p>
                        <p>
                          <label for="pf-liurl" class="c-label">LinkedIn URL</label>
                          {!! Form::text('linkedin_url',@$userData['linkedin_url'],['class'=>'c-input-text c-input-text--md','id'=>'pf-liurl']) !!}
                        </p>
                    </div>
                </div>
                <div class="o-grid o-grid--gutter">
                    <div class="o-grid__item u-1-of-3-bp3">
                      <p>
                        <label for="pf-company" class="c-label">Company<abbr
                          title="Required field">*</abbr></label>
                          {!! Form::text('company_name',@$userData['company_name'],['class'=>'c-input-text c-input-text--md','id'=>'pf-company']) !!}
                        </p>
                    </div>
                    <div class="o-grid__item u-1-of-3-bp3">
                      <p>
                        <label for="pf-job" class="c-label">Job title<abbr title="Required field">*</abbr></label>
                        {!! Form::text('job_position',@$userData['job_position'],['class'=>'c-input-text c-input-text--md','id'=>'pf-job']) !!}
                      </p>
                    </div>
                    <div class="o-grid__item u-1-of-3-bp3">
                      <p>
                        <label for="pf-phone" class="c-label">Phone<abbr title="Required field">*</abbr></label>
                        {!! Form::tel('phone',@$userData['phone'],['class'=>'c-input-text c-input-text--md','id'=>'pf-phone']) !!}
                      </p>
                    </div>
                </div>
                <div class="o-grid o-grid--gutter">
                    <div class="o-grid__item u-1-of-2-bp3">
                      <label for="pf-photo" class="c-label">Use photo from</label>
                      <div class="o-grid o-grid--gutter">
                        <div class="o-grid__item u-1-of-3-bp4">
                          <a href="{!! url('facebook/authorize/profile/photo') !!}"
                          class="c-button c-button--alpha c-button--md c-button--block c-button--social c-button--facebook"><span
                          class="c-icon--facebook-f"></span> Facebook</a>
                        </div>
                        <div class="o-grid__item u-1-of-3-bp4">
                          <a href="{!! url('twitter/authorize/profile/photo') !!}"
                          class="c-button c-button--alpha c-button--md c-button--block c-button--social c-button--twitter"><span
                          class="c-icon--twitter"></span> Twitter</a>
                        </div>
                        <div class="o-grid__item u-1-of-3-bp4">
                          <a href="{!! url('linkedin/authorize/profile/photo') !!}"
                          class="c-button c-button--alpha c-button--md c-button--block c-button--social c-button--linkedin"><span
                          class="c-icon--linkedin"></span> LinkedIn</a>
                        </div>
                      </div>
                    </div>
                    <div class="o-grid__item u-1-of-2-bp3">
                      <label for="pf-photo" class="c-label">Upload photo</label>
                      {!! Form::file('image_avatar',['class'=>'c-input-text c-input-text--md','id'=>'pf-photo-file']) !!}
                    </div>
                </div>
                <div class="o-grid o-grid--gutter u-mt-jota">
                    <div class="o-grid__item u-1-of-2-bp3">
                        <p>
                            <label for="pf-oldpass" class="c-label">Old password</label>
                            {!! Form::password('old_password',['class'=>'c-input-text c-input-text--md','id'=>'pf-oldpass']) !!}
                        </p>
                        <p>
                            <label for="pf-newpass1" class="c-label">New password</label>
                            {!! Form::password('new_password',['class'=>'c-input-text c-input-text--md','id'=>'pf-newpass1']) !!}
                        </p>
                        <p>
                            <label for="pf-newpass2" class="c-label">Repeat new password</label>
                            {!! Form::password('re_new_password',['class'=>'c-input-text c-input-text--md','id'=>'pf-newpass2']) !!}
                        </p>
                    </div>
                    <div class="o-grid__item u-1-of-2-bp3 u-align-right">
                        {{--<img src="http://placehold.it/396x290" alt="">--}}
                        <img src="{!! \App\User::$avatar_path.@$userData['image_avatar'] !!}" alt="" height="290px">

                    </div>
                </div>
                <div class="o-grid o-grid--gutter u-mt-gamma u-mb-jota">
                    <p class="o-grid__item u-1-of-2-bp3 u-align-left js-match-height">
                        <input type="checkbox" class="css-checkbox" id="pf-checkbox"/> <label class="css-label"
                                                                                              for="pf-checkbox">Subscribe
                            to our newsletter</label>
                    </p>
                    <p class="o-grid__item u-1-of-2-bp3 u-align-right js-match-height">
                        {!! Form::submit('Send',['class'=>'c-button c-button--md c-button--beta u-mb-beta']) !!}
                    </p>
                </div>
                {!! Form::close() !!}

                <div class="o-grid o-grid--gutter">
                    <div class="o-grid__item u-2-of-2-bp3">
                        <table class="o-table-responsive o-table-files">

                            <tr class="u-bgcolor-neutral-xx-light">
                                <th class="u-color-neutral-base u-ms-2 u-fw-beta u-pt-gamma u-pb-gamma">File name
                                </th>
                                <th class="u-color-neutral-base u-ms-2 u-fw-beta u-pt-gamma u-pb-gamma">Time &amp;
                                    date
                                </th>
                            </tr>

                            @foreach($downloadHistory as $key=>$oneFile)
                                <tr class="u-bgcolor-neutral-xxxx-light">
                                    <td class="u-pt-delta u-pb-beta">
                                        <span class="c-icon c-icon--file-empty u-color-neutral-base"></span>
                                        <?php $obj = \App\DownloadsItemsEntity::getFileObj($oneFile['user_download_id']) ?>

                                        @if($currentDataCardId==$obj['id'])
                                            <a
                                                    class="u-color-neutral-base u-ms-1 o-link__noline u-fw-gamma"
                                                    href="{!! url('pipelines#pipeid'.$obj['id']) !!}"
                                                    target="_blank">
                                                {!! $key+1 !!}. {!! $obj['title_'.\App::getLocale()] !!}</a>
                                        @else
                                            <a
                                                    class="u-color-neutral-base u-ms-1 o-link__noline u-fw-gamma"
                                                    href="{!! url('downloads/paginate/'.$obj['id'].'#file_item_'.$obj['id']) !!}"
                                                    target="_blank">

                                                {!! $key+1 !!}. {!! $obj['title_'.\App::getLocale()] !!}</a>

                                        @endif
                                    </td>
                                    <td class="u-color-neutral-base u-ms-1 u-fw-gamma u-pt-delta u-pb-beta"><span
                                                class="u-color-neutral-light">{!! \App\Classes\GlobalFunctions::getTimestamToTimeOfDayStr($oneFile['created_at']) !!}</span>
                                        /{!! \App\Classes\GlobalFunctions::getStampToStrFullMonth($oneFile['created_at']) !!}
                                        /
                                    </td>

                                </tr>
                            @endforeach

                        </table>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
@endsection

@section( 'view-scripts' )
    <script src="{!! asset('vendor/tablesaw/dist/tablesaw.js') !!}"></script>
    <script src="{!! asset('vendor/tablesaw/dist/tablesaw-init.js') !!}"></script>
@endsection
