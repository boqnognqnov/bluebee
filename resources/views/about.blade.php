@extends( 'master' )

@section( 'content' )
    @if( $pageData['raw']['head_is_on']==true)
        @include('components.intro', $pageData['intro'])
    @endif

    <div class="c-row--md c-row--border-top u-bgcolor-neutral-xxx-light c-main-section s-content u-pb-gamma">
        <!-- change width with utility classes u-max-width-... -->
        <div class="u-align-horizontal u-max-width-sm">
            <h1 class="u-align-center">
                {!! $pageData['raw']['intro_title_'.\App::getLocale()]  !!}
            </h1>

            <p class="u-align-center">
                {!! $pageData['raw']['intro_text_'.\App::getLocale()]  !!}
            </p>

            <div class="u-pt-gamma u-mb-jota">
                <img class="p-lazy p-lazy--preview js-blazy"
                     src="{!! asset(\App\AboutPageEntity::$path.$pageData['raw']['intro_image']) !!}"
                     data-src="{!! asset(\App\AboutPageEntity::$pathMedium.$pageData['raw']['intro_image']) !!}"
                     data-src-small="{!! asset(\App\AboutPageEntity::$pathSmall.$pageData['raw']['intro_image']) !!}"
                     alt="alt text"
                        />
                <!-- <p class="u-ms-1 u-color-neutral-base u-align-center u-pt-gamma">
                  image caption Lorem ipsum dolor sit amet.
                </p> -->
            </div>
        </div>
    </div>
    @if(!empty($pageData['raw']['services_title_'.\App::getLocale()] && !empty($pageData['raw']['services_text_'.\App::getLocale()])))
        <div class="c-row--md c-row--border-top u-bgcolor-neutral-xxx-light u-pb-gamma c-main-section s-content">
            <div class="u-align-horizontal u-max-width-sm">

                <div class="u-pt-gamma u-mb-jota">
                    <h2>{!! $pageData['raw']['services_title_'.\App::getLocale()]  !!}</h2>
                    {{--<ol class="o-list c-list-check">--}}
                    {{--<li class="c-list-check__item">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti,--}}
                    {{--reprehenderit!--}}
                    {{--</li>--}}
                    {{--<li class="c-list-check__item">Lorem ipsum dolor sit amet, consectetur adipisicing.</li>--}}
                    {{--<li class="c-list-check__item">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo,--}}
                    {{--architecto id!--}}
                    {{--</li>--}}
                    {{--<li class="c-list-check__item">Lorem ipsum dolor sit amet, consectetur.</li>--}}
                    {{--</ol>--}}

                    {!! $pageData['raw']['services_text_'.\App::getLocale()]  !!}
                    @if(!empty($pageData['raw']['intro_button_'.\App::getLocale()]))
                        <div class="u-align-right"><a class="c-button c-button--gamma c-button--md u-mt-beta"
                                                      href="{!! $pageData['raw']['intro_button_ref'] !!}"
                                                      role="button">{!! $pageData['raw']['intro_button_'.\App::getLocale()]  !!}</a>
                        </div>
                    @endif
                </div>

            </div>
        </div>
    @endif
    @if(!empty($pageData['raw']['achievement_title_'.\App::getLocale()]) && !empty($pageData['raw']['achievement_text_'.\App::getLocale()])))
    <div class="c-row--md c-row--border-top u-bgcolor-neutral-xxx-light u-pb-gamma c-main-section s-content">
        <div class="u-align-horizontal u-max-width-sm">
            <div class="u-pt-gamma u-mb-jota">
                <h2>{!! $pageData['raw']['achievement_title_'.\App::getLocale()]  !!}</h2>

                {!! $pageData['raw']['achievement_text_'.\App::getLocale()]  !!}
                {{--<table summary="This is a simple responsive table" class="o-table-responsive">--}}
                {{--<!-- <caption>This is a simple responsive table</caption> -->--}}
                {{--<thead>--}}
                {{--<tr>--}}
                {{--<th id="year">Year</th>--}}
                {{--<th id="opponent">Opponent</th>--}}
                {{--<th id="record" abbr="Record">Season Record (W-L)</th>--}}
                {{--</tr>--}}
                {{--</thead>--}}
                {{--<tfoot>--}}
                {{--<tr>--}}
                {{--<td headers="Year">1900</td>--}}
                {{--<td headers="Opponent">Last</td>--}}
                {{--<td headers="Record">00-00</td>--}}
                {{--</tr>--}}
                {{--</tfoot>--}}
                {{--<tbody>--}}
                {{--<tr>--}}
                {{--<td headers="Year">1918</td>--}}
                {{--<td headers="Opponent">Chicago Cubs</td>--}}
                {{--<td headers="Record">75-51</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                {{--<td headers="Year">1915</td>--}}
                {{--<td headers="Opponent">Philadelphia Phillies</td>--}}
                {{--<td headers="Record">101-50</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                {{--<td headers="Year">1918</td>--}}
                {{--<td headers="Opponent">Chicago Cubs</td>--}}
                {{--<td headers="Record">75-51</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                {{--<td headers="Year">1915</td>--}}
                {{--<td headers="Opponent">Philadelphia Phillies</td>--}}
                {{--<td headers="Record">101-50</td>--}}
                {{--</tr>--}}
                {{--</tbody>--}}
                {{--</table>--}}
            </div>
        </div>
    </div>
    @endif

    <div class="c-row--md c-main-section s-content" id="team-section">
        <!-- change width with utility classes u-max-width-... -->
        <div class="u-align-horizontal u-max-width-sm">
            <h2>
                {!! $pageData['raw']['team_title_'.\App::getLocale()]  !!}
            </h2>

            <p>
                {!! $pageData['raw']['team_text_'.\App::getLocale()]  !!}
            </p>

            <div class="u-align-center u-mt-jota u-mb-jota">
                <a class="c-button c-button--ghost-alpha c-button--md u-text-uppercase"
                   href="{!! url('/team/managers') !!}" role="button" style="margin-right: 1rem;">Management team</a>
                <a class="c-button c-button--ghost-alpha c-button--md u-text-uppercase" href="{!! url('/team') !!}"
                   role="button">Advisory board</a>
            </div>
            <div class="u-pt-gamma u-mb-jota">
                <img class="p-lazy p-lazy--preview js-blazy"
                     src="{!! asset(\App\AboutPageEntity::$path.$pageData['raw']['team_image']) !!}"
                     data-src="{!! asset(\App\AboutPageEntity::$pathMedium.$pageData['raw']['team_image']) !!}"
                     data-src-small="{!! asset(\App\AboutPageEntity::$pathSmall.$pageData['raw']['team_image']) !!}"
                     alt="alt text"
                        />
                <!-- <p class="u-ms-1 u-color-neutral-base u-align-center u-pt-gamma">
                  image caption Lorem ipsum dolor sit amet.
                </p> -->
            </div>
        </div>
    </div>

    <div class="c-row c-row--md u-bgcolor-neutral-xxx-light u-pb-delta">
        <div class="u-mb-neg-gamma">
            <div class="u-max-width-md u-align-horizontal">
                <div class="o-container">
                    <div class="o-grid o-grid--gutter">
                        @foreach($pageData['offices'] as $oneOffice)
                            <article class="o-grid__item u-1-of-2-bp3">
                                @include('components.card', ['office'=>$oneOffice])
                            </article>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if( $pageData['raw']['partners_is_on']==true)
        @include('components.partners',['partners'=>$pageData['partners']])
    @endif
    @if( $pageData['raw']['try_is_on']==true)
        @include('components.cta-trial',$pageData['trial'])
    @endif
@endsection

@section( 'view-scripts' )

@endsection