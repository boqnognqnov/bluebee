@extends( 'master' )

@section( 'content' )
    @if($pageData['raw']['head_is_on']==true)
        @include('components.intro', $pageData['intro'])
    @endif
    <div class="c-row c-row--md u-mb-neg-jota u-bgcolor-neutral-xxx-light">
        <div class="o-container">
            <div class="c-main-section c-main-section--lg">
                <div class="u-max-width-sm s-content u-align-horizontal u-align-center">
                    <h1 class="u-mb-gamma">
                        {!! $pageData['bodyTexts']['title'] !!}
                    </h1>
                    <p class="u-color-neutral-base u-ms1">
                        {!! $pageData['bodyTexts']['body'] !!}
                    </p>
                </div>
            </div>
        </div>
        @include('components.solutions',['items'=>$pageData['items']])
    </div>
    @if($pageData['raw']['opinion_is_on']==true)
        @include('components.quote',$pageData['quote'])
    @endif
    @if($pageData['raw']['try_is_on']==true)
        @include('components.cta-trial',$pageData['trial'])
    @endif
@endsection

@section( 'view-scripts' )

@endsection
