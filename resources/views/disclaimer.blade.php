@extends( 'master' )

@section( 'content' )
    @include('components.intro', ['title'=>'Disclaimer','body'=>'','button'=>'','background'=>['default'=>'img/photos/intro/2400/lab-4.jpg','small'=>'img/photos/intro/600/lab-4.jpg','medium'=>'img/photos/intro/1200/lab-4.jpg']])
    <div class="c-row c-row--md c-row--border-top u-bgcolor-neutral-xxx-light u-pb-jota u-mb-neg-beta">
        <div class="u-align-horizontal u-max-width-sm c-main-section s-content">
            {!! $pageData['text_'.\App::getLocale()] !!}
        </div>
    </div>
@endsection

@section( 'view-scripts' )

@endsection
