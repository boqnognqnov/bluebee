@extends( 'master' )

@section( 'content' )
    @if($pageData['raw']['head_is_on']==true)
        @include('components.intro', $pageData['intro'])
    @endif
    <div class="c-row c-row--sm u-bgcolor-neutral-xxxx-light u-mb-neg-jota">
        <div class="o-container">
            @include('components.main', ['page_title'=>$pageData['raw']['title_'.\App::getLocale()],'page_desc'=>$pageData['raw']['text_'.\App::getLocale()]])
        </div>
    </div>
    <div class="c-stepmenu u-bgcolor-neutral-xxxx-light">
        {{--<div class="o-container">--}}
        {{--@include('components.steps', ['steps'=>@$pageData['steps']])--}}
        {{--</div>--}}
        <div class="o-container">
            <div class="c-main-section c-main-section--lg u-mb-neg-gamma">
                <div class="o-grid">
                    <article class="c-step o-grid__item u-1-of-2-bp3 u-1-of-4-bp4 u-mb-gamma">
                        <div class="u-align-center">
                            <a href="#stepOne" class="c-step__number o-link__noline">
                                1
                            </a>

                            <div class="c-step__content">
                                <h2 class="c-card__title u-ms1 u-align-center u-color-alpha-base">
                                    {!! $pageData['steps'][1]['title'] !!}
                                </h2>
                            </div>
                        </div>
                    </article>
                    <article class="c-step o-grid__item u-1-of-2-bp3 u-1-of-4-bp4 u-mb-gamma">
                        <div class="u-align-center">
                            <a href="#stepTwo" class="c-step__number o-link__noline">
                                2
                            </a>

                            <div class="c-step__content">
                                <h2 class="c-card__title u-ms1 u-align-center u-color-alpha-base">
                                    {!! $pageData['steps'][2]['title'] !!}
                                </h2>
                            </div>
                        </div>
                    </article>
                    <article class="c-step o-grid__item u-1-of-2-bp3 u-1-of-4-bp4 u-mb-gamma">
                        <div class="u-align-center">
                            <a href="#stepThree" class="c-step__number o-link__noline">
                                3
                            </a>

                            <div class="c-step__content">
                                <h2 class="c-card__title u-ms1 u-align-center u-color-alpha-base">
                                    {!! $pageData['steps'][3]['title'] !!}
                                </h2>
                            </div>
                        </div>
                    </article>
                    <article class="c-step o-grid__item u-1-of-2-bp3 u-1-of-4-bp4 u-mb-gamma">
                        <div class="u-align-center">
                            <a href="#stepFour" class="c-step__number o-link__noline">
                                4
                            </a>

                            <div class="c-step__content">
                                <h2 class="c-card__title u-ms1 u-align-center u-color-alpha-base">
                                    {!! $pageData['steps'][4]['title'] !!}
                                </h2>
                            </div>
                        </div>
                    </article>
                </div>
            </div>
        </div>
    </div>
    <div class="c-steplist u-bgcolor-neutral-xxx-light">
        <div class="c-row c-row--md">
            <div class="o-container">
                <div class="u-align-horizontal s-content u-max-width-sm">
                    <div id="stepOne" class="scrollTarget u-pb-jota">
                        <h2 class="c-steplist__title u-mb-beta">{!! $pageData['raw']['one_title_' . \App::getLocale()] !!}</h2>

                        <p class="c-steplist__desc">{!! $pageData['raw']['one_text_' . \App::getLocale()] !!}</p>
                        <div class="u-pt-gamma u-mb-jota">
                            @if(!empty($pageData['raw']['one_image']))
                                <div class="c-browser">
                                    <div class="c-browser__content">
                                        <img class="p-lazy p-lazy--preview js-blazy"
                                             src="{!! asset(\App\MethodsEntity::$pathStepsIMG['pathStep1'].$pageData['raw']['one_image']) !!}"
                                             data-src="{!! asset(\App\MethodsEntity::$pathStepsIMG['pathStep1m'].$pageData['raw']['one_image']) !!}"
                                             data-src-small="{!! asset(\App\MethodsEntity::$pathStepsIMG['pathStep1s'].$pageData['raw']['one_image']) !!}"
                                             alt="alt text"/>
                                    </div>
                                </div>

                                <p class="u-ms-1 u-color-neutral-base u-align-center u-pt-gamma">
                                    {{--image caption Lorem ipsum dolor sit amet.--}}
                                </p>
                            @endif
                        </div>
                    </div>

                    <div id="stepTwo" class="scrollTarget u-pb-jota">
                        <h2 class="c-steplist__title u-mb-beta">{!! $pageData['raw']['second_title_' . \App::getLocale()] !!}</h2>

                        <p class="c-steplist__desc">{!! $pageData['raw']['second_text_' . \App::getLocale()] !!}</p>
                        <div class="u-pt-gamma u-mb-jota">
                            @if(!empty($pageData['raw']['second_image']))
                                <div class="c-browser">
                                    <div class="c-browser__content">
                                        <img class="p-lazy p-lazy--preview js-blazy"
                                             src="{!! asset(\App\MethodsEntity::$pathStepsIMG['pathStep2'].$pageData['raw']['second_image']) !!}"
                                             data-src="{!! asset(\App\MethodsEntity::$pathStepsIMG['pathStep2m'].$pageData['raw']['second_image']) !!}"
                                             data-src-small="{!! asset(\App\MethodsEntity::$pathStepsIMG['pathStep2s'].$pageData['raw']['second_image']) !!}"
                                             alt="alt text"/>
                                    </div>
                                </div>

                                <p class="u-ms-1 u-color-neutral-base u-align-center u-pt-gamma">
                                    {{--image caption Lorem ipsum dolor sit amet.--}}
                                </p>
                            @endif
                        </div>
                    </div>

                    <div id="stepThree" class="scrollTarget u-pb-jota">
                        <h2 class="c-steplist__title u-mb-beta">{!! $pageData['raw']['third_title_' . \App::getLocale()] !!}</h2>

                        <p class="c-steplist__desc">{!! $pageData['raw']['third_text_' . \App::getLocale()] !!}</p>
                        <div class="u-pt-gamma u-mb-jota">
                            @if(!empty($pageData['raw']['third_image']))
                                <div class="c-browser">
                                    <div class="c-browser__content">
                                        <img class="p-lazy p-lazy--preview js-blazy"
                                             src="{!! asset(\App\MethodsEntity::$pathStepsIMG['pathStep3'].$pageData['raw']['third_image']) !!}"
                                             data-src="{!! asset(\App\MethodsEntity::$pathStepsIMG['pathStep3m'].$pageData['raw']['third_image']) !!}"
                                             data-src-small="{!! asset(\App\MethodsEntity::$pathStepsIMG['pathStep3s'].$pageData['raw']['third_image']) !!}"
                                             alt="alt text"/>
                                    </div>
                                </div>

                                <p class="u-ms-1 u-color-neutral-base u-align-center u-pt-gamma">
                                    {{--image caption Lorem ipsum dolor sit amet.--}}
                                </p>
                            @endif
                        </div>
                    </div>

                    <div id="stepFour" class="scrollTarget u-pb-jota">
                        <h2 class="c-steplist__title u-mb-beta">{!! $pageData['raw']['fourth_title_' . \App::getLocale()] !!}</h2>

                        <p class="c-steplist__desc">{!! $pageData['raw']['fourth_text_' . \App::getLocale()] !!}</p>
                        <div class="u-pt-gamma u-mb-jota">
                            @if(!empty($pageData['raw']['fourth_image']))
                                <div class="c-browser">
                                    <div class="c-browser__content">
                                        <img class="p-lazy p-lazy--preview js-blazy"
                                             src="{!! asset(\App\MethodsEntity::$pathStepsIMG['pathStep4'].$pageData['raw']['fourth_image']) !!}"
                                             data-src="{!! asset(\App\MethodsEntity::$pathStepsIMG['pathStep4m'].$pageData['raw']['fourth_image']) !!}"
                                             data-src-small="{!! asset(\App\MethodsEntity::$pathStepsIMG['pathStep4s'].$pageData['raw']['fourth_image']) !!}"
                                             alt="alt text"/>
                                    </div>
                                </div>

                                <p class="u-ms-1 u-color-neutral-base u-align-center u-pt-gamma">
                                    {{--image caption Lorem ipsum dolor sit amet.--}}
                                </p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if($pageData['raw']['why_blue_is_on']==true)
        <div class="c-row c-row--md u-bgcolor-neutral-xxxx-light">
            <div class="o-container">
                <div class="c-main-section c-main-section--lg u-mb-neg-gamma">
                    <div class="u-mb-neg-gamma">
                        <div class="o-grid o-grid--gutter">
                            @include('components.sections.benefits',['items'=> @$pageData['benefit']['items']])
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    @if($pageData['raw']['try_is_on']==true)
        @include('components.cta-trial',$pageData['trial'])
    @endif
@endsection

@section( 'view-scripts' )

    {{--<script src="{!! asset('js/affix.js') !!}"></script>
    <script src="{!! asset('js/scrollspy.js') !!}"></script>

    <script>
        // stick menu to top when scrolling past it
        $('.c-stepmenu')
                .affix({
                    offset: {
                        top: function () {
                            return (this.top = $('.c-stepmenu').offset().top)
                        }
                    }
                })
                .on('affixed-top.bs.affix', function () {
                    // bind some custom events here if needed
                })
                .on('affix.bs.affix', function () {
                    // bind some custom events here if needed
                })
        ;

        // add active class to currently visible nav item
        $('.scrollTarget').on('scrollSpy:enter', function () {
            console.log('enter:', $(this).attr('id'));
            //$('.active').removeClass('active');
            $('a[href="#' + $(this).attr('id') + '"]').parents('article').addClass('active');
        });

        $('.scrollTarget').on('scrollSpy:exit', function () {
            console.log('exit:', $(this).attr('id'));
            $('a[href="#' + $(this).attr('id') + '"]').parents('article').removeClass('active');
        });

        $('.scrollTarget').scrollSpy();
    </script>--}}

@endsection