@extends( 'master' )

@section( 'content' )
    @if( $pageData['raw']['head_is_on']==true)
        @include('components.intro',$pageData['intro'])
    @endif
    <div class="c-row c-row--md c-row--border-top u-bgcolor-neutral-xxx-light">
        <!-- change width with utility classes u-max-width-... -->
        <div class="u-align-horizontal u-max-width-sm c-main-section s-content">
            <h2>
                {!!  $pageData['office']['title_'.\App::getLocale()] !!}
            </h2>

            <div class="u-pt-gamma u-mb-jota">
                <img class="p-lazy p-lazy--preview js-blazy"
                     src="{!! asset(\App\OfficesItemsEntity::$path.$pageData['office']['image']) !!}"
                     data-src="{!! asset(\App\OfficesItemsEntity::$pathMedium.$pageData['office']['image']) !!}"
                     data-src-small="{!! asset(\App\OfficesItemsEntity::$pathSmall.$pageData['office']['image']) !!}"
                     alt="alt text"
                        />
                <!-- <p class="u-ms-1 u-color-neutral-base u-align-center u-pt-gamma">
                  image caption Lorem ipsum dolor sit amet.
                </p> -->
            </div>
            <p class="u-color-neutral-base u-ms1">
                {!!  $pageData['office']['text_'.\App::getLocale()] !!}
            </p>

            {{--<h2>--}}
            {{--Ready-to-run pipelines--}}
            {{--</h2>--}}

            {{--<p>--}}
            {{--The rapid evolution in NGS is generating huge amounts of genomics data making it very difficult for the--}}
            {{--clinical user base to handle and interpret that data in a efficient way. The problem is generally--}}
            {{--perceived as a “big data” challenge and is more often than not tackled with ever more hardware used as--}}
            {{--“brute-force” computing and proprietary (instead of generally accepted) algorithms to crunch the--}}
            {{--numbers.--}}
            {{--</p>--}}
        </div>
    </div>
    <div class="c-row c-row--md c-row--border-top u-bgcolor-neutral-xxx-light u-pt-jota">
        <div class="u-align-horizontal u-max-width-sm c-main-section s-content">
            <div class="o-container u-z-beta">
                <div class="o-grid o-grid--gutter">
                    <div class="o-grid__item u-6-of-6-bp3">
                        <h3 class="u-mb-alpha u-mt-jota">Address</h3>

                        <div class="o-list">
                            {{--<div class="o-list__item">Molengraaffsingel 12 - 14 2629 JD Delft</div>--}}
                            {{--<div class="o-list__item">The Netherlands</div>--}}
                            <div class="o-list__item">{!!  $pageData['office']['address_'.\App::getLocale()] !!}</div>

                        </div>
                        <h3 class="u-mb-alpha u-mt-gamma">E-mail</h3>

                        <div class="o-list">
                            <div class="o-list__item">
                                <a href="mailto:{!!  $pageData['office']['email'] !!}">{!!  $pageData['office']['email'] !!}</a>
                            </div>
                        </div>
                        <h3 class="u-mb-alpha u-mt-gamma">Telephone</h3>

                        <div class="o-list">
                            <div class="o-list__item">{!!  $pageData['office']['phone'] !!}</div>
                        </div>

                        <h3 class="u-mb-beta u-mt-jota">Find us on:</h3>
                        <ul class="o-list o-list--horizontal c-social-links">
                            <li class="o-list__item c-social-links__item"><a target="_blank" class="c-social-links__link c-button--ghost-neutral"href="https://www.linkedin.com/company/bluebee?trk=biz-companies-cym"><span aria-hidden="true" class="c-icon c-icon--linkedin"></span><span class="is-accessible-hidden"> LinkedIn</span></a></li>
                            <li class="o-list__item c-social-links__item"><a target="_blank" class="c-social-links__link c-button--ghost-neutral"href="https://twitter.com/BluebeeGenomics"><span aria-hidden="true" class="c-icon c-icon--twitter"></span><span class="is-accessible-hidden"> Twitter</span></a></li>
                            <li class="o-list__item c-social-links__item"><a target="_blank" class="c-social-links__link c-button--ghost-neutral"href="https://www.facebook.com/bluebeegenomics?ref=hl"><span aria-hidden="true" class="c-icon c-icon--facebook-f"></span><span class="is-accessible-hidden"> Facebook</span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="u-pt-jota u-mb-jota">
            @include('components.map',$pageData['office'])
        </div>
    </div>
    @if($pageData['raw']['contact_form_is_on']==true)
        <div class="c-row c-row--md c-row--border-top u-bgcolor-neutral-xxx-light u-pt-jota">
            <div class="u-max-width-sm u-align-horizontal">
                <div class="o-container">
                    <h2>Contact us</h2>

                    @if(Session::has('successMessage'))
                        <div class="c-alert-box c-alert-box--success">
                            <p class="u-ms-1">
                                <span>{!! Session::get('successMessage') !!}</span>
                            </p>
                        </div>
                    @endif

                    @if($errors -> any() )
                        <div class="c-alert-box c-alert-box--error">
                            <p class="u-ms-1">
                            @foreach ($errors->all() as $error)
                                <span>{{ $error }}</span>
                            @endforeach
                            </p>
                        </div>
                    @endif

                    {!! Form::open(array('action'=>'CContactsController@contactMail','method'=>'post','files'=>true)) !!}
                    <fieldset>
                        <div class="o-grid o-grid--gutter">
                            <p class="o-grid__item u-1-of-2-bp3">
                                <label for="contact-first-name" class="c-label">First Name<abbr
                                            title="Required field">*</abbr></label>

                                {!! Form::text( 'firstName',null, array( 'class' => 'c-input-text c-input-text--md','id'=>'contact-first-name' ) ) !!}
                            </p>

                            <p class="o-grid__item u-1-of-2-bp3">
                                <label for="contact-last-name" class="c-label">Last Name<abbr
                                            title="Required field">*</abbr></label>

                                {!! Form::text( 'lastName',null, array( 'class' => 'c-input-text c-input-text--md','id'=>'contact-last-name' ) ) !!}
                            </p>
                        </div>
                        <div class="o-grid o-grid--gutter">
                            <p class="o-grid__item u-2-of-2-bp3">
                                <label for="contact-company" class="c-label">Company<abbr
                                            title="Required field">*</abbr></label>

                                {!! Form::text( 'company',null, array( 'class' => 'c-input-text c-input-text--md','id'=>'contact-company' ) ) !!}
                            </p>
                        </div>
                        <div class="o-grid o-grid--gutter">
                            <p class="o-grid__item u-1-of-2-bp3">
                                <label for="contact-email" class="c-label">Email<abbr
                                            title="Required field">*</abbr></label>

                                {!! Form::email( 'email',null, array( 'class' => 'c-input-text c-input-text--md','id'=>'contact-email' ) ) !!}
                            </p>

                            <p class="o-grid__item u-1-of-2-bp3">
                                <label for="contact-phone" class="c-label">Phone</label>
                                {{--<input type="text" id="contact-phone" class="c-input-text c-input-text--md"--}}
                                {{--placeholder="Phone"/>--}}
                                {!! Form::text( 'phone',null, array( 'class' => 'c-input-text c-input-text--md','id'=>'contact-phone' ) ) !!}
                            </p>
                        </div>

                        <div class="o-grid o-grid--gutter">
                            <p class="o-grid__item u-1-of-2-bp3">
                                <label for="contact-office" class="c-label">Office</label>
                                {!! Form::select( 'officeIdFective',$officesList,$pageData['office']['id'], array( 'class' => 'c-input-text c-input-text--md u-bgcolor-neutral-xx-light','id'=>'contact-office','disabled' ) ) !!}
                            </p>
                            {!! Form::hidden('officeId',$pageData['office']['id']) !!}

                            <p class="o-grid__item u-1-of-2-bp3">
                                <label for="contact-reason" class="c-label">Enquiry type<abbr
                                            title="Required field">*</abbr></label>
                                {!! Form::select( 'contactReason',$reasons,null, array( 'class' => 'c-input-text c-input-text--md','id'=>'contact-reason' ) ) !!}
                            </p>
                        </div>
                        <div class="o-grid o-grid--gutter">
                            <p class="o-grid__item u-2-of-2-bp3">
                                <label for="contact-message" class="c-label">Message<abbr
                                            title="Required field">*</abbr></label>
                                {!! Form::textarea( 'message',null, array( 'class' => 'c-input-text c-input-text--md','id'=>'contact-message','rows'=>'5','cols'=>'30' ) ) !!}
                            </p>
                        </div>
                        <div class="o-grid o-grid--gutter">
                            <div class="o-grid__item u-2-of-4-bp3">
                                <div class="cf">{!! app('captcha')->display(); !!}</div>
                            </div>

                            <div class="o-grid__item u-2-of-4-bp3 u-align-right">
                                {!! Form::submit('Send',['class'=>'c-button c-button--md c-button--beta u-mb-beta']) !!}
                            </div>
                        </div>
                    </fieldset>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    @endif
    @if( $pageData['raw']['try_is_on']==true)
        @include('components.cta-trial',$pageData['trial'])
    @endif
@endsection


@section( 'view-scripts' )

@endsection
