@extends( 'master' )

@section( 'content' )
    @if( $pageData['raw']['head_is_on']==true)
        @include('components.intro',   $pageData['intro'])
    @endif
    @foreach($pageData['partners'] as $oneRow)
        <div class="c-row c-row--md c-row--border-top u-bgcolor-neutral-xxx-light u-pb-jota u-mb-neg-beta">
            <div class="u-align-horizontal u-max-width-lg">
                <div class="o-collab flex-container">
                    @foreach($oneRow as $oneRec)
                        <div class="flex-item">
                            <div class="Aligner u-bgcolor-neutral-xxxx-light u-mb-delta">
                                <div class="Aligner-item u-align-horizontal">
                                    <div class="overlay u-z-beta"></div>
                                    <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                                         data-src="{!! asset(\App\PartnersListEntity::$path.$oneRec['image']) !!}"
                                         class="p-blazy js-blazy u-z-alpha u-align-horizontal" alt="Logo">
                                </div>
                            </div>
                            <div class="textbox">{!! $oneRec['text_'.\App::getLocale()] !!}
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>
    @endforeach
    {{--<div class="c-row c-row--md c-row--border-top u-bgcolor-neutral-xxx-light u-pt-jota">--}}
    {{--<div class="u-align-horizontal u-max-width-lg">--}}
    {{--<div class="o-collab flex-container">--}}
    {{--<div class="flex-item">--}}
    {{--<div class="Aligner u-bgcolor-neutral-xxxx-light u-mb-delta">--}}
    {{--<div class="Aligner-item u-align-horizontal">--}}
    {{--<div class="overlay u-z-beta"></div>--}}
    {{--<img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="--}}
    {{--data-src="{!! asset('img/logos/university-birmingham.png') !!}"--}}
    {{--class="p-blazy js-blazy u-z-alpha u-align-horizontal" alt="Logo">--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<div class="textbox">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod--}}
    {{--tempor.--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<div class="flex-item">--}}
    {{--<div class="Aligner u-bgcolor-neutral-xxxx-light u-mb-delta">--}}
    {{--<div class="Aligner-item u-align-horizontal">--}}
    {{--<div class="overlay u-z-beta"></div>--}}
    {{--<img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="--}}
    {{--data-src="{!! asset('img/logos/university-cornell.png') !!}"--}}
    {{--class="p-blazy js-blazy u-z-alpha u-align-horizontal" alt="Logo">--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<div class="textbox">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod--}}
    {{--tempor.--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<div class="flex-item">--}}
    {{--<div class="Aligner u-bgcolor-neutral-xxxx-light u-mb-delta">--}}
    {{--<div class="Aligner-item u-align-horizontal">--}}
    {{--<div class="overlay u-z-beta"></div>--}}
    {{--<img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="--}}
    {{--data-src="{!! asset('img/logos/university-goteborg.png') !!}"--}}
    {{--class="p-blazy js-blazy u-z-alpha u-align-horizontal" alt="Logo">--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<div class="textbox">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod--}}
    {{--tempor.--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    @if( $pageData['raw']['try_is_on']==true)
        @include('components.cta-trial',$pageData['trial'])
    @endif
@endsection

@section( 'view-scripts' )

@endsection
