@extends( 'master' )

@section( 'content' )
    @if($pageData['raw']['head_is_on']==true)
        @include('components.intro',  $pageData['intro'])
    @endif
    <div class="c-row c-row--md c-row--border-top u-bgcolor-neutral-xxx-light u-pb-jota u-mb-neg-beta">
        <div class="u-align-horizontal u-max-width-md c-main-section s-content">

            {!! Form::open(array('action'=>'CDemoRequestController@postDemoRequest','method'=>'post','files'=>true)) !!}
            {!! Form::hidden('request_page',$data['request_page']) !!}
            {!! Form::hidden('request_click_position',$data['request_click_position']) !!}

            <h2 class="u-mb-jota">
                Thank you for your interest in Bluebee. Please fill out the form below, we will get back to you shortly.
            </h2>

            @if(Session::has('successMessage'))
                <div class="c-alert-box c-alert-box--success">
                    <p class="u-ms-1">
                        <span>{!! Session::get('successMessage') !!}</span>
                    </p>
                </div>
            @endif

            @if($errors -> any() )
                <div class="c-alert-box c-alert-box--error">
                    <p class="u-ms-1">
                        @foreach ($errors->all() as $error)
                            <span>{{ $error }}</span>
                        @endforeach
                    </p>
                </div>
            @endif


            <fieldset>
                <div class="o-grid o-grid--gutter">
                    <p class="o-grid__item u-1-of-2-bp3">
                        <label for="demo-fname" class="c-label">First name<abbr title="Required field">*</abbr></label>
                        {!! Form::text('first_name', null, ['class' => 'c-input-text c-input-text--md', 'id' => 'demo-fname']) !!}
                    </p>

                    <p class="o-grid__item u-1-of-2-bp3">
                        <label for="demo-lname" class="c-label">Last name<abbr title="Required field">*</abbr></label>
                        {!! Form::text('last_name', null, ['class' => 'c-input-text c-input-text--md', 'id' => 'demo-lname']) !!}
                    </p>
                </div>
                <div class="o-grid o-grid--gutter">
                    <p class="o-grid__item u-1-of-2-bp3">
                        <label for="demo-email" class="c-label">Email<abbr title="Required field">*</abbr></label>
                        {!! Form::text('email', null, ['class' => 'c-input-text c-input-text--md', 'id' => 'demo-email']) !!}
                    </p>

                    <p class="o-grid__item u-1-of-2-bp3">
                        <label for="demo-phone" class="c-label">Phone</label>
                        {!! Form::text('phone', null, ['class' => 'c-input-text c-input-text--md', 'id' => 'demo-phone']) !!}
                    </p>
                </div>
                <div class="o-grid o-grid--gutter">
                    <p class="o-grid__item u-1-of-2-bp3">
                        <label for="demo-company" class="c-label">Company<abbr title="Required field">*</abbr></label>
                        {!! Form::text('company', null, ['class' => 'c-input-text c-input-text--md', 'id' => 'demo-company']) !!}
                    </p>

                    <p class="o-grid__item u-1-of-2-bp3">
                        <label for="demo-job" class="c-label">Role / Job title<abbr
                                    title="Required field">*</abbr></label>
                        {!! Form::text('job_title', null, ['class' => 'c-input-text c-input-text--md', 'id' => 'demo-job']) !!}
                    </p>
                </div>
                <div class="o-grid o-grid--gutter">
                    <p class="o-grid__item u-3-of-3-bp3">
                        <label for="demo-occupation" class="c-label">Please select your line of business</label>
                        <span class="dropdown">{!! Form::select('job_scope', $dropdowns['job_scope'],null,['class'=>'c-input-text c-input-text--md','id'=>'demo-occupation']) !!}</span>
                    </p>
                </div>
                <div class="o-grid o-grid--gutter">
                    <p class="o-grid__item u-3-of-3-bp3">
                        <label for="demo-requestType" class="c-label">Request type</label>
                        <span class="dropdown">{!! Form::select('request_type',$dropdowns['request_types'],$data['selected_req_type'],['class'=>'c-input-text c-input-text--md','id'=>'demo-requestType']) !!}</span>
                    </p>
                </div>
                <div class="o-grid o-grid--gutter">
                    <p class="o-grid__item u-2-of-2-bp3">
                        <label for="demo-message" class="c-label">Please describe your project</label>
                        {!! Form::textarea('project_description', null, ['class' => 'c-input-text c-input-text--md', 'id' => 'demo-message']) !!}
                    </p>
                </div>
                <div class="o-grid o-grid--gutter">
                    <div class="o-grid__item u-2-of-4-bp3">
                        <div class="cf">{!! app('captcha')->display(); !!}</div>
                    </div>

                    <div class="o-grid__item u-2-of-4-bp3 u-align-right">
                        {!! Form::submit('Send',['class'=>'c-button c-button--md c-button--beta u-mb-beta']) !!}
                    </div>
                </div>
            </fieldset>
            {!! Form::close() !!}
        </div>
    </div>
@endsection

@section( 'view-scripts' )

@endsection