<!DOCTYPE html>
<html class="no-js" lang="{!! App::getLocale() !!}">

<head>
    {{--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">--}}
    <meta charset="utf-8">
    <meta http-equiv="cleartype" content="on">

    <!-- Mobile settings http://t.co/dKP3o1e -->
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Always Load jQuery First -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="{!! asset('/vendor/jquery/dist/jquery.min.js') !!}"><\/script>')</script>

    <!-- About this document -->
    {{--<meta name="description" content="Bluebee is a leading provider of high performance genomics solutions.">--}}
    {{--<meta name="author" content="Bluebee">--}}
    {{--<meta name="author"                 content="One Creative" />--}}
    {{--<meta name="keywords"               content="@yield( 'meta_keywords_section' )"/>--}}
    {{--<meta name="description"            content="@yield( 'meta_description_section' )"/>--}}
    <meta property="og:url"             content="@yield( 'facebook_url' )" />
    <meta property="og:type"            content="website" />
    <meta property="og:title"           content="@yield( 'facebook_title' )" />
    <meta property="og:description"     content="@yield( 'facebook_description' )" />
    <meta property="og:image"           content="@yield( 'facebook_image' )" />
    <meta property="fb:app_id"          content="1572623149695716"/>
    <title>@yield('title_section')</title>

    <!-- Custom modernizr build: http://modernizr.com — load other JavaScript at the end of document -->
    <script src="{!! asset('vendor/modernizr/modernizr.js') !!}"></script>

    <script src="https://use.typekit.net/onp5dtw.js"></script>
    <script>try {
            Typekit.load({async: true});
        } catch (e) {
        }</script>

    <script src='https://www.google.com/recaptcha/api.js'></script>

    <!-- Stylesheets -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{!! asset('fonts/feelthebern/stylesheet.css') !!}">
    <link rel="stylesheet" href="{!! asset('vendor/slick/slick.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/screen.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/icons.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/overrides.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/jquery.modal.css') !!}" type="text/css" media="screen"/>

    <!-- Enable HTML5 in IE<9 -->
    <!--[if lt IE 9  & (!IEMobile)]>
    <script src="/vendor/html5shiv/dist/html5shiv.min.js"></script><![endif]-->
    <link rel="icon" href="{!! asset('/favicon.png') !!}">
    <!--
    Place all other favicons in website root (e.g. http://web.dev/favicon.ico).
    Add your font license here, if necessary.
  -->
</head>


<body class="c-home">

<!-- Deprecated browser alert -->
<!--[if lt IE 9]>
<div class="c-alert-box">
    <p>You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your
        browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to
        improve your experience.</p>
</div>
<![endif]-->
<!-- End deprecated browser alert -->

<!-- Cookies alert -->
{{-- <div class="c-cookie-notice">
<div class="c-row ">
<div class="o-container">
<div class="o-grid o-grid--gutter">
<div class="o-grid__item u-4-of-5-bp4">
<p class="c-cookie-notice__text">
This site uses cookies to provide you with a more responsive and personalized service. By using this site you agree to our use of cookies. Please read our <a class="c-cookie-notice__link" href="#!">cookie notice</a> for more information on the cookies we use and how to delete or block them.
</p>
</div>
<div class="o-grid__item u-1-of-5-bp4">
<p class="c-cookie-notice__text">
<a class="c-cookie-notice__link js-hide" href="#!" data-target-selector=".c-cookie-notice">Accept and close</a>
</p>
</div>
</div>
</div>
</div>
</div> --}}
<input type="hidden"  name="rand" value="{{ csrf_token() }}">

@yield( 'body' )

        <!-- Make responsive website responsive in IE7 & 8 -->
<!--[if IE 8 | IE 7]>
<script src="/vendor/respond/dest/respond.min.js"></script><![endif]-->

<!-- Theme specific JavaScript. Add your other scripts here -->
<script src="{!! asset('vendor/blazy/blazy.min.js') !!}"></script>
<script src="{!! asset('vendor/matchHeight/jquery.matchHeight-min.js') !!}"></script>
<script src="{!! asset('js/theme.concat.js') !!}"></script>

<script src="{!! asset('js/authClientFunctions.js') !!}"></script>

@yield( 'scripts' )

<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-49350466-1', 'auto');
    ga('send', 'pageview');
</script>

<!-- End footer javascript -->

</body>
</html>
