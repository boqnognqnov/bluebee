@extends( 'master' )

@section( 'content' )
    @if( $pageData['raw']['head_is_on']==true)
        @include('components.intro',$pageData['intro'])
    @endif

    @if(Session::has('successMessage'))
        <div class="c-row c-row--md u-bgcolor-neutral-xxxx-light">
            <div class="c-alert-box c-alert-box--success u-max-width-sm u-align-horizontal">
                <p class="u-ms-1">
                    <span>{!! Session::get('successMessage') !!}</span>
                </p>
            </div>
        </div>
    @endif

    <div class="c-row c-row--md u-bgcolor-neutral-xxxx-light">

        <div class="o-container">
            @include('components.main', ['page_title'=>'','page_desc'=>$pageData['raw']['pipe_text_'.\App::getLocale()]])
        </div>

        <div class="o-container u-align-center u-mt-jota u-mb-neg-jota">
            <img src="{!! asset(\App\PipeLinesPageEntity::$path.$pageData['raw']['image']) !!}">
        </div>

        <div class="o-container s-content u-mt-neg-jota">
            <div class="o-grid o-grid--gutter">
                @foreach($pageData['colls'] as $oneCol)
                    <div class="o-grid__item u-1-of-3-bp3 u-align-center">
                        <h2>{!! $oneCol['title_'.\App::getLocale()] !!}</h2>

                        <p>{!! $oneCol['text_'.\App::getLocale()] !!}</p>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

    <div class="c-row c-row--md c-row--border-top u-bgcolor-neutral-xxx-light u-pb-jota">
        <div class="u-align-horizontal u-max-width-md c-main-section s-content">
            <p class="u-ms1">
                {!! $pageData['raw']['s_pipe_text_'.\App::getLocale()] !!}
            </p>
        </div>
    </div>

    <div class="c-row c-row--md c-row--border-top u-bgcolor-neutral-xxx-light u-pb-jota">
        @foreach($pageData['datacards'] as $oneDataCard)
            <div class="u-align-horizontal u-max-width-sm c-main-section s-content">

                <div class="o-grid o-grid--gutter">
                    <div class="o-grid__item u-2-of-6-bp3">
                        <img class="p-lazy p-lazy--preview js-blazy"
                             src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                             data-src="{!! asset(\App\DownloadsItemsEntity::$pathFrontImage.$oneDataCard['image']) !!}"
                             data-src-small="{!! asset(\App\DownloadsItemsEntity::$pathFrontImageSmall.$oneDataCard['image']) !!}"
                             data-src-medium="{!! asset(\App\DownloadsItemsEntity::$pathFrontImageMedium.$oneDataCard['image']) !!}"
                             alt="datacard image"
                        />
                    </div>
                    <div class="o-grid__item u-4-of-6-bp3">
                        <h2 id="pipeid{!! $oneDataCard['id'] !!}">{!! $oneDataCard['title_'.\App::getLocale()] !!}</h2>

                        <div>
                            {!! $oneDataCard['text_'.\App::getLocale()] !!}
                        </div>

                        <p class="u-ms2 u-mt-gamma">
                            {{--<a href="{!! asset('admin/downloads/download/file/' . $oneDataCard['id']) !!}" class="o-link__noline">Request download</a>--}}

                            @if(\Auth::check())
                                @if(\Auth::user()->is_approve==true)
                                    <a href="{!! asset('downloads/download/file/' . $oneDataCard['id']) !!}">Request
                                        download</a>

                                @else
                                    <a href="#datacardModal" rel="modal:open" class="o-link__noline dCartClass"
                                       data_datacard_name="{!! $oneDataCard['title_'.\App::getLocale()] !!}">Request
                                        the datacard</a>
                                @endif
                            @else
                                <a href="#loginModal"
                                   rel="modal:open">{!! $oneDataCard['title_'.\App::getLocale()] !!}</a>
                            @endif

                            {{--<a href="#datacardModal" rel="modal:open" class="o-link__noline dCartClass"--}}
                            {{--data_datacard_name="{!! $oneDataCard['title_'.\App::getLocale()] !!}">Request--}}
                            {{--the datacard</a>--}}
                        </p>
                    </div>
                </div>
            </div>
        @endforeach
    </div>

    {{--@include('components.sections.benefits',['items'=> @$pageData['benefit']['items']])--}}

    @if(@$pageData['raw']['try_is_on']==true)
        @include('components.cta-trial',@$pageData['trial'])
    @endif

    <div class="behindthecurtain">
        <!-- DATACARD REQUEST FORM -->
        <!-- href="#datacardModal" rel="modal:open" -->
        <div id="datacardModal" class="c-card c-card--alpha u-z-jota u-max-width-md">
            {{--<form action="#" method="post">--}}
            {!! Form::open(array('action'=>'CPipelinesController@pipeRequest','method'=>'post','files'=>true)) !!}
            {!! Form::hidden('dataCardName') !!}

            <div class="c-card__content c-card__header">
                <h2>
                    Request a datacard
                </h2>
            </div>
            <div class="c-card__content">

                @if($errors -> any() )
                    <div class="c-alert-box c-alert-box--error">
                        <p class="u-ms-1">
                            @foreach ($errors->all() as $error)
                                <span>{{ $error }}</span>
                            @endforeach
                        </p>
                    </div>
                @endif

                <div class="o-grid o-grid--gutter">
                    <p class="o-grid__item u-1-of-2-bp3">
                        <label for="datareq-fname" class="c-label">First name<abbr
                                    title="Required field">*</abbr></label>
                        {!! Form::text('first_name', null, ['class' => 'c-input-text c-input-text--md', 'id' => 'datareq-fname']) !!}
                    </p>

                    <p class="o-grid__item u-1-of-2-bp3">
                        <label for="datareq-lname" class="c-label">Last name<abbr
                                    title="Required field">*</abbr></label>
                        {!! Form::text('last_name', null, ['class' => 'c-input-text c-input-text--md', 'id' => 'datareq-lname']) !!}
                    </p>
                </div>
                <div class="o-grid o-grid--gutter">
                    <p class="o-grid__item u-1-of-2-bp3">
                        <label for="datareq-email" class="c-label">Email<abbr
                                    title="Required field">*</abbr></label>
                        {{--{!! Form::email('email', null, ['class' => 'c-input-text c-input-text--md', 'id' => 'datareq-email', 'required' => 'required']) !!}--}}


                        @if(!empty(@$pageData['raw']['email']))
                            {!! Form::text('email',@$pageData['userData']['email'],['placeholder'=>'email&#64;address.com','class'=>'c-input-text c-input-text--md','id'=>'datareq-email','readonly'=>'readonly']) !!}
                        @else
                            {!! Form::text('email',@$pageData['userData']['email'],['placeholder'=>'email&#64;address.com','class'=>'c-input-text c-input-text--md','id'=>'datareq-email']) !!}
                        @endif
                    </p>

                    <p class="o-grid__item u-1-of-2-bp3">
                        <label for="datareq-phone" class="c-label">Phone</label>
                        {!! Form::text('phone', null, ['class' => 'c-input-text c-input-text--md', 'id' => 'datareq-phone']) !!}
                    </p>
                </div>
                <div class="o-grid o-grid--gutter">
                    <p class="o-grid__item u-1-of-2-bp3">
                        <label for="datareq-company" class="c-label">Company<abbr
                                    title="Required field">*</abbr></label>
                        {!! Form::text('company', null, ['class' => 'c-input-text c-input-text--md', 'id' => 'datareq-company', 'required' => 'required']) !!}
                    </p>

                    <p class="o-grid__item u-1-of-2-bp3">
                        <label for="datareq-job" class="c-label">Role / Job title<abbr
                                    title="Required field">*</abbr></label>
                        {!! Form::text('job_title', null, ['class' => 'c-input-text c-input-text--md', 'id' => 'datareq-job', 'required' => 'required']) !!}
                    </p>
                </div>
                {{--<div class="o-grid o-grid--gutter">--}}
                {{--<p class="o-grid__item u-3-of-3-bp3">--}}
                {{--<label for="datareq-cardType" class="c-label">Select datacard</label>--}}
                {{--<span class="dropdown">--}}
                {{--<select class="c-input-text c-input-text--md" id="datareq-cardType" name="card_type">--}}
                {{--<option value="0" selected="selected">SELECT</option>--}}
                {{--</select>--}}
                {{--</span>--}}
                {{--<span class="dropdown">{!! Form::select('request_type',@$dropdowns['request_types'],@$data['selected_req_type'],['class'=>'c-input-text c-input-text--md','id'=>'datareq-requestType']) !!}</span>--}}
                {{--</p>--}}
                {{--</div>--}}
            </div>
            <div class="c-card__content c-card__footer">
                <p class="c-button-group">
                    {{--<input type="submit" class="c-button c-button--float-left c-button--md c-button--beta"--}}
                    {{--value="Request"/>--}}
                    {!! Form::submit('Request',['class'=>'c-button c-button--float-left c-button--md c-button--beta']) !!}
                    {{--<a href="#registrationModal" rel="modal:open" class="c-button c-button--float-left c-button--md c-button--text c-button--text-neutral">Cancel</a>--}}
                </p>
            </div>
            {!! Form::close() !!}
        </div>

        <!-- END DATACARD REQUEST FORM -->
    </div>
@endsection

@section( 'view-scripts' )

    <script>

        $(document).ready(function () {
            @if($errors -> any() )
            $('#datacardModal').modal();
            @endif


                         $('.dCartClass').on('click', function (event) {

                var dataCardName = $(this).attr('data_datacard_name');
//                alert(dataCardName);
//                console.log(text);
                $('input[name="dataCardName"]').val(dataCardName);
                $('#datacardModal').modal();
            });

        });
    </script>

@endsection