@extends( 'master' )

@section( 'content' )
  @include('components.intro', ['title'=>'Terms and Conditions','body'=>'','button'=>'','background'=>['default'=>'img/photos/intro/2400/lab-4.jpg','small'=>'img/photos/intro/600/lab-4.jpg','medium'=>'img/photos/intro/1200/lab-4.jpg']])
  <div class="c-row c-row--md c-row--border-top u-bgcolor-neutral-xxx-light u-pb-jota u-mb-neg-beta">
    <div class="u-align-horizontal u-max-width-sm c-main-section s-content">
      <h2>
        Terms and Conditions
      </h2>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
      </p>
      <div class="u-pt-gamma u-mt-jota u-mb-jota">
        <h3>Standard orderded list</h3>
        <ol>
            <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ullam, dignissimos.</li>
            <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi id rerum eveniet quisquam pariatur labore.</li>
            <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
        </ol>
      </div>
    </div>
  </div>
@endsection

@section( 'view-scripts' )

@endsection
