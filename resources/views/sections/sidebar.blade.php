<div class="o-sidebar__heading u-bgcolor-alpha-dark u-color-neutral-xxxx-light">
    <h3 class="o-sidebar__title">Categories</h3>
</div>

<ul class="o-catlist">
    @foreach($sidebar['categories'] as $oneCat)
        <li class="o-catlist__item o-catlist__item__border"><a
                    href="{!! url($sidebar['itemType'].'/list/category/'.$oneCat['title_'.\App::getLocale()]) !!}"
                    class="o-link__noline"><span
                        class="u-color-neutral-dark">{!! $oneCat['title_'.\App::getLocale()] !!} </span></a></li>
    @endforeach
</ul>
<div class="o-sidebar__heading u-bgcolor-alpha-dark u-color-neutral-xxxx-light">
    <h3 class="o-sidebar__title">Tags</h3>
</div>
<div class="o-taglist">
    @foreach($sidebar['tags'] as $oneTag)
        <a href="{!! url($sidebar['itemType'].'/list/tag/'.$oneTag['title_'.\App::getLocale()]) !!}"
           class="o-taglist__item c-button c-button--ghost-neutral c-button--sm">{!! $oneTag['title_'.\App::getLocale()] !!}</a>
    @endforeach
</div>
<div class="o-sidebar__heading u-bgcolor-alpha-dark u-color-neutral-xxxx-light">
    <h3 class="o-sidebar__title">Archive</h3>
</div>
<ul class="o-archive">
    @foreach($sidebar['years'] as $oneYear)
        <li class="o-archive__item o-archive__item__border u-color-neutral-x-dark"><a
                    href="{!! url($sidebar['itemType'].'/list/year/'.$oneYear) !!}"
                    class="o-link__noline">{!! $oneYear !!}</a></li>
    @endforeach

</ul>
