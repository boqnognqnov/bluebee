<div class="o-sidebar__heading u-bgcolor-alpha-dark u-color-neutral-xxxx-light">
    <h3 class="o-sidebar__title">Filter</h3>
</div>

<ul class="o-catlist">
    @foreach($sidebar['categories'] as $oneCat)
        <li class="o-catlist__item o-catlist__item__border"><a
                    href="{!! url($sidebar['itemType'].'/list/category/'.$oneCat['title_'.\App::getLocale()]) !!}"
                    class="o-link__noline"><span
                        class="u-color-neutral-dark">{!! $oneCat['title_'.\App::getLocale()] !!}</span></a></li>
    @endforeach

</ul>