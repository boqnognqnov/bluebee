<div class="c-row c-row--sm c-row--border-top u-bgcolor-neutral-xxx-light">
    <div class="o-container">
        <div class="o-grid o-grid--gutter u-mb-gamma">
            <div class="o-grid__item u-1-of-2-bp4 js-match-height">
                <div class="u-align-horizontal u-align-vertical-bp4 u-max-width-sm s-content u-mb-jota">
                    <h2>
                        {!! $title !!}
                    </h2>

                    <p>
                        {!! $content !!}
                    </p>

                    <p>
                        {{--<a href="#" class="c-link">Learn more</a>--}}
                    </p>
                </div>
            </div>
            <div class="o-grid__item u-1-of-2-bp4 js-match-height">
                <div class="u-align-horizontal u-max-width-md s-content">
                    <div class="c-browser">
                        <div class="c-browser__content">
                            <img class="c-browser__image p-lazy p-lazy--preview js-blazy"
                                 src="{!! asset($image) !!}"
                                 data-src="{!! asset($image) !!}"
                                 alt="Dashboard"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>