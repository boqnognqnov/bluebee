<?php
//
$firstData = \App\FavoritesComponentEntity::viewFavDataByPosition('first', $favId);
//return dump($firstData);
$secondData = \App\FavoritesComponentEntity::viewFavDataByPosition('second', $favId);
//return dump($secondData);
$thirdData = \App\FavoritesComponentEntity::viewFavDataByPosition('third', $favId);
//$blueBeeDefaultPath = Config::get('GlobalArrays.defaultImagePath');
?>

<div class="u-mb-neg-gamma">
    <div class="o-grid o-grid--gutter">
        <article class="o-grid__item u-1-of-2-bp3 u-1-of-3-bp4">
            <div class="c-card c-card--alpha u-mb-jota js-match-height">
                <div class="c-card__image c-card__image-top u-ratio-landscape u-bgcolor-neutral-xxx-light">
                    <div class="c-background-image p-blazy js-blazy"
                         data-src="{!! asset($firstData['image']['default']) !!}"
                         data-src-medium="{!! asset($firstData['image']['medium']) !!}"
                         data-src-small="{!! asset($firstData['image']['small']) !!}"
                    ></div>
                </div>
                <div class="c-card__content c-card__content--lg">
                    <div class="u-width-xs u-align-horizontal">
                        <h2 class="c-card__title u-ms1 u-color-neutral-xx-dark">
                            {{--Lorem ipsum dolor sit--}}
                            {!! $firstData['title'] !!}
                        </h2>
                        <div class="u-ms-1 u-color-neutral-base js-truncate">
                            {{--Leverage our high performance computer infrastructure and drastically reduce your processing--}}
                            {{--time.--}}
                            {!! $firstData['text'] !!}
                        </div>
                        <p class="u-ms-1">
                            <a href="{!! url($firstData['url']) !!}" class="c-link">Learn more</a>
                        </p>
                        {{--<div class="o-label u-text-uppercase u-ms-2 u-fw-gamma">News</div>--}}
                    </div>
                </div>
            </div>
        </article>
        <article class="o-grid__item u-1-of-2-bp3 u-1-of-3-bp4">
            <div class="c-card c-card--alpha u-mb-jota js-match-height">
                <div class="c-card__image c-card__image-top u-ratio-landscape u-bgcolor-neutral-xxx-light">
                    <div class="c-background-image p-blazy js-blazy"
                         data-src="{!! asset($secondData['image']['default']) !!}"
                         data-src-medium="{!! asset($secondData['image']['medium']) !!}"
                         data-src-small="{!! asset($secondData['image']['small']) !!}"
                    ></div>
                </div>
                <div class="c-card__content c-card__content--lg">
                    <div class="u-width-xs u-align-horizontal">
                        <h2 class="c-card__title u-ms1 u-color-neutral-xx-dark">
                            {{--Lorem ipsum dolor--}}
                            {!! $secondData['title'] !!}
                        </h2>
                    <div class="u-ms-1 u-color-neutral-base js-truncate">te">
                            {{--Our innovative hybrid-core architecture demonstrates impressive performance gains, at--}}
                            {{--substantially lower cost.--}}
                            {!! $secondData['text'] !!}
                    </div></p>
                        <p class="u-ms-1">
                            <a href="{!! url($secondData['url']) !!}" class="c-link">Learn more</a>
                        </p>
                    </div>
                </div>
            </div>
        </article>
        <article class="o-grid__item u-1-of-2-bp3 u-1-of-3-bp4">
            <div class="c-card c-card--alpha u-mb-jota js-match-height">
                <div class="c-card__image c-card__image-top u-ratio-landscape u-bgcolor-neutral-xxx-light">
                    <div class="c-background-image p-blazy js-blazy"
                         data-src="{!! asset($thirdData['image']['default']) !!}"
                         data-src-medium="{!! asset($thirdData['image']['medium']) !!}}"
                         data-src-small="{!! asset($thirdData['image']['small']) !!}"
                    ></div>
                </div>
                <div class="c-card__content c-card__content--lg">
                    <div class="u-width-xs u-align-horizontal">
                        <h2 class="c-card__title u-ms1 u-color-neutral-xx-dark">
                            {{--Lorem ipsum dolor sit amet--}}
                            {!! $thirdData['title'] !!}
                        </h2>
                        <div class="u-ms-1 u-color-neutral-base js-truncate">
                            {{--Bluebee’s cloud based solutions allow you to focus efforts and your best people for the--}}
                            {{--right cause.--}}
                            {!! $thirdData['text'] !!}
                        </div>
                        <p class="u-ms-1">
                            <a href="{!! url($thirdData['url']) !!}" class="c-link">Learn more</a>
                        </p>
                    </div>
                </div>
            </div>
        </article>
    </div>
</div>
