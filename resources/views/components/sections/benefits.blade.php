<div class="c-main-section c-main-section--lg u-mb-neg-gamma">
    <div class="u-mb-neg-gamma">
        <div class="o-grid o-grid--gutter">
            @foreach($items as $item)
                {{--@include('components.benefit', ['title'=>$item['title'],'desc'=>$item['desc']])--}}
                @include('components.benefit', $item)

            @endforeach
        </div>

    </div>
</div>

{{--@include('components.feature', ['title'=>'High Performance','desc'=>'Leverage our high performance computer infrastructure and drastically reduce your processing time.','link'=>['url'=>'features','text'=>'more info'],'icon'=>'stopwatch'])--}}
{{--@include('components.feature', ['title'=>'Security','desc'=>'Our innovative hybrid-core architecture demonstrates impressive performance gains, at substantially lower cost.','link'=>['url'=>'features','text'=>'more info'],'icon'=>'shield'])--}}
{{--@include('components.feature', ['title'=>'Convenience','desc'=>'Bluebee’s cloud based solutions allow you to focus efforts and your best people for the right cause.','link'=>['url'=>'features','text'=>'more info'],'icon'=>'cloud'])--}}
{{--@include('components.feature', ['title'=>'Cost-effective','desc'=>'Bluebee offers a flexible solution based on configurable and editable pipelines. Lorem ipsum dolor.','link'=>['url'=>'features','text'=>'more info'],'icon'=>'money2'])--}}