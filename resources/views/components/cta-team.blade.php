<div class="c-row c-row--lg u-bgcolor-beta-base">
  <div class="c-background-image u-opacity-30 p-blazy js-blazy"
  data-src="img/photos/intro/2400/lab-bw.jpg"
  data-src-medium="img/photos/intro/1200/lab-bw.jpg"
  data-src-small="img/photos/intro/600/lab-bw.jpg"
  ></div>
  <div class="o-container">
    <div class="u-mw-sm u-align-center u-align-horizontal">
    	<h2 class="u-ms4 u-ms6-bp3 u-fw-alpha u-color-neutral-xxxx-light u-mb-beta">Lorem ipsum dolor sit amet, consectetur adipisicing elit?</h2>
    	<p class="u-ms2 u-color-neutral-xxxx-light u-opacity-70 u-mb-jota">tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
    	<p>
    		<a href="#!" class="c-button c-button--lg c-button--light-gamma c-button--effect-1">Join us</a>
    	</p>
    </div>
  </div>
</div>
