<div class="c-row c-row--sm u-mb-jota u-mt-neg-jota u-bgcolor-neutral-xxx-light">
    <div class="o-container">
        <div class="o-grid o-grid--gutter">
            @foreach($items as $oneItem)
                <div class="o-grid__item u-1-of-2-bp3">
                    <div class="c-choice u-bgcolor-alpha-dark u-mb-jota js-match-height">
                        <a href="{!! url('why-bluebee/'.$oneItem['slug']) !!}" class="c-choice__block-link">
                            <div class="c-background-image c-choice__image u-opacity-20 p-blazy js-blazy"
                                 data-src="{!! asset(\App\BenefitInnerPagesEntity::$pathBackground.$oneItem['imageBackground']) !!}"
                                 data-src-medium="{!! asset(\App\BenefitInnerPagesEntity::$pathBackgroundM.$oneItem['imageBackground']) !!}"
                                 data-src-small="{!! asset(\App\BenefitInnerPagesEntity::$pathBackgroundS.$oneItem['imageBackground']) !!}"
                                    ></div>
                            <p class="c-choice__heading c-choice__badge c-badge u-ms3 u-color-neutral-xxx-light">
                                {{--High Performance--}}
                                {!! $oneItem['title'] !!}
                            </p>

                            <p class="c-choice__icon c-choice__badge c-icon-sprite--60 u-align-right c-badge u-ms-2 u-color-neutral-xxx-light oh-safari">
                                <img class="p-blazy js-blazy"
                                     src=data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
                                     data-src="{!! asset($oneItem['image']) !!}"
                                     alt="stopwatch icon"
                                        />
                            </p>

                            <div class="c-choice__content">
                                <h2 class="c-choice__title">
                                    {{--Faster processing and increased throughput, built to scale--}}
                                    {!! $oneItem['text'] !!}
                                </h2>

                                <p class="c-choice__desc">
                                    {!! $oneItem['textDescription'] !!}
                                </p>
                            </div>
                        </a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
