<div class="c-row c-row--lg u-bgcolor-beta-base">
    <div class="c-background-image u-opacity-30 p-blazy js-blazy"
         data-src="{!! asset($background['default']) !!}"
         data-src-medium="{!! asset($background['medium']) !!}"
         data-src-small="{!! asset($background['small']) !!}"
            ></div>
    <div class="o-container">
        <div class="u-mw-sm u-align-center u-align-horizontal">
            <h2 class="u-ms4 u-ms6-bp3 u-fw-gamma u-color-neutral-xxxx-light u-mb-beta u-mb-jota">{!! $text !!}</h2>
            {{-- <p class="u-ms2 u-color-neutral-xxxx-light u-opacity-70 u-mb-jota">Try it immediately with free cloud storage and compute for 3 months.</p> --}}
            {{-- <p>
                <a href="#!" class="c-button c-button--lg c-button--light-gamma c-button--effect-1">Try Bluebee for free</a>
            </p> --}}
            <div class="u-max-width-sm u-align-horizontal">
                <form action="//bluebee.us11.list-manage.com/subscribe/post?u=d76e7e3d4caf196d7a1027643&id=d7d490178b"
                      method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate"
                      target="_blank" novalidate>
                    <div class="o-grid o-grid--gutter">
                        <p class="o-grid__item u-4-of-5-bp3">
                            <input name="EMAIL" id="mce-EMAIL" class="c-input-text c-input-text--lg js-match-height"
                                   placeholder="Enter your email address" type="email">
                        </p>

                        <p class="o-grid__item u-1-of-5-bp3">
                            <input class="c-button c-button--lg c-button--gamma js-match-height" value="{!! $button !!}"
                                   type="submit">
                        </p>
                    </div>
                    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text"
                                                                                              name="b_d76e7e3d4caf196d7a1027643_d7d490178b"
                                                                                              tabindex="-1" value="">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
