<div class="u-mb-neg-gamma u-max-width-lg u-align-horizontal">
    <div class="o-grid o-grid--gutter">
        <article class="o-grid__item u-1-of-2-bp3">
            <div class="c-card c-card--alpha u-mb-jota js-match-height">
                <div class="c-card__content u-pb-jota u-pt-jota c-card__content--lg">
                    <div class="u-width-xs u-align-horizontal">
                        <p class="u-align-center u-mb-alpha">

                            @if(isset($uses[0]['image']))
                                <a href="{!! url('use-cases/'.@$uses[0]['slug']) !!}" class="c-icon-sprite c-icon-sprite--60 {!! @$uses[0]['imgClass'] !!}"
                                      style="background-image: url({!! $uses[0]['image'] !!});"></a>
                            @else
                                <span class="c-icon-sprite c-icon-sprite--60 c-icon-sprite--microscope"></span>
                            @endif
                        </p>

                        <a href="{!! url('use-cases/'.@$uses[0]['slug']) !!}" class="o-link__noline">
                            <h2 class="c-card__title u-ms1 u-align-center u-color-alpha-base">
                                {!! @$uses[0]['title'] !!}
                            </h2>
                        </a>

                        <p class="u-ms-1 u-align-center u-mb-delta u-color-neutral-base">
                            {!! @$uses[0]['text'] !!}
                        </p>

                        <p class="u-align-center">
                            <a href="{!! url('use-cases/'.@$uses[0]['slug']) !!}" class="c-button c-button--ghost-beta c-button--md">learn more</a>
                        </p>
                    </div>
                </div>
            </div>
        </article>
        <article class="o-grid__item u-1-of-2-bp3">
            <div class="c-card c-card--alpha u-mb-jota js-match-height">
                <div class="c-card__content u-pb-jota u-pt-jota c-card__content--lg">
                    <div class="u-width-xs u-align-horizontal">
                        <p class="u-align-center u-mb-alpha">
                            @if(isset($uses[1]['image']))
                                <a href="{!! url('use-cases/'.@$uses[1]['slug']) !!}" class="c-icon-sprite c-icon-sprite--60 {!! @$uses[1]['imgClass'] !!}"
                                      style="background-image: url({!! $uses[1]['image'] !!});"></a>
                            @else
                                <span class="c-icon-sprite c-icon-sprite--60 c-icon--clinical_lab"></span>
                            @endif

                        </p>

                        <a href="{!! url('use-cases/'.@$uses[1]['slug']) !!}" class="o-link__noline">
                            <h2 class="c-card__title u-ms1 u-align-center u-color-alpha-base">
                                {!! @$uses[1]['title'] !!}
                            </h2>
                        </a>

                        <p class="u-ms-1 u-align-center u-mb-delta u-color-neutral-base">
                            {!! @$uses[1]['text'] !!}
                        </p>

                        <p class="u-align-center">
                            <a href="{!! url('use-cases/'.@$uses[1]['slug']) !!}" class="c-button c-button--ghost-beta c-button--md">learn more</a>
                        </p>
                    </div>
                </div>
            </div>
        </article>
        <article class="o-grid__item u-1-of-2-bp3">
            <div class="c-card c-card--alpha u-mb-jota js-match-height">
                <div class="c-card__content u-pb-jota u-pt-jota c-card__content--lg">
                    <div class="u-width-xs u-align-horizontal">
                        <p class="u-align-center u-mb-alpha">
                            @if(isset($uses[2]['image']))
                                <a href="{!! url('use-cases/'.@$uses[2]['slug']) !!}" class="c-icon-sprite c-icon-sprite--60 {!! @$uses[2]['imgClass'] !!}"
                                      style="background-image: url({!! $uses[2]['image'] !!});"></a>
                            @else
                                <span class="c-icon-sprite c-icon-sprite--60 c-icon-sprite--testtube-search"></span>
                            @endif

                        </p>

                        <a href="{!! url('use-cases/'.@$uses[2]['slug']) !!}" class="o-link__noline">
                            <h2 class="c-card__title u-ms1 u-align-center u-color-alpha-base">
                                {!! @$uses[2]['title'] !!}
                            </h2>
                        </a>

                        <p class="u-ms-1 u-align-center u-mb-delta u-color-neutral-base">
                            {!! @$uses[2]['text'] !!}
                        </p>

                        <p class="u-align-center">
                            <a href="{!! url('use-cases/'.@$uses[2]['slug']) !!}" class="c-button c-button--ghost-beta c-button--md">learn more</a>
                        </p>
                    </div>
                </div>
            </div>
        </article>
        <article class="o-grid__item u-1-of-2-bp3">
            <div class="c-card c-card--alpha u-mb-jota js-match-height">
                <div class="c-card__content u-pb-jota u-pt-jota c-card__content--lg">
                    <div class="u-width-xs u-align-horizontal">
                        <p class="u-align-center u-mb-alpha">
                            @if(isset($uses[3]['image']))
                                <a href="{!! url('use-cases/'.@$uses[3]['slug']) !!}" class="c-icon-sprite c-icon-sprite--60 {!! @$uses[3]['imgClass'] !!}"
                                      style="background-image: url({!! $uses[3]['image'] !!});"></a>
                            @else
                                <span class="c-icon-sprite c-icon-sprite--60 c-icon-sprite--dna-2"></span>
                            @endif

                        </p>

                        <a href="{!! url('use-cases/'.@$uses[3]['slug']) !!}" class="o-link__noline">
                            <h2 class="c-card__title u-ms1 u-align-center u-color-alpha-base">
                                {!! @$uses[3]['title'] !!}
                            </h2>
                        </a>

                        <p class="u-ms-1 u-align-center u-mb-delta u-color-neutral-base">
                            {!! @$uses[3]['text'] !!}
                        </p>

                        <p class="u-align-center">
                            <a href="{!! url('use-cases/'.@$uses[3]['slug']) !!}"
                               class="c-button c-button--ghost-beta c-button--md">learn more</a>
                        </p>
                    </div>
                </div>
            </div>
        </article>
    </div>
</div>
