<nav class="c-pagination u-mt-jota o-media--border">
  <ul class="o-list o-list--horizontal c-pagination__list">
    <li class="o-list__item c-pagination__item u-ms-2 is-previous"><span class="c-pagination__link">&lt; Previous</span></li>
    <li class="o-list__item c-pagination__item u-ms-2 is-current"><span class="c-pagination__link">1</span></li>
    <li class="o-list__item c-pagination__item u-ms-2"><a href="#" title="Go to page 2" rel="nofollow" class="c-pagination__link">2</a></li>
    <li class="o-list__item c-pagination__item u-ms-2"><a href="#" title="Go to page 3" rel="nofollow" class="c-pagination__link">3</a></li>
    <li class="o-list__item c-pagination__item u-ms-2"><span class="c-pagination__link">&hellip;</span></li>
    <li class="o-list__item c-pagination__item u-ms-2"><a href="#" title="Go to page 130" rel="nofollow" class="c-pagination__link">130</a></li>
    <li class="o-list__item c-pagination__item u-ms-2"><a href="#" title="Go to page 131" rel="nofollow" class="c-pagination__link">131</a></li>
    <li class="o-list__item c-pagination__item u-ms-2 is-next"><a href="#" rel="next nofollow" title="Next" class="c-pagination__link">Next &gt;</a></li>
  </ul>
</nav>
