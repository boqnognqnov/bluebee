<div class="c-main-section c-main-section--lg u-mb-neg-gamma">
  <div class="o-grid">
    <article class="c-step o-grid__item u-1-of-2-bp3 u-1-of-4-bp4 u-mb-gamma">
      <div class="u-align-center">
        <p class="c-step__number">
          1
        </p>
        <div class="c-step__content">
          <h2 class="c-card__title u-ms1 u-align-center u-color-alpha-base js-match-height">
            {!! @$steps[1]['title'] !!}
          </h2>
          <p class="u-ms-1 u-align-center u-mb-delta u-color-neutral-base">
            {!! @$steps[1]['desc'] !!}
          </p>
        </div>
      </div>
    </article>
    <article class="c-step o-grid__item u-1-of-2-bp3 u-1-of-4-bp4 u-mb-gamma">
      <div class="u-align-center">
        <p class="c-step__number">
          2
        </p>
        <div class="c-step__content">
          <h2 class="c-card__title u-ms1 u-align-center u-color-alpha-base js-match-height">
            {!! @$steps[2]['title'] !!}
          </h2>
          <p class="u-ms-1 u-align-center u-mb-delta u-color-neutral-base">
            {!! @$steps[2]['desc'] !!}
          </p>
        </div>
      </div>
    </article>
    <article class="c-step o-grid__item u-1-of-2-bp3 u-1-of-4-bp4 u-mb-gamma">
      <div class="u-align-center">
        <p class="c-step__number">
          3
        </p>
        <div class="c-step__content">
          <h2 class="c-card__title u-ms1 u-align-center u-color-alpha-base js-match-height">
            {!! @$steps[3]['title'] !!}
          </h2>
          <p class="u-ms-1 u-align-center u-mb-delta u-color-neutral-base">
            {!! @$steps[3]['desc'] !!}
          </p>
        </div>
      </div>
    </article>
    <article class="c-step o-grid__item u-1-of-2-bp3 u-1-of-4-bp4 u-mb-gamma">
      <div class="u-align-center">
        <p class="c-step__number">
          4
        </p>
        <div class="c-step__content">
          <h2 class="c-card__title u-ms1 u-align-center u-color-alpha-base js-match-height">
            {!! @$steps[4]['title'] !!}
          </h2>
          <p class="u-ms-1 u-align-center u-mb-delta u-color-neutral-base">
            {!! @$steps[4]['desc'] !!}
          </p>
        </div>
      </div>
    </article>
  </div>
</div>