<div class="u-mb-neg-gamma">
    <div class="o-grid o-grid--gutter news-slider">


        @foreach($items as $item)

            <article class="o-grid__item">
                <div class="c-card c-card--alpha u-mb-jota js-match-height">
                    <div class="c-card__image c-card__image-top u-ratio-landscape u-bgcolor-neutral-xxx-light">
                        <div class="c-background-image p-blazy js-blazy"
                             src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                             data-src="{!! asset($item['background']['default']) !!}"
                             data-src-medium="{!! asset($item['background']['medium']) !!}"
                             data-src-small="{!! asset($item['background']['small']) !!}"
                                ></div>
                    </div>
                    <div class="c-card__content c-card__content--lg">
                        <div class="u-width-xs u-align-horizontal">
                            <h2 class="c-card__title u-ms1 u-color-neutral-xx-dark js-truncate">
                                {!! $item['title'] !!}
                            </h2>

                            <div class="u-ms-1 u-mb-delta u-color-neutral-base c-card__body js-truncate">
                                {!! $item['text'] !!}
                            </div>

                            <div class="o-grid o-grid--gutter">
                                <div class="o-grid__item u-1-of-2-bp3 js-match-height">
                                    <p class="u-ms-1">
                                        <a href="{!! url('news/'.$item['slug']) !!}" class="c-link">Learn more</a>
                                    </p>
                                </div>
                                <div class="o-grid__item u-1-of-2-bp3 js-match-height">
                                    <ul class="o-list o-list--horizontal c-social-links u-align-right">
                                        <li class="o-list__item c-social-links__item"><a target="_blank" class="c-social-links__link c-button--ghost-neutral"href="https://www.linkedin.com/shareArticle?mini=true&url={!! Request::url() !!}"><span aria-hidden="true" class="c-icon c-icon--linkedin"></span><span class="is-accessible-hidden"> LinkedIn</span></a></li>
                                        <li class="o-list__item c-social-links__item"><a target="_blank" class="c-social-links__link c-button--ghost-neutral"href="https://twitter.com/intent/tweet?text=via+%40bluebeegenomics%3a&url={!! Request::url() !!}"><span aria-hidden="true" class="c-icon c-icon--twitter"></span><span class="is-accessible-hidden"> Twitter</span></a></li>
                                        <li class="o-list__item c-social-links__item"><a target="_blank" class="c-social-links__link c-button--ghost-neutral"href="https://www.facebook.com/sharer/sharer.php?u={!! Request::url() !!}"><span aria-hidden="true" class="c-icon c-icon--facebook-f"></span><span class="is-accessible-hidden"> Facebook</span></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        @endforeach
    </div>
</div>
