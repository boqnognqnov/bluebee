<div class="c-row c-row--md c-row--border-top u-bgcolor-neutral-xxxx-light">
    <div class="o-container">
        <div class="c-main-section">
            <h2 class="u-align-center u-text-uppercase u-ms-1 u-color-neutral-base">
                Our partners
            </h2>
        </div>
        <div class="c-main-section">
            <div class="c-partners">
                @foreach($partners as $onePartner)
                    {{--<a href="{!! url($onePartner['url']) !!}"><img--}}
                    <a href="{!! url('collaborations') !!}"><img
                                src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                                data-src="{!! asset($onePartner['image']) !!}" class="c-partner__logo p-blazy js-blazy"
                                alt="Logo"></a>
                @endforeach
            </div>
        </div>
    </div>
</div>


{{--<a href="{!! url("/collaborations") !!}"><img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="img/logos/partner-tudelft.jpg" class="c-partner__logo p-blazy js-blazy" alt="Logo"></a>--}}
{{--<a href="{!! url("/collaborations") !!}"><img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="img/logos/partner-openpower.jpg" class="c-partner__logo p-blazy js-blazy" alt="Logo"></a>--}}
{{--<a href="{!! url("/collaborations") !!}"><img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="img/logos/partner-cecad.png" class="c-partner__logo p-blazy js-blazy" alt="Logo"></a>--}}
{{--<a href="{!! url("/collaborations") !!}"><img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="img/logos/partner-IBM.png" class="c-partner__logo p-blazy js-blazy" alt="Logo"></a>--}}
{{--<a href="{!! url("/collaborations") !!}"><img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="img/logos/partner-GA4GH.png" class="c-partner__logo p-blazy js-blazy" alt="Logo"></a>--}}
