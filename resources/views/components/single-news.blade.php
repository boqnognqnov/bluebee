<article class="o-media o-media--border">
    <div class="o-grid o-grid--gutter">
        <div class="o-grid__item u-6-of-6-bp3 u-pb-delta">
            <p class="u-ms-2 u-text-uppercase u-fw-beta u-mb-alpha u-color-neutral-light">

                {!! \App\Classes\GlobalFunctions::getStampToStrFullMonth($oneNews['created_at']) !!}

            </p>

            <h2 class="o-media__title">
                <a href="{!! url('news/'.$oneNews['slug_'.\App::getLocale()]) !!}">{!! $oneNews['title_'.\App::getLocale()] !!}</a>
            </h2>

            <div class="list-item js-truncate">
                {!! $oneNews['text_'.\App::getLocale()] !!}
            </div>

            <div class="o-grid o-grid--gutter">
                {{--<div class="o-grid__item u-4-of-6-bp3">--}}
                {{-- <ul class="o-taglist"> --}}
                {{--<a href="#" class="o-taglist__item c-button c-button--ghost-neutral c-button--sm">Lorem ipsum</a>--}}
                {{--<a href="#" class="o-taglist__item c-button c-button--ghost-neutral c-button--sm">Dolor sit amet</a>--}}
                {{--<a href="#" class="o-taglist__item c-button c-button--ghost-neutral c-button--sm">DNA</a>--}}
                {{--<a href="#" class="o-taglist__item c-button c-button--ghost-neutral c-button--sm">Extra long tag--}}
                {{--title with many words</a>--}}
                {{--<a href="#" class="o-taglist__item c-button c-button--ghost-neutral c-button--sm">Whatiftherewasonereallybigwordhere</a>--}}
                {{-- </ul> --}}
                {{--</div>--}}
                <div class="o-grid__item u-6-of-6-bp3 u-mt-delta">
                    <ul class="o-list o-list--horizontal c-social-links">
                        <li class="o-list__item c-social-links__item"><a class="c-social-links__link c-button--ghost-neutral"href="https://www.facebook.com/sharer/sharer.php?u={!! url('news/'.$oneNews['slug_'.\App::getLocale()]) !!}" target="_blank"><span aria-hidden="true" class="c-icon c-icon--facebook"></span><span class="is-accessible-hidden"> Facebook</span></a></li>
                        <li class="o-list__item c-social-links__item"><a class="c-social-links__link c-button--ghost-neutral" href="https://twitter.com/intent/tweet?text={!! $oneNews['title_'.\App::getLocale()] !!}&url={!! url('news/'.$oneNews['slug_'.\App::getLocale()]) !!}&via=bluebeegenomics" target="_blank"><span aria-hidden="true" class="c-icon c-icon--twitter"></span><span class="is-accessible-hidden"> Twitter</span></a></li>
                        <li class="o-list__item c-social-links__item"><a class="c-social-links__link c-button--ghost-neutral"href="https://www.linkedin.com/shareArticle?mini=true&url={!! url('news/'.$oneNews['slug_'.\App::getLocale()]) !!}" target="_blank"><span aria-hidden="true" class="c-icon c-icon--linkedin"></span><span class="is-accessible-hidden"> LinkedIn</span></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</article>
