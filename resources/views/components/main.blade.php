<div class="c-main-section c-main-section--lg">
    <div class="u-max-width-sm s-content u-align-horizontal u-align-center">
        @if(!empty($page_title))
            <h1 class="u-mb-gamma">
                {!! @$page_title !!}
            </h1>
        @endif
        @if(!empty($page_desc))
            <p class="u-color-neutral-base u-ms1">
                {!! @$page_desc !!}
            </p>
        @endif
    </div>
</div>