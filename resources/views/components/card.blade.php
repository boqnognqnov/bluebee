<div class="c-card c-card--alpha u-mb-jota js-match-height">
    <a href="{!! url('contact/'.$office['id']) !!}"
       class="c-card__image c-card__image-top u-ratio-landscape u-bgcolor-neutral-xxx-light"
       style="display: block;">

        <div class="c-background-image p-blazy js-blazy"
             data-src="{!! asset(\App\OfficesItemsEntity::$path.$office['image']) !!}"
             data-src-medium="{!! asset(\App\OfficesItemsEntity::$pathMedium.$office['image']) !!}"
             data-src-small="{!! asset(\App\OfficesItemsEntity::$pathSmall.$office['image']) !!}"
                ></div>
    </a>

    <div class="c-card__content c-card__content--lg">
        <div class="u-width-xs u-align-horizontal">
            <h2 class="c-card__title u-ms1 u-color-neutral-xx-dark">
                {!! @$office['title_'.\App::getLocale()] !!}
            </h2>

            <p class="u-ms-1 u-color-neutral-base">
                {{--{!! @$office['text_'.\App::getLocale()] !!}--}}
                {!! @$office['address_'.\App::getLocale()] !!}
            </p>

            <p class="u-ms-1">
                <a href="{!! url('contact/'.$office['id']) !!}" class="c-link">Learn more</a>
            </p>
        </div>
    </div>
</div>