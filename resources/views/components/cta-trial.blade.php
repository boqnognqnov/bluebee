<div class="c-row c-row--lg u-bgcolor-beta-base">
    <div class="c-background-image u-opacity-30 p-blazy js-blazy"
         data-src="{!! asset($image['default']) !!}"
         data-src-medium="{!! asset($image['medium']) !!}"
         data-src-small="{!! asset($image['small']) !!}"
            ></div>
    <div class="o-container">
        <div class="u-mw-sm u-align-center u-align-horizontal">
            <h2 class="u-ms4 u-ms6-bp3 u-fw-alpha u-color-neutral-xxxx-light u-mb-beta">{!! $title !!}</h2>

            <p class="u-ms2 u-color-neutral-xxxx-light u-opacity-70 u-mb-jota">{!! $text !!}</p>

            <p>
                <a href="{!! url($url) !!}" target="_blank" class="c-button c-button--lg c-button--light-gamma c-button--effect-1">{!! $button !!}</a>
            </p>
        </div>
    </div>
</div>
