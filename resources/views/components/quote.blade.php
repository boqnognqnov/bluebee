<div class="c-row c-row--md u-bgcolor-neutral-xxxx-light">
    <div class="o-container">
        <p class="u-ms2 u-color-neutral-base u-align-center u-mb-jota">
            {!! @$name !!}
        </p>
        <blockquote
                class="u-ms1 u-ms3-bp3 u-align-center u-max-width-sm u-align-horizontal u-color-alpha-base u-mb-jota">
            {!! $text !!}
        </blockquote>
        @if(!empty($url) && !empty($button))
            <p class="c-button-group u-align-center">
                <a class="c-button c-button--gamma c-button--md" href="{!! url(@$url) !!}"
                   role="button">{!! @$button !!}</a>
            </p>
        @endif
    </div>
</div>
