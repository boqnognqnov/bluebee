<!-- Why Bluebee inner - no image -->

<div class="c-row c-row--sm c-row--border-top u-bgcolor-neutral-xxx-light">
  <div class="o-container">
    <div class="o-grid o-grid--gutter u-mb-gamma ">
      <div class="o-grid__item u-2-of-2-bp4">
        <div class="u-align-horizontal s-content u-mb-jota">
          <h2>
            {!! $title !!}
          </h2>
          <p>
            {!! $content !!}
          </p>
          <p>
            <a href="#" class="c-link">Learn more</a>
          </p>
        </div>
      </div>
    </div>
  </div>
</div>