<article class="o-grid__item u-1-of-2-bp3 u-1-of-3-bp4">
    <div class="c-card c-card--alpha u-mb-jota js-match-height">
        <div class="c-card__image u-border-radius-tr u-ratio-square u-bgcolor-neutral-xxx-light">
            <div class="c-background-image u-border-radius-tr p-blazy js-blazy"
                 data-src="{!! asset(\App\TeamItemsEntity::$path.$img) !!}"
                 data-src-small="{!! asset(\App\TeamItemsEntity::$pathMedium.$img) !!}"
                 data-src-medium="{!! asset(\App\TeamItemsEntity::$pathSmall.$img) !!}"
            ></div>
        </div>
        <div class="c-card__content c-card__content--lg">
            <div class="u-width-xs u-align-horizontal">
                <h2 class="c-card__title u-ms2 u-color-neutral-xx-dark u-mb-alpha">
                    {!! @$name !!}
                </h2>

                <p class="u-ms-1 u-color-neutral-base">
                    {!! @$job !!}
                </p>

                <p>
                    {!! @$description !!}
                </p>
                <ul class="o-list o-list--horizontal c-social-links">
                    @if(!empty($linkedin))
                        <li class="o-list__item c-social-links__item"><a
                                    class="c-social-links__link c-button--ghost-neutral"
                                    href="{!! @$linkedin !!}" target="_blank"><span
                                        aria-hidden="true" class="c-icon c-icon--linkedin"></span><span
                                        class="is-accessible-hidden"> LinkedIn</span></a></li>
                    @endif
                    @if(!empty($twitter))
                        <li class="o-list__item c-social-links__item"><a
                                    class="c-social-links__link c-button--ghost-neutral"
                                    href="{!! @$twitter !!}" target="_blank"><span
                                        aria-hidden="true" class="c-icon c-icon--twitter"></span><span
                                        class="is-accessible-hidden"> Twitter</span></a></li>
                    @endif

                    @if(!empty($email))
                        <li class="o-list__item c-social-links__item"><a
                                    class="c-social-links__link c-button--ghost-neutral"
                                    href="mailto:{!! @$email !!}" target="_blank"><span
                                        aria-hidden="true" class="c-icon c-icon--envelope"></span><span
                                        class="is-accessible-hidden" target="_top"> Mail</span></a></li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
</article>
