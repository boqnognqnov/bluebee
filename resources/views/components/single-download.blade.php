<article class="o-media o-event o-media--border">
    <div class="o-grid o-grid--gutter">
        <div class="o-grid__item u-2-of-6-bp3 js-match-height">
            <img src="{!! asset(\App\DownloadsItemsEntity::$pathFrontImageSmall.$oneDownload['image']) !!}" alt=""
                 class="u-mb-delta o-downloads__img u-align-vertical">
        </div>
        <div class="o-grid__item u-4-of-6-bp3 js-match-height">
            <p class="u-ms-2 u-text-uppercase u-fw-beta u-mb-alpha u-color-neutral-light">
                {{--December 11, 2015--}}
                {{--{!! \App\Classes\GlobalFunctions::getStampToStrFullMonth($oneDownload['created_at']) !!}--}}
            </p>

            <h2 class="o-media__title" id="file_item_{!! $oneDownload['id'] !!}">

                @if($currentDataCardId!=$oneDownload['category_id'])

                    {{--<a href="#fectModal" rel="modal:open"--}}
                    {{--data_file_id="{!! $oneDownload['id'] !!}"--}}
                    {{--class="fModalClass">{!! $oneDownload['title_'.\App::getLocale()] !!}</a>--}}

                    @if(\Auth::check())
                        <a href="{!! asset('downloads/download/file/' . $oneDownload['id']) !!}">{!! $oneDownload['title_'.\App::getLocale()] !!}</a>
                    @else
                        <a href="#loginModal" rel="modal:open">{!! $oneDownload['title_'.\App::getLocale()] !!}</a>
                    @endif

                @else
                    <a href="{!! url('pipelines#pipeid'.$oneDownload['id']) !!}">
                        {!! $oneDownload['title_'.\App::getLocale()] !!}</a>
                @endif
            </h2>

            <p>
                {!! $oneDownload['text_'.\App::getLocale()] !!}
            </p>

            <p>


                @if($currentDataCardId!=$oneDownload['category_id'])

                    {{--<span class="c-icon c-icon--file-empty u-color-neutral-base"></span> <a--}}
                    {{--href="#fectModal" rel="modal:open" data_file_id="{!! $oneDownload['id'] !!}"--}}
                    {{--class="fModalClass">Click here to download the document.</a>--}}
                    @if(\Auth::check())
                        <span class="c-icon c-icon--file-empty u-color-neutral-base"></span> <a
                                href="{!! asset('downloads/download/file/' . $oneDownload['id']) !!}">Click here
                            to
                            download</a>
                    @else
                        <span class="c-icon c-icon--file-empty u-color-neutral-base"></span> <a href="#loginModal"
                                                                                                rel="modal:open">Click
                            here to download the document.</a>


                    @endif
                @else

                    <span class="c-icon c-icon--file-empty u-color-neutral-base"></span> <a
                            href="{!! url('pipelines#pipeid'.$oneDownload['id']) !!}">Click here to request the
                        datacard.</a>

                @endif
            </p>
        </div>
    </div>
</article>
