<script src="https://code.jquery.com/jquery-2.2.2.min.js"></script>
<div class="u-align-horizontal u-max-width-sm">
    <div class="c-card c-card--alpha u-mb-gamma">
        <div id="jsMap" class="p-gmap p-gmap--md"></div>
    </div>
    <input type="hidden" name="coordA" value="{!! $coordA !!}">
    <input type="hidden" name="coordB" value="{!! $coordB !!}">

    <script>
        //        $(document).ready(function () {
        var a = $('input[name="coordA"]').val();
        var b = $('input[name="coordB"]').val();
        //        console.log(b);
        //        alert(parseFloat(a));
        //        alert(parseFloat(b));
        //        alert($('input[name="coordA"]').val());
        //        alert($('input[name="coordB"]').val());
        function initMap() {
//            var myLatLng = {lat: 43.233273, lng: 27.863636};
            var myLatLng = {
                lat: parseFloat($('input[name="coordA"]').val()),
                lng: parseFloat($('input[name="coordB"]').val())
            };
            var mapDiv = document.getElementById('jsMap');
            var map = new google.maps.Map(mapDiv, {
                center: myLatLng,
                scrollwheel: false,
                zoom: 16
            });
            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map
            });
        }
        //        });

    </script>
    <script src="https://maps.googleapis.com/maps/api/js?callback=initMap" async defer></script>
</div>