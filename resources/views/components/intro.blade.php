<!-- change background color or background gradient here -->
<div class="c-row c-intro u-bgcolor-alpha-dark u-bggradient-alpha-dark-beta-base-to-right">
    <!-- change background image here -->
    <div class="c-intro__image c-background-image u-opacity-20 u-z-alpha p-blazy js-blazy"
         data-src={!! asset($background['default']) !!}
                 data-src-medium={!! asset($background['medium']) !!}
                 data-src-small={!! asset($background['small']) !!}
    ></div>
    <!-- possibilty to add a background visual
    <div class="c-background-visual c-background-visual--white c-background-visual--br u-opacity-10 u-z-alpha"></div>
    -->
    {{--@if(isset($fectiveVal))--}}
    <div class="c-row c-breadcrumb u-z-beta">
        <div class="o-container">

            @if(isset($breadcrumb) && (@$breadcrumb !== ''))
                <ul class="o-list o-list--horizontal c-breadcrumb-list">
                    <li class="o-list__item c-breadcrumb-list__item"><a class="c-breadcrumb-list__link"
                                                                        href="/">Home</a></li>
                    @foreach($breadcrumb as $oneCrumb)
                        <li class="o-list__item c-breadcrumb-list__item"><a class="c-breadcrumb-list__link"
                                                                            href="{!! url(@$oneCrumb['url']) !!}">{!! @$oneCrumb['name'] !!}</a>
                        </li>
                    @endforeach
                </ul>
            @elseif(isset($breadcrumb) && (@$breadcrumb == ''))

            @else
                <ul class="o-list o-list--horizontal c-breadcrumb-list">
                    <li class="o-list__item c-breadcrumb-list__item"><a class="c-breadcrumb-list__link"
                                                                        href="/">Home</a></li>
                    <li class="o-list__item c-breadcrumb-list__item">{!! @$title !!}</li>
                </ul>
            @endif
        </div>
    </div>
    {{--@endif--}}
    <div class="o-container c-intro__container u-z-gamma">
        <div class="c-intro__content">
            <div class=" u-max-width-sm">
                <h1 class="u-color-neutral-xxxx-light u-mb-gamma">
                    {!! @$title !!}
                </h1>

                <p class="u-color-neutral-xxxx-light u-fw-alpha u-ms1">
                    {!! @$body !!}
                </p>
                @if(isset($button))
                    @if(isset($button['text']))
                        @if($button['text'] !== '')
                            <a href="{!! @$button['link'] !!}" class="c-button c-button--md c-button--gamma">
                                {!! @$button['text'] !!}
                            </a>
                        @endif
                    @endif
                @endif
            </div>
        </div>
    </div>
</div>
