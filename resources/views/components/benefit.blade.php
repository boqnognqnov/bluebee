<article class="o-grid__item u-1-of-2-bp3 u-1-of-4-bp4">
    <div class="c-card c-card--alpha u-mb-jota">
        <div class="c-card__content c-card__content--lg">
            {{--<a href="#" class="c-card__content c-card__content--lg" style="display: block; text-decoration: none !important;">--}}
            <div class="u-width-xs u-align-horizontal">
                <p class="u-align-center u-mb-alpha">
                    {{--<span class="c-icon-sprite c-icon-sprite--60 c-icon-sprite--{!! @$icon !!}"></span>--}}
                    <span class="c-icon-sprite c-icon-sprite--60 {!! $imgClass !!}"
                          style="background-image: url({!! @$icon !!});"></span>
                </p>

                <h2 class="c-card__title u-ms1 u-align-center u-color-alpha-base">
                    {!! @$title !!}
                </h2>

                <p class="u-ms-1 u-align-center u-mb-delta u-color-neutral-base js-match-height">
                    {!! @$desc !!}
                </p>
                <p class="u-align-center">
                    <a href="{!! url('why-bluebee/'.@$url) !!}"
                       class="c-button c-button--ghost-beta c-button--md">{!! @$buttonText !!}</a>
                </p>
            </div>
        </div>
    </div>
</article>