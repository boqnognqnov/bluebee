@extends( 'master' )

@section( 'content' )
    @if( $pageData['raw']['head_is_on']==true)
        @include('components.intro',$pageData['intro'])
    @endif
    <div class="c-row c-row--md c-row--border-top u-bgcolor-neutral-xxx-light u-mb-neg-gamma">
        <div class="o-container">
        @if($pageData['is_manager']==true)
                <h2>Management team</h2>
            @else
                <h2>Advisory board</h2>
            @endif

            <div class="o-grid o-grid--gutter">
                @foreach( $pageData['team'] as $employee)
                    @include('components.team-member', $employee)
                @endforeach
            </div>
        </div>
    </div>
    @if( $pageData['raw']['try_is_on']==true)
        {{--@include('components.cta-team')--}}
        @include('components.cta-trial',$pageData['trial'])
    @endif
@endsection

@section( 'view-scripts' )

@endsection
