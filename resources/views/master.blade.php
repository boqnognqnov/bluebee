@extends( 'layouts.default' )

@section( 'body' )

<!-- Page header -->
<div class="c-header js-nav-holder u-z-alpha" data-spy="affix" data-offset-top="60" data-offset-bottom="200">
    <div class="o-container">
        <div class="o-grid">
            <div class="o-grid__item">
                <div class="c-header__top">
                    {{--<form class="c-header__search" action="#">
                      <input type="text" placeholder="search" class="c-input-text c-input-text--sm c-header__search-input">
                      <button class="c-button c-header__search-button c-button--sm"><span class="c-icon c-icon--search" aria-hidden="true"></span><span class="is-accessible-hidden">Search</span></button>
                    </form>--}}
                    <nav class="c-nav-login">
                        <ul class="o-list c-nav-login__list">
                            <li class="c-nav-login__item">
                                @if(isset($demoSeeder))
                                    <a href="{!! url($demoSeeder) !!}"
                                       class="c-nav-login__link c-button c-button--sm c-button--beta"><span
                                                class="c-nav-login__label">Free trial</span></a>
                                @else
                                    <a href="{!! url('demo/header/unknown') !!}"
                                       class="c-nav-login__link c-button c-button--sm c-button--beta"><span
                                                class="c-nav-login__label">Free trial</span></a>
                                @endif
                            </li>
                            <li class="c-nav-login__item">
                                <a href="https://demo.bluebee.com/demo/#!login"
                                   class="c-nav-login__link c-button c-button--text c-button--text-neutral c-button--sm c-nav-login__login-link"><span
                                            class="c-icon c-icon--lock" aria-hidden="true"></span> <span
                                            class="c-nav-login__label">Login</span></a>
                            </li>
                        </ul>
                    </nav>
                </div>


                <h1 class="c-logo">
                    <a href="{!! url('/') !!}" class="c-logo__link">
                        Bluebee
                    </a>
                </h1>


                <nav class="c-nav-main js-nav">
                    <ul class="o-list c-nav-main__list">
                        <li class="c-nav-main__item @if( Request::is( '/') ) is-selected @endif">
                            <a href="{!! url('/') !!}" class="c-nav-main__link">Home</a>
                        </li>
                        <li class="c-nav-main__item @if( Request::is( 'features') ) is-selected @endif">
                            <a href="{!! url('/features') !!}" class="c-nav-main__link">Features</a>
                            {{--<ul>--}}
                            {{--<li><a href="#">how does it work</a></li>--}}
                            {{--<li><a href="#">pipelines</a></li>--}}
                            {{--</ul>--}}
                        </li>
                        <li class="c-nav-main__item @if( Request::is( 'why-bluebee') ) is-selected @endif">
                            <a href="{!! url('/why-bluebee') !!}" class="c-nav-main__link">Why Bluebee</a>
                        </li>
                        <li class="c-nav-main__item @if( Request::is( 'use-cases') ) is-selected @endif">
                            <a href="{!! url('/use-cases') !!}" class="c-nav-main__link">Use cases</a>

                        </li>
                        <li class="c-nav-main__item @if( Request::is( 'knowledge-center') ) is-selected @endif">
                            <a href="{!! url('/knowledge-center') !!}" class="c-nav-main__link">Knowledge Centre</a>
                            {{--<ul>--}}
                            {{--<li><a href="#">News</a></li>--}}
                            {{--<li><a href="#">Events</a></li>--}}
                            {{--<li><a href="#">Downloads</a></li>--}}
                            {{--</ul>--}}
                        </li>
                        <li class="c-nav-main__item @if( Request::is( 'about') ) is-selected @endif">
                            <a href="{!! url('/about') !!}" class="c-nav-main__link">About Bluebee</a>
                            {{--<ul>--}}
                            {{--<li><a href="#">Team</a></li>--}}
                            {{--<li><a href="#">Contacts</a></li>--}}
                            {{--<li><a href="#">Collaborators</a></li>--}}
                            {{--</ul>--}}
                        </li>
                        <li class="c-nav-main__item @if( Request::is( 'join') ) is-selected @endif">
                            <a href="{!! url('/join') !!}" class="c-nav-main__link">Join the team</a>
                        </li>
                    </ul>
                </nav>
                <button class="c-nav-main-trigger js-nav-trigger">
                    <span class="c-nav-main-trigger__icon"></span>
                    <span class="is-accessible-hidden">Menu</span>
                </button>
            </div>
        </div>
    </div>
</div>

<!-- End page header -->

@yield( 'content' )

<!-- Page footer -->
<footer class="c-row c-row--lg c-doormat">
    <div class="o-container">
        <div class="o-grid o-grid--gutter">
            <div class="o-grid__item u-1-of-5-bp3 u-1-of-5-bp4">
                <h4 class="c-doormat__title">
                    <a href="{!! url('features') !!}" class="o-link__noline"><span class="u-color-neutral-xxxx-light">What is Bluebee</span></a>
                </h4>
                <ul class="o-list c-doormat-list">
                    <li class="c-doormat-list__item"><a class="c-doormat__link"
                                                        href="{!! url('features') !!}">Features</a></li>
                    <li class="c-doormat-list__item"><a class="c-doormat__link" href="{!! url('how-it-works') !!}">How
                            does it work</a></li>
                    <li class="c-doormat-list__item"><a class="c-doormat__link" href="{!! url('pipelines') !!}">Pipelines</a>
                    </li>
                </ul>
            </div>
            <div class="o-grid__item u-1-of-5-bp3 u-1-of-5-bp4">
                <h4 class="c-doormat__title">
                    <a href="{!! url('use-cases') !!}" class="o-link__noline"><span class="u-color-neutral-xxxx-light">Who is it for</span></a>
                </h4>
                <ul class="o-list c-doormat-list">
                    <li class="c-doormat-list__item"><a class="c-doormat__link"
                                                        href="{!! url('use-cases/research-centers') !!}">Research
                            centers</a></li>
                    <li class="c-doormat-list__item"><a class="c-doormat__link"
                                                        href="{!! url('use-cases/clinical-labs') !!}">Clinical labs</a>
                    </li>
                    <li class="c-doormat-list__item"><a class="c-doormat__link"
                                                        href="{!! url('use-cases/diagnostic-test-providers') !!}">Diagnostic
                            test providers</a></li>
                    <li class="c-doormat-list__item"><a class="c-doormat__link"
                                                        href="{!! url('use-cases/sequencing-service-providers') !!}">Sequencing
                            service providers</a></li>
                </ul>
            </div>
            <div class="o-grid__item u-1-of-5-bp3 u-1-of-5-bp4">
                <h4 class="c-doormat__title">
                    <a href="{!! url('why-bluebee') !!}" class="o-link__noline"><span
                                class="u-color-neutral-xxxx-light">Why choose Bluebee</span></a>
                </h4>
                <ul class="o-list c-doormat-list">
                    <li class="c-doormat-list__item"><a class="c-doormat__link"
                                                        href="{!! url('why-bluebee/high-performance') !!}">High
                            Performance</a></li>
                    <li class="c-doormat-list__item"><a class="c-doormat__link"
                                                        href="{!! url('why-bluebee/security') !!}">Security</a></li>
                    <li class="c-doormat-list__item"><a class="c-doormat__link"
                                                        href="{!! url('why-bluebee/convenience') !!}">Convenience</a>
                    </li>
                    <li class="c-doormat-list__item"><a class="c-doormat__link"
                                                        href="{!! url('why-bluebee/cost-effective') !!}">Cost
                            Effective</a></li>
                </ul>
            </div>
            <div class="o-grid__item u-1-of-5-bp3 u-1-of-5-bp4">
                <h4 class="c-doormat__title">
                    <a href="{!! url('about') !!}" class="o-link__noline"><span class="u-color-neutral-xxxx-light">About Bluebee</span></a>
                </h4>
                <ul class="o-list c-doormat-list">
                    <li class="c-doormat-list__item"><a class="c-doormat__link" href="{!! url('news/list') !!}">News</a>
                    </li>
                    <li class="c-doormat-list__item"><a class="c-doormat__link"
                                                        href="{!! url('events/list') !!}">Events</a></li>
                    <li class="c-doormat-list__item"><a class="c-doormat__link"
                                                        href="{!! url('about#team-section') !!}">Team</a></li>
                    <li class="c-doormat-list__item"><a class="c-doormat__link" href="{!! url('join') !!}">Careers</a>
                    </li>
                </ul>
            </div>
            <div class="o-grid__item u-1-of-5-bp3 u-1-of-5-bp4">
                <h4 class="c-doormat__title">
                    <a href="{!! url('contact') !!}" class="o-link__noline"><span class="u-color-neutral-xxxx-light">Contact</span></a>
                </h4>
                <ul class="o-list c-doormat-list">
                    <li class="c-doormat-list__item">Molengraaffsingel 12 - 14</li>
                    <li class="c-doormat-list__item">2629 JD Delft</li>
                    <li class="c-doormat-list__item">The Netherlands</li>
                    <li class="c-doormat-list__item"><a class="c-doormat__link" href="mailto:info@bluebee.com">info&#64;bluebee.com</a>
                    </li>
                    <li class="c-doormat-list__item"><a class="c-doormat__link c-doormat__tel o-link__noline"
                                                        href="tel:0031152036002">+31 15 203 6002</a></li>
                </ul>
            </div>
        </div>
        <div class="c-footer">
            <p class="c-footer-copy">
                <span class="c-footer-copy__item">&copy;2016 Bluebee</span>
                <a class="c-footer-copy__item c-footer-copy__link" href="{!! url('disclaimer') !!}">Disclaimer</a> <a
                        class="c-footer-copy__item c-footer-copy__link" href="{!! url('privacy') !!}">Privacy</a>
                <a class="footer-creditails" target="_blank" href="http://onecreative.eu/">Created by: <img src="{!! asset('img/logos/symbol.png') !!}" /></a>
            </p>
            <ul class="o-list o-list--horizontal c-footer-social">
                <li class="o-list__item c-footer-social__item"><a href="https://www.facebook.com/bluebeegenomics?ref=hl"
                                                                  target="_blank" class="c-footer-social__link"><span
                                aria-hidden="true" class="c-icon c-icon--facebook"></span><span
                                class="is-accessible-hidden"> Facebook</span></a></li>
                <li class="o-list__item c-footer-social__item"><a href="https://twitter.com/BluebeeGenomics"
                                                                  target="_blank" class="c-footer-social__link"><span
                                aria-hidden="true" class="c-icon c-icon--twitter"></span><span
                                class="is-accessible-hidden"> Twitter</span></a></li>
                <li class="o-list__item c-footer-social__item"><a
                            href="https://www.linkedin.com/company/bluebee?trk=biz-companies-cym" target="_blank"
                            class="c-footer-social__link"><span aria-hidden="true"
                                                                class="c-icon c-icon--linkedin"></span><span
                                class="is-accessible-hidden"> LinkedIn</span></a></li>
            </ul>
        </div>
    </div>
</footer>

<!-- End page footer -->

<div class="behindthecurtain">
    <!-- LOGIN FORM -->
    <!-- href="#loginModal" rel="modal:open" -->
    <div id="loginModal" class="c-card c-card--alpha u-z-jota u-max-width-md">
        <form action="#" method="post">
            <div class="c-card__content c-card__header">
                <h2>
                    Login
                </h2>
            </div>
            <div class="c-card__content">

                {{--<div class="c-alert-box c-alert-box--error">--}}
                <div id="loginModalAlertDIv">
                    {{--<p class="u-ms-1">--}}
                    {{--Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit aperiam culpa eaque quibusdam--}}
                    {{--consectetur adipisci, corporis.--}}
                    {{--</p>--}}
                    @if(\Session::get('loginError'))
                        <p class="u-ms-1">{!! \Session::get('loginError') !!}</p>
                    @endif
                </div>
                <div class="o-grid o-grid--gutter">
                    <p class="o-grid__item u-1-of-3-bp3">


                        <a href="{!! url('facebook/authorize/'.str_replace('/','-',\Request::path())) !!}"
                           class="c-button c-button--md c-button--block c-button--social c-button--facebook"
                           id="change-to-fb-root-when-ready" onclick="fb_login();"><span
                                    class="c-icon--facebook-f"></span> Log in with Facebook</a>
                    </p>
                    <p class="o-grid__item u-1-of-3-bp3">
                        <a href="{!! url('twitter/authorize/'.str_replace('/','-',\Request::path())) !!}"
                           class="c-button c-button--md c-button--block c-button--social c-button--twitter"><span
                                    class="c-icon--twitter"></span> Log in with Twitter</a>
                    </p>
                    <p class="o-grid__item u-1-of-3-bp3">
                        <a href="{!! url('linkedin/authorize/'.str_replace('/','-',\Request::path())) !!}"
                           class="c-button c-button--md c-button--block c-button--social c-button--linkedin"
                           onclick="liAuth()"><span class="c-icon--linkedin"></span> Log in with LinkedIn</a>
                        {{--<a href="{!! url('linkedin/authorize') !!}"--}}
                        {{--class="c-button c-button--md c-button--block c-button--social c-button--linkedin"--}}
                        {{--onclick="liAuth()"><span class="c-icon--linkedin"></span> Log in with LinkedIn</a>--}}
                    </p>
                </div>
                <p>
                    <label for="lf-name" class="c-label">Email<abbr title="Required field">*</abbr></label>
                    <input type="email" name="email" id="lf-name" class="c-input-text c-input-text--md"/>
                </p>
                <p>
                    <label for="lf-pass" class="c-label">Password<abbr title="Required field">*</abbr></label>
                    <input type="password" name="password" id="lf-pass" class="c-input-text c-input-text--md"/>
                </p>
            </div>
            <div class="c-card__content c-card__footer">
                <p class="c-button-group">
                    <input class="c-button c-button--float-left c-button--md c-button--beta"
                           onclick="loginRequest()" value="Login"/>
                    <a href="#registrationModal" rel="modal:open"
                       class="c-button c-button--float-left c-button--md c-button--text c-button--text-neutral">Register</a>

                    <a href="#forgetPasswordModal" rel="modal:open"
                       class="c-button c-button--float-left c-button--md c-button--text c-button--text-neutral">forget
                        password</a>
                </p>
            </div>
        </form>
    </div>

    {{--FORGET PASSWORD MODAL--}}
    <div id="forgetPasswordModal" class="c-card c-card--alpha u-z-jota u-max-width-md">
        <form method="POST" action="/password/email">
            {!! csrf_field() !!}
            <div class="c-card__content c-card__header">
                <h2>
                    Send Password Reset Link
                </h2>
            </div>
            <div class="c-card__content">

                {{--<div class="c-alert-box c-alert-box--error">--}}
                <div id="loginModalAlertDIv"></div>
                <div class="o-grid o-grid--gutter">


                </div>
                <p>
                    <label for="lf-name" class="c-label">Email<abbr title="Required field">*</abbr></label>
                    <input type="email" name="email" value="{{ old('email') }}" class="c-input-text c-input-text--md">
                </p>

            </div>
            <div class="c-card__content c-card__footer">
                <p class="c-button-group">
                    <button type="submit" class="c-button c-button--float-left c-button--md c-button--beta">
                        Send Password Reset Link
                    </button>
                </p>
            </div>
        </form>
    </div>


    {{--FORGET PASSWORD MODAL--}}
            <!-- END LOGIN FORM -->

    <!-- REGISTRATION FORM -->
    <!-- href="#registrationModal" rel="modal:open" -->

    <div id="registrationModal" class="c-card c-card--alpha u-z-jota u-max-width-md">
        <form action="#" method="post">
            <div class="c-card__content c-card__header">
                <h2>
                    Registration
                </h2>
            </div>
            <div class="c-card__content">
                <div class="registration_alert_div">
                    {{--<div class="c-alert-box c-alert-box--error registration_alert_div">--}}

                </div>
                <div class="o-grid o-grid--gutter">
                    <p class="o-grid__item u-1-of-3-bp3">
                        <a href="{!! url('facebook/authorize/'.str_replace('/','-',\Request::path())) !!}"
                           class="c-button c-button--alpha c-button--md c-button--block c-button--social c-button--facebook"
                           id="change-to--fb-root--when-ready"><span class="c-icon--facebook-f"></span> Sign up with
                            Facebook</a>
                    </p>
                    <p class="o-grid__item u-1-of-3-bp3">
                        <a href="{!! url('twitter/authorize/'.str_replace('/','-',\Request::path())) !!}"
                           class="c-button c-button--alpha c-button--md c-button--block c-button--social c-button--twitter"><span
                                    class="c-icon--twitter"></span> Sign up with Twitter</a>
                    </p>
                    <p class="o-grid__item u-1-of-3-bp3">
                        <a href="{!! url('linkedin/authorize/'.str_replace('/','-',\Request::path())) !!}"
                           class="c-button c-button--alpha c-button--md c-button--block c-button--social c-button--linkedin"><span
                                    class="c-icon--linkedin"></span> Sign up with LinkedIn</a>
                    </p>
                </div>
                <fieldset>
                    <div class="o-grid o-grid--gutter">
                        <p class="o-grid__item u-1-of-2-bp3">
                            <label for="rf-name" class="c-label">Name<abbr title="Required field">*</abbr></label>
                            <input name="name" type="text" id="rf-name" class="c-input-text c-input-text--md"
                                   placeholder="Full Name"/>
                        </p>
                        <p class="o-grid__item u-1-of-2-bp3">
                            <label for="rf-email" class="c-label">Email address<abbr
                                        title="Required field">*</abbr></label>
                            <input type="email" name="email" id="rf-email" class="c-input-text c-input-text--md"
                                   placeholder="email&#64;address.com"/>
                        </p>
                    </div>
                    <div class="o-grid o-grid--gutter">
                        <p class="o-grid__item u-2-of-2-bp3">
                            <label for="rf-company" class="c-label">Company<abbr title="Required field">*</abbr></label>
                            <input type="text" name="company" id="rf-company" class="c-input-text c-input-text--md"
                                   placeholder="Company"/>
                        </p>
                    </div>
                    <div class="o-grid o-grid--gutter">
                        <p class="o-grid__item u-1-of-2-bp3">
                            <label for="rf-pass1" class="c-label">Password<abbr title="Required field">*</abbr></label>
                            <input type="password" name="password" id="rf-pass1" class="c-input-text c-input-text--md"/>
                        </p>
                        <p class="o-grid__item u-1-of-2-bp3">
                            <label for="rf-pass2" class="c-label">Repeat password<abbr
                                        title="Required field">*</abbr></label>
                            <input type="password" name="re_password" id="rf-pass2"
                                   class="c-input-text c-input-text--md"/>
                        </p>
                    </div>
                </fieldset>
            </div>
            <div class="c-card__content c-card__footer">
                <div class="o-grid o-grid--gutter">
                    <div class="o-grid__item u-1-of-4-bp3">
                    </div>
                    <div class="o-grid__item u-3-of-4-bp3 u-align-right">
                        <a href="#" onclick="signUpRequest()"
                           class="c-button c-button--md c-button--right c-button--beta u-mb-gamma">Sign up</a>
                        <p class="u-ms-3">
                            Already have an account? Please <a href="#loginModal" rel="modal:open">log in here</a>.
                        </p>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <!-- END REGISTRATION FORM -->
</div>

@if(\Session::get('loginError'))
    <script>
        $(document).ready(function () {
            $('#loginModal').modal('show');
        });

    </script>

@endif

@endsection

@section( 'scripts' )



    <script src="{!! asset('js/jquery.modal.min.js') !!}" type="text/javascript" charset="utf-8"></script>
    <script src="{!! asset('js/otherdropdown.js') !!}" type="text/javascript" charset="utf-8"></script>
    <script src="{!! asset('js/scripts.js') !!}" type="text/javascript" charset="utf-8"></script>

    @yield( 'view-scripts' )

@endsection
