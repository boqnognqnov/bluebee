@extends( 'master' )

@section( 'content' )
    {{--@include('components.intro', ['title'=>'High-performance genomics solutions that reduce cost and time-to-diagnose','body'=>'Bluebee\'s unique cloud-based accelerated genomics analysis platform enables fast, efficient and affordable processing of large volumes of clinical data.','button'=>['link'=>'#','text'=>'Learn more'],'breadcrumb'=>'','background'=>['default'=>'img/photos/intro/2400/macbook.jpg','small'=>'img/photos/intro/600/macbook.jpg','medium'=>'img/photos/intro/1200/macbook.jpg']])--}}
    @if($pageData['head_is_on']==true)
        @include('components.intro',  @$pageData['intro'])
    @endif
    @if($pageData['how_work_is_on']==true)
        <div class="c-row c-row--md u-bgcolor-neutral-xxxx-light">
            <div class="o-container">

                @include('components.main', @$pageData['main'])
                {{--@include('components.steps', ['steps'=>[1=>['title'=>'','desc'=>''],2=>['title'=>'','desc'=>''],3=>['title'=>'','desc'=>''],4=>['title'=>'','desc'=>'']]])--}}
                @include('components.steps', ['steps'=>@$pageData['steps']])
                <div class="u-align-center u-mt-jota"><a class="c-button c-button--ghost-beta c-button--md"
                                                         href="{!! url("/how-it-works") !!}" role="button">More info</a>
                </div>
            </div>
        </div>
    @endif
    @if($pageData['why_blue_is_on']==true)
        <div class="c-row c-row--md u-bgcolor-neutral-xxx-light">
            <div class="c-background-visual c-background-visual--br u-opacity-50 u-z-alpha"></div>
            <div class="o-container u-z-beta">
                <div class="c-main-section c-main-section--lg">
                    <div class="u-max-width-sm s-content u-align-horizontal u-align-center">
                        <h1 class="u-mb-gamma">
                            {!! $pageData['benefit']['main_title'] !!}
                        </h1>

                        <p class="u-color-neutral-base u-ms1">
                            {!! $pageData['benefit']['main_text'] !!}
                        </p>
                    </div>
                </div>
                @include('components.sections.benefits',['items'=> @$pageData['benefit']['items']])

            </div>
        </div>
    @endif
    @if($pageData['opinion_is_on']==true)
        @include('components.quote',$pageData['quote'])
    @endif
    @if($pageData['partners_is_on']==true)
        {{--<a href="{!! url('collaborations') !!}">--}}
        @include('components.partners',['partners'=>$pageData['partners']])
        {{--</a>--}}
    @endif
    @if($pageData['try_is_on']==true)
        @include('components.cta-trial',$pageData['trial'])
    @endif
@endsection

@section( 'view-scripts' )

@endsection

@section('title_section', 'Bluebee | Leading provider of high performance genomics solutions')
