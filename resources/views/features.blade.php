@extends( 'master' )

@section( 'content' )


    {{--@include('components.intro', ['title'=>'Features','body'=>'Leverage our high performance computer infrastructure and drastically reduce your processing time. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloremque.','button'=>'','background'=>['default'=>'img/photos/intro/2400/lab-3.jpg','small'=>'img/photos/intro/600/lab-3.jpg','medium'=>'img/photos/intro/1200/lab-3.jpg']])--}}
    @if($pageData['head_is_on']==true)
        @include('components.intro',  @$pageData['intro'])
    @endif
    @foreach($pageData['features'] as $key=>$oneFeature)
        @if($key%2!=0 && !empty($oneFeature['image']))
            @include('components.benefit-media-left', $oneFeature)

        @elseif($key%2==0 && !empty($oneFeature['image']))
            @include('components.benefit-media-right', $oneFeature)

        @else
            @include('components.featuresItem', $oneFeature)

        @endif
    @endforeach



    {{--@include('components.benefit-media-right', ['title'=>'Cost-effective','content'=>'With this option at hand labs can start using preconfigured pipelines for DNA—Seq and RNA-Seq to process their genome data on the Bluebee cloud platform right away. With very limited set-up of user information and data upload configuration, users can start running their analysis within hours and experience the convenience Bluebee offers. No pre—existing onsite data processing pipeline is required.'])--}}
    @if($pageData['favorites_is_on']==true)
        <div class="c-row u-bgcolor-neutral-xxx-light">
            <div class="o-container u-z-beta">
                @include('components.cards-with-label',['favId'=>$pageData['favoriteId']])
            </div>
        </div>
    @endif
    @if($pageData['try_is_on']==true)
        @include('components.cta-trial',$pageData['trial'])
    @endif
@endsection

@section( 'view-scripts' )

@endsection