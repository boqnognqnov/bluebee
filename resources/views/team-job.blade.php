@extends( 'master' )

@section( 'content' )
    @if( $pageData['raw']['head_is_on']==true)
        @include('components.intro',$pageData['intro'])
    @endif
    <div class="c-row c-row--md c-row--border-top u-bgcolor-neutral-xxx-light u-pb-jota u-mb-neg-beta">
        <div class="u-align-horizontal u-max-width-sm c-main-section s-content">
            <h2>
                 {!! $pageData['position']['title_'.\App::getLocale()] !!}
            </h2>

            <p>
                {!! $pageData['position']['requirements_'.\App::getLocale()] !!}
            </p>
            @if(!empty($pageData['position']['image']))
                <div class="u-pt-gamma u-mb-jota">
                    <img class="p-lazy p-lazy--preview js-blazy"
                         src="{!! asset(\App\JobPositionsItemsEntity::$path.$pageData['position']['image']) !!}"
                         data-src="{!! asset(\App\JobPositionsItemsEntity::$pathMedium.$pageData['position']['image']) !!}"
                         data-src-small="{!! asset(\App\JobPositionsItemsEntity::$pathSmall.$pageData['position']['image']) !!}"
                         alt="alt text"
                            />
                    <!-- <p class="u-ms-1 u-color-neutral-base u-align-center u-pt-gamma">
                      image caption Lorem ipsum dolor sit amet.
                    </p> -->
                </div>
            @endif
            {{--<div class="u-pt-gamma u-mb-jota">--}}
                {{--<p><i>Please contact us using the email <a href="mailto:careers@bluebee.com">careers&#64;bluebee.com</a></i>--}}
                {{--</p>--}}
            {{--</div>--}}
        </div>
    </div>
    @if( $pageData['raw']['try_is_on']==true)
        @include('components.cta-trial',$pageData['trial'])
    @endif
@endsection

@section( 'view-scripts' )

@endsection
