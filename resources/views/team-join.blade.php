@extends( 'master' )

@section( 'content' )
    @if( $pageData['raw']['head_is_on']==true)
        @include('components.intro',$pageData['intro'])
    @endif
    <div class="c-row c-row--md c-row--border-top u-bgcolor-neutral-xxx-light u-pb-jota u-mb-neg-beta">
        <div class="u-align-horizontal u-max-width-sm c-main-section s-content">
            <h2>
                {!! $pageData['raw']['title_'.\App::getLocale()] !!}
            </h2>

            <p>
                {!! $pageData['raw']['text_'.\App::getLocale()] !!}
            </p>

            <div class="u-pt-gamma u-mb-jota">
                @if(!empty($pageData['raw']['image']))
                    <img class="p-lazy p-lazy--preview js-blazy"
                         src="{!! asset(\App\JoinTheTeamPageEntity::$path.$pageData['raw']['image']) !!}"
                         data-src="{!! asset(\App\JoinTheTeamPageEntity::$pathMedium.$pageData['raw']['image']) !!}"
                         data-src-small="{!! asset(\App\JoinTheTeamPageEntity::$pathSmall.$pageData['raw']['image']) !!}"
                         alt="alt text"
                    />
                    @endif
                            <!-- <p class="u-ms-1 u-color-neutral-base u-align-center u-pt-gamma">
                  image caption Lorem ipsum dolor sit amet.
                </p> -->
            </div>
            @if(sizeof($pageData['positions']>0))
                <div class="u-pt-gamma u-mb-jota">
                    <h2>Current available positions</h2>
                    <ul class="o-list c-list-hexagon">
                        @foreach($pageData['positions'] as $onePosition)
                            <li class="c-list-hexagon__item"><a
                                        href="{!! url('join/apply/'.$onePosition['id']) !!}">{!! $onePosition['title_'.\App::getLocale()] !!}</a>
                            </li>

                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="u-align-horizontal u-max-width-sm c-main-section s-content">
                {!! $pageData['raw']['text_s_'.\App::getLocale()] !!}
            </div>

        </div>
    </div>
    @if( $pageData['raw']['try_is_on']==true)
        @include('components.cta-trial',$pageData['trial'])
    @endif
@endsection

@section( 'view-scripts' )

@endsection
