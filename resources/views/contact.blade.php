@extends( 'master' )

@section( 'content' )
    @if( $pageData['raw']['head_is_on']==true)
        @include('components.intro',$pageData['intro'])
    @endif
    <div class="c-row c-row--md u-bgcolor-neutral-xxx-light u-pb-delta">
        <div class="u-mb-neg-gamma">
            <div class="u-max-width-md u-align-horizontal">
                <div class="o-container">
                    <div class="o-grid o-grid--gutter">
                        @foreach($pageData['offices'] as $oneOffice)
                            <article class="o-grid__item u-1-of-2-bp3">
                                @include('components.card', ['office'=>$oneOffice])
                            </article>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="c-row c-row--md c-row--border-top u-bgcolor-neutral-xxx-light u-pt-jota" id="contact-form-section">
        <div class="u-max-width-sm u-align-horizontal">
            <div class="o-container">
                <h2>Contact us</h2>

                @if(Session::has('successMessage'))
                    <div class="c-alert-box c-alert-box--success">
                        <p class="u-ms-1">
                            <span>{!! Session::get('successMessage') !!}</span>
                        </p>
                    </div>
                @endif

                @if($errors -> any() )
                    <div class="c-alert-box c-alert-box--error">
                        <p class="u-ms-1">
                        @foreach ($errors->all() as $error)
                            <span>{{ $error }}</span>
                        @endforeach
                        </p>
                    </div>
                @endif

                {!! Form::open(array('action'=>'CContactsController@contactMail','method'=>'post','files'=>true)) !!}
                <fieldset>
                    <div class="o-grid o-grid--gutter">
                        <p class="o-grid__item u-1-of-2-bp3">
                            <label for="contact-first-name" class="c-label">First Name<abbr
                                        title="Required field">*</abbr></label>

                            {!! Form::text( 'firstName',null, array( 'class' => 'c-input-text c-input-text--md','id'=>'contact-first-name' ) ) !!}
                        </p>

                        <p class="o-grid__item u-1-of-2-bp3">
                            <label for="contact-last-name" class="c-label">Last Name<abbr
                                        title="Required field">*</abbr></label>

                            {!! Form::text( 'lastName',null, array( 'class' => 'c-input-text c-input-text--md','id'=>'contact-last-name' ) ) !!}
                        </p>
                    </div>
                    <div class="o-grid o-grid--gutter">
                        <p class="o-grid__item u-2-of-2-bp3">
                            <label for="contact-company" class="c-label">Company<abbr
                                        title="Required field">*</abbr></label>

                            {!! Form::text( 'company',null, array( 'class' => 'c-input-text c-input-text--md','id'=>'contact-company' ) ) !!}
                        </p>
                    </div>
                    <div class="o-grid o-grid--gutter">
                        <p class="o-grid__item u-1-of-2-bp3">
                            <label for="contact-email" class="c-label">Email<abbr
                                        title="Required field">*</abbr></label>

                            {!! Form::email( 'email',null, array( 'class' => 'c-input-text c-input-text--md','id'=>'contact-email' ) ) !!}
                        </p>

                        <p class="o-grid__item u-1-of-2-bp3">
                            <label for="contact-phone" class="c-label">Phone</label>
                            {{--<input type="text" id="contact-phone" class="c-input-text c-input-text--md"--}}
                            {{--placeholder="Phone"/>--}}
                            {!! Form::text( 'phone',null, array( 'class' => 'c-input-text c-input-text--md','id'=>'contact-phone' ) ) !!}
                        </p>
                    </div>

                    <div class="o-grid o-grid--gutter">
                        {{--<p class="o-grid__item u-1-of-2-bp3">--}}
                            {{--<label for="contact-office" class="c-label">Office</label>--}}
                            {{--{!! Form::select( 'officeId',$officesList,0, array( 'class' => 'c-input-text c-input-text--md','id'=>'contact-office' ) ) !!}--}}
                        {{--</p>--}}

                        {!! Form::hidden('officeId','1') !!}


                        {{--<p class="o-grid__item u-1-of-2-bp3">--}}
                            {{--<label for="contact-reason" class="c-label">Enquiry type<abbr--}}
                                        {{--title="Required field">*</abbr></label>--}}
                            {{--{!! Form::select( 'contactReason',$reasons,null, array( 'class' => 'c-input-text c-input-text--md','id'=>'contact-reason' ) ) !!}--}}
                        {{--</p>--}}
                        {!! Form::hidden('contactReason','4') !!}
                    </div>
                    <div class="o-grid o-grid--gutter">
                        <p class="o-grid__item u-2-of-2-bp3">
                            <label for="contact-message" class="c-label">Message<abbr
                                        title="Required field">*</abbr></label>
                            {!! Form::textarea( 'message',null, array( 'class' => 'c-input-text c-input-text--md','id'=>'contact-message','rows'=>'5','cols'=>'30' ) ) !!}
                        </p>
                    </div>
                    <div class="o-grid o-grid--gutter">
                        <div class="o-grid__item u-2-of-4-bp3">
                            <div class="cf">{!! app('captcha')->display(); !!}</div>
                        </div>

                        <div class="o-grid__item u-2-of-4-bp3 u-align-right">
                            {!! Form::submit('Send',['class'=>'c-button c-button--md c-button--beta u-mb-beta']) !!}
                        </div>
                    </div>
                </fieldset>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    @if( $pageData['raw']['try_is_on']==true)
        @include('components.cta-trial',$pageData['trial'])
    @endif
@endsection

@section( 'view-scripts' )
    @if(Session::has('successMessage') || ($errors -> any()))
        <script>
            $(document).ready(function () {
                var aid = 'contact-form-section';
                var aTag = $("#"+ aid);
                $('html,body').animate({scrollTop: aTag.offset().top},'slow');
            });
        </script>
    @endif
@endsection
