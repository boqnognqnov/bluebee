@extends( 'master' )

@section( 'content' )
    @if( $pageData['head_is_on']==true)
        @include('components.intro', $pageData['intro'])
    @endif

    <div class="c-row c-row-md u-bgcolor-neutral-xxx-light">
        {{--<div class="c-background-visual c-background-visual--grey c-background-visual--br u-opacity-50 u-z-alpha"></div>--}}
        <div class="c-row u-bgcolor-neutral-xxx-light">
            <div class="o-container u-z-beta">
                <div class="o-grid o-grid--gutter">
                    <div class="o-grid__item u-1-of-3-bp3 u-1-of-5-bp4 u-push-1-of-5-bp4"><a
                                class="c-button c-button--ghost-alpha c-button--lg c-button--block"
                                href="{!! url('/news/list') !!}#list-view" role="button">News</a></div>
                    <div class="o-grid__item u-1-of-3-bp3 u-1-of-5-bp4 u-push-1-of-5-bp4"><a
                                class="c-button c-button--ghost-beta c-button--lg c-button--block"
                                href="{!! url('/events/list') !!}#list-view" role="button">Events</a></div>
                    <div class="o-grid__item u-1-of-3-bp3 u-1-of-5-bp4 u-push-1-of-5-bp4"><a
                                class="c-button c-button--ghost-gamma c-button--lg c-button--block"
                                href="{!! url('/downloads/list') !!}#list-view" role="button">Downloads</a></div>
                </div>
            </div>
        </div>
        <div class="c-row u-bgcolor-neutral-xxx-light">
            <div class="o-container u-z-beta">
                <div class="o-grid o-grid--gutter">
                    <div class="o-grid__item u-5-of-6-bp3">
                        <h3 class="u-color-alpha-dark">News</h3>
                    </div>
                    <div class="o-grid__item u-1-of-6-bp3 u-align-right">
                        <p>
                            <button class="c-button c-button--gamma c-button--sm c-news-prev" role="button"><i
                                        class="fa fa-chevron-left"></i></button>
                            <button class="c-button c-button--gamma c-button--sm c-news-next" role="button"><i
                                        class="fa fa-chevron-right"></i></button>
                        </p>
                    </div>
                </div>
                @include('components.cards-article',['items'=>$pageData['newsData']])

            </div>
        </div>
        <div class="c-row u-bgcolor-neutral-xxx-light">
            <div class="o-container u-z-beta">
                <div class="o-grid o-grid--gutter">
                    <div class="o-grid__item u-5-of-6-bp3">
                        <h3 class="u-color-alpha-dark">Events</h3>
                    </div>
                    <div class="o-grid__item u-1-of-6-bp3 u-align-right">
                        <p>
                            <a class="c-button c-button--gamma c-button--sm c-events-prev" role="button"><i
                                        class="fa fa-chevron-left"></i></a>
                            <a class="c-button c-button--gamma c-button--sm c-events-next" role="button"><i
                                        class="fa fa-chevron-right"></i></a>
                        </p>
                    </div>
                </div>
                @include('components.cards-event',['events'=>$pageData['eventList']])
            </div>
        </div>
        <div class="c-row u-bgcolor-neutral-xxx-light">
            <div class="o-container u-z-beta">
                <div class="o-grid o-grid--gutter">
                    <div class="o-grid__item u-5-of-6-bp3">
                        <h3 class="u-color-alpha-dark">Downloads</h3>
                    </div>
                    <div class="o-grid__item u-1-of-6-bp3 u-align-right">
                        <p>
                            <button class="c-button c-button--gamma c-button--sm c-downloads-prev" role="button"><i
                                        class="fa fa-chevron-left"></i></button>
                            <button class="c-button c-button--gamma c-button--sm c-downloads-next" role="button"><i
                                        class="fa fa-chevron-right"></i></button>
                        </p>
                    </div>
                </div>
                @include('components.cards-downloads',['items'=>$pageData['downloads'],'currentDataCardId'=>$pageData['currentDataCardId']])
            </div>
        </div>
    </div>
    @if($pageData['sign_up_is_on']==true)
        @include('components.cta-newsletter',$pageData['newsletter'])
    @endif
@endsection

@section( 'view-scripts' )
    <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="{!! asset('vendor/slick/slick.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('js/slick-init.js') !!}"></script>
    {{-- <script type="text/javascript" src="{!! asset('js/js-truncate.js') !!}"></script> --}}
    <script>
        $('.news-slider').on('afterChange', function (slick, currentSlide) {
            console.log(currentSlide);
            chopstick.bLazy.revalidate();
        });
        $('.events-slider').on('afterChange', function (slick, currentSlide) {
            console.log(currentSlide);
            chopstick.bLazy.revalidate();
        });
        $('.downloads-slider').on('afterChange', function (slick, currentSlide) {
            console.log(currentSlide);
            chopstick.bLazy.revalidate();
        });
    </script>
@endsection
