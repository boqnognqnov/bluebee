@extends( 'master' )

@section( 'content' )
    @if($pageData['raw']['head_is_on']==true)
        @include('components.intro',$pageData['intro'] )
    @endif

    <div class="c-row--md c-main-section s-content u-mb-neg-jota">
        <!-- change width with utility classes u-max-width-... -->
        <div class="u-align-horizontal u-max-width-sm">
            {{--<h2>--}}
            {{--{!!  $pageData['raw']['front_title_'.\App::getLocale()] !!}--}}
            {{--</h2>--}}

            <div class="use-case-body">
                {!!  $pageData['raw']['case_text_'.\App::getLocale()] !!}

                <div class="u-pt-gamma">
                    <img class="p-lazy p-lazy--preview js-blazy"
                         src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
                         data-src-medium="{!! asset(\App\CasesInnerPageEntity::$pathStepsInnerImgMed.$pageData['raw']['case_image']) !!}"
                         data-src="{!! asset(\App\CasesInnerPageEntity::$pathStepsInnerImg.$pageData['raw']['case_image']) !!}"
                         data-src-small="{!! asset(\App\CasesInnerPageEntity::$pathStepsInnerImgSmall.$pageData['raw']['case_image']) !!}"
                         alt="alt text"
                    />
                </div>
            </div>

            {{--<div class="u-pt-gamma u-mb-jota">--}}
                
                <!-- <p class="u-ms-1 u-color-neutral-base u-align-center u-pt-gamma">
                  image caption Lorem ipsum dolor sit amet.
                </p> -->
            {{--</div>--}}

            @if(!empty($pageData['raw']['case_second_title_'.\App::getLocale()]))
            <h2>
                {!!  $pageData['raw']['case_second_title_'.\App::getLocale()] !!}
            </h2>
            @endif

            @if(!empty($pageData['raw']['case_second_text_'.\App::getLocale()]))
            <div>
                {!!  $pageData['raw']['case_second_text_'.\App::getLocale()] !!}
            </div>
            @endif
        </div>
    </div>

    @if($pageData['raw']['favorites_is_on']==true)
        <div class="c-row u-bgcolor-neutral-xxx-light">
            <div class="o-container u-z-beta">
                @include('components.cards-with-label',['favId'=>$pageData['favoriteId']])
            </div>
        </div>
    @endif
    @if($pageData['raw']['opinion_is_on']==true)
        @include('components.quote',$pageData['quote'])
    @endif

    @if($pageData['raw']['try_is_on']==true)
        @include('components.cta-trial',$pageData['trial'])
    @endif

@endsection

@section( 'view-scripts' )

@endsection