@extends( 'master' )

@section( 'content' )
    @include('components.intro', ['title'=>'My profile','body'=>'','button'=>'','background'=>['default'=>'img/photos/intro/2400/lab-4.jpg','small'=>'img/photos/intro/600/lab-4.jpg','medium'=>'img/photos/intro/1200/lab-4.jpg']])

    <div class="c-row c-row--md c-row--border-top u-bgcolor-neutral-xxx-light u-pb-jota u-mb-neg-beta">
        <div class="u-align-horizontal u-max-width-md c-main-section s-content">

            <div class="u-mb-beta">
              <h2>
                  Change password
              </h2>
            </div>

            <div class="u-mb-beta">
              <p class="u-color-neutral-light">
                <i>Please enter your e-mail in order to pick a new password</i>
              </p>
            </div>

            @if(Session::has('successMessage'))
                <div class="c-alert-box c-alert-box--success">
                    <p class="u-ms-1">
                        <span>{!! Session::get('successMessage') !!}</span>
                    </p>
                </div>
            @endif

            @if($errors -> any() )
                <div class="c-alert-box c-alert-box--error">
                    <p class="u-ms-1">
                        @foreach ($errors->all() as $error)
                            <span>{{ $error }}</span>
                        @endforeach
                    </p>
                </div>
            @endif

            <div class="login-box">
              {!! Form::open(array('url'=>'/password/reset','method'=>'post')) !!}

                {!! Form::hidden('token',$token) !!}
                {!! csrf_field() !!}

                <p>
                    <label for="passres-email" class="c-label">E-mail<abbr title="Required field">*</abbr></label>
                    {!! Form::email('email',old('email'),['class'=>'c-input-text c-input-text--md','id'=>'passres-email']) !!}
                </p>

                <p>
                  <label for="passres-newpass1" class="c-label">Password<abbr title="Required field">*</abbr></label>
                  {!! Form::password('password',['class'=>'c-input-text c-input-text--md','id'=>'passres-newpass1']) !!}
                </p>

                <p>
                  <label for="passres-newpass2" class="c-label">Confirm Password<abbr title="Required field">*</abbr></label>
                  {!! Form::password('password_confirmation',['class'=>'c-input-text c-input-text--md','id'=>'passres-newpass2']) !!}
                </p>

                <p class="u-align-right u-mt-jota">
                  {!! Form::submit('Reset Password',['class'=>'c-button c-button--md c-button--beta u-mb-beta']) !!}
                </p>
              {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@section( 'view-scripts' )

@endsection
