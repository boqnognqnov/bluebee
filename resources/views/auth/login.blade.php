@extends('admin.layouts.default')
@section('body')

    <div class="login-box">
        <div class="login-logo">
            <a href="{!! url('home') !!}"><b>Bluebee</b> Administration</a>
        </div>
        <div class="login-box-body">
            <form action="{{ url('auth/login') }}" method="post">
                {{--{!! Form::open(array('action' => 'AdministratorController@postAdminLogin','method'=>'post')) !!}--}}
                {!! csrf_field() !!}
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} has-feedback">
                    {{--<label class="col-md-12 control-label">E-Mail Address</label>--}}
                    <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    @if ($errors->has('email'))
                        <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} has-feedback">
                    {{--<label class="col-md-12 control-label">Password</label>--}}
                    <input type="password" class="form-control" name="password">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    @if ($errors->has('password'))
                        <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>
                    @endif
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <button type="submit" class="btn btn-primary btn-block">
                            <i class="fa fa-btn fa-sign-in"></i>Login
                        </button>
                    </div>
                </div>
            </form>
            {{--{!! Form::close() !!}--}}
        </div>
    </div>

@endsection
