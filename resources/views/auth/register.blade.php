@extends('admin.layouts.default')
@section('body')

    <div class="register-box">
        <div class="register-logo">
            <a href="{!! url('home') !!}"><b>BlueBee</b> Admin Registration</a>
        </div>
        <div class="register-box-body">
            <form action="{{ url('auth/register') }}" method="post">
                {!! csrf_field() !!}
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} has-feedback">
                    <input type="name" class="form-control" name="name" value="{{ old('name') }}"
                           placeholder="user name...">
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    @if ($errors->has('name'))
                        <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} has-feedback">
                    <input type="email" class="form-control" name="email" value="{{ old('email') }}"
                           placeholder="E-mail address...">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    @if ($errors->has('email'))
                        <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} has-feedback">
                    <input type="password" class="form-control" name="password" placeholder="Password...">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    @if ($errors->has('password'))
                        <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }} has-feedback">
                    <input type="password" class="form-control" name="password_confirmation"
                           placeholder="Retype Password...">
                    <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                    @if ($errors->has('password_confirmation'))
                        <span class="help-block"><strong>{{ $errors->first('password_confirmation') }}</strong></span>
                    @endif
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <button type="submit" class="btn btn-primary btn-block">
                            <i class="fa fa-btn fa-sign-in"></i>Register
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection
