@extends( 'admin.master' )
@section( 'content' )

    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-no-padding">
                        <div class="box-header">
                            <h3 class="box-title">Join the team page</h3>

                            <div class="box-tools">
                                <a href="{!! url('admin/jobs/items/list') !!}" class="btn btn-primary">Current open
                                    positions</a>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">

                                    @if(Session::has('successMessage'))
                                        <div class="alert alert-success">
                                            {!! Session::get('successMessage') !!}
                                        </div>
                                    @endif

                                    @if($errors -> any() )
                                        <div class="alert alert-danger" style="width: 100%;">
                                            @foreach ($errors->all() as $error)
                                                {{--<button type="button" class="close" data-dismiss="alert">?</button>--}}
                                                <p>{{ $error }}</p>
                                            @endforeach
                                        </div>
                                    @endif
                                </div>
                            </div>
                            {{--DATA--}}
                            {!! Form::open(array('action'=>'JobsController@updatePage','method'=>'post','files'=>true)) !!}
                            {{--{!! Form::hidden('id',$pageData['id']) !!}--}}

                            <div class="form-group clearfix">
                                <div class="container-fluid">
                                    <div class="checkbox">
                                        <label for="editPassOld" class="control-label">
                                            {!! Form::checkbox('head_is_on',true,@$pageData['head_is_on']) !!}
                                            On/Off header
                                        </label>
                                    </div>
                                </div>
                            </div>

                            @include('admin.components.headerComponent',['headerObj'=>\App\HeaderComponentEntity::getHeaderObj(@$pageData['head_id'])])

                            <hr>
                            {{--DATA--}}
                            <div class="form-group clearfix">
                                <label for="headerIMG" class="col-sm-4 col-md-2 control-label">Image in page
                                    <img src="{!! asset(\App\JoinTheTeamPageEntity::$path.$pageData['image']) !!}"
                                         class="img-responsive" style="display: block;">

                                    @if(!empty($pageData['image']))
                                        <a href="{!! url('admin/jobs/page/destroyImage') !!}"
                                           class="btn btn-danger">Delete image</a>
                                    @endif
                                </label>

                                <div class="col-sm-8 col-md-10">
                                    {!! Form::file('image',['placeholder'=>'upload','class'=>'form-control','id'=>'headerIMG']) !!}
                                    @if($errors -> first('image') != '')
                                        <span class="help-block">{!! $errors -> first('image') !!}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="clearfix">
                                <label for="" class="col-sm-4 col-md-2 control-label">Texts</label>

                                <div class="col-sm-8 col-md-10">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#JTeam_area_en"
                                                                                  aria-controls="JTeam_area_en"
                                                                                  role="tab" data-toggle="tab">EN</a>
                                        </li>
                                        <li role="presentation"><a href="#JTeam_area_de" aria-controls="JTeam_area_de"
                                                                   role="tab" data-toggle="tab">DE</a></li>
                                        <li role="presentation"><a href="#JTeam_area_fr" aria-controls="JTeam_area_fr"
                                                                   role="tab" data-toggle="tab">FR</a></li>
                                    </ul>

                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="JTeam_area_en">
                                            <p>{!! Form::text( 'title_en',$pageData['title_en'], array( 'class' => 'form-control', 'rows' => '6', 'placeholder' => 'Heading' ) ) !!}</p>

                                            <p>{!! Form::textarea( 'text_en',$pageData['text_en'], array( 'class' => 'form-control html5area', 'rows' => '6', 'placeholder' => 'Body' ) ) !!}</p>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="JTeam_area_de">
                                            <p>{!! Form::text( 'title_de',$pageData['title_de'], array( 'class' => 'form-control', 'rows' => '6', 'placeholder' => 'Heading' ) ) !!}</p>

                                            <p>{!! Form::textarea( 'text_de', $pageData['text_de'], array( 'class' => 'form-control html5area', 'rows' => '6', 'placeholder' => 'Body' ) ) !!}</p>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="JTeam_area_fr">
                                            <p>{!! Form::text( 'title_fr',$pageData['title_fr'], array( 'class' => 'form-control', 'rows' => '6', 'placeholder' => 'Heading' ) ) !!}</p>

                                            <p>{!! Form::textarea( 'text_fr', $pageData['text_fr'], array( 'class' => 'form-control html5area', 'rows' => '6', 'placeholder' => 'Body' ) ) !!}</p>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <hr>
                            <div class="clearfix">
                                <label for="" class="col-sm-4 col-md-2 control-label">Texts below open positions</label>

                                <div class="col-sm-8 col-md-10">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#JTeamS_area_en"
                                                                                  aria-controls="JTeamS_area_en"
                                                                                  role="tab" data-toggle="tab">EN</a>
                                        </li>
                                        <li role="presentation"><a href="#JTeamS_area_de" aria-controls="JTeamS_area_de"
                                                                   role="tab" data-toggle="tab">DE</a></li>
                                        <li role="presentation"><a href="#JTeamS_area_fr" aria-controls="JTeamS_area_fr"
                                                                   role="tab" data-toggle="tab">FR</a></li>
                                    </ul>

                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="JTeamS_area_en">

                                            <p>{!! Form::textarea( 'text_s_en',$pageData['text_s_en'], array( 'class' => 'form-control html5area', 'rows' => '6', 'placeholder' => 'Body' ) ) !!}</p>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="JTeamS_area_de">
                                            <p>{!! Form::textarea( 'text_s_de', $pageData['text_s_de'], array( 'class' => 'form-control html5area', 'rows' => '6', 'placeholder' => 'Body' ) ) !!}</p>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="JTeamS_area_fr">
                                            <p>{!! Form::textarea( 'text_s_fr', $pageData['text_s_fr'], array( 'class' => 'form-control html5area', 'rows' => '6', 'placeholder' => 'Body' ) ) !!}</p>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <hr>
                            {{--DATA--}}
                            <div class="form-group clearfix">
                                <div class="container-fluid">
                                    <div class="checkbox">
                                        <label for="editPassOld" class="col-sm-4 col-md-2 control-label">
                                            {!! Form::checkbox('try_is_on',true,@$pageData['try_is_on']) !!}
                                            @if($errors -> first('try_is_on') != '')
                                                <span class="help-block">{!! $errors -> first('try_is_on') !!}</span>
                                            @endif
                                            On/Off Try component
                                        </label>
                                    </div>
                                </div>
                            </div>

                            @include('admin.components.tryComponent',['tryObj'=>\App\TryComponentEntity::getTryObj(@$pageData['try_id'])])
                            <hr>
                            <div class="col-sm-6 text-right">
                                {!! Form::submit('Save',['class'=>'btn btn-success']) !!}
                            </div>
                            {!! Form::close() !!}

                            {{--END OF DATA--}}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection