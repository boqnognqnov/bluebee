@extends( 'admin.master' )
@section( 'content' )

    <div class="content-wrapper">
        <section class="content">
            <div class="box box-no-padding">
                <div class="box-header">
                    <h3 class="box-title">About page</h3>

                    <div class="box-tools">
                        {{--<a href="{!! url('admin/news/list') !!}" class="btn btn-primary">List of news</a>--}}
                        {{--<a href="{!! url('admin/news/showChildPage') !!}" class="btn btn-bitbucket">Edit child news page</a>--}}
                    </div>
                </div>
                <div class="box-body">
                    <div class="container-fluid">
                        <div class="row">

                            @if(Session::has('successMessage'))
                                <div class="alert alert-success">
                                    {!! Session::get('successMessage') !!}
                                </div>
                            @endif

                            @if($errors -> any() )
                                <div class="alert alert-danger" style="width: 100%;"></div>
                                @endif
                                @if($errors -> any() )
                                        <!-- collect the error messages -->
                                @foreach ($errors->all() as $error)
                                    <input type="hidden" class="error-message" value="{{ $error }}">
                                @endforeach
                            @endif

                            <script>
                                $(document).ready(function () {
                                    // loop through hidden fields with error messages
//
                                    var errors = '';
                                    var tempError = '';
                                    $('.error-message').each(function () {
                                        // show an alert with the message

                                        if (tempError != $(this).attr('value')) {
                                            errors += $(this).attr('value') + '<br>';
                                            tempError = $(this).attr('value');
                                        }


                                    });
                                    $('.alert-danger').html(errors);
                                    // loop through hidden fields with element names
                                });
                            </script>
                            <!-- END OF ERROR HANDLING -->


                        </div>

                        {{--DATA--}}
                        <div class="row">
                            <div class="container-fluid">
                                {!! Form::open(array('action'=>'AboutController@updatePage','method'=>'post','files'=>true)) !!}
                                {{--{!! Form::hidden('id',$pageData['id']) !!}--}}

                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <div class="checkbox">
                                            <label for="editPassOld" class="control-label">
                                                {!! Form::checkbox('head_is_on',true,@$pageData['head_is_on']) !!}
                                                Intro
                                            </label>
                                        </div>
                                    </div>
                                    <div class="panel-body">
                                        @include('admin.components.headerComponent',['headerObj'=>\App\HeaderComponentEntity::getHeaderObj(@$pageData['head_id'])])
                                    </div>
                                </div>

                                {{--INPUTS--}}


                                <div class="panel panel-info">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Page images</h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label for="about_core_img" class="col-md-4 control-label">Core
                                                        image</label>

                                                    <div class="col-md-8">
                                                        {!! Form::file('intro_image',['placeholder'=>'intro_image','class'=>'form-control','id'=>'about_core_img']) !!}
                                                        @if($errors -> first('intro_image') != '')
                                                            <span class="help-block">{!! $errors -> first('intro_image') !!}</span>
                                                        @endif
                                                    </div>

                                                    <div class="col-md-4">
                                                        <img src="{!! asset(\App\AboutPageEntity::$path.@$pageData['intro_image']) !!}"
                                                             class="img-responsive" style="display: block;">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <label for="about_team_img" class="col-md-4 control-label">Team
                                                        image</label>

                                                    <div class="col-md-8">
                                                        {!! Form::file('team_image',['placeholder'=>'team_image','class'=>'form-control','id'=>'about_team_img']) !!}
                                                        @if($errors -> first('team_image') != '')
                                                            <span class="help-block">{!! $errors -> first('team_image') !!}</span>
                                                        @endif
                                                    </div>

                                                    <div class="col-md-4">
                                                        <img src="{!! asset(\App\AboutPageEntity::$path.@$pageData['team_image']) !!}"
                                                             class="img-responsive" style="display: block;">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label for="" class="col-sm-4 col-md-2 control-label">Button URL</label>

                                        <div class="col-sm-8 col-md-10">
                                            {!! Form::text( 'intro_button_ref',$pageData['intro_button_ref'], array( 'class' => 'form-control', 'rows' => '6', 'placeholder' => 'http://something.com' ) ) !!}
                                        </div>
                                    </div>
                                </div>
                                <hr>


                                <div class="panel panel-warning">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Page texts</h3>
                                    </div>
                                    <div class="panel-body">
                                        <ul class="nav nav-tabs nav-justified" role="tablist">
                                            <li role="presentation" class="active">
                                                <a href="#about_area_en" aria-controls="about_area_en" role="tab"
                                                   data-toggle="tab"><p class="lead">EN</p></a>
                                            </li>
                                            <li role="presentation">
                                                <a href="#about_area_de" aria-controls="about_area_de" role="tab"
                                                   data-toggle="tab"><p class="lead">DE</p></a>
                                            </li>
                                            <li role="presentation">
                                                <a href="#about_area_fr" aria-controls="about_area_fr" role="tab"
                                                   data-toggle="tab"><p class="lead">FR</p></a>
                                            </li>
                                        </ul>

                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane active" id="about_area_en">
                                                <div class="form-group clearfix">
                                                    <label for="intro_title_en" class="control-label col-md-2">Intro
                                                        section</label>

                                                    <div class="col-md-10">
                                                        <p>{!! Form::text( 'intro_title_en',$pageData['intro_title_en'], array( 'class' => 'form-control', 'rows' => '6', 'placeholder' => 'Heading' ) ) !!}</p>

                                                        <p>{!! Form::textarea( 'intro_text_en',$pageData['intro_text_en'], array( 'class' => 'form-control html5area', 'rows' => '6', 'placeholder' => 'Body' ) ) !!}</p>
                                                    </div>
                                                </div>
                                                <hr>
                                                <div class="form-group clearfix">
                                                    <label for="intro_title_en" class="control-label col-md-2">Services
                                                        section</label>

                                                    <div class="col-md-10">
                                                        <p>{!! Form::text( 'services_title_en',$pageData['services_title_en'], array( 'class' => 'form-control', 'rows' => '6', 'placeholder' => 'Heading' ) ) !!}</p>

                                                        <p>{!! Form::textarea( 'services_text_en',$pageData['services_text_en'], array( 'class' => 'form-control html5area', 'rows' => '6', 'placeholder' => 'Body' ) ) !!}</p>

                                                        <p>{!! Form::text( 'intro_button_en',$pageData['intro_button_en'], array( 'class' => 'form-control', 'rows' => '6', 'placeholder' => 'Button text' ) ) !!}</p>
                                                    </div>
                                                </div>
                                                <hr>
                                                <div class="form-group clearfix">
                                                    <label for="" class="control-label col-md-2">Achievements
                                                        section</label>

                                                    <div class="col-md-10">
                                                        <p>{!! Form::text( 'achievement_title_en',$pageData['achievement_title_en'], array( 'class' => 'form-control', 'rows' => '6', 'placeholder' => 'Heading' ) ) !!}</p>

                                                        <p>{!! Form::textarea( 'achievement_text_en',$pageData['achievement_text_en'], array( 'class' => 'form-control html5area', 'rows' => '6', 'placeholder' => 'Body' ) ) !!}</p>
                                                    </div>
                                                </div>
                                                <hr>
                                                <div class="form-group clearfix">
                                                    <label for="" class="control-label col-md-2">Team section</label>

                                                    <div class="col-md-10">
                                                        <p>{!! Form::text( 'team_title_en',$pageData['team_title_en'], array( 'class' => 'form-control', 'rows' => '6', 'placeholder' => 'Heading' ) ) !!}</p>

                                                        <p>{!! Form::textarea( 'team_text_en',$pageData['team_text_en'], array( 'class' => 'form-control html5area', 'rows' => '6', 'placeholder' => 'Body' ) ) !!}</p>

                                                        <p>{!! Form::text( 'team_button_en',$pageData['team_button_en'], array( 'class' => 'form-control', 'rows' => '6', 'placeholder' => 'Button text' ) ) !!}</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="about_area_de">
                                                <div class="form-group clearfix">
                                                    <label for="intro_title_de" class="control-label col-md-2">Intro
                                                        sektion</label>

                                                    <div class="col-md-10">
                                                        <p>{!! Form::text( 'intro_title_de',$pageData['intro_title_de'], array( 'class' => 'form-control', 'rows' => '6', 'placeholder' => 'Heading' ) ) !!}</p>

                                                        <p>{!! Form::textarea( 'intro_text_de',$pageData['intro_text_de'], array( 'class' => 'form-control html5area', 'rows' => '6', 'placeholder' => 'Body' ) ) !!}</p>
                                                    </div>
                                                </div>
                                                <hr>
                                                <div class="form-group clearfix">
                                                    <label for="intro_title_de" class="control-label col-md-2">Services
                                                        sektion</label>

                                                    <div class="col-md-10">
                                                        <p>{!! Form::text( 'services_title_de',$pageData['services_title_de'], array( 'class' => 'form-control', 'rows' => '6', 'placeholder' => 'Heading' ) ) !!}</p>

                                                        <p>{!! Form::textarea( 'services_text_de',$pageData['services_text_de'], array( 'class' => 'form-control html5area', 'rows' => '6', 'placeholder' => 'Body' ) ) !!}</p>

                                                        <p>{!! Form::text( 'intro_button_de',$pageData['intro_button_de'], array( 'class' => 'form-control', 'rows' => '6', 'placeholder' => 'Button text' ) ) !!}</p>
                                                    </div>
                                                </div>
                                                <hr>
                                                <div class="form-group clearfix">
                                                    <label for="" class="control-label col-md-2">Achievements
                                                        sektion</label>

                                                    <div class="col-md-10">
                                                        <p>{!! Form::text( 'achievement_title_de',$pageData['achievement_title_de'], array( 'class' => 'form-control', 'rows' => '6', 'placeholder' => 'Heading' ) ) !!}</p>

                                                        <p>{!! Form::textarea( 'achievement_text_de',$pageData['achievement_text_de'], array( 'class' => 'form-control html5area', 'rows' => '6', 'placeholder' => 'Body' ) ) !!}</p>
                                                    </div>
                                                </div>
                                                <hr>
                                                <div class="form-group clearfix">
                                                    <label for="" class="control-label col-md-2">Team sektion</label>

                                                    <div class="col-md-10">
                                                        <p>{!! Form::text( 'team_title_de',$pageData['team_title_de'], array( 'class' => 'form-control', 'rows' => '6', 'placeholder' => 'Heading' ) ) !!}</p>

                                                        <p>{!! Form::textarea( 'team_text_de',$pageData['team_text_de'], array( 'class' => 'form-control html5area', 'rows' => '6', 'placeholder' => 'Body' ) ) !!}</p>

                                                        <p>{!! Form::text( 'team_button_de',$pageData['team_button_de'], array( 'class' => 'form-control', 'rows' => '6', 'placeholder' => 'Button text' ) ) !!}</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="about_area_fr">
                                                <div class="form-group clearfix">
                                                    <label for="intro_title_fr" class="control-label col-md-2">Intro
                                                        sektion</label>

                                                    <div class="col-md-10">
                                                        <p>{!! Form::text( 'intro_title_fr',$pageData['intro_title_fr'], array( 'class' => 'form-control', 'rows' => '6', 'placeholder' => 'Heading' ) ) !!}</p>

                                                        <p>{!! Form::textarea( 'intro_text_fr',$pageData['intro_text_fr'], array( 'class' => 'form-control html5area', 'rows' => '6', 'placeholder' => 'Body' ) ) !!}</p>
                                                    </div>
                                                </div>
                                                <hr>
                                                <div class="form-group clearfix">
                                                    <label for="intro_title_fr" class="control-label col-md-2">Services
                                                        sektion</label>

                                                    <div class="col-md-10">
                                                        <p>{!! Form::text( 'services_title_fr',$pageData['services_title_fr'], array( 'class' => 'form-control', 'rows' => '6', 'placeholder' => 'Heading' ) ) !!}</p>

                                                        <p>{!! Form::textarea( 'services_text_fr',$pageData['services_text_fr'], array( 'class' => 'form-control html5area', 'rows' => '6', 'placeholder' => 'Body' ) ) !!}</p>

                                                        <p>{!! Form::text( 'intro_button_fr',$pageData['intro_button_fr'], array( 'class' => 'form-control', 'rows' => '6', 'placeholder' => 'Button text' ) ) !!}</p>
                                                    </div>
                                                </div>
                                                <hr>
                                                <div class="form-group clearfix">
                                                    <label for="" class="control-label col-md-2">Achievements
                                                        sektion</label>

                                                    <div class="col-md-10">
                                                        <p>{!! Form::text( 'achievement_title_fr',$pageData['achievement_title_fr'], array( 'class' => 'form-control', 'rows' => '6', 'placeholder' => 'Heading' ) ) !!}</p>

                                                        <p>{!! Form::textarea( 'achievement_text_fr',$pageData['achievement_text_fr'], array( 'class' => 'form-control html5area', 'rows' => '6', 'placeholder' => 'Body' ) ) !!}</p>
                                                    </div>
                                                </div>
                                                <hr>
                                                <div class="form-group clearfix">
                                                    <label for="" class="control-label col-md-2">Team sektion</label>

                                                    <div class="col-md-10">
                                                        <p>{!! Form::text( 'team_title_fr',$pageData['team_title_fr'], array( 'class' => 'form-control', 'rows' => '6', 'placeholder' => 'Heading' ) ) !!}</p>

                                                        <p>{!! Form::textarea( 'team_text_fr',$pageData['team_text_fr'], array( 'class' => 'form-control html5area', 'rows' => '6', 'placeholder' => 'Body' ) ) !!}</p>

                                                        <p>{!! Form::text( 'team_button_fr',$pageData['team_button_fr'], array( 'class' => 'form-control', 'rows' => '6', 'placeholder' => 'Button text' ) ) !!}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                {{--INPUTS--}}

                                <div class="panel panel-danger">
                                    <div class="panel-heading clearfix">
                                        <div class="checkbox">
                                            <label for="editPassOld" class="col-sm-4 col-md-2 control-label">
                                                {!! Form::checkbox('partners_is_on',true,@$pageData['partners_is_on']) !!}
                                                @if($errors -> first('why_blue_is_on') != '')
                                                    <span class="help-block">{!! $errors -> first('partners_is_on') !!}</span>
                                                @endif
                                                Partners
                                            </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-success">
                                    <div class="panel-heading clearfix">
                                        <div class="checkbox">
                                            <label for="editPassOld" class="col-sm-4 col-md-2 control-label">
                                                {!! Form::checkbox('try_is_on',true,@$pageData['try_is_on']) !!}
                                                @if($errors -> first('try_is_on') != '')
                                                    <span class="help-block">{!! $errors -> first('try_is_on') !!}</span>
                                                @endif
                                                Trial
                                            </label>
                                        </div>
                                    </div>
                                    <div class="panel-body">
                                        @include('admin.components.tryComponent',['tryObj'=>\App\TryComponentEntity::getTryObj(@$pageData['try_id'])])
                                    </div>
                                </div>

                                <div class="col-sm-6 text-right">
                                    {!! Form::submit('Save',['class'=>'btn btn-success']) !!}
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                        {{--END OF DATA--}}
                    </div>
                </div>
            </div>
        </section>
    </div>



@endsection