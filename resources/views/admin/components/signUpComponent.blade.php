<div class="form-group clearfix">
    <label for="tryIMG" class="col-sm-4 col-md-2 control-label">Background image <img
                src="{!! asset(\App\SignUpComponentEntity::$path.$signObj['image']) !!}" class="img-responsive"
                style="display: block;"></label>

    <div class="col-sm-8 col-md-10">
        {!! Form::file('sign_data[image]',['class'=>'form-control','id'=>'tryIMG']) !!}
        @if($errors -> first('try_data[image]') != '')
            <span class="help-block">{!! $errors -> first('try_data[image]') !!}</span>
        @endif
    </div>
</div>

<div class="form-group clearfix">
    <label for="editPassNew" class="col-sm-4 col-md-2 control-label">News letter component texts</label>

    <div class="col-sm-8 col-md-10">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#try_area_en" aria-controls="try_area_en"
                                                      role="tab"
                                                      data-toggle="tab">EN</a></li>
            <li role="presentation"><a href="#try_area_de" aria-controls="try_area_de" role="tab"
                                       data-toggle="tab">DE</a></li>
            <li role="presentation"><a href="#try_area_fr" aria-controls="try_area_fr" role="tab"
                                       data-toggle="tab">FR</a></li>
        </ul>

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="try_area_en">
                {!! Form::textarea( 'sign_data[text_en]',$signObj['text_en'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                <label for="editPassNew" class="col-sm-4 col-md-2 control-label">Button text</label>
                {!! Form::text( 'sign_data[button_en]',$signObj['button_en'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
            </div>
            <div role="tabpanel" class="tab-pane" id="try_area_de">
                {!! Form::textarea( 'sign_data[text_de]', $signObj['text_de'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                <label for="editPassNew" class="col-sm-4 col-md-2 control-label">Button text</label>
                {!! Form::text( 'sign_data[button_de]',$signObj['button_de'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
            </div>
            <div role="tabpanel" class="tab-pane" id="try_area_fr">
                {!! Form::textarea( 'sign_data[text_fr]', $signObj['text_fr'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                <label for="editPassNew" class="col-sm-4 col-md-2 control-label">Button text</label>
                {!! Form::text( 'sign_data[button_fr]',$signObj['button_fr'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
            </div>
        </div>
    </div>
</div>