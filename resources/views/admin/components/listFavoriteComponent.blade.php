<?php
$models = \App\FavoritesComponentEntity::getDropDownForModel();
?>
<div class="form-group clearfix">
    @foreach($models as $modelName=>$one)
        <a href="{!! url('admin/favorites/showTool/setDropdownModelToSession/'.$modelName) !!}" class="btn btn-primary"
           style="display: inline-block">{!! $one !!}</a>
    @endforeach

</div>
