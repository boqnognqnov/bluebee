<div class="form-group clearfix">
    <label for="headerIMG" class="col-sm-4 col-md-2 control-label">Background image
    	<img src="{!! asset(\App\HeaderComponentEntity::$path.$headerObj['image']) !!}" class="img-responsive" style="display: block;">
    </label>

    <div class="col-sm-8 col-md-10">
        {!! Form::file('head_data[image]',['placeholder'=>'upload','class'=>'form-control','id'=>'headerIMG']) !!}
        @if($errors -> first('password') != '')
            <span class="help-block">{!! $errors -> first('password') !!}</span>
        @endif
    </div>
</div>

<div class="clearfix">
    <label for="" class="col-sm-4 col-md-2 control-label">Texts</label>

    <div class="col-sm-8 col-md-10">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#cHead_area_en" aria-controls="cHead_area_en" role="tab"data-toggle="tab">EN</a></li>
            <li role="presentation"><a href="#cHead_area_de" aria-controls="cHead_area_de" role="tab" data-toggle="tab">DE</a></li>
            <li role="presentation"><a href="#cHead_area_fr" aria-controls="cHead_area_fr" role="tab" data-toggle="tab">FR</a></li>
        </ul>

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="cHead_area_en">
                <p>{!! Form::text( 'head_data[title_en]',$headerObj['title_en'], array( 'class' => 'form-control', 'rows' => '6', 'placeholder' => 'Heading' ) ) !!}</p>
                <p>{!! Form::textarea( 'head_data[text_en]',$headerObj['text_en'], array( 'class' => 'form-control html5area', 'rows' => '6', 'placeholder' => 'Body' ) ) !!}</p>
            </div>
            <div role="tabpanel" class="tab-pane" id="cHead_area_de">
                <p>{!! Form::text( 'head_data[title_de]',$headerObj['title_de'], array( 'class' => 'form-control', 'rows' => '6', 'placeholder' => 'Heading' ) ) !!}</p>
                <p>{!! Form::textarea( 'head_data[text_de]', $headerObj['text_de'], array( 'class' => 'form-control html5area', 'rows' => '6', 'placeholder' => 'Body' ) ) !!}</p>
            </div>
            <div role="tabpanel" class="tab-pane" id="cHead_area_fr">
                <p>{!! Form::text( 'head_data[title_fr]',$headerObj['title_fr'], array( 'class' => 'form-control', 'rows' => '6', 'placeholder' => 'Heading' ) ) !!}</p>
                <p>{!! Form::textarea( 'head_data[text_fr]', $headerObj['text_fr'], array( 'class' => 'form-control html5area', 'rows' => '6', 'placeholder' => 'Body' ) ) !!}</p>
            </div>
        </div>
    </div>

</div>