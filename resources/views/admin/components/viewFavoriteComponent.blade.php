<?php
//
$firstData = \App\FavoritesComponentEntity::viewFavDataByPosition('first', $favId);

$secondData = \App\FavoritesComponentEntity::viewFavDataByPosition('second', $favId);
//return dump($secondData);
$thirdData = \App\FavoritesComponentEntity::viewFavDataByPosition('third', $favId);
//$blueBeeDefaultPath = Config::get('GlobalArrays.defaultImagePath');
?>

<div class="form-group clearfix">
    <div class="col-sm-4">
        {!! $firstData['slotStatus'] !!}<br>
        <img src="{!! asset($firstData['image']['default']) !!}"
             width="150px">
        <br>

        <a href="{!! url($firstData['url']) !!}">Link</a>

        <p>{!! $firstData['title'] !!}</p>

        <p>{!! $firstData['text'] !!}</p>

        <div class="form-group clearfix">
            {!! Form::open(array('action'=>'FavoritesController@addFav','method'=>'post','files'=>true)) !!}
            {!! Form::hidden('atachModelName',$atachModelName) !!}
            {!! Form::hidden('atachModelId',$atachModelId) !!}

            {{--{!! Form::select('toModel',\App\FavoritesComponentEntity::getDropDownForModel(),\Session::get('favSelectedModelTo')) !!}--}}
            {!! Form::hidden('toModel',\Session::get('favSelectedModelTo')) !!}
            {!! Form::hidden('position','f') !!}
            <br>
            {!! Form::submit('Set custom item',['class'=>'btn btn-success']) !!}
            {!! Form::close() !!}
        </div>
        <div class="form-group clearfix">
            {!! Form::open(array('action'=>'FavoritesController@addFavByDate','method'=>'post','files'=>true)) !!}
            {!! Form::hidden('atachModelName',$atachModelName) !!}
            {{--{!! Form::select('toModel',\App\FavoritesComponentEntity::getDropDownForModel(),\Session::get('favSelectedModelTo')) !!}--}}
            {!! Form::hidden('toModel',\Session::get('favSelectedModelTo')) !!}
            {!! Form::hidden('position','f') !!}
            <br>
            {!! Form::submit('Set latest item',['class'=>'btn btn-success']) !!}
            {!! Form::close() !!}
        </div>


        <div class="form-group clearfix">
            {!! Form::open(array('action'=>'FavoritesController@addSetSlotRandom','method'=>'post','files'=>true)) !!}
            {!! Form::hidden('atachModelName',$atachModelName) !!}
            {!! Form::hidden('toModel',\Session::get('favSelectedModelTo')) !!}
            {!! Form::hidden('position','f') !!}
            <br>
            {!! Form::submit('Reset slot',['class'=>'btn btn-warning']) !!}
            {!! Form::close() !!}
        </div>

    </div>
    <div class="col-sm-4">
        {!! $secondData['slotStatus'] !!}<br>
        <img src="{!! asset($secondData['image']['default']) !!}"
             width="150px">
        <br>

        <a href="{!! url($secondData['url']) !!}">Link</a>

        <p>{!! $secondData['title'] !!}</p>

        <p>{!! $secondData['text'] !!}</p>
        <div class="form-group clearfix">
            {!! Form::open(array('action'=>'FavoritesController@addFav','method'=>'post','files'=>true)) !!}
            {!! Form::hidden('atachModelName',$atachModelName) !!}
            {!! Form::hidden('atachModelId',$atachModelId) !!}

            {{--{!! Form::select('toModel',\App\FavoritesComponentEntity::getDropDownForModel(),\Session::get('favSelectedModelTo')) !!}--}}
            {!! Form::hidden('toModel',\Session::get('favSelectedModelTo')) !!}
            {!! Form::hidden('position','s') !!}
            <br>
            {!! Form::submit('Set custom item',['class'=>'btn btn-success']) !!}
            {!! Form::close() !!}
        </div>
        <div class="form-group clearfix">
            {!! Form::open(array('action'=>'FavoritesController@addFavByDate','method'=>'post','files'=>true)) !!}
            {!! Form::hidden('atachModelName',$atachModelName) !!}
            {{--{!! Form::select('toModel',\App\FavoritesComponentEntity::getDropDownForModel(),\Session::get('favSelectedModelTo')) !!}--}}
            {!! Form::hidden('toModel',\Session::get('favSelectedModelTo')) !!}
            {!! Form::hidden('position','s') !!}
            <br>
            {!! Form::submit('Set latest item',['class'=>'btn btn-success']) !!}
            {!! Form::close() !!}
        </div>
        <div class="form-group clearfix">
            {!! Form::open(array('action'=>'FavoritesController@addSetSlotRandom','method'=>'post','files'=>true)) !!}
            {!! Form::hidden('atachModelName',$atachModelName) !!}

            {!! Form::hidden('toModel',\Session::get('favSelectedModelTo')) !!}
            {!! Form::hidden('position','s') !!}
            <br>
            {!! Form::submit('Reset slot',['class'=>'btn btn-warning']) !!}
            {!! Form::close() !!}
        </div>
    </div>
    <div class="col-sm-4">
        {!! $thirdData['slotStatus'] !!}<br>
        <img src="{!! asset($thirdData['image']['default']) !!}"
             width="150px">
        <br>
        <a href="{!! url($thirdData['url']) !!}">Link</a>

        <p>{!! $thirdData['title'] !!}</p>

        <p>{!! $thirdData['text'] !!}</p>
        <div class="form-group clearfix">
            {!! Form::open(array('action'=>'FavoritesController@addFav','method'=>'post','files'=>true)) !!}
            {!! Form::hidden('atachModelName',$atachModelName) !!}
            {!! Form::hidden('atachModelId',$atachModelId) !!}

            {{--{!! Form::select('toModel',\App\FavoritesComponentEntity::getDropDownForModel(),\Session::get('favSelectedModelTo')) !!}--}}
            {!! Form::hidden('toModel',\Session::get('favSelectedModelTo')) !!}
            {!! Form::hidden('position','t') !!}
            <br>
            {!! Form::submit('Set custom item',['class'=>'btn btn-success']) !!}
            {!! Form::close() !!}
        </div>
        <div class="form-group clearfix">
            {!! Form::open(array('action'=>'FavoritesController@addFavByDate','method'=>'post','files'=>true)) !!}
            {!! Form::hidden('atachModelName',$atachModelName) !!}
            {{--{!! Form::select('toModel',\App\FavoritesComponentEntity::getDropDownForModel(),\Session::get('favSelectedModelTo')) !!}--}}
            {!! Form::hidden('toModel',\Session::get('favSelectedModelTo')) !!}
            {!! Form::hidden('position','t') !!}
            <br>
            {!! Form::submit('Set latest item',['class'=>'btn btn-success']) !!}
            {!! Form::close() !!}
        </div>
        <div class="form-group clearfix">
            {!! Form::open(array('action'=>'FavoritesController@addSetSlotRandom','method'=>'post','files'=>true)) !!}
            {!! Form::hidden('atachModelName',$atachModelName) !!}
            {!! Form::hidden('toModel',\Session::get('favSelectedModelTo')) !!}
            {!! Form::hidden('position','t') !!}
            <br>
            {!! Form::submit('Reset slot',['class'=>'btn btn-warning']) !!}
            {!! Form::close() !!}
        </div>
    </div>
</div>
