<div class="form-group clearfix">
    <label for="tryIMG" class="col-sm-4 col-md-2 control-label">Background image
    	<img src="{!! asset(\App\TryComponentEntity::$path.$tryObj['image']) !!}" class="img-responsive"style="display: block;">
    </label>
    <div class="col-sm-8 col-md-10">
        {!! Form::file('try_data[image]',['class'=>'form-control','id'=>'tryIMG']) !!}
        @if($errors -> first('try_data[image]') != '')
            <span class="help-block">{!! $errors -> first('try_data[image]') !!}</span>
        @endif
    </div>
</div>

<div class="form-group clearfix">
    <label for="" class="col-sm-4 col-md-2 control-label">try component text</label>

    <div class="col-sm-8 col-md-10">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
            	<a href="#try_area_en" aria-controls="try_area_en" role="tab" data-toggle="tab">EN</a>
        	</li>
            <li role="presentation">
            	<a href="#try_area_de" aria-controls="try_area_de" role="tab" data-toggle="tab">DE</a>
        	</li>
            <li role="presentation">
            	<a href="#try_area_fr" aria-controls="try_area_fr" role="tab" data-toggle="tab">FR</a>
        	</li>
        </ul>

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="try_area_en">
                <p>{!! Form::text( 'try_data[title_en]',$tryObj['title_en'], array( 'class' => 'form-control', 'rows' => '6', 'placeholder' => 'Heading' ) ) !!}</p>
                <p>{!! Form::textarea( 'try_data[text_en]',$tryObj['text_en'], array( 'class' => 'form-control html5area', 'rows' => '6', 'placeholder' => 'Body' ) ) !!}</p>
                <p>{!! Form::text( 'try_data[button_en]',$tryObj['button_en'], array( 'class' => 'form-control', 'rows' => '6', 'placeholder' => 'Button text' ) ) !!}</p>
            </div>
            <div role="tabpanel" class="tab-pane" id="try_area_de">
                <p>{!! Form::text( 'try_data[title_de]',$tryObj['title_de'], array( 'class' => 'form-control', 'rows' => '6', 'placeholder' => 'Heading' ) ) !!}</p>
                <p>{!! Form::textarea( 'try_data[text_de]', $tryObj['text_de'], array( 'class' => 'form-control html5area', 'rows' => '6', 'placeholder' => 'Body' ) ) !!}</p>
                <p>{!! Form::text( 'try_data[button_de]',$tryObj['button_de'], array( 'class' => 'form-control', 'rows' => '6', 'placeholder' => 'Button text' ) ) !!}</p>
            </div>
            <div role="tabpanel" class="tab-pane" id="try_area_fr">
                <p>{!! Form::text( 'try_data[title_fr]',$tryObj['title_fr'], array( 'class' => 'form-control', 'rows' => '6', 'placeholder' => 'Heading' ) ) !!}</p>
                <p>{!! Form::textarea( 'try_data[text_fr]', $tryObj['text_fr'], array( 'class' => 'form-control html5area', 'rows' => '6', 'placeholder' => 'Body' ) ) !!}</p>
                <p>{!! Form::text( 'try_data[button_fr]',$tryObj['button_fr'], array( 'class' => 'form-control', 'rows' => '6', 'placeholder' => 'Button text' ) ) !!}</p>
            </div>
        </div>
    </div>

    <label for="editPassNew" class="col-sm-4 col-md-2 control-label">Button URL</label>
    <div class="col-sm-8 col-md-10">
    	{!! Form::text( 'try_data[url]',$tryObj['url'], array( 'class' => 'form-control', 'rows' => '6', 'placeholder' => 'http://something.com' ) ) !!}
    </div>
</div>