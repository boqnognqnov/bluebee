<div class="form-group clearfix">
    <label for="editPassNew" class="col-sm-4 col-md-2 control-label">Name</label>

    <div class="col-sm-8 col-md-10">
        {!! Form::text('opinion_data[name]',@$opinionObj['name'],['class'=>'form-control']) !!}
        @if($errors -> first('opinion_data[name]') != '')
            <span class="help-block">{!! $errors -> first('opinion_data[name]') !!}</span>
        @endif
    </div>
</div>

{{--<div class="form-group clearfix">--}}
{{--<label for="opinionIMG" class="col-sm-4 col-md-2 control-label">Opinion image <img--}}
{{--src="{!! asset(\App\OpinionComponentEntity::$path.$opinionObj['image']) !!}"--}}
{{--class="img-responsive"--}}
{{--style="display: block;"></label>--}}

{{--<div class="col-sm-8 col-md-10">--}}
{{--{!! Form::file('opinion_data[image]',['class'=>'form-control','id'=>'opinionIMG']) !!}--}}
{{--</div>--}}
{{--</div>--}}

<div class="form-group clearfix">
    <label for="" class="col-sm-4 col-md-2 control-label">Edit opinion text</label>

    <div class="col-sm-8 col-md-10">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#opinion_area_en" aria-controls="area_en"
                                                      role="tab"
                                                      data-toggle="tab">EN</a></li>
            <li role="presentation"><a href="#opinion_area_de" aria-controls="area_de" role="tab"
                                       data-toggle="tab">DE</a></li>
            <li role="presentation"><a href="#opinion_area_fr" aria-controls="area_fr" role="tab"
                                       data-toggle="tab">FR</a></li>

        </ul>

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="opinion_area_en">
                {!! Form::textarea( 'opinion_data[text_en]',@$opinionObj['text_en'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                <label for="">Button text</label>
                {!! Form::text( 'opinion_data[button_en]',@$opinionObj['button_en'], array( 'class' => 'form-control' ) ) !!}
            </div>
            <div role="tabpanel" class="tab-pane" id="opinion_area_de">
                {!! Form::textarea( 'opinion_data[text_de]', @$opinionObj['text_de'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                <label for="">Button text</label>
                {!! Form::text( 'opinion_data[button_de]',@$opinionObj['button_de'], array( 'class' => 'form-control' ) ) !!}
            </div>
            <div role="tabpanel" class="tab-pane" id="opinion_area_fr">
                {!! Form::textarea( 'opinion_data[text_fr]', @$opinionObj['text_fr'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                <label for="">Button text</label>
                {!! Form::text( 'opinion_data[button_fr]',@$opinionObj['button_fr'], array( 'class' => 'form-control' ) ) !!}
            </div>
        </div>
    </div>
</div>

<div class="form-group clearfix">
    <label for="" class="col-sm-4 col-md-2 control-label">Button Url</label>

    <div class="col-sm-8 col-md-10">
        {!! Form::text( 'opinion_data[url]',@$opinionObj['url'], array( 'class' => 'form-control' ) ) !!}
    </div>
</div>