@extends('admin.master')
@section('content')

    <script src="{!! url('https://code.jquery.com/ui/1.12.0-beta.1/jquery-ui.min.js') !!}"></script>
    <link rel="stylesheet" href="{!! url('https://code.jquery.com/ui/1.11.4/themes/black-tie/jquery-ui.css') !!}">
    <script>
        $(function () {
            $("#accordion").accordion();
        });
    </script>

    <div class="content-wrapper">
        <section class="content">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Page Collaboration</h3>
                    <div class="box-tools">
                        <a href="{!! url('admin/partners/list/show') !!}" class="btn btn-primary">Partners list</a>
                    </div>
                </div>


                <div class="box-body no-padding">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">

                                @if(Session::has('successMessage'))
                                    <div class="alert alert-success">
                                        {!! Session::get('successMessage') !!}
                                    </div>
                                @endif

                                @if($errors -> any() )
                                    <div class="alert alert-danger" style="width: 100%;">
                                        @foreach ($errors->all() as $error)
                                            {{--<button type="button" class="close" data-dismiss="alert">?</button>--}}
                                            <p>{{ $error }}</p>
                                        @endforeach
                                    </div>
                                @endif
                            </div>
                        </div>


                        {!! Form::open(array('action'=>'PartnersController@updatePage','method'=>'post','files'=>true)) !!}

                        <div class="form-group clearfix">
                            <div class="container-fluid">
                                <div class="checkbox">
                                    <label for="editPassOld" class="control-label">
                                        {!! Form::checkbox('head_is_on',true,@$partnersPageData['head_is_on']) !!}
                                        On/Off header
                                    </label>
                                </div>
                            </div>
                        </div>

                        @include('admin.components.headerComponent',['headerObj'=>\App\HeaderComponentEntity::getHeaderObj(@$partnersPageData['head_id'])])

                        <hr>


                        <div class="form-group clearfix">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="checkbox">
                                            <label for="editPassOld" class="control-label">
                                                {!! Form::checkbox('try_is_on',true,@$partnersPageData['try_is_on']) !!}
                                                On/Off try component
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @include('admin.components.tryComponent',['tryObj'=>\App\TryComponentEntity::getTryObj(@$partnersPageData['try_id'])])
                        <hr>

                        <div class="form-group">

                            <div class="col-sm-6 text-right">

                            </div>
                        </div>


                    </div>
                </div>


                <div class="text-center">
                    {!! Form::submit('Save page state',['class'=>'btn btn-success']) !!}
                </div>
                {!! Form::close() !!}
            </div>


        </section>
    </div>


@endsection