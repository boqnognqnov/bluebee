@extends('admin.master')
@section('content')



    <div class="content-wrapper">
        <section class="content">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Edit each partner</h3>

                    <div class="box-tools">
                        <a href="{!! url('admin/partners/page/show') !!}" class="btn btn-primary">Back to Partner
                            page</a>
                    </div>
                </div>


                <div class="box-body no-padding">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">

                                @if(Session::has('successMessage'))
                                    <div class="alert alert-success">
                                        {!! Session::get('successMessage') !!}
                                    </div>
                                @endif

                                @if($errors -> any() )
                                    <div class="alert alert-danger" style="width: 100%;">
                                        @foreach ($errors->all() as $error)
                                            {{--<button type="button" class="close" data-dismiss="alert">?</button>--}}
                                            <p>{{ $error }}</p>
                                        @endforeach
                                    </div>
                                @endif
                            </div>
                        </div>
                        <table class="table">
                            <thead>
                            <th>Image</th>
                            {{--<th>Title</th>--}}
                            <th>Link</th>
                            <th colspan="2"></th>
                            <th colspan="2">Move record position</th>
                            </thead>
                            <tbody>


                            @foreach($partnersListData as $onePartner)
                                @if($onePartner['is_favorite']==true)
                                    <tr style="background: #7bc937">
                                @else
                                    <tr>
                                        @endif
                                        <td>
                                            <img src="{!! asset(\App\PartnersListEntity::$path.$onePartner['image']) !!}"
                                                 width="100px">
                                        </td>
                                        {{--<td>{!! substr($onePartner['text_en'],0,15) !!}</td>--}}
                                        <td><a href="{!! url($onePartner['url']) !!}" target="_blank">Link</a></td>
                                        <td>
                                            <a href="{!! url('admin/partners/list/show/'.$onePartner['id']) !!}"
                                               class="btn btn-primary">Edit</a>
                                        </td>
                                        <td>
                                            <button class="btn btn-danger partnerDel"
                                                    data-partner-id="{!! $onePartner['id'] !!}">Delete
                                            </button>
                                        </td>
                                        <td>
                                            <a href="{!! url('admin/partners/list/moveUp/'.$onePartner['id']) !!}" class="glyphicon glyphicon-arrow-up"></a>
                                        </td>
                                        <td>
                                            <a href="{!! url('admin/partners/list/moveDown/'.$onePartner['id']) !!}" class="glyphicon glyphicon-arrow-down"></a>
                                        </td>
                                    </tr>
                                    @endforeach
                            </tbody>
                        </table>


                        {!! Form::open(array('action'=>'PartnersController@addOrEditPartner','method'=>'post','files'=>true)) !!}
                        @if($selectedPartner)
                            {!! Form::hidden('id',$selectedPartner['id']) !!}
                        @else
                            {!! Form::hidden('id',0) !!}
                        @endif


                        <div class="form-group clearfix">
                            <label for="headerIMG" class="col-sm-4 col-md-2 control-label">Background image <img
                                        src="{!! asset(\App\PartnersListEntity::$path.@$selectedPartner['image']) !!}"
                                        class="img-responsive"
                                        style="display: block;"></label>

                            <div class="col-sm-8 col-md-10">
                                {!! Form::file('image',['placeholder'=>'Парола','class'=>'form-control','id'=>'headerIMG']) !!}
                                @if($errors -> first('password') != '')
                                    <span class="help-block">{!! $errors -> first('password') !!}</span>
                                @endif
                            </div>
                        </div>
                        <hr>
                        <div class="form-group clearfix">
                            <div class="container-fluid">
                                <div class="checkbox">
                                    <label for="editPassOld" class="control-label">
                                        {!! Form::checkbox('is_favorite',true,@$selectedPartner['is_favorite']) !!}
                                        Is on favorites
                                    </label>
                                </div>
                            </div>
                        </div>
                        <hr>

                        <div class="form-group">
                            <label for="" class="col-sm-4 col-md-2 control-label">Partner title and text</label>

                            <div class="col-sm-8 col-md-10">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#partner_area_en"
                                                                              aria-controls="partner_area_en"
                                                                              role="tab"
                                                                              data-toggle="tab">EN</a></li>
                                    <li role="presentation"><a href="#partner_area_de" aria-controls="partner_area_de"
                                                               role="tab"
                                                               data-toggle="tab">DE</a></li>
                                    <li role="presentation"><a href="#partner_area_fr" aria-controls="partner_area_fr"
                                                               role="tab"
                                                               data-toggle="tab">FR</a></li>

                                </ul>

                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="partner_area_en">
                                        {{--{!! Form::text( 'title_en',@$selectedPartner['title_en'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}--}}
                                        {!! Form::textarea( 'text_en',@$selectedPartner['text_en'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="partner_area_de">
                                        {{--{!! Form::text( 'title_de',@$selectedPartner['title_de'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}--}}
                                        {!! Form::textarea( 'text_de', @$selectedPartner['text_de'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="partner_area_fr">
                                        {{--{!! Form::text( 'title_fr',@$selectedPartner['title_fr'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}--}}
                                        {!! Form::textarea( 'text_fr', @$selectedPartner['text_fr'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                    </div>

                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-4 col-md-2">
                                    <label for="" class=" control-label">URL</label>
                                </div>
                                <div class="col-sm-8  col-md-10">
                                    {!! Form::text( 'url',@$selectedPartner['url'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <hr>
                <div class="text-center">
                    {!! Form::submit('Save page state',['class'=>'btn btn-success']) !!}
                    <a href="{!! url('admin/partners/list/show') !!}" class="btn btn-primary">Clear</a>
                </div>
                {!! Form::close() !!}
            </div>


        </section>
    </div>

    <div style="display: none">
        {!! Form::open(array('action'=>'PartnersController@destroyPartner','method'=>'post','id'=>'form-del-partner')) !!}
        {!! Form::hidden('partnerId') !!}
        {!! Form::close() !!}
    </div>


    <script>

        $(document).ready(function () {

            $('.partnerDel').on('click', function (event) {

                var id = $(this).attr('data-partner-id');
                $('#form-del-partner input[name="partnerId"]').val(id);
                var r = confirm("Do you really want to delete this record ?");
                if (r == true) {
                    $('#form-del-partner').submit();
                }
            });
        });
    </script>


@endsection