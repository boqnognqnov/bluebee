@extends('admin.layouts.default')
@section('body')

    <div class="wrapper">

        <header class="main-header">
            <a href="{!! url('admin/sliders') !!}" class="logo">
                <span class="logo-mini"><b>B</b>B</span>
                <span class="logo-lg"><b>Blue</b>bee</span>
            </a>
            <nav class="navbar navbar-static-top" role="navigation">
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>
            </nav>
        </header>

        <aside class="main-sidebar">

            <section class="sidebar">

                <ul class="sidebar-menu">
                    <li class="header">NAVIGATION</li>
                    {{--<li class="active"><a href="{!! url('admin/home') !!}"><i class="fa fa-link"></i><span>Начало</span></a></li>--}}
                    <li class=""><a href="{!! url('admin/homePage/show') !!}"><i class="fa fa-link"></i>
                            <span>Home Page</span></a></li>
                    <li class=""><a href="{!! url('admin/features/page/show') !!}"><i class="fa fa-link"></i> <span>Features</span></a>
                    </li>
                    <li class=""><a href="{!! url('admin/benefits/page') !!}"><i class="fa fa-link"></i> <span>Why Bluebee</span></a>
                    </li>
                    <li class=""><a href="{!! url('admin/cases/main/showPage') !!}"><i class="fa fa-link"></i> <span>Cases</span></a>
                    </li>
                    <li class=""><a href="{!! url('admin/knowCenter/page/show') !!}"><i class="fa fa-link"></i> <span>Knowledge Center</span></a>
                    </li>
                    <li class=""><a href="{!! url('admin/news/editMainPage') !!}"><i class="fa fa-link"></i>
                            <span>News</span></a></li>
                    <li class=""><a href="{!! url('admin/events/editMainPage') !!}"><i class="fa fa-link"></i> <span>Events</span></a>
                    </li>
                    <li class=""><a href="{!! url('admin/downloads/page/show') !!}"><i class="fa fa-link"></i> <span>Downloads</span></a>
                    </li>
                    <li class=""><a href="{!! url('admin/about/page/show') !!}"><i class="fa fa-link"></i> <span>About page</span></a>
                    </li>
                    <li class=""><a href="{!! url('admin/jobs/page/show') !!}"><i class="fa fa-link"></i> <span>Join the team</span></a>
                    </li>
                    <li class=""><a href="{!! url('admin/contacts/pages/main/show') !!}"><i class="fa fa-link"></i>
                            <span>Contacts</span></a></li>
                    <li class=""><a href="{!! url('admin/team/page/show') !!}"><i class="fa fa-link"></i>
                            <span>Our Team</span></a></li>
                    <li class=""><a href="{!! url('admin/partners/page/show') !!}"><i class="fa fa-link"></i> <span>Partners</span></a>
                    </li>

                    <li class=""><a href="{!! url('admin/methods/show') !!}"><i class="fa fa-link"></i> <span>How does it work page</span></a>
                    </li>


                    <li class=""><a href="{!! url('admin/pipelines/page/show') !!}"><i class="fa fa-link"></i> <span>Pipelines page</span></a>
                    </li>


                    {{--<li class=""><a href="{!! url('admin/jobs/apply/list') !!}"><i class="fa fa-link"></i> <span>Apply positions</span></a>--}}
                    {{--</li>--}}
                    <li class=""><a href="{!! url('admin/administrator/page/show') !!}"><i class="fa fa-link"></i>
                            <span>Administration</span></a>
                    </li>

                    <li class=""><a href="{!! url('admin/demo/page/show') !!}"><i class="fa fa-link"></i>
                            <span>Demo page</span></a>
                    </li>

                    <li class=""><a href="{!! url('admin/privacy') !!}"><i class="fa fa-link"></i>
                            <span>Privacy</span></a>
                    </li>
                    <li class=""><a href="{!! url('admin/disclaimer') !!}"><i class="fa fa-link"></i>
                            <span>Disclaimer</span></a>
                    </li>


                    <li class=""><a href="{!! url('logout') !!}"><i class="fa fa-link"></i> <span>Exit</span></a></li>


                </ul>
            </section>
        </aside>

        @yield('content')


        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                Bluebee Administration
            </div>
            <strong>Copyright &copy; 2015 <a href="#">Bluebee</a>.</strong> All rights reserved.
        </footer>
    </div>

@endsection