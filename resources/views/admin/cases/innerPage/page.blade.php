@extends( 'admin.master' )
@section( 'content' )

    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-md-12">

                    @if(Session::has('successMessage'))
                        <div class="alert alert-success">
                            {!! Session::get('successMessage') !!}
                        </div>
                    @endif

                    @if($errors -> any() )
                        <div class="alert alert-danger" style="width: 100%;">
                            @foreach ($errors->all() as $error)
                                {{--<button type="button" class="close" data-dismiss="alert">?</button>--}}
                                <p>{{ $error }}</p>
                            @endforeach
                        </div>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-no-padding">
                        <div class="box-header">
                            <h3 class="box-title">Cases-> {!! $pageData['front_title_en'] !!}</h3>
                            <hr>
                            <a href="{!! url('admin/cases/main/showPage') !!}" class="btn btn-primary">Back</a>
                        </div>
                        <div class="box-body">

                            {{--DATA--}}
                            {{--PART 1--}}
                            <div class="text-center"><h3>Change data in page</h3></div>
                            {!! Form::open(array('action'=>'CasesController@updateInnerPage','method'=>'post','files'=>true)) !!}
                            {!! Form::hidden('id',$pageData['id']) !!}

                            <div class="form-group clearfix">
                                <div class="container-fluid">
                                    <div class="checkbox">
                                        <label for="editPassOld" class="control-label">
                                            {!! Form::checkbox('head_is_on',true,@$pageData['head_is_on']) !!}
                                            On/Off header
                                        </label>
                                    </div>
                                </div>
                            </div>

                            @include('admin.components.headerComponent',['headerObj'=>\App\HeaderComponentEntity::getHeaderObj(@$pageData['head_id'])])

                            <hr>


                            <div class="form-group clearfix">
                                <label for="headerIMG" class="col-sm-4 col-md-2 control-label">Inner page image <img
                                            src="{!! asset(\App\CasesInnerPageEntity::$pathStepsInnerImg.$pageData['case_image']) !!}"
                                            class="img-responsive"
                                            style="display: block;">
                                    @if(!empty($pageData['case_image']))
                                        <a href="{!! url('admin/cases/destroyImage/'.$pageData['id']) !!}"
                                           class="btn btn-danger">Delete image</a>
                                    @endif
                                </label>

                                <div class="col-sm-8 col-md-10">
                                    {!! Form::file('case_image',['placeholder'=>'case_image','class'=>'form-control','id'=>'headerIMG']) !!}
                                    @if($errors -> first('case_image') != '')
                                        <span class="help-block">{!! $errors -> first('case_image') !!}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group clearfix">
                                <label for="" class="col-sm-4 col-md-2 control-label">Inner titles and texts</label>

                                <div class="col-sm-8 col-md-10">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#innerCaseInfo_area_en"
                                                                                  aria-controls="innerCaseInfo_area_en"
                                                                                  role="tab"
                                                                                  data-toggle="tab">EN</a></li>
                                        <li role="presentation"><a href="#innerCaseInfo_area_de"
                                                                   aria-controls="innerCaseInfo_area_de" role="tab"
                                                                   data-toggle="tab">DE</a></li>
                                        <li role="presentation"><a href="#innerCaseInfoe_area_fr"
                                                                   aria-controls="innerCaseInfo_area_fr" role="tab"
                                                                   data-toggle="tab">FR</a></li>

                                    </ul>

                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="innerCaseInfo_area_en">
                                            {!! Form::textarea( 'case_text_en',$pageData['case_text_en'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                            {!! Form::text( 'case_second_title_en',$pageData['case_second_title_en'], array( 'class' => 'form-control', 'rows' => '6','style'=>'margin: 3rem auto;' ) ) !!}
                                            {!! Form::textarea( 'case_second_text_en',$pageData['case_second_text_en'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="innerCaseInfo_area_de">
                                            {!! Form::textarea( 'case_text_de',$pageData['case_text_de'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                            {!! Form::text( 'case_second_title_de',$pageData['case_second_title_de'], array( 'class' => 'form-control', 'rows' => '6','style'=>'margin: 3rem auto;' ) ) !!}
                                            {!! Form::textarea( 'case_second_text_de',$pageData['case_second_text_de'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="innerCaseInfoe_area_fr">
                                            {!! Form::textarea( 'case_text_fr',$pageData['case_text_fr'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                            {!! Form::text( 'case_second_title_fr',$pageData['case_second_title_fr'], array( 'class' => 'form-control', 'rows' => '6','style'=>'margin: 3rem auto;' ) ) !!}
                                            {!! Form::textarea( 'case_second_text_fr',$pageData['case_second_text_fr'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group clearfix">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="checkbox">
                                                <label for="editPassOld" class="control-label">
                                                    {!! Form::checkbox('opinion_is_on',true,@$pageData['opinion_is_on']) !!}
                                                    On/Off opinion component
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @include('admin.components.opinionComponent',['opinionObj'=>\App\OpinionComponentEntity::getOpinionObj(@$pageData['opinion_id'])])
                            <hr>


                            <div class="form-group clearfix">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="checkbox">
                                                <label for="editPassOld" class="control-label">
                                                    {!! Form::checkbox('try_is_on',true,@$pageData['try_is_on']) !!}
                                                    On/Off try component
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @include('admin.components.tryComponent',['tryObj'=>\App\TryComponentEntity::getTryObj(@$pageData['try_id'])])
                            <hr>
                            <div class="form-group clearfix">
                                <div class="container-fluid">
                                    <div class="checkbox">
                                        <label for="editPassOld" class="control-label">
                                            {!! Form::checkbox('favorites_is_on',true,@$pageData['favorites_is_on']) !!}
                                            On/Off Favorite
                                        </label>
                                    </div>
                                </div>
                            </div>


                            <hr>
                            <div class="text-center">
                                {!! Form::submit('Save page state',['class'=>'btn btn-success']) !!}
                            </div>
                            {!! Form::close() !!}
                            {{--END PART 1--}}
                            {{--PART 2--}}
                            <div class="text-center"><h3>Change front info</h3></div>


                            {!! Form::open(array('action'=>'CasesController@updateFrontCaseView','method'=>'post','files'=>true)) !!}
                            {!! Form::hidden('id',$pageData['id']) !!}
                            <div class="form-group clearfix">
                                <label for="headerIMG" class="col-sm-4 col-md-2 control-label">Front image <img
                                            src="{!! asset(\App\CasesInnerPageEntity::$pathStepsFront.$pageData['front_image']) !!}"
                                            class="img-responsive"
                                            style="display: block;"></label>

                                <div class="col-sm-8 col-md-10">
                                    {!! Form::file('front_image',['placeholder'=>'front_image','class'=>'form-control','id'=>'headerIMG']) !!}
                                    @if($errors -> first('front_image') != '')
                                        <span class="help-block">{!! $errors -> first('front_image') !!}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="" class="col-sm-4 col-md-2 control-label">Front title and text</label>

                                <div class="col-sm-8 col-md-10">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#frontCase_area_en"
                                                                                  aria-controls="frontCase_area_en"
                                                                                  role="tab"
                                                                                  data-toggle="tab">EN</a></li>
                                        <li role="presentation"><a href="#frontCase_area_de"
                                                                   aria-controls="frontCase_area_de"
                                                                   role="tab"
                                                                   data-toggle="tab">DE</a></li>
                                        <li role="presentation"><a href="#frontCase_area_fr"
                                                                   aria-controls="frontCase_area_fr"
                                                                   role="tab"
                                                                   data-toggle="tab">FR</a></li>

                                    </ul>

                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="frontCase_area_en">
                                            {!! Form::text( 'front_title_en',$pageData['front_title_en'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                            {!! Form::textarea( 'front_text_en',$pageData['front_text_en'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="frontCase_area_de">
                                            {!! Form::text( 'front_title_de',$pageData['front_title_de'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                            {!! Form::textarea( 'front_text_de', $pageData['front_title_de'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="frontCase_area_fr">
                                            {!! Form::text( 'front_title_fr',$pageData['front_title_fr'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                            {!! Form::textarea( 'front_title_fr', $pageData['front_title_fr'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="text-center">
                                {!! Form::submit('Save page state',['class'=>'btn btn-success']) !!}
                            </div>
                            {{--END PART 2--}}

                            {{--END DATA--}}
                        </div>
                    </div>
                </div>
            </div>


        </section>
    </div>

@endsection