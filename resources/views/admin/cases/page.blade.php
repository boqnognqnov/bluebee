@extends('admin.master')
@section('content')


    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-md-12">

                    @if(Session::has('successMessage'))
                        <div class="alert alert-success">
                            {!! Session::get('successMessage') !!}
                        </div>
                    @endif

                    @if($errors -> any() )
                        <div class="alert alert-danger" style="width: 100%;">
                            @foreach ($errors->all() as $error)
                                {{--<button type="button" class="close" data-dismiss="alert">?</button>--}}
                                <p>{{ $error }}</p>
                            @endforeach
                        </div>
                    @endif
                </div>
            </div>
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Page Cases</h3>
                </div>
                <div class="box-body no-padding">
                    <div class="text-center">
                        @foreach($innerPagesList as $oneInnerPage)
                            <a href="{!! url('admin/cases/inner/showPage/'.$oneInnerPage['id']) !!}"
                               class="btn btn-primary">{!! $oneInnerPage['front_title_en'] !!}</a>
                        @endforeach
                    </div>
                </div>

                <div class="box-body no-padding">
                    <div class="container">

                        {!! Form::open(array('action'=>'CasesController@updateMainPage','method'=>'post','files'=>true)) !!}

                        <div class="form-group clearfix">
                            <div class="container-fluid">
                                <div class="checkbox">
                                    <label for="editPassOld" class="control-label">
                                        {!! Form::checkbox('head_is_on',true,@$pageData['head_is_on']) !!}
                                        On/Off header
                                    </label>
                                </div>
                            </div>
                        </div>

                        @include('admin.components.headerComponent',['headerObj'=>\App\HeaderComponentEntity::getHeaderObj(@$pageData['head_id'])])

                        <hr>
                        {{--HEADER TEXT--}}
                        <div class="form-group clearfix">
                            <label for="editPassNew" class="col-sm-4 col-md-2 control-label">Edit texts between header and four blocks </label>

                            <div class="col-sm-8 col-md-10">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#pben_area_en"
                                                                              aria-controls="pben_area_en"
                                                                              role="tab"
                                                                              data-toggle="tab">EN</a></li>
                                    <li role="presentation"><a href="#pben_area_de" aria-controls="pben_area_de"
                                                               role="tab"
                                                               data-toggle="tab">DE</a></li>
                                    <li role="presentation"><a href="#pben_area_fr" aria-controls="pben_area_fr"
                                                               role="tab"
                                                               data-toggle="tab">FR</a></li>

                                </ul>

                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="pben_area_en">
                                        {!! Form::text('title_en',$pageData['title_en'],['placeholder'=>'title_en','class'=>'form-control']) !!}
                                        {!! Form::textarea('text_en',$pageData['text_en'],['placeholder'=>'text_en','class'=>'form-control html5area']) !!}
                                        @if($errors -> first('text_en') != '')
                                            <span class="help-block">{!! $errors -> first('text_en') !!}</span>
                                        @endif
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="pben_area_de">
                                        {!! Form::text('title_de',$pageData['title_de'],['placeholder'=>'title_en','class'=>'form-control']) !!}
                                        {!! Form::textarea('text_de',$pageData['text_de'],['placeholder'=>'text_de','class'=>'form-control html5area']) !!}
                                        @if($errors -> first('text_de') != '')
                                            <span class="help-block">{!! $errors -> first('text_de') !!}</span>
                                        @endif
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="pben_area_fr">
                                        {!! Form::text('title_fr',$pageData['title_fr'],['placeholder'=>'title_en','class'=>'form-control']) !!}
                                        {!! Form::textarea('text_fr',$pageData['text_fr'],['placeholder'=>'text_fr','class'=>'form-control html5area']) !!}
                                        @if($errors -> first('text_fr') != '')
                                            <span class="help-block">{!! $errors -> first('text_fr') !!}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>

                        <div class="form-group clearfix">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="checkbox">
                                            <label for="editPassOld" class="control-label">
                                                {!! Form::checkbox('opinion_is_on',true,@$pageData['opinion_is_on']) !!}
                                                On/Off opinion component
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @include('admin.components.opinionComponent',['opinionObj'=>\App\OpinionComponentEntity::getOpinionObj(@$pageData['opinion_id'])])
                        <hr>


                        <div class="form-group clearfix">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="checkbox">
                                            <label for="editPassOld" class="control-label">
                                                {!! Form::checkbox('try_is_on',true,@$pageData['try_is_on']) !!}
                                                On/Off try component
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @include('admin.components.tryComponent',['tryObj'=>\App\TryComponentEntity::getTryObj(@$pageData['try_id'])])
                        <hr>

                        <div class="form-group">

                            <div class="col-sm-6 text-right">

                            </div>
                        </div>


                    </div>
                </div>


                <div class="text-center">
                    {!! Form::submit('Save page state',['class'=>'btn btn-success']) !!}
                </div>
                {!! Form::close() !!}
            </div>


        </section>
    </div>


@endsection