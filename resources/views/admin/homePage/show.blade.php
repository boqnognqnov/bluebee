@extends('admin.master')
@section('content')

    <div class="content-wrapper">
        <section class="content">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Page Home</h3>
                </div>
                <div class="box-body no-padding">
                    <div>
                        {{--{!! Form::open(array('action' => 'RoomsEntityController@setImageToGallery','class'=>'form-horizontal','files'=>true)) !!}--}}
                        {{--{!! Form::hidden('room_id',$roomId) !!}--}}
                        {{--{!! Form::file('image') !!}--}}
                        {{--{!! Form::submit('Upload',['class'=>'btn btn-success']) !!}--}}
                        {{--{!! Form::close() !!}--}}
                    </div>
                </div>


                <div class="box-body no-padding">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">

                                @if(Session::has('successMessage'))
                                    <div class="alert alert-success">
                                        {!! Session::get('successMessage') !!}
                                    </div>
                                @endif

                                @if($errors -> any() )
                                    <div class="alert alert-danger" style="width: 100%;">
                                        @foreach ($errors->all() as $error)
                                            {{--<button type="button" class="close" data-dismiss="alert">?</button>--}}
                                            <p>{{ $error }}</p>
                                        @endforeach
                                    </div>
                                @endif
                            </div>
                        </div>


                        {!! Form::open(array('action'=>'HomePageController@update','method'=>'post','files'=>true)) !!}
                        <div class="form-group clearfix">
                            <div class="container-fluid">
                                <div class="checkbox">
                                    <label for="editPassOld" class="col-sm-4 col-md-2 control-label">
                                        {!! Form::checkbox('head_is_on',true,@$pageData['head_is_on']) !!}
                                        @if($errors -> first('head_is_on') != '')
                                            <span class="help-block">{!! $errors -> first('head_is_on') !!}</span>
                                        @endif
                                        On/Off Header
                                    </label>
                                </div>
                            </div>
                        </div>
                        {{--dsadasdas--}}
                        <div class="form-group clearfix">
                            <label for="headerIMG" class="col-sm-4 col-md-2 control-label">Background image <img
                                        src="{!! asset(\App\HomePageEntity::$pathHeader.@$pageData['head_image']) !!}"
                                        class="img-responsive"
                                        style="display: block;"></label>

                            <div class="col-sm-8 col-md-10">
                                {!! Form::file('head_image',['placeholder'=>'image','class'=>'form-control','id'=>'headerIMG']) !!}
                                @if($errors -> first('head_image') != '')
                                    <span class="help-block">{!! $errors -> first('head_image') !!}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="" class="col-sm-4 col-md-2 control-label">Header text</label>

                            <div class="col-sm-8 col-md-10">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#hHead_area_en"
                                                                              aria-controls="hHead_area_en"
                                                                              role="tab"
                                                                              data-toggle="tab">EN</a></li>
                                    <li role="presentation"><a href="#hHead_area_de" aria-controls="hHead_area_de"
                                                               role="tab"
                                                               data-toggle="tab">DE</a></li>
                                    <li role="presentation"><a href="#hHead_area_fr" aria-controls="hHead_area_fr"
                                                               role="tab"
                                                               data-toggle="tab">FR</a></li>

                                </ul>

                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="hHead_area_en">
                                        {!! Form::text( 'head_title_en',@$pageData['head_title_en'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                        {!! Form::textarea( 'head_text_en',@$pageData['head_text_en'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}

                                        {!! Form::label('Button text','Button text',['class'=>'control-label form-control']) !!}
                                        {!! Form::text( 'head_button_en',@$pageData['head_button_en'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="hHead_area_de">
                                        {!! Form::text( 'head_title_de',@$pageData['head_title_de'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                        {!! Form::textarea( 'head_text_de', @$pageData['head_text_de'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}

                                        {!! Form::label('Button text','Button text',['class'=>'control-label form-control']) !!}
                                        {!! Form::text( 'head_button_de',@$pageData['head_button_de'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="hHead_area_fr">
                                        {!! Form::text( 'head_title_fr',@$pageData['head_title_fr'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                        {!! Form::textarea( 'head_text_fr', @$pageData['head_text_fr'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}

                                        {!! Form::label('Button text','Button text',['class'=>'control-label form-control']) !!}
                                        {!! Form::text( 'head_button_fr',@$pageData['head_button_fr'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}


                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="form-group">

                            {!! Form::label('Button URL','Button URL',['class'=>'control-label form-control']) !!}<br>
                            {!! Form::text( 'head_button_url',@$pageData['head_button_url'], array( 'class' => 'form-control' ) ) !!}
                        </div>
                        <hr>

                        {{--dasdasd--}}

                        <div class="form-group clearfix">
                            <div class="container-fluid">
                                <div class="checkbox">
                                    <label for="editPassOld" class="col-sm-4 col-md-2 control-label">
                                        {!! Form::checkbox('how_work_is_on',true,@$pageData['how_work_is_on']) !!}
                                        @if($errors -> first('how_work_is_on') != '')
                                            <span class="help-block">{!! $errors -> first('how_work_is_on') !!}</span>
                                        @endif
                                        On/Off How Work Component
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group clearfix">
                            <div class="container-fluid">
                                <div class="checkbox">
                                    <label for="editPassOld" class="col-sm-4 col-md-2 control-label">
                                        {!! Form::checkbox('why_blue_is_on',true,@$pageData['why_blue_is_on']) !!}
                                        @if($errors -> first('why_blue_is_on') != '')
                                            <span class="help-block">{!! $errors -> first('why_blue_is_on') !!}</span>
                                        @endif
                                        On/Off Why Bluebee Component
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group clearfix">
                            <div class="container-fluid">
                                <div class="checkbox">
                                    <label for="editPassOld" class="col-sm-4 col-md-2 control-label">
                                        {!! Form::checkbox('partners_is_on',true,@$pageData['partners_is_on']) !!}
                                        @if($errors -> first('why_blue_is_on') != '')
                                            <span class="help-block">{!! $errors -> first('partners_is_on') !!}</span>
                                        @endif
                                        On/Off Partners Component
                                    </label>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group clearfix">
                            <div class="container-fluid">
                                <div class="checkbox">
                                    <label for="editPassOld" class="col-sm-4 col-md-2 control-label">
                                        {!! Form::checkbox('opinion_is_on',true,@$pageData['opinion_is_on']) !!}
                                        @if($errors -> first('why_blue_is_on') != '')
                                            <span class="help-block">{!! $errors -> first('opinion_is_on') !!}</span>
                                        @endif
                                        On/Off Opinion Component
                                    </label>
                                </div>
                            </div>
                        </div>
                        @include('admin.components.opinionComponent',['opinionObj'=>\App\OpinionComponentEntity::getOpinionObj($pageData['opinion_id'])])
                        <hr>

                        <div class="form-group clearfix">
                            <div class="container-fluid">
                                <div class="checkbox">
                                    <label for="editPassOld" class="col-sm-4 col-md-2 control-label">
                                        {!! Form::checkbox('try_is_on',true,@$pageData['try_is_on']) !!}
                                        @if($errors -> first('try_is_on') != '')
                                            <span class="help-block">{!! $errors -> first('try_is_on') !!}</span>
                                        @endif
                                        On/Off Try component
                                    </label>
                                </div>
                            </div>
                        </div>

                        @include('admin.components.tryComponent',['tryObj'=>\App\TryComponentEntity::getTryObj(@$pageData['try_id'])])
                        <hr>

                    </div>
                </div>


                <br>

                <div class="text-center">


                    {!! Form::submit('Save page state',['class'=>'btn btn-success']) !!}
                    {!! Form::close() !!}

                </div>
            </div>


        </section>
    </div>


@endsection