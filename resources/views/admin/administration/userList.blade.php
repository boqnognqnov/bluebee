@extends( 'admin.master' )
@section( 'content' )

        <!-- DataTables CSS -->
<link rel="stylesheet" type="text/css"
      href="{!! asset('plugins/DataTables-1.10.9/media/css/jquery.dataTables.css') !!}">

<!-- jQuery -->
<script type="text/javascript" charset="utf8"
        src="{!! asset('plugins/DataTables-1.10.9/media/js/jquery.js') !!}"></script>

<!-- DataTables -->
<script type="text/javascript" charset="utf8"
        src="{!! asset('plugins/DataTables-1.10.9/media/js/jquery.dataTables.js') !!}"></script>

<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-md-12">

                @if(Session::has('successMessage'))
                    <div class="alert alert-success">
                        {!! Session::get('successMessage') !!}
                    </div>
                @endif

                @if($errors -> any() )
                    <div class="alert alert-danger" style="width: 100%;">
                        @foreach ($errors->all() as $error)
                            {{--<button type="button" class="close" data-dismiss="alert">?</button>--}}
                            <p>{{ $error }}</p>
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-warning">
                    <div class="box-header">
                        <h3 class="box-title">List of users</h3>

                        <div class="box-tools">
                            <a href="{!! url('admin/administrator/record/new') !!}" class="btn btn-warning"
                               style="display: inline-block">Create <strong>user </strong></a>

                        </div>
                    </div>
                    <div class="box-body table-responsive no-padding">
                        <div id="example_wrapper" class="dataTables_wrapper">
                            <table id="example" class="display pageResize" cellspacing="0" width="100%">

                                <thead>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Admin/User</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Approve download</th>
                                <th></th>
                                <th></th>
                                </thead>
                                <tbody>
                                @foreach($users as $oneUser)
                                    @if($oneUser['is_admin'])
                                        <tr style="background-color: #ff8080">
                                    @else
                                        <tr>
                                            @endif
                                            <td>{!! $oneUser['id'] !!}</td>
                                            <td>{!! $oneUser['name'] !!}</td>
                                            @if($oneUser['is_admin']==1)
                                                <td>Admin</td>
                                            @else
                                                <td>Client</td>
                                            @endif
                                            <td>{!! $oneUser['email'] !!}</td>
                                            <td>{!! $oneUser['phone'] !!}</td>
                                            <td>
                                                @if($oneUser['is_approve']==true)
                                                    {!! Form::button('Approved', ['style'=>'min-width: 120px;','class'=>'btn btn-success approveActivate','data-user-id'=>$oneUser['id']]) !!}
                                                @else
                                                    {!! Form::button('Not approved', ['style'=>'min-width: 120px;','class'=>'btn btn-danger approveActivate','data-user-id'=>$oneUser['id']]) !!}
                                                @endif
                                            </td>
                                            <td><a href="{!! url('admin/administrator/user/show/'.$oneUser['id'] ) !!}"
                                                   class="btn btn-primary">Edit</a></td>
                                            <td>
                                                <button class="btn btn-danger delUser"
                                                        data-user-id="{!! $oneUser['id'] !!}">Delete
                                                </button>
                                            </td>

                                        </tr>
                                        @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>


<script>
    $('#example').DataTable(
            {
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                "bFilter": true,
                "bLengthChange": true
            }
    );
</script>

<div style="display: none">
    {!! Form::open(array('action'=>'AdministratorController@deleteUser','method'=>'post','id'=>'formUserDel')) !!}
    {!! Form::hidden('userId') !!}
    {!! Form::close() !!}
</div>


<script>

    $(document).ready(function () {

        $('.delUser').on('click', function (event) {

            var id = $(this).attr('data-user-id');

            $('#formUserDel input[name="userId"]').val(id);
            var r = confirm("Do you really want to delete this user ?");
            if (r == true) {
                $('#formUserDel').submit();
            }
        });
    });
</script>

<div style="display: none">
    {!! Form::open(array('action' => 'AdministratorController@changeApprove','id'=>'form-approve-user')) !!}
    {!! Form::hidden('id') !!}
    {!! Form::close() !!}
</div>

<script>

    $(document).ready(function () {

        $('.approveActivate').on('click', function (event) {
            var id = $(this).attr('data-user-id');
            $('#form-approve-user input[name="id"]').val(id);
            $('#form-approve-user').submit();
        });
    });

    $('.sliderActivate.btn-danger').hover(function () {
        $(this).attr('class', 'activation btn btn-success');
        $(this).text('Set approve');
    }, function () {
        $(this).attr('class', 'activation btn btn-danger');
        $(this).text('Not approved');
    });

    $('.sliderActivate.btn-success').hover(function () {
        $(this).attr('class', 'activation btn btn-danger');
        $(this).text('Stop approve');
    }, function () {
        $(this).attr('class', 'activation btn btn-success');
        $(this).text('Approved');
    });
</script>


@endsection