@extends( 'admin.master' )
@section( 'content' )

    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-no-padding">
                        <div class="box-header">
                            <h3 class="box-title">Update user</h3>

                            <div class="box-tools">
                                <a href="{!! url('admin/administrator/page/show') !!}" class="btn btn-primary">Back to
                                    user list</a>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">

                                    @if(Session::has('successMessage'))
                                        <div class="alert alert-success">
                                            {!! Session::get('successMessage') !!}
                                        </div>
                                    @endif

                                    @if($errors -> any() )
                                        <div class="alert alert-danger" style="width: 100%;">
                                            @foreach ($errors->all() as $error)
                                                {{--<button type="button" class="close" data-dismiss="alert">?</button>--}}
                                                <p>{{ $error }}</p>
                                            @endforeach
                                        </div>
                                    @endif
                                </div>
                            </div>


                            {{--END OF DATA--}}
                            {{--sadsadasda--}}
                            {!! Form::open(array('action' => 'AdministratorController@updateUser','method'=>'post','class'=>'form-horizontal')) !!}
                            {!! Form::hidden('id',$user['id']) !!}
                            {{--{!! Form::hidden('token',csrf_field()) !!}--}}

                            <div class="form-group">
                                <label for="editPassOld" class="col-sm-4 control-label">Name:</label>

                                <div class="col-sm-8">
                                    {!! Form::text('name',@$user['name'],['placeholder'=>'name','class'=>'form-control','id'=>'editPassOld']) !!}
                                    @if($errors -> first('name') != '')
                                        <span class="help-block">{!! $errors -> first('name') !!}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="editPassNew" class="col-sm-4 control-label">Email:</label>

                                <div class="col-sm-8">
                                    {!! Form::text('email',@$user['email'],['placeholder'=>'email','class'=>'form-control','id'=>'editPassNew']) !!}
                                    @if($errors -> first('email') != '')
                                        <span class="help-block">{!! $errors -> first('email') !!}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="editPassNewAgain" class="col-sm-4 control-label">Phone</label>

                                <div class="col-sm-8">
                                    {!! Form::text('phone',@$user['phone'],['placeholder'=>'phone','class'=>'form-control','id'=>'editPassNewAgain']) !!}
                                    @if($errors -> first('phone') != '')
                                        <span class="help-block">{!! $errors -> first('phone') !!}</span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="pf-oldpass" class="col-sm-4 control-label">Old password</label>
                                <div class="col-sm-8">
                                    {!! Form::password('old_password',['class'=>'form-control','id'=>'pf-oldpass']) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="pf-newpass1" class="col-sm-4 control-label">New password</label>
                                <div class="col-sm-8">
                                    {!! Form::password('new_password',['class'=>'form-control','id'=>'pf-newpass1']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="pf-newpass2" class="col-sm-4 control-label">Repeat new password</label>
                                <div class="col-sm-8">
                                    {!! Form::password('re_new_password',['class'=>'form-control','id'=>'pf-newpass2']) !!}
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="editPassNewAgain" class="col-sm-4 control-label">Enable Admin
                                    status:</label>

                                <div class="col-sm-8">
                                    {!! Form::checkbox('is_admin',true,$user['is_admin']) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="editPassNewAgain" class="col-sm-4 control-label">is approve user:</label>

                                <div class="col-sm-8">
                                    {!! Form::checkbox('is_approve',true,$user['is_approve']) !!}
                                </div>
                            </div>

                        </div>

                        <div class="modal-footer">
                            {!! Form::submit('Save',['class'=>'btn btn-success']) !!}
                        </div>
                        {!! Form::close() !!}


                        {{--asdsadadsad--}}
                    </div>
                </div>
            </div>
    </div>
    </section>
    </div>

@endsection