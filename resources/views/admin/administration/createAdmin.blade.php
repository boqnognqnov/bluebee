@extends( 'admin.master' )
@section( 'content' )

    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-no-padding">
                        <div class="box-header">
                            <h3 class="box-title">Create new user</h3>

                            <div class="box-tools">
                                <a href="{!! url('admin/administrator/page/show') !!}" class="btn btn-primary">Back to
                                    user list</a>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">

                                    @if(Session::has('successMessage'))
                                        <div class="alert alert-success">
                                            {!! Session::get('successMessage') !!}
                                        </div>
                                    @endif

                                    @if($errors -> any() )
                                        <div class="alert alert-danger" style="width: 100%;">
                                            @foreach ($errors->all() as $error)
                                                {{--<button type="button" class="close" data-dismiss="alert">?</button>--}}
                                                <p>{{ $error }}</p>
                                            @endforeach
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                {{--DATA--}}
                                {{--{!! Form::open(array('action'=>'AdministratorController@storeEditAdmin','method'=>'post','files'=>true)) !!}--}}
                                {{--{!! Form::hidden('id',0) !!}--}}
                                {{--{!! Form::text( 'name',null, array( 'class' => 'form-control' ) ) !!}--}}
                                {{--{!! Form::text( 'email',null, array( 'class' => 'form-control' ) ) !!}--}}
                                {{--{!! Form::text( 'phone',null, array( 'class' => 'form-control' ) ) !!}--}}
                                {{--<div class="col-sm-6 text-right">--}}
                                {{--{!! Form::submit('Save',['class'=>'btn btn-success']) !!}--}}
                                {{--</div>--}}
                                {{--{!! Form::close() !!}--}}
                            </div>

                            {{--END OF DATA--}}
                            {{--sadsadasda--}}
                            {!! Form::open(array('action' => 'AdministratorController@storeUser','method'=>'post','class'=>'form-horizontal')) !!}
                            {!! Form::hidden('id',0) !!}
                            {{--{!! Form::hidden('token',csrf_field()) !!}--}}

                            <div class="form-group">
                                <label for="editPassOld" class="col-sm-4 control-label">Name:</label>

                                <div class="col-sm-8">
                                    {!! Form::text('name',null,['placeholder'=>'name','class'=>'form-control','id'=>'editPassOld']) !!}
                                    @if($errors -> first('name') != '')
                                        <span class="help-block">{!! $errors -> first('name') !!}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="editPassNew" class="col-sm-4 control-label">Email:</label>

                                <div class="col-sm-8">
                                    {!! Form::text('email',null,['placeholder'=>'email','class'=>'form-control','id'=>'editPassNew']) !!}
                                    @if($errors -> first('email') != '')
                                        <span class="help-block">{!! $errors -> first('email') !!}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="editPassNewAgain" class="col-sm-4 control-label">Phone:</label>

                                <div class="col-sm-8">
                                    {!! Form::text('phone',null,['placeholder'=>'phone','class'=>'form-control','id'=>'editPassNewAgain']) !!}
                                    @if($errors -> first('phone') != '')
                                        <span class="help-block">{!! $errors -> first('phone') !!}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="editPassNewAgain" class="col-sm-4 control-label">Password:</label>

                                <div class="col-sm-8">
                                    {!! Form::password('password',['placeholder'=>'password','class'=>'form-control','id'=>'editPassNewAgain']) !!}
                                    @if($errors -> first('password') != '')
                                        <span class="help-block">{!! $errors -> first('password') !!}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="editPassNewAgain" class="col-sm-4 control-label">Enable Admin
                                    status:</label>

                                <div class="col-sm-8">
                                    {!! Form::checkbox('is_admin',null,false) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="editPassNewAgain" class="col-sm-4 control-label">is approve user:</label>

                                <div class="col-sm-8">
                                    {!! Form::checkbox('is_approve',null,false) !!}
                                </div>
                            </div>

                        </div>

                        <div class="modal-footer">
                            {!! Form::submit('Save',['class'=>'btn btn-success']) !!}
                        </div>
                        {!! Form::close() !!}


                        {{--asdsadadsad--}}
                    </div>
                </div>
            </div>
    </div>
    </section>
    </div>

@endsection