@extends('admin.master')
@section('content')

    <script src="{!! url('https://code.jquery.com/ui/1.12.0-beta.1/jquery-ui.min.js') !!}"></script>
    <link rel="stylesheet" href="{!! url('https://code.jquery.com/ui/1.11.4/themes/black-tie/jquery-ui.css') !!}">
    <script>
        $(function () {
            $("#accordion").accordion();
        });
    </script>

    <div class="content-wrapper">
        <section class="content">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Page How Does It Work</h3>
                </div>
                <div class="box-body no-padding">
                    <div>
                        {{--{!! Form::open(array('action' => 'RoomsEntityController@setImageToGallery','class'=>'form-horizontal','files'=>true)) !!}--}}
                        {{--{!! Form::hidden('room_id',$roomId) !!}--}}
                        {{--{!! Form::file('image') !!}--}}
                        {{--{!! Form::submit('Upload',['class'=>'btn btn-success']) !!}--}}
                        {{--{!! Form::close() !!}--}}
                    </div>
                </div>

                <div class="box-body no-padding">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">

                                @if(Session::has('successMessage'))
                                    <div class="alert alert-success">
                                        {!! Session::get('successMessage') !!}
                                    </div>
                                @endif

                                @if($errors -> any() )
                                    <div class="alert alert-danger" style="width: 100%;"></div>
                                    @endif
                                    @if($errors -> any() )
                                            <!-- collect the error messages -->
                                    @foreach ($errors->all() as $error)
                                        <input type="hidden" class="error-message" value="{{ $error }}">
                                    @endforeach
                                @endif

                                <script>
                                    $(document).ready(function () {
                                        // loop through hidden fields with error messages
//
                                        var errors = '';
                                        var tempError = '';
                                        $('.error-message').each(function () {
                                            // show an alert with the message

                                            if (tempError != $(this).attr('value')) {
                                                errors += $(this).attr('value') + '<br>';
                                                tempError = $(this).attr('value');
                                            }


                                        });
                                        $('.alert-danger').html(errors);
                                        // loop through hidden fields with element names
                                    });
                                </script>
                                <!-- END OF ERROR HANDLING -->
                            </div>
                        </div>


                        {!! Form::open(array('action'=>'MethodsController@update','method'=>'post','files'=>true)) !!}

                        {{--<div class="form-group clearfix">--}}
                            {{--<div class="container-fluid">--}}
                                {{--<div class="checkbox">--}}
                                    {{--<label for="editPassOld" class="control-label">--}}
                                        {{--{!! Form::checkbox('head_is_on',true,@$methodsData['head_is_on']) !!}--}}
                                        {{--On/Off header--}}
                                    {{--</label>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}


                        {{--<div class="form-group clearfix">--}}
                            {{--<label for="editPassNew" class="col-sm-4 col-md-2 control-label">Background image <img--}}
                                        {{--src="{!! asset(\App\MethodsEntity::$path.@$methodsData['head_image']) !!}"--}}
                                        {{--class="img-responsive" style="display: block;"></label>--}}

                            {{--<div class="col-sm-8 col-md-10">--}}
                                {{--{!! Form::file('head_image',['class'=>'form-control','id'=>'editPassNew']) !!}--}}
                                {{--@if($errors -> first('head_image') != '')--}}
                                    {{--<span class="help-block">{!! $errors -> first('head_image') !!}</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        <div class="form-group clearfix">
                            <div class="container-fluid">
                                <div class="checkbox">
                                    <label for="editPassOld" class="control-label">
                                        {!! Form::checkbox('head_is_on',true,@$methodsData['head_is_on']) !!}
                                        On/Off header
                                    </label>
                                </div>
                            </div>
                        </div>

                        @include('admin.components.headerComponent',['headerObj'=>\App\HeaderComponentEntity::getHeaderObj(@$methodsData['head_id'])])

                        <hr>

                        <hr>
                        {{--HEADER TEXT--}}
                        <div class="form-group clearfix">
                            <label for="editPassNew" class="col-sm-4 col-md-2 control-label">Edit title and
                                text in page</label>

                            <div class="col-sm-8 col-md-10">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#pben_area_en"
                                                                              aria-controls="pben_area_en"
                                                                              role="tab"
                                                                              data-toggle="tab">EN</a></li>
                                    <li role="presentation"><a href="#pben_area_de" aria-controls="pben_area_de"
                                                               role="tab"
                                                               data-toggle="tab">DE</a></li>
                                    <li role="presentation"><a href="#pben_area_fr" aria-controls="pben_area_fr"
                                                               role="tab"
                                                               data-toggle="tab">FR</a></li>

                                </ul>

                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="pben_area_en">
                                        {!! Form::text('title_en',$methodsData['title_en'],['placeholder'=>'Парола','class'=>'form-control']) !!}
                                        @if($errors -> first('text_en') != '')
                                            <span class="help-block">{!! $errors -> first('text_en') !!}</span>
                                        @endif
                                        {!! Form::textarea('text_en',$methodsData['text_en'],['placeholder'=>'Парола','class'=>'form-control html5area']) !!}
                                        @if($errors -> first('text_en') != '')
                                            <span class="help-block">{!! $errors -> first('text_en') !!}</span>
                                        @endif
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="pben_area_de">
                                        {!! Form::text('title_de',$methodsData['title_de'],['placeholder'=>'Парола','class'=>'form-control']) !!}
                                        @if($errors -> first('text_en') != '')
                                            <span class="help-block">{!! $errors -> first('text_en') !!}</span>
                                        @endif
                                        {!! Form::textarea('text_de',$methodsData['text_de'],['placeholder'=>'Парола','class'=>'form-control html5area']) !!}
                                        @if($errors -> first('text_de') != '')
                                            <span class="help-block">{!! $errors -> first('text_de') !!}</span>
                                        @endif
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="pben_area_fr">
                                        {!! Form::text('title_fr',$methodsData['title_fr'],['placeholder'=>'Парола','class'=>'form-control']) !!}
                                        @if($errors -> first('text_en') != '')
                                            <span class="help-block">{!! $errors -> first('text_en') !!}</span>
                                        @endif
                                        {!! Form::textarea('text_fr',$methodsData['text_fr'],['placeholder'=>'Парола','class'=>'form-control html5area']) !!}
                                        @if($errors -> first('text_fr') != '')
                                            <span class="help-block">{!! $errors -> first('text_fr') !!}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{--END HEADER TEXT--}}

                        <hr>


                        {{--<div class="form-group">--}}
                        <div id="accordion">
                            <h3>Step 1</h3>

                            <div>
                                <div class="form-group">
                                    <label for="headerIMG" class="col-sm-4 col-md-2 control-label">Background image <img
                                                src="{!! asset(\App\MethodsEntity::$pathStepsIMG['pathStep1'].$methodsData['one_image']) !!}"
                                                class="img-responsive"
                                                style="display: block;">
                                        @if(!empty($methodsData['one_image']))
                                            <a href="{!! url('admin/methods/destroyImage/'.$methodsData['one_image'].'/one') !!}"
                                               class="btn btn-danger">Delete image</a>
                                        @endif
                                    </label>

                                    <div class="col-sm-8 col-md-10">
                                        {!! Form::file('one_image',['placeholder'=>'Парола','class'=>'form-control','id'=>'headerIMG']) !!}
                                        @if($errors -> first('password') != '')
                                            <span class="help-block">{!! $errors -> first('password') !!}</span>
                                        @endif
                                    </div>


                                </div>

                                {{--STEP 1 TAB CONTENT--}}
                                <div class="col-sm-8 col-md-10">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#mstp1_area_en"
                                                                                  aria-controls="mstp1_area_en"
                                                                                  role="tab"
                                                                                  data-toggle="tab">EN</a></li>
                                        <li role="presentation"><a href="#mstp1_area_de" aria-controls="mstp1_area_de"
                                                                   role="tab"
                                                                   data-toggle="tab">DE</a></li>
                                        <li role="presentation"><a href="#mstp1_area_fr" aria-controls="mstp1_area_fr"
                                                                   role="tab"
                                                                   data-toggle="tab">FR</a></li>

                                    </ul>
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="mstp1_area_en">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    {!! Form::text( 'one_title_en',$methodsData['one_title_en'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    {!! Form::textarea( 'one_text_en',$methodsData['one_text_en'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                                </div>
                                            </div>
                                            {{--SHORT TEXTS--}}
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    {!! Form::text( 'one_title_short_en',$methodsData['one_title_short_en'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    {!! Form::textarea( 'one_text_short_en',$methodsData['one_text_short_en'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="mstp1_area_de">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    {!! Form::text( 'one_title_de',$methodsData['one_title_de'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    {!! Form::textarea( 'one_text_de',$methodsData['one_text_de'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                                </div>
                                            </div>
                                            {{--SHORT TEXTS--}}
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    {!! Form::text( 'one_title_short_de',$methodsData['one_title_short_de'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    {!! Form::textarea( 'one_text_short_de',$methodsData['one_text_short_de'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="mstp1_area_fr">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    {!! Form::text( 'one_title_fr',$methodsData['one_title_fr'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    {!! Form::textarea( 'one_text_fr',$methodsData['one_text_fr'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                                </div>
                                            </div>
                                            {{--SHORT TEXTS--}}
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    {!! Form::text( 'one_title_short_fr',$methodsData['one_title_short_fr'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    {!! Form::textarea( 'one_text_short_fr',$methodsData['one_text_short_fr'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                {{--END STEP 1 TAB CONTENT--}}
                            </div>
                            <h3>Step 2</h3>

                            <div>
                                <div class="form-group">
                                    <label for="headerIMG" class="col-sm-4 col-md-2 control-label">Background image <img
                                                src="{!! asset(\App\MethodsEntity::$pathStepsIMG['pathStep2'].$methodsData['second_image']) !!}"
                                                class="img-responsive"
                                                style="display: block;">
                                        @if(!empty($methodsData['second_image']))
                                            <a href="{!! url('admin/methods/destroyImage/'.$methodsData['second_image'].'/second') !!}"
                                               class="btn btn-danger">Delete image</a>
                                        @endif
                                    </label>


                                    <div class="col-sm-8 col-md-10">
                                        {!! Form::file('second_image',['placeholder'=>'Парола','class'=>'form-control','id'=>'headerIMG']) !!}
                                        @if($errors -> first('password') != '')
                                            <span class="help-block">{!! $errors -> first('password') !!}</span>
                                        @endif
                                    </div>

                                </div>
                                {{--STEP 2 TAB CONTENT--}}
                                <div class="col-sm-8 col-md-10">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#mstp2_area_en"
                                                                                  aria-controls="mstp2_area_en"
                                                                                  role="tab"
                                                                                  data-toggle="tab">EN</a></li>
                                        <li role="presentation"><a href="#mstp2_area_de" aria-controls="mstp2_area_de"
                                                                   role="tab"
                                                                   data-toggle="tab">DE</a></li>
                                        <li role="presentation"><a href="#mstp2_area_fr" aria-controls="mstp2_area_fr"
                                                                   role="tab"
                                                                   data-toggle="tab">FR</a></li>

                                    </ul>
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="mstp2_area_en">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    {!! Form::text( 'second_title_en',$methodsData['second_title_en'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    {!! Form::textarea( 'second_text_en',$methodsData['second_text_en'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                                </div>
                                            </div>
                                            {{--SHORT TEXTS--}}
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    {!! Form::text( 'second_title_short_en',$methodsData['second_title_short_en'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    {!! Form::textarea( 'second_text_short_en',$methodsData['second_text_short_en'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="mstp2_area_de">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    {!! Form::text( 'second_title_de',$methodsData['second_title_de'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    {!! Form::textarea( 'second_text_de',$methodsData['second_text_de'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                                </div>
                                            </div>
                                            {{--SHORT TEXTS--}}
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    {!! Form::text( 'second_title_short_de',$methodsData['one_title_short_fr'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    {!! Form::textarea( 'second_text_short_de',$methodsData['one_text_short_fr'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="mstp2_area_fr">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    {!! Form::text( 'second_title_fr',$methodsData['second_title_fr'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    {!! Form::textarea( 'second_text_fr',$methodsData['second_text_fr'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                                </div>
                                            </div>
                                            {{--SHORT TEXTS--}}
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    {!! Form::text( 'second_title_short_fr',$methodsData['one_title_short_fr'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    {!! Form::textarea( 'second_text_short_fr',$methodsData['one_text_short_fr'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                {{--END STEP 2 TAB CONTENT--}}
                            </div>
                            <h3>Step 3</h3>

                            <div>
                                <div class="form-group">
                                    <label for="headerIMG" class="col-sm-4 col-md-2 control-label">Background image <img
                                                src="{!! asset(\App\MethodsEntity::$pathStepsIMG['pathStep3'].$methodsData['third_image']) !!}"
                                                class="img-responsive"
                                                style="display: block;">
                                        @if(!empty($methodsData['third_image']))
                                            <a href="{!! url('admin/methods/destroyImage/'.$methodsData['third_image'].'/third') !!}"
                                               class="btn btn-danger">Delete image</a>
                                        @endif
                                    </label>

                                    <div class="col-sm-8 col-md-10">
                                        {!! Form::file('third_image',['placeholder'=>'Парола','class'=>'form-control','id'=>'headerIMG']) !!}
                                        @if($errors -> first('password') != '')
                                            <span class="help-block">{!! $errors -> first('password') !!}</span>
                                        @endif
                                    </div>

                                </div>

                                {{--STEP 3 TAB CONTENT--}}
                                <div class="col-sm-8 col-md-10">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#mstp3_area_en"
                                                                                  aria-controls="mstp3_area_en"
                                                                                  role="tab"
                                                                                  data-toggle="tab">EN</a></li>
                                        <li role="presentation"><a href="#mstp3_area_de" aria-controls="mstp3_area_de"
                                                                   role="tab"
                                                                   data-toggle="tab">DE</a></li>
                                        <li role="presentation"><a href="#mstp3_area_fr" aria-controls="mstp3_area_fr"
                                                                   role="tab"
                                                                   data-toggle="tab">FR</a></li>

                                    </ul>
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="mstp3_area_en">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    {!! Form::text( 'third_title_en',$methodsData['third_title_en'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    {!! Form::textarea( 'third_text_en',$methodsData['third_text_en'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                                </div>
                                            </div>
                                            {{--SHORT TEXTS--}}
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    {!! Form::text( 'third_title_short_en',$methodsData['third_title_short_en'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    {!! Form::textarea( 'third_text_short_en',$methodsData['third_text_short_en'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="mstp3_area_de">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    {!! Form::text( 'third_title_de',$methodsData['third_title_de'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    {!! Form::textarea( 'third_text_de',$methodsData['third_text_de'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                                </div>
                                            </div>
                                            {{--SHORT TEXTS--}}
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    {!! Form::text( 'third_title_short_de',$methodsData['one_title_short_fr'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    {!! Form::textarea( 'third_text_short_de',$methodsData['one_text_short_fr'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="mstp3_area_fr">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    {!! Form::text( 'third_title_fr',$methodsData['third_title_fr'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    {!! Form::textarea( 'third_text_fr',$methodsData['third_text_fr'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                                </div>
                                            </div>
                                            {{--SHORT TEXTS--}}
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    {!! Form::text( 'third_title_short_fr',$methodsData['one_title_short_fr'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    {!! Form::textarea( 'third_text_short_fr',$methodsData['one_text_short_fr'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                {{--END STEP 3 TAB CONTENT--}}

                            </div>
                            <h3>Step 4</h3>

                            <div>
                                <div class="form-group">
                                    <label for="headerIMG" class="col-sm-4 col-md-2 control-label">Background image <img
                                                src="{!! asset(\App\MethodsEntity::$pathStepsIMG['pathStep4'].$methodsData['fourth_image']) !!}"
                                                class="img-responsive"
                                                style="display: block;">
                                        @if(!empty($methodsData['fourth_image']))
                                            <a href="{!! url('admin/methods/destroyImage/'.$methodsData['fourth_image'].'/fourth') !!}"
                                               class="btn btn-danger">Delete image</a>
                                        @endif
                                    </label>

                                    <div class="col-sm-8 col-md-10">
                                        {!! Form::file('fourth_image',['placeholder'=>'Парола','class'=>'form-control','id'=>'headerIMG']) !!}
                                        @if($errors -> first('password') != '')
                                            <span class="help-block">{!! $errors -> first('password') !!}</span>
                                        @endif
                                    </div>

                                </div>

                                {{--STEP 4 TAB CONTENT--}}
                                <div class="col-sm-8 col-md-10">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#mstp4_area_en"
                                                                                  aria-controls="mstp4_area_en"
                                                                                  role="tab"
                                                                                  data-toggle="tab">EN</a></li>
                                        <li role="presentation"><a href="#mstp4_area_de" aria-controls="mstp4_area_de"
                                                                   role="tab"
                                                                   data-toggle="tab">DE</a></li>
                                        <li role="presentation"><a href="#mstp4_area_fr" aria-controls="mstp4_area_fr"
                                                                   role="tab"
                                                                   data-toggle="tab">FR</a></li>

                                    </ul>
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="mstp4_area_en">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    {!! Form::text( 'fourth_title_en',$methodsData['fourth_title_en'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    {!! Form::textarea( 'fourth_text_en',$methodsData['fourth_text_en'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                                </div>
                                            </div>
                                            {{--SHORT TEXTS--}}
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    {!! Form::text( 'fourth_title_short_en',$methodsData['fourth_title_short_en'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    {!! Form::textarea( 'fourth_text_short_en',$methodsData['fourth_text_short_en'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="mstp4_area_de">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    {!! Form::text( 'fourth_title_de',$methodsData['fourth_title_de'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    {!! Form::textarea( 'fourth_text_de',$methodsData['fourth_text_de'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                                </div>
                                            </div>
                                            {{--SHORT TEXTS--}}
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    {!! Form::text( 'fourth_title_short_de',$methodsData['one_title_short_fr'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    {!! Form::textarea( 'fourth_text_short_de',$methodsData['one_text_short_fr'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="mstp4_area_fr">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    {!! Form::text( 'fourth_title_fr',$methodsData['fourth_title_fr'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    {!! Form::textarea( 'fourth_text_fr',$methodsData['fourth_text_fr'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                                </div>
                                            </div>
                                            {{--SHORT TEXTS--}}
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    {!! Form::text( 'fourth_title_short_fr',$methodsData['one_title_short_fr'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    {!! Form::textarea( 'fourth_text_short_fr',$methodsData['one_text_short_fr'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                {{--END STEP 4 TAB CONTENT--}}

                            </div>
                        </div>
                        {{--</div>--}}
                        <hr>
                        <div class="form-group clearfix">
                            <div class="container-fluid">
                                <div class="checkbox">
                                    <label for="editPassOld" class="control-label">
                                        {!! Form::checkbox('why_blue_is_on',true,@$methodsData['why_blue_is_on']) !!}
                                        On/Off Why Bluebee component
                                    </label>
                                </div>
                            </div>
                        </div>

                        <hr>

                        <div class="form-group clearfix">
                            <div class="container-fluid">
                                <div class="checkbox">
                                    <label for="editPassOld" class="control-label">
                                        {!! Form::checkbox('try_is_on',true,@$methodsData['try_is_on']) !!}
                                        On/Off try component
                                    </label>
                                </div>
                            </div>
                        </div>


                        @include('admin.components.tryComponent',['tryObj'=>\App\TryComponentEntity::getTryObj(@$methodsData['try_id'])])
                        <div class="form-group">

                            <div class="col-sm-6 text-right">
                                {!! Form::submit('Save',['class'=>'btn btn-success']) !!}
                            </div>
                        </div>

                        {!! Form::close() !!}
                    </div>
                </div>


                <br>

                <div class="text-center">


                    {{--<a href="{!! url('admin/complexes/'.$complexId.'/rooms') !!}"--}}
                    {{--class="btn btn-primary">Назад</a>--}}
                </div>
            </div>


        </section>
    </div>


@endsection