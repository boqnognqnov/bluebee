@extends('admin.master')
@section('content')

    <div class="content-wrapper">
        <section class="content">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Page Why Bluebee part: {!! $innerPage['front_title_en'] !!} </h3>

                    <div class="box-tools">
                        <a href="{!! url('/admin/benefits/items/list/'.$innerPage['id']) !!}"
                           class="btn btn-success">{!! $innerPage['front_title_en'] !!} items</a>
                        <a href="{!! url('admin/benefits/page') !!}" class="btn btn-primary">Back
                            to benefit main page</a>
                    </div>
                </div>


                <div class="box-body no-padding">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">

                                @if(Session::has('successMessage'))
                                    <div class="alert alert-success">
                                        {!! Session::get('successMessage') !!}
                                    </div>
                                @endif

                                @if($errors -> any() )
                                    <div class="alert alert-danger" style="width: 100%;">
                                        @foreach ($errors->all() as $error)
                                            {{--<button type="button" class="close" data-dismiss="alert">?</button>--}}
                                            <p>{{ $error }}</p>
                                        @endforeach
                                    </div>
                                @endif
                            </div>
                        </div>


                        {!! Form::open(array('action'=>'BenefitsController@editBenefitInnerPage','method'=>'post','class'=>'form-horizontal','files'=>true)) !!}
                        {!! Form::hidden('id',$innerPage['id']) !!}
                        <div class="form-group clearfix">
                            <div class="container-fluid">
                                <div class="checkbox">
                                    <label class="control-label">
                                        {!! Form::checkbox('head_is_on',true,@$innerPage['head_is_on']) !!}
                                        @if($errors -> first('old_password') != '')
                                            <span class="help-block">{!! $errors -> first('old_password') !!}</span>
                                        @endif
                                        On/Off header
                                    </label>
                                </div>
                            </div>
                        </div>

                        @include('admin.components.headerComponent',['headerObj'=>\App\HeaderComponentEntity::getHeaderObj(@$innerPage['head_id'])])

                        <hr>
                        <div class="form-group clearfix">
                            <div class="container-fluid">
                                <div class="checkbox">
                                    <label class="control-label">
                                        {!! Form::checkbox('favorites_is_on',true,@$innerPage['favorites_is_on']) !!}
                                        @if($errors -> first('favorites_is_on') != '')
                                            <span class="help-block">{!! $errors -> first('favorites_is_on') !!}</span>
                                        @endif
                                        On/Off favorites component
                                    </label>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group clearfix">
                            <div class="container-fluid">
                                <div class="checkbox">
                                    <label for="editPassOld" class="col-sm-4 col-md-2 control-label">
                                        {!! Form::checkbox('opinion_is_on',true,@$innerPage['opinion_is_on']) !!}
                                        @if($errors -> first('opinion_is_on') != '')
                                            <span class="help-block">{!! $errors -> first('opinion_is_on') !!}</span>
                                        @endif
                                        On/Off Opinion
                                    </label>
                                </div>
                            </div>
                        </div>

                        @include('admin.components.opinionComponent',['opinionObj'=>\App\OpinionComponentEntity::getOpinionObj($innerPage['opinion_id'])])
                        <hr>

                        <div class="form-group clearfix">
                            <div class="container-fluid">
                                <div class="checkbox">
                                    <label for="editPassOld" class="col-sm-4 col-md-2 control-label">
                                        {!! Form::checkbox('try_is_on',true,@$innerPage['try_is_on']) !!}
                                        @if($errors -> first('try_is_on') != '')
                                            <span class="help-block">{!! $errors -> first('try_is_on') !!}</span>
                                        @endif
                                        On/Off Try component
                                    </label>
                                </div>
                            </div>
                        </div>

                        @include('admin.components.tryComponent',['tryObj'=>\App\TryComponentEntity::getTryObj(@$innerPage['try_id'])])

                        <hr>
                        <div class="form-group">
                            <label for="headerIMG"
                                   class="col-sm-4 col-md-2 control-label">{!! $innerPage['front_title_en'] !!} <strong>front
                                    image </strong><img
                                        src="{!! asset(\App\BenefitInnerPagesEntity::$path.$innerPage['front_image']) !!}"
                                        class="img-responsive"
                                        style="display: block;"></label>

                            <div class="col-sm-8 col-md-10">
                                {!! Form::file('front_image',['placeholder'=>'Парола','class'=>'form-control','id'=>'headerIMG']) !!}
                                @if($errors -> first('front_image') != '')
                                    <span class="help-block">{!! $errors -> first('front_image') !!}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="headerIMG"
                                   class="col-sm-4 col-md-2 control-label">{!! $innerPage['front_title_en'] !!} <strong>front
                                    negative
                                    image </strong><img
                                        src="{!! asset(\App\BenefitInnerPagesEntity::$pathNegative.$innerPage['front_negative_image']) !!}"
                                        class="img-responsive"
                                        style="display: block;"></label>

                            <div class="col-sm-8 col-md-10">
                                {!! Form::file('front_negative_image',['placeholder'=>'front_negative_image','class'=>'form-control','id'=>'headerIMG']) !!}
                                @if($errors -> first('front_negative_image') != '')
                                    <span class="help-block">{!! $errors -> first('front_negative_image') !!}</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="headerIMG"
                                   class="col-sm-4 col-md-2 control-label">{!! $innerPage['front_title_en'] !!} <strong>front
                                    background image </strong><img
                                        src="{!! asset(\App\BenefitInnerPagesEntity::$pathBackgroundM.$innerPage['front_background_image']) !!}"
                                        class="img-responsive"
                                        style="display: block;"></label>

                            <div class="col-sm-8 col-md-10">
                                {!! Form::file('front_background_image',['placeholder'=>'front_background_image','class'=>'form-control','id'=>'headerIMG']) !!}
                                @if($errors -> first('front_background_image') != '')
                                    <span class="help-block">{!! $errors -> first('front_background_image') !!}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="" class="col-sm-4 col-md-2 control-label">Edit title and text</label>

                            <div class="col-sm-8 col-md-10">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#inBData_area_en"
                                                                              aria-controls="inBData_area_en"
                                                                              role="tab"
                                                                              data-toggle="tab">EN</a></li>
                                    <li role="presentation"><a href="#inBData_area_de" aria-controls="inBData_area_de"
                                                               role="tab"
                                                               data-toggle="tab">DE</a></li>
                                    <li role="presentation"><a href="#inBData_area_fr" aria-controls="inBData_area_fr"
                                                               role="tab"
                                                               data-toggle="tab">FR</a></li>

                                </ul>

                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="inBData_area_en">

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                {!! Form::text( 'front_title_en',$innerPage['front_title_en'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                {!! Form::textarea( 'front_text_en',$innerPage['front_text_en'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                {!! Form::textarea( 'front_text_s_en',$innerPage['front_text_s_en'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="inBData_area_de">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                {!! Form::text( 'front_title_de',$innerPage['front_title_de'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                {!! Form::textarea( 'front_text_de', $innerPage['front_text_de'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                {!! Form::textarea( 'front_text_s_de',$innerPage['front_text_s_de'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="inBData_area_fr">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                {!! Form::text( 'front_title_fr',$innerPage['front_title_fr'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                {!! Form::textarea( 'front_text_fr', $innerPage['front_text_fr'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                {!! Form::textarea( 'front_text_s_fr',$innerPage['front_text_s_de'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>


                        {!! Form::submit('Save',['class'=>'btn btn-success']) !!}

                        {!! Form::close() !!}
                    </div>
                </div>


                <br>

                {{--<div class="text-center">--}}
                {{----}}
                {{--</div>--}}
            </div>
    </div>

    </section>
    </div>


@endsection