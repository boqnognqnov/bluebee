@extends( 'admin.master' )
@section( 'content' )

        <!-- DataTables CSS -->
<link rel="stylesheet" type="text/css"
      href="{!! asset('plugins/DataTables-1.10.9/media/css/jquery.dataTables.css') !!}">

<!-- jQuery -->
<script type="text/javascript" charset="utf8"
        src="{!! asset('plugins/DataTables-1.10.9/media/js/jquery.js') !!}"></script>

<!-- DataTables -->
<script type="text/javascript" charset="utf8"
        src="{!! asset('plugins/DataTables-1.10.9/media/js/jquery.dataTables.js') !!}"></script>

<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-md-12">

                @if(Session::has('successMessage'))
                    <div class="alert alert-success">
                        {!! Session::get('successMessage') !!}
                    </div>
                @endif

                @if($errors -> any() )
                    <div class="alert alert-danger" style="width: 100%;">
                        @foreach ($errors->all() as $error)
                            {{--<button type="button" class="close" data-dismiss="alert">?</button>--}}
                            <p>{{ $error }}</p>
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-warning">
                    <div class="box-header">
                        <h3 class="box-title">List of additional files</h3>

                        <div class="box-tools">
                            <a href="{!! url('/admin/additioanal_files/create') !!}" class="btn btn-primary"
                               style="display: inline-block">Upload new file</a>
                            {{--<a href="{!! url('/admin/news/categoryList') !!}"--}}
                            {{--class="btn btn-bitbucket" style="display: inline-block">Categories</a>--}}
                            {{--<a href="{!! url('admin/news/list/create') !!}"--}}
                            {{--class="btn btn-warning" style="display: inline-block">Add news</a>--}}
                            {{--<a href="{!! url('admin/news/tagList') !!}"--}}
                            {{--class="btn btn-success" style="display: inline-block">Edit tag list</a>--}}
                        </div>
                    </div>
                    <div class="box-body table-responsive no-padding">
                        <div id="example_wrapper" class="dataTables_wrapper">
                            <table id="example" class="display pageResize" cellspacing="0" width="100%">

                                <thead>
                                <th>ID</th>
                                <th>Info</th>
                                <td>Url</td>
                                <th>Created date</th>
                                <th>Delete</th>

                                </thead>
                                <tbody>
                                @foreach($files as $oneFile)
                                    <tr>
                                        <td>{!! $oneFile['id'] !!}</td>

                                        <td>{!! $oneFile['info'] !!}</td>
                                        <td>{!! Request::root().'/downloads/additional/download/file/'.$oneFile['id']!!}</td>
                                        <td>{!! $oneFile['created_at'] !!}</td>
                                        <td>
                                            <button class="btn btn-danger delFile"
                                                    data-file-id="{!! $oneFile['id'] !!}">Delete
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>


<div style="display: none">
    {!! Form::open(array('action'=>'AdditionalFilesController@deleteFile','method'=>'post','id'=>'formFileDel')) !!}
    {!! Form::hidden('fileId') !!}
    {!! Form::close() !!}
</div>


<script>
    $('#example').DataTable(
            {
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                "bFilter": true,
                "bLengthChange": true
            }
    );
    $('#example').on('draw.dt', function () {
        defineDeleteFunction();
    });

    function defineDeleteFunction() {
        $('.delFile').on('click', function (event) {

            var id = $(this).attr('data-file-id');

            $('#formFileDel input[name="fileId"]').val(id);
            var r = confirm("Do you really want to delete this file path ?");
            if (r == true) {
                $('#formFileDel').submit();
            }
        });
    }

    $(document).ready(function () {

        defineDeleteFunction();
    });


</script>


@endsection