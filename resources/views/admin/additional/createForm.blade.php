@extends( 'admin.master' )
@section( 'content' )



    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-no-padding">
                        <div class="box-header">
                            <h3 class="box-title">Create an additional file</h3>

                            <div class="box-tools">
                                <a href="{!! url('admin/additioanal_files/list') !!}" class="btn btn-primary">Back to
                                    additional file list</a>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">

                                    @if(Session::has('successMessage'))
                                        <div class="alert alert-success">
                                            {!! Session::get('successMessage') !!}
                                        </div>
                                    @endif

                                    @if($errors -> any() )
                                        <div class="alert alert-danger" style="width: 100%;">
                                            @foreach ($errors->all() as $error)
                                                {{--<button type="button" class="close" data-dismiss="alert">?</button>--}}
                                                <p>{{ $error }}</p>
                                            @endforeach
                                        </div>
                                    @endif
                                </div>
                            </div>
                            {{--DATA--}}
                            {!! Form::open(array('action'=>'AdditionalFilesController@storeFile','method'=>'post','files'=>true)) !!}
                            {!! Form::hidden('id',0) !!}


                            <div class="form-group clearfix">
                                {{--<label for="headerIMG" class="col-sm-4 col-md-2 control-label">Background image <img--}}
                                {{--src="{!! asset(\App\HeaderComponentEntity::$path.$headerObj['image']) !!}"--}}
                                {{--class="img-responsive"--}}
                                {{--style="display: block;"></label>--}}

                                {{--<div class="col-sm-8 col-md-10">--}}
                                <div class="col-sm-12 col-md-12">
                                    {!! Form::file('file_name',['placeholder'=>'file_name','class'=>'form-control']) !!}
                                    @if($errors -> first('file_name') != '')
                                        <span class="help-block">{!! $errors -> first('file_name') !!}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-4 col-md-2 control-label">File info</label>
                                <div class="col-sm-8 col-md-10">
                                    {!! Form::text( 'info', null, array( 'class' => 'form-control' ) ) !!}
                                </div>
                            </div>

                            <div class="col-sm-6 text-right">
                                {!! Form::submit('Save',['class'=>'btn btn-success']) !!}
                            </div>
                            {!! Form::close() !!}

                            {{--END OF DATA--}}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection