@extends( 'admin.master' )
@section( 'content' )

        <!-- DataTables CSS -->
<link rel="stylesheet" type="text/css"
      href="{!! asset('plugins/DataTables-1.10.9/media/css/jquery.dataTables.css') !!}">

<!-- jQuery -->
<script type="text/javascript" charset="utf8"
        src="{!! asset('plugins/DataTables-1.10.9/media/js/jquery.js') !!}"></script>

<!-- DataTables -->
<script type="text/javascript" charset="utf8"
        src="{!! asset('plugins/DataTables-1.10.9/media/js/jquery.dataTables.js') !!}"></script>

<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-warning">
                    <div class="box-header">
                        <h3 class="box-title">List of pages where you can append EVENT: <strong>{!! $name !!}</strong>
                        </h3>

                        <div class="box-tools">
                            <a href="{!! url('/admin/events/list') !!}" class="btn btn-bitbucket"
                               style="display: inline-block">Back to event list</a>
                        </div>
                    </div>
                    <div class="container">
                        @include('admin.components.listFavoriteComponent')
                        @if(!empty(\Session::get('favSelectedModelTo')))
                            @include('admin.components.addFavoriteComponent',['atachModelName'=>$atachModelName,'atachModelId'=>$atachModelId])
                            @include('admin.components.viewFavoriteComponent',['favId'=>\Session::get('favId')])
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection