@extends( 'admin.master' )
@section( 'content' )

    <div class="content-wrapper">
        <section class="content">
            <div class="box box-no-padding">
                <div class="box-header">
                    <h3 class="box-title">Privacy page</h3>

                    <div class="box-tools">
                        {{--<a href="{!! url('admin/news/list') !!}" class="btn btn-primary">List of news</a>--}}
                        {{--<a href="{!! url('admin/news/showChildPage') !!}" class="btn btn-bitbucket">Edit child news page</a>--}}
                    </div>
                </div>
                <div class="box-body">
                    <div class="container-fluid">

                        <div class="row">
                            <div class="col-md-12">

                                @if(Session::has('successMessage'))
                                    <div class="alert alert-success">
                                        {!! Session::get('successMessage') !!}
                                    </div>
                                @endif

                                @if($errors -> any() )
                                    <div class="alert alert-danger" style="width: 100%;">
                                        @foreach ($errors->all() as $error)
                                            {{--<button type="button" class="close" data-dismiss="alert">?</button>--}}
                                            <p>{{ $error }}</p>
                                        @endforeach
                                    </div>
                                @endif
                            </div>
                        </div>

                        {{--DATA--}}
                        <div class="row">
                            <div class="container-fluid">
                                {!! Form::open(array('action'=>'PrivacyDisclaimerController@updatePrivacy','method'=>'post','files'=>true)) !!}

                                {{--INPUTS--}}


                                <div class="panel panel-warning">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Page texts</h3>
                                    </div>
                                    <div class="panel-body">
                                        <ul class="nav nav-tabs nav-justified" role="tablist">
                                            <li role="presentation" class="active">
                                                <a href="#about_area_en" aria-controls="about_area_en" role="tab"
                                                   data-toggle="tab"><p class="lead">EN</p></a>
                                            </li>
                                            <li role="presentation">
                                                <a href="#about_area_de" aria-controls="about_area_de" role="tab"
                                                   data-toggle="tab"><p class="lead">DE</p></a>
                                            </li>
                                            <li role="presentation">
                                                <a href="#about_area_fr" aria-controls="about_area_fr" role="tab"
                                                   data-toggle="tab"><p class="lead">FR</p></a>
                                            </li>
                                        </ul>

                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane active" id="about_area_en">
                                                <div class="form-group clearfix">
                                                    <label for="intro_title_en" class="control-label col-md-2">Intro
                                                        section</label>

                                                    <div class="col-md-10">
                                                        <p>{!! Form::textarea( 'text_en',$pageData['text_en'], array( 'class' => 'form-control html5area', 'rows' => '6', 'placeholder' => 'Body' ) ) !!}</p>
                                                    </div>
                                                </div>


                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="about_area_de">
                                                <div class="form-group clearfix">
                                                    <label for="intro_title_de" class="control-label col-md-2">Intro
                                                        sektion</label>

                                                    <div class="col-md-10">

                                                        <p>{!! Form::textarea( 'text_de',$pageData['text_de'], array( 'class' => 'form-control html5area', 'rows' => '6', 'placeholder' => 'Body' ) ) !!}</p>
                                                    </div>
                                                </div>

                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="about_area_fr">
                                                <div class="form-group clearfix">
                                                    <label for="intro_title_fr" class="control-label col-md-2">Intro
                                                        sektion</label>

                                                    <div class="col-md-10">
                                                        <p>{!! Form::textarea( 'text_fr',$pageData['text_fr'], array( 'class' => 'form-control html5area', 'rows' => '6', 'placeholder' => 'Body' ) ) !!}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                {{--INPUTS--}}


                                <div class="col-sm-6 text-right">
                                    {!! Form::submit('Save',['class'=>'btn btn-success']) !!}
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                        {{--END OF DATA--}}
                    </div>
                </div>
            </div>
        </section>
    </div>



@endsection