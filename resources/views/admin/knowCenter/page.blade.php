@extends( 'admin.master' )
@section( 'content' )

    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-no-padding">
                        <div class="box-header">
                            <h3 class="box-title">Knowledge center</h3>

                            <div class="box-tools">
                                <a href="{!! url('admin/additioanal_files/list') !!}" class="btn btn-primary">List of additional
                                    files</a>
                                {{--<a href="{!! url('admin/events/showChildPage') !!}" class="btn btn-bitbucket">Edit child--}}
                                {{--event page</a>--}}
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">

                                    @if(Session::has('successMessage'))
                                        <div class="alert alert-success">
                                            {!! Session::get('successMessage') !!}
                                        </div>
                                    @endif

                                    @if($errors -> any() )
                                        <div class="alert alert-danger" style="width: 100%;">
                                            @foreach ($errors->all() as $error)
                                                {{--<button type="button" class="close" data-dismiss="alert">?</button>--}}
                                                <p>{{ $error }}</p>
                                            @endforeach
                                        </div>
                                    @endif
                                </div>
                            </div>
                            {{--DATA--}}
                            {!! Form::open(array('action'=>'KnowCenterController@update','method'=>'post','files'=>true)) !!}
                            {{--{!! Form::hidden('id',$pageData['id']) !!}--}}

                            <div class="form-group clearfix">
                                <div class="container-fluid">
                                    <div class="checkbox">
                                        <label for="editPassOld" class="control-label">
                                            {!! Form::checkbox('head_is_on',true,@$pageData['head_is_on']) !!}
                                            On/Off header
                                        </label>
                                    </div>
                                </div>
                            </div>

                            @include('admin.components.headerComponent',['headerObj'=>\App\HeaderComponentEntity::getHeaderObj(@$pageData['head_id'])])

                            <hr>

                            <div class="form-group clearfix">
                                <div class="container-fluid">
                                    <div class="checkbox">
                                        <label for="editPassOld" class="control-label">
                                            {!! Form::checkbox('sign_up_is_on',true,@$pageData['sign_up_is_on']) !!}
                                            On/Off Sign up
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <hr>
                            @include('admin.components.signUpComponent',['signObj'=>\App\SignUpComponentEntity::getSignUpObj($pageData['sign_up_id'])])
                            <div class="col-sm-6 text-right">
                                {!! Form::submit('Save',['class'=>'btn btn-success']) !!}
                            </div>
                            {!! Form::close() !!}

                            {{--END OF DATA--}}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection