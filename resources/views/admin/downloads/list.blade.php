@extends( 'admin.master' )
@section( 'content' )

        <!-- DataTables CSS -->
<link rel="stylesheet" type="text/css"
      href="{!! asset('plugins/DataTables-1.10.9/media/css/jquery.dataTables.css') !!}">

<!-- jQuery -->
<script type="text/javascript" charset="utf8"
        src="{!! asset('plugins/DataTables-1.10.9/media/js/jquery.js') !!}"></script>

<!-- DataTables -->
<script type="text/javascript" charset="utf8"
        src="{!! asset('plugins/DataTables-1.10.9/media/js/jquery.dataTables.js') !!}"></script>

<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="row">
                <div class="col-md-12">
                    @if(Session::has('successMessage'))
                        <div class="alert alert-success">
                            {!! Session::get('successMessage') !!}
                        </div>
                    @endif

                    @if($errors -> any() )
                        <div class="alert alert-danger" style="width: 100%;">
                            @foreach ($errors->all() as $error)
                                {{--<button type="button" class="close" data-dismiss="alert">?</button>--}}
                                <p>{{ $error }}</p>
                            @endforeach
                        </div>
                    @endif
                </div>
            </div>
            <div class="col-xs-12">
                <div class="box box-warning">
                    <div class="box-header">
                        <h3 class="box-title">File List</h3>

                        <div class="box-tools">
                            <a href="{!! url('/admin/downloads/page/show') !!}" class="btn btn-primary"
                               style="display: inline-block">Back to Download page</a>
                            <a href="{!! url('admin/downloads/categoryList') !!}"
                               class="btn btn-bitbucket" style="display: inline-block">Categories</a>
                            <a href="{!! url('admin/downloads/list/create') !!}"
                               class="btn btn-warning" style="display: inline-block">Add File record</a>
                        </div>
                    </div>
                    <div class="box-body table-responsive no-padding">
                        <div id="example_wrapper" class="dataTables_wrapper">
                            <table id="example" class="display pageResize" cellspacing="0" width="100%">

                                <thead>
                                <th>ID</th>
                                <th>Image</th>
                                <th>Title</th>
                                <th>Category</th>
                                <th>Add to favorites</th>
                                <th>File</th>
                                <th></th>
                                <th></th>
                                </thead>
                                <tbody>
                                @foreach($fileList as $oneFile)
                                    <tr>
                                        <td>{!! $oneFile['id'] !!}</td>
                                        <td>
                                            <img src="{!! asset(\App\DownloadsItemsEntity::$pathFrontImage.$oneFile['image']) !!}"
                                                 width="100px">
                                        </td>
                                        <td>{!! $oneFile['title_en'] !!}</td>
                                        <td>{!! \App\DownloadsItemsEntity::getCategory($oneFile['category_id']) !!}</td>
                                        <td>
                                            <a href="{!! url('admin/favorites/showTool/downloads/'.$oneFile['id']) !!}"
                                               class="btn btn-success"
                                               data-news-id="">Add
                                            </a>
                                        </td>
                                        <td>
                                            <a href="{!! asset('admin/downloads/download/file/' . $oneFile['id']) !!}">Download
                                                file</a>
                                        </td>
                                        <td>
                                            <a href="{!! url('/admin/downloads/list/show/'.$oneFile['id']) !!}"
                                               class="btn btn-warning">Edit</a>
                                        </td>
                                        <td>
                                            <button class="btn btn-danger delDownloadFile"
                                                    data-download-id="{!! $oneFile['id'] !!}">Delete
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>


<div style="display: none">
    {!! Form::open(array('action'=>'FilesController@destroyFileRecord','method'=>'post','id'=>'formDownloadDel')) !!}
    {!! Form::hidden('downloadRecordId') !!}
    {!! Form::close() !!}
</div>


<script>
    $('#example').DataTable(
            {
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                "bFilter": true,
                "bLengthChange": true
            }
    );

    $('#example').on('draw.dt', function () {
        defineDeleteFunction();
    });


    $(document).ready(function () {
        defineDeleteFunction();

    });


    function defineDeleteFunction() {
        $('.delDownloadFile').on('click', function (event) {

            var id = $(this).attr('data-download-id');

            $('#formDownloadDel input[name="downloadRecordId"]').val(id);
            var r = confirm("Do you really want to delete this record ?");
            if (r == true) {
                $('#formDownloadDel').submit();
            }
        });
    }
</script>


@endsection