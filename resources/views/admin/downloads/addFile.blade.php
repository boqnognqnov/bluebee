@extends( 'admin.master' )
@section( 'content' )

    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-no-padding">
                        <div class="box-header">
                            <h3 class="box-title">Add new file</h3>

                            <div class="box-tools">
                                <a href="{!! url('admin/downloads/list') !!}" class="btn btn-primary">File List</a>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    @if(Session::has('successMessage'))
                                        <div class="alert alert-success">
                                            {!! Session::get('successMessage') !!}
                                        </div>
                                    @endif

                                    @if($errors -> any() )
                                        <div class="alert alert-danger" style="width: 100%;">
                                            @foreach ($errors->all() as $error)
                                                {{--<button type="button" class="close" data-dismiss="alert">?</button>--}}
                                                <p>{{ $error }}</p>
                                            @endforeach
                                        </div>
                                    @endif
                                </div>
                            </div>
                            {{--DATA--}}
                            {!! Form::open(array('action'=>'FilesController@addOrUpdateFileRecord','method'=>'post','files'=>true)) !!}
                            {!! Form::hidden('id',0) !!}

                            <div class="form-group">
                                <div class="col-sm-4 col-md-2">
                                    <label for="" class="control-label">Select category</label>
                                </div>
                                {!! Form::select('category_id',$catList,null,['class'=>'form-control col-md-8']) !!}

                            </div>

                            <div class="form-group">
                                <label for="" class="col-sm-4 col-md-2 control-label">Front image</label>
                            </div>
                            <div class="form-group clearfix">
                                {{--<label for="headerIMG" class="col-sm-4 col-md-2 control-label">Background image <img--}}
                                {{--src="{!! asset(\App\HeaderComponentEntity::$path.$headerObj['image']) !!}"--}}
                                {{--class="img-responsive"--}}
                                {{--style="display: block;"></label>--}}

                                <div class="col-sm-8 col-md-10">
                                    {!! Form::file('image',['placeholder'=>'Парола','class'=>'form-control','id'=>'headerIMG']) !!}
                                    @if($errors -> first('image') != '')
                                        <span class="help-block">{!! $errors -> first('image') !!}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="" class="col-sm-4 col-md-2 control-label">Upload file</label>
                            </div>
                            <div class="form-group clearfix">
                                <div class="col-sm-8 col-md-10">
                                    {!! Form::file('file',['placeholder'=>'file','class'=>'form-control','id'=>'headerIMG']) !!}
                                    @if($errors -> first('file') != '')
                                        <span class="help-block">{!! $errors -> first('file') !!}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="" class="col-sm-4 col-md-2 control-label">News title and text</label>

                                <div class="col-sm-8 col-md-10">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#cHead_area_en"
                                                                                  aria-controls="cHead_area_en"
                                                                                  role="tab"
                                                                                  data-toggle="tab">EN</a></li>
                                        <li role="presentation"><a href="#cHead_area_de" aria-controls="cHead_area_de"
                                                                   role="tab"
                                                                   data-toggle="tab">DE</a></li>
                                        <li role="presentation"><a href="#cHead_area_fr" aria-controls="cHead_area_fr"
                                                                   role="tab"
                                                                   data-toggle="tab">FR</a></li>

                                    </ul>

                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="cHead_area_en">
                                            {!! Form::text( 'title_en',null, array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                            {!! Form::textarea( 'text_en',null, array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="cHead_area_de">
                                            {!! Form::text( 'title_de',null, array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                            {!! Form::textarea( 'text_de', null, array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="cHead_area_fr">
                                            {!! Form::text( 'title_fr',null, array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                            {!! Form::textarea( 'text_fr', null, array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 text-right">
                                {!! Form::submit('Save',['class'=>'btn btn-success']) !!}
                            </div>
                            {!! Form::close() !!}

                            {{--END OF DATA--}}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection