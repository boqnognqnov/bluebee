@extends( 'admin.master' )
@section( 'content' )



    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-warning">
                        <div class="box-header">
                            <h3 class="box-title">Categories of downloads</h3>

                            <div class="box-tools">
                                <a href="{!! url('admin/downloads/list') !!}" class="btn btn-primary"
                                   style="display: inline-block">Back to download list</a>
                            </div>
                        </div>
                        {{--<div class="box-body table-responsive no-padding">--}}
                        @foreach($categories as $oneCat)
                            <a href="{!! url('admin/downloads/categoryList/'.$oneCat['id']) !!}"
                               class="btn btn-primary">{!! $oneCat['title_en'] !!}</a>
                            {{--<button class="glyphicon glyphicon-trash delNewsCat"--}}
                            {{--data-newsCat-id="{!! $oneCat['id'] !!}"></button>--}}

                            {{--<button class="btn btn-danger delNewsCat" data-newsCat-id="{!! $oneCat['id'] !!}"><span--}}
                            {{--class="glyphicon glyphicon-trash"></span> delete--}}
                            {{--</button>--}}
                            <button class="btn btn-danger delCat" data-download-id="{!! $oneCat['id'] !!}"><span
                                        class="glyphicon glyphicon-trash"></span>
                            </button>
                        @endforeach
                        <hr>


                        {!! Form::open(array('action'=>'FilesController@addOrEditCategories','method'=>'post')) !!}
                        @if(!isset($selectedCat['id']))
                            {!! Form::hidden('id',0) !!}
                        @else
                            {!! Form::hidden('id',$selectedCat['id']) !!}
                        @endif


                        <div class="form-group">
                            {!! Form::label('Title En','Title En',['class'=>'control-label col-sm-2']) !!}
                            <div class="col-sm-10">
                                {!! Form::text('title_en',@$selectedCat['title_en'],['class'=>'form-control','id'=>'title_en']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('Title De','Title De',['class'=>'control-label col-sm-2']) !!}
                            <div class="col-sm-10">
                                {!! Form::text('title_de',@$selectedCat['title_de'],['class'=>'form-control','id'=>'title_de']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('Title Fr','Title Fr',['class'=>'control-label col-sm-2']) !!}
                            <div class="col-sm-10">
                                {!! Form::text('title_fr',@$selectedCat['title_fr'],['class'=>'form-control','id'=>'title_fr']) !!}
                            </div>
                        </div>


                        <div class="col-sm-6 text-right">
                            {!! Form::submit('Save',['class'=>'btn btn-success']) !!}
                            <a href="{!! url('/admin/downloads/categoryList') !!}" class="btn btn-primary">Clear
                                form</a>
                        </div>
                        {!! Form::close() !!}

                    </div>
                </div>
            </div>

        </section>
    </div>

    <div style="display: none">
        {!! Form::open(array('action'=>'FilesController@destroyCategory','method'=>'post','id'=>'formCatDel')) !!}
        {!! Form::hidden('catId') !!}
        {!! Form::close() !!}
    </div>


    <script>

        $(document).ready(function () {

            $('.delCat').on('click', function (event) {

                var id = $(this).attr('data-download-id');

                $('#formCatDel input[name="catId"]').val(id);
                var r = confirm("Do you really want to delete this category with all files ?");
                if (r == true) {
                    $('#formCatDel').submit();
                }
            });
        });
    </script>



@endsection