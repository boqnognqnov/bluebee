@extends( 'admin.master' )
@section( 'content' )

        <!-- DataTables CSS -->
<link rel="stylesheet" type="text/css"
      href="{!! asset('plugins/DataTables-1.10.9/media/css/jquery.dataTables.css') !!}">

<!-- jQuery -->
<script type="text/javascript" charset="utf8"
        src="{!! asset('plugins/DataTables-1.10.9/media/js/jquery.js') !!}"></script>

<!-- DataTables -->
<script type="text/javascript" charset="utf8"
        src="{!! asset('plugins/DataTables-1.10.9/media/js/jquery.dataTables.js') !!}"></script>

<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-md-12">

                @if(Session::has('successMessage'))
                    <div class="alert alert-success">
                        {!! Session::get('successMessage') !!}
                    </div>
                @endif

                @if($errors -> any() )
                    <div class="alert alert-danger" style="width: 100%;">
                        @foreach ($errors->all() as $error)
                            {{--<button type="button" class="close" data-dismiss="alert">?</button>--}}
                            <p>{{ $error }}</p>
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-warning">
                    <div class="box-header">
                        <h3 class="box-title">List of features items</h3>

                        <div class="box-tools">
                            <a href="{!! url('admin/features/page/show') !!}" class="btn btn-primary"
                               style="display: inline-block">Back to features page</a>
                            <a href="{!! url('admin/features/items/create') !!}"
                               class="btn btn-warning" style="display: inline-block">Add item</a>
                        </div>
                    </div>
                    <div class="box-body table-responsive no-padding">
                        <div id="example_wrapper" class="dataTables_wrapper">
                            <table id="example" class="display pageResize" cellspacing="0" width="100%">

                                <thead>
                                <th>ID</th>
                                <th>Image</th>
                                <th>Title</th>
                                <th>Add to Favorites</th>
                                <th></th>
                                <th></th>
                                </thead>
                                <tbody>
                                @foreach($items as $item)
                                    <tr>
                                        <td>{!! $item['id'] !!}</td>
                                        <td><img src="{!! asset(\App\FeaturesItemEntity::$path.$item['image']) !!}"
                                                 width="100px">
                                        </td>
                                        <td>{!! $item['title_en'] !!}</td>
                                        <td>
                                            <a href="{!! url('admin/favorites/showTool/features_elements/'.$item['id']) !!}"
                                               class="btn btn-success"
                                               data-news-id="">Add
                                            </a>
                                        </td>
                                        <td>
                                            <a href="{!! url('admin/features/items/show/'.$item['id']) !!}"
                                               class="btn btn-warning">Edit</a>
                                        </td>
                                        <td>
                                            <button class="btn btn-danger delFItem"
                                                    data-fItem-id="{!! $item['id'] !!}">Delete
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>


<script>
    $('#example').DataTable(
            {
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                "bFilter": true,
                "bLengthChange": true
            }
    );
</script>

<div style="display: none">
    {!! Form::open(array('action'=>'FeaturesController@destroyItemById','method'=>'post','id'=>'formFItemDel')) !!}
    {!! Form::hidden('itemId') !!}
    {!! Form::close() !!}
</div>


<script>

    $(document).ready(function () {

        $('.delFItem').on('click', function (event) {

            var id = $(this).attr('data-fItem-id');

            $('#formFItemDel input[name="itemId"]').val(id);
            var r = confirm("Do you really want to delete this item ?");
            if (r == true) {
                $('#formFItemDel').submit();
            }
        });
    });
</script>


@endsection