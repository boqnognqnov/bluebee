@extends('admin.master')
@section('content')

    <div class="content-wrapper">
        <section class="content">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">{!! $bIPageData['front_title_en'] !!} Items </h3>

                    <div class="box-tools">
                        <a href="{!! url('admin/benefits/items/create/'.$bIPageData['id']) !!}" class="btn btn-success">Create
                            new item </a>
                        <a href="{!! url('admin/benefits/page_inner/'.$bIPageData['id']) !!}" class="btn btn-primary">Back
                            to {!! $bIPageData['front_title_en'] !!} page</a>
                    </div>
                </div>
                {{--<div class="box-body no-padding">--}}

                {{--</div>--}}

                <div class="box-body no-padding">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">

                                @if(Session::has('successMessage'))
                                    <div class="alert alert-success">
                                        {!! Session::get('successMessage') !!}
                                    </div>
                                @endif

                                @if($errors -> any() )
                                    <div class="alert alert-danger" style="width: 100%;">
                                        @foreach ($errors->all() as $error)
                                            {{--<button type="button" class="close" data-dismiss="alert">?</button>--}}
                                            <p>{{ $error }}</p>
                                        @endforeach
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="box-body table-responsive no-padding">
                            <table class="display pageResize" width="100%">
                                <thead>
                                <th>ID</th>
                                <th>Image</th>
                                <th>Title En</th>
                                <th>Created At</th>
                                <th colspan="2"></th>
                                </thead>
                                <tbody>
                                @foreach($items as $item)
                                    <tr>
                                        <td>{!! $item['id'] !!}</td>
                                        <td><img src="{!! asset(\App\BenefitElementsEntity::$path.$item['image']) !!}"
                                                 width="100px"></td>
                                        <td>{!! $item['title_en'] !!}</td>
                                        <td>{!! $item['created_at'] !!}</td>
                                        <td>
                                            <a href="{!! url('admin/benefits/items/show/'.$item['id']) !!}"
                                               class="btn btn-primary">Change</a>
                                        </td>
                                        <td>
                                            <button class="btn btn-danger itemDel" data-item-id="{!! $item['id'] !!}">
                                                Delete
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>

                </div>


                <br>

                {{--<div class="text-center">--}}
                {{----}}
                {{--</div>--}}
            </div>


        </section>
    </div>

    <div style="display: none">
        {!! Form::open(array('action' => 'BenefitsController@destroyBenefitItemById','id'=>'form-del-benefitItem')) !!}
        {!! Form::hidden('item_id') !!}

        {!! Form::close() !!}
    </div>

    <script>

        $(document).ready(function () {

            $('.itemDel').on('click', function (event) {

                var id = $(this).attr('data-item-id');

                $('#form-del-benefitItem input[name="item_id"]').val(id);
                var r = confirm("Do you really want to delete the record ?");
                if (r == true) {
                    $('#form-del-benefitItem').submit();
                }
            });
        });
    </script>


@endsection