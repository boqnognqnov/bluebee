@extends('admin.master')
@section('content')

    <div class="content-wrapper">
        <section class="content">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Create new item in
                        <strong>{!! $benefitInnerPage['front_title_en'] !!}</strong> page </h3>

                    <div class="box-tools">
                        <a href="{!! url('admin/benefits/items/list/'.$benefitInnerPage['id']) !!}"
                           class="btn btn-primary">Back to {!! $benefitInnerPage['front_title_en'] !!} item list</a>
                    </div>
                </div>


                <div class="box-body no-padding">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">

                                @if(Session::has('successMessage'))
                                    <div class="alert alert-success">
                                        {!! Session::get('successMessage') !!}
                                    </div>
                                @endif

                                @if($errors -> any() )
                                    <div class="alert alert-danger" style="width: 100%;">
                                        @foreach ($errors->all() as $error)
                                            {{--<button type="button" class="close" data-dismiss="alert">?</button>--}}
                                            <p>{{ $error }}</p>
                                        @endforeach
                                    </div>
                                @endif
                            </div>
                        </div>


                        {!! Form::open(array('action'=>'BenefitsController@updateOrStoreNewBenefitItems','method'=>'post','class'=>'form-horizontal','files'=>true)) !!}
                        {!! Form::hidden('innerPageId',$benefitInnerPage['id']) !!}
                        {!! Form::hidden('itemId',0) !!}
                        <div class="form-group">
                            <label for="" class="col-sm-4 col-md-2 control-label">Item image</label>

                            <div class="col-sm-8 col-md-10">
                                {!! Form::file('image',['placeholder'=>'Парола','class'=>'form-control','id'=>'headerIMG']) !!}
                                @if($errors -> first('image') != '')
                                    <span class="help-block">{!! $errors -> first('image') !!}</span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="" class="col-sm-4 col-md-2 control-label">Edit title and text</label>

                            <div class="col-sm-8 col-md-10">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#iBItem_area_en"
                                                                              aria-controls="iBItem_area_en"
                                                                              role="tab"
                                                                              data-toggle="tab">EN</a></li>
                                    <li role="presentation"><a href="#iBItem_area_de" aria-controls="iBItem_area_de"
                                                               role="tab"
                                                               data-toggle="tab">DE</a></li>
                                    <li role="presentation"><a href="#iBItem_area_fr" aria-controls="iBItem_area_fr"
                                                               role="tab"
                                                               data-toggle="tab">FR</a></li>

                                </ul>

                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="iBItem_area_en">

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                {!! Form::text( 'title_en',null, array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                {!! Form::textarea( 'text_en',null, array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="iBItem_area_de">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                {!! Form::text( 'title_de',null, array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                {!! Form::textarea( 'text_de', null, array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="iBItem_area_fr">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                {!! Form::text( 'title_fr',null, array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                {!! Form::textarea( 'text_fr', null, array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>


                        {!! Form::submit('Save',['class'=>'btn btn-success']) !!}

                        {!! Form::close() !!}

                    </div>
                </div>


                <br>

                <div class="text-center">
                    {{--<a href="{!! url('admin/complexes/'.$complexId.'/rooms') !!}"--}}
                    {{--class="btn btn-primary">Назад</a>--}}
                </div>
            </div>
    </div>

    </section>
    </div>


@endsection