@extends( 'admin.master' )
@section( 'content' )



    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-md-12">

                    @if(Session::has('successMessage'))
                        <div class="alert alert-success">
                            {!! Session::get('successMessage') !!}
                        </div>
                    @endif

                    @if($errors -> any() )
                        <div class="alert alert-danger" style="width: 100%;">
                            @foreach ($errors->all() as $error)
                                {{--<button type="button" class="close" data-dismiss="alert">?</button>--}}
                                <p>{{ $error }}</p>
                            @endforeach
                        </div>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-warning">
                        <div class="box-header">
                            <h3 class="box-title">Tags of news</h3>

                            <div class="box-tools">
                                <a href="{!! url('admin/news/list') !!}" class="btn btn-primary"
                                   style="display: inline-block">Back to list of news</a>
                            </div>
                        </div>
                        {{--<div class="box-body table-responsive no-padding">--}}
                        @foreach($tagList as $oneTag)
                            <a href="{!! url('admin/news/tagList/'.$oneTag['id']) !!}"
                               class="btn btn-primary">{!! $oneTag['title_en'] !!}</a>
                            {{--<button class="glyphicon glyphicon-trash delNewsTag"--}}
                            {{--data-newsTag-id="{!! $oneTag['id'] !!}"></button>--}}

                            <button class="btn btn-danger delNewsTag" data-newsTag-id="{!! $oneTag['id'] !!}"><span
                                        class="glyphicon glyphicon-trash"></span>
                            </button>
                        @endforeach
                        <hr>


                        {!! Form::open(array('action'=>'NewsController@addOrEditTag','method'=>'post')) !!}
                        @if(!isset($selectedTag['id']))
                            {!! Form::hidden('id',0) !!}
                        @else
                            {!! Form::hidden('id',$selectedTag['id']) !!}
                        @endif

                        <div class="form-group">
                            {!! Form::label('Title En','Title En',['class'=>'control-label col-sm-2']) !!}
                            <div class="col-sm-10">
                                {!! Form::text('title_en',@$selectedTag['title_en'],['class'=>'form-control','id'=>'title_en']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('Title De','Title De',['class'=>'control-label col-sm-2']) !!}
                            <div class="col-sm-10">
                                {!! Form::text('title_de',@$selectedTag['title_de'],['class'=>'form-control','id'=>'title_de']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('Title Fr','Title Fr',['class'=>'control-label col-sm-2']) !!}
                            <div class="col-sm-10">
                                {!! Form::text('title_fr',@$selectedTag['title_fr'],['class'=>'form-control','id'=>'title_fr']) !!}
                            </div>
                        </div>


                        <div class="col-sm-6 text-right">
                            {!! Form::submit('Save',['class'=>'btn btn-success']) !!}
                            <a href="{!! url('/admin/news/tagList') !!}" class="btn btn-primary">Clear form</a>
                        </div>
                        {!! Form::close() !!}

                    </div>
                </div>
            </div>

        </section>
    </div>

    <div style="display: none">
        {!! Form::open(array('action'=>'NewsController@destroyTag','method'=>'post','id'=>'formTagDel')) !!}
        {!! Form::hidden('tagId') !!}
        {!! Form::close() !!}
    </div>


    <script>

        $(document).ready(function () {

            $('.delNewsTag').on('click', function (event) {

                var id = $(this).attr('data-newsTag-id');

                $('#formTagDel input[name="tagId"]').val(id);
                var r = confirm("Do you really want to delete this tag ?");
                if (r == true) {
                    $('#formTagDel').submit();
                }
            });
        });
    </script>



@endsection