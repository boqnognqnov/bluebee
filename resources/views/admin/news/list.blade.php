@extends( 'admin.master' )
@section( 'content' )

        <!-- DataTables CSS -->
<link rel="stylesheet" type="text/css"
      href="{!! asset('plugins/DataTables-1.10.9/media/css/jquery.dataTables.css') !!}">

<!-- jQuery -->
<script type="text/javascript" charset="utf8"
        src="{!! asset('plugins/DataTables-1.10.9/media/js/jquery.js') !!}"></script>

<!-- DataTables -->
<script type="text/javascript" charset="utf8"
        src="{!! asset('plugins/DataTables-1.10.9/media/js/jquery.dataTables.js') !!}"></script>

<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-md-12">

                @if(Session::has('successMessage'))
                    <div class="alert alert-success">
                        {!! Session::get('successMessage') !!}
                    </div>
                @endif

                @if($errors -> any() )
                    <div class="alert alert-danger" style="width: 100%;">
                        @foreach ($errors->all() as $error)
                            {{--<button type="button" class="close" data-dismiss="alert">?</button>--}}
                            <p>{{ $error }}</p>
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-warning">
                    <div class="box-header">
                        <h3 class="box-title">List of news</h3>

                        <div class="box-tools">
                            <a href="{!! url('admin/news/editMainPage') !!}" class="btn btn-primary"
                               style="display: inline-block">Back to news page</a>
                            <a href="{!! url('/admin/news/categoryList') !!}"
                               class="btn btn-bitbucket" style="display: inline-block">Categories</a>
                            <a href="{!! url('admin/news/list/create') !!}"
                               class="btn btn-warning" style="display: inline-block">Add news</a>
                            <a href="{!! url('admin/news/tagList') !!}"
                               class="btn btn-success" style="display: inline-block">Edit tag list</a>
                        </div>
                    </div>
                    <div class="box-body table-responsive no-padding">
                        <div id="example_wrapper" class="dataTables_wrapper">
                            <table id="example" class="display pageResize" cellspacing="0" width="100%">

                                <thead>
                                <th>ID</th>
                                <th>Image</th>
                                <th>Title</th>
                                <th>Created date</th>
                                <th>Category</th>
                                <th>Add to Favorites</th>
                                <th>Tags</th>
                                <th></th>
                                <th></th>
                                </thead>
                                <tbody>
                                @foreach($newsList as $oneNews)
                                    <tr>
                                        <td>{!! $oneNews['id'] !!}</td>
                                        <td><img src="{!! asset(\App\NewsItemsEntity::$path.$oneNews['image']) !!}"
                                                 width="100px">
                                        </td>
                                        <td>{!! $oneNews['title_en'] !!}</td>
                                        <td>{!! \App\Classes\GlobalFunctions::generateDateTimeToStr($oneNews['created_at']) !!}</td>
                                        <td>{!! \App\NewsItemsEntity::getCategory($oneNews['category_id']) !!}</td>
                                        <td>
                                            <a href="{!! url('admin/favorites/showTool/news/'.$oneNews['id']) !!}"
                                               class="btn btn-success"
                                               data-news-id="">Add
                                            </a>
                                        </td>
                                        <td>
                                            <button class="btn btn-success view-tags"
                                                    data-news-id="{!! $oneNews['id'] !!}" data-toggle="modal"
                                                    data-target="#getTagsModalNews">Edit tags
                                            </button>
                                        </td>
                                        <td>
                                            <a href="{!! url('admin/news/list/show/'.$oneNews['id']) !!}"
                                               class="btn btn-warning">Edit</a>
                                        </td>
                                        <td>
                                            <button class="btn btn-danger delNews"
                                                    data-news-id="{!! $oneNews['id'] !!}">Delete
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>


<div style="display: none">
    {!! Form::open(array('action'=>'NewsController@destroyNews','method'=>'post','id'=>'formNewsDel')) !!}
    {!! Form::hidden('newsId') !!}
    {!! Form::close() !!}
</div>


<div id="getTagsModalNews" class="modal fade cat1">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"> Edit tags</h4>
            </div>
            <div class="col-sm-12">
                <div class="modal-body" id="modal-checkboxes">
                    {!! Form::open(array('action' => 'NewsController@setNewsTag','class'=>'form-horizontal')) !!}

                    {!! Form::hidden('news_id') !!}

                    <div class="form-group check-boxes-div"></div>
                    <div class="form-group col-sm-6">
                        {!! Form::submit('change', ['class'=>'btn btn-success']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>


<input type="hidden" name="temp_news_id" value="{!! Session::get('successSetTags') !!}">


<script>
    $('#example').DataTable(
            {
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                "bFilter": true,
                "bLengthChange": true
            }
    );
    $('#example').on('draw.dt', function () {
        defineDeleteFunction();
        viewExtrasEventByEventId();
    });

    function defineDeleteFunction() {
        $('.delNews').on('click', function (event) {

            var id = $(this).attr('data-news-id');

            $('#formNewsDel input[name="newsId"]').val(id);
            var r = confirm("Do you really want to delete this news ?");
            if (r == true) {
                $('#formNewsDel').submit();
            }
        });
    }

    $(document).ready(function () {
        viewExtrasEventByEventId();
        defineDeleteFunction();

                @if(Session::has('successSetTags'))

        var data = {};
        data['id'] = $('input[name="temp_news_id"]').val();
//        alert(data['id']);
        getTagsFromNews(data);
        $('#getTagsModalNews').modal('show');

        @endif

    });


    function viewExtrasEventByEventId() {
        $('.view-tags').on('click', function (event) {
            var newsId = $(this).attr('data-news-id');
            var data = {};
            data['id'] = newsId;
            getTagsFromNews(data);
        });
    }
    function getTagsFromNews(data) {
        $.ajaxSetup({headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'}});
        $.ajax({
            url: "/api/getNewsTags",
            type: "POST",
            dataType: "json",
            data: data
        }).done(function (result) {
            var tags = result['data'];
            printTags(data['id'], tags);
        }).fail(function (result) {
//            alert('error');
        })
    }

    function printTags(newsId, tags) {
        $('#modal-checkboxes .check-boxes-div').empty();
        $('#modal-checkboxes input[name="news_id"]').val(newsId);

        var checkBoxes = '';
        $.each(tags, function (tagsId, tagsData) {
            console.log(JSON.stringify(tagsData));

            if (tagsData['checked'] == true) {
                checkBoxes += '<label class="checkbox-inline"><input type="checkbox" name="check_boxes[' + tagsId + ']" value="' + tagsId + '" checked>' + tagsData['label'] + '</label>';
            } else {
                checkBoxes += '<label class="checkbox-inline"><input type="checkbox" name="check_boxes[' + tagsId + ']" value="' + tagsId + '" >' + tagsData['label'] + '</label>';
            }
        });
        $('#modal-checkboxes .check-boxes-div').append(checkBoxes);
    }


</script>


@endsection