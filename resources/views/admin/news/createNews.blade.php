@extends( 'admin.master' )
@section( 'content' )

    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script>
        $(function () {
            $(".datepicker").datepicker();
        });

    </script>

    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-no-padding">
                        <div class="box-header">
                            <h3 class="box-title">Create News</h3>

                            <div class="box-tools">
                                <a href="{!! url('admin/news/list') !!}" class="btn btn-primary">News List</a>
                                <a href="{!! url('admin/additioanal_files/list') !!}" class="btn btn-primary" target="_blank">List of
                                    additional
                                    files</a>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">

                                    @if(Session::has('successMessage'))
                                        <div class="alert alert-success">
                                            {!! Session::get('successMessage') !!}
                                        </div>
                                    @endif

                                    @if($errors -> any() )
                                        <div class="alert alert-danger" style="width: 100%;">
                                            @foreach ($errors->all() as $error)
                                                {{--<button type="button" class="close" data-dismiss="alert">?</button>--}}
                                                <p>{{ $error }}</p>
                                            @endforeach
                                        </div>
                                    @endif
                                </div>
                            </div>
                            {{--DATA--}}
                            {!! Form::open(array('action'=>'NewsController@addOrUpdateNews','method'=>'post','files'=>true)) !!}
                            {!! Form::hidden('id',0) !!}
                            {!! Form::label( 'Created at date:' ) !!}
                            {!! Form::text( 'created_at', null, array( 'class' => 'form-control datepicker' ) ) !!}
                            {!! Form::label( 'Category:' ) !!}
                            {!! Form::select('category_id',$newsCatList,null,['class'=>'form-control']) !!}
                            <div class="form-group clearfix">
                                {{--<label for="headerIMG" class="col-sm-4 col-md-2 control-label">Background image <img--}}
                                {{--src="{!! asset(\App\HeaderComponentEntity::$path.$headerObj['image']) !!}"--}}
                                {{--class="img-responsive"--}}
                                {{--style="display: block;"></label>--}}

                                <div class="col-sm-8 col-md-10">
                                    {!! Form::file('image',['placeholder'=>'Парола','class'=>'form-control','id'=>'headerIMG']) !!}
                                    @if($errors -> first('image') != '')
                                        <span class="help-block">{!! $errors -> first('image') !!}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="" class="col-sm-4 col-md-2 control-label">News title and text</label>

                                <div class="col-sm-8 col-md-10">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#cHead_area_en"
                                                                                  aria-controls="cHead_area_en"
                                                                                  role="tab"
                                                                                  data-toggle="tab">EN</a></li>
                                        <li role="presentation"><a href="#cHead_area_de" aria-controls="cHead_area_de"
                                                                   role="tab"
                                                                   data-toggle="tab">DE</a></li>
                                        <li role="presentation"><a href="#cHead_area_fr" aria-controls="cHead_area_fr"
                                                                   role="tab"
                                                                   data-toggle="tab">FR</a></li>

                                    </ul>

                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="cHead_area_en">
                                            {!! Form::text( 'title_en',null, array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                            {!! Form::textarea( 'text_en',null, array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="cHead_area_de">
                                            {!! Form::text( 'title_de',null, array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                            {!! Form::textarea( 'text_de', null, array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="cHead_area_fr">
                                            {!! Form::text( 'title_fr',null, array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                            {!! Form::textarea( 'text_fr', null, array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 text-right">
                                {!! Form::submit('Save',['class'=>'btn btn-success']) !!}
                            </div>
                            {!! Form::close() !!}

                            {{--END OF DATA--}}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection