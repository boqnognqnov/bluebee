@extends( 'admin.master' )
@section( 'content' )

        <!-- DataTables CSS -->
<link rel="stylesheet" type="text/css"
      href="{!! asset('plugins/DataTables-1.10.9/media/css/jquery.dataTables.css') !!}">

<!-- jQuery -->
<script type="text/javascript" charset="utf8"
        src="{!! asset('plugins/DataTables-1.10.9/media/js/jquery.js') !!}"></script>

<!-- DataTables -->
<script type="text/javascript" charset="utf8"
        src="{!! asset('plugins/DataTables-1.10.9/media/js/jquery.dataTables.js') !!}"></script>

<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-md-12">

                @if(Session::has('successMessage'))
                    <div class="alert alert-success">
                        {!! Session::get('successMessage') !!}
                    </div>
                @endif

                @if($errors -> any() )
                    <div class="alert alert-danger" style="width: 100%;">
                        @foreach ($errors->all() as $error)
                            {{--<button type="button" class="close" data-dismiss="alert">?</button>--}}
                            <p>{{ $error }}</p>
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-warning">
                    <div class="box-header">
                        <h3 class="box-title">List of news</h3>

                        <div class="box-tools">
                            <a href="{!! url('admin/news/editMainPage') !!}" class="btn btn-primary"
                               style="display: inline-block">Back to news page</a>
                        </div>
                    </div>
                    <div class="box-body table-responsive no-padding">
                        <div id="example_wrapper" class="dataTables_wrapper">
                            <table id="example" class="display pageResize" cellspacing="0" width="100%">

                                <thead>
                                <th>ID</th>
                                <th>Image</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Created date</th>
                                <th>News</th>
                                <th></th>
                                </thead>
                                <tbody>
                                @foreach($commentList as $oneComment)
                                    <tr>
                                        <td>{!! $oneComment['id'] !!}</td>
                                        <td><img src="{!! asset(\App\User::$avatar_path.$oneComment['image']) !!}"
                                                 width="100px">
                                        </td>
                                        <td>{!! $oneComment['name'] !!}</td>
                                        <td>{!! $oneComment['email'] !!}</td>
                                        <td>{!! $oneComment['created_at'] !!}</td>
                                        <td>{!! \App\CommentNewsEntity::getNewsTitle($oneComment['news_id']) !!}</td>

                                        <td>
                                            <button class="btn btn-danger delComment"
                                                    data-comment-id="{!! $oneComment['id'] !!}">Delete
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>


<div style="display: none">
    {!! Form::open(array('action'=>'NewsController@deleteCommentNews','method'=>'post','id'=>'formCommentDel')) !!}
    {!! Form::hidden('commentId') !!}
    {!! Form::close() !!}
</div>


<input type="hidden" name="temp_news_id" value="{!! Session::get('successSetTags') !!}">


<script>
    $('#example').DataTable(
            {
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                "bFilter": true,
                "bLengthChange": true
            }
    );
    $('#example').on('draw.dt', function () {
        defineDeleteFunction();
    });

    function defineDeleteFunction() {
        $('.delComment').on('click', function (event) {

            var id = $(this).attr('data-comment-id');

            $('#formCommentDel input[name="commentId"]').val(id);
            var r = confirm("Do you really want to delete this comment ?");
            if (r == true) {
                $('#formCommentDel').submit();
            }
        });
    }

    $(document).ready(function () {
        defineDeleteFunction();
    });


</script>


@endsection