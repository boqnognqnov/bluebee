@extends( 'admin.master' )
@section( 'content' )

    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-no-padding">
                        <div class="box-header">
                            <h3 class="box-title">Our team page</h3>

                            <div class="box-tools">
                                <a href="{!! url('admin/team/list') !!}" class="btn btn-primary">Team</a>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    @if(Session::has('successMessage'))
                                        <div class="alert alert-success">
                                            {!! Session::get('successMessage') !!}
                                        </div>
                                    @endif

                                    @if($errors -> any() )
                                        <div class="alert alert-danger" style="width: 100%;">
                                            @foreach ($errors->all() as $error)
                                                {{--<button type="button" class="close" data-dismiss="alert">?</button>--}}
                                                <p>{{ $error }}</p>
                                            @endforeach
                                        </div>
                                    @endif
                                </div>
                            </div>
                            {{--DATA--}}
                            {!! Form::open(array('action'=>'TeamController@updatePage','method'=>'post','files'=>true)) !!}
                            {{--{!! Form::hidden('id',$pageData['id']) !!}--}}

                            <div class="form-group clearfix">
                                <div class="container-fluid">
                                    <div class="checkbox">
                                        <label for="editPassOld" class="control-label">
                                            {!! Form::checkbox('head_is_on',true,@$pageData['head_is_on']) !!}
                                            On/Off header
                                        </label>
                                    </div>
                                </div>
                            </div>

                            @include('admin.components.headerComponent',['headerObj'=>\App\HeaderComponentEntity::getHeaderObj(@$pageData['head_id'])])

                            <hr>
                            <div class="form-group clearfix">
                                <div class="container-fluid">
                                    <div class="checkbox">
                                        <label for="editPassOld" class="col-sm-4 col-md-2 control-label">
                                            {!! Form::checkbox('try_is_on',true,@$pageData['try_is_on']) !!}
                                            @if($errors -> first('try_is_on') != '')
                                                <span class="help-block">{!! $errors -> first('try_is_on') !!}</span>
                                            @endif
                                            On/Off Try component
                                        </label>
                                    </div>
                                </div>
                            </div>

                            @include('admin.components.tryComponent',['tryObj'=>\App\TryComponentEntity::getTryObj(@$pageData['try_id'])])
                            <hr>
                            <div class="col-sm-6 text-right">
                                {!! Form::submit('Save',['class'=>'btn btn-success']) !!}
                            </div>
                            {!! Form::close() !!}

                            {{--END OF DATA--}}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection