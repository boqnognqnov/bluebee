@extends( 'admin.master' )
@section( 'content' )

        <!-- DataTables CSS -->
<link rel="stylesheet" type="text/css"
      href="{!! asset('plugins/DataTables-1.10.9/media/css/jquery.dataTables.css') !!}">

<!-- jQuery -->
<script type="text/javascript" charset="utf8"
        src="{!! asset('plugins/DataTables-1.10.9/media/js/jquery.js') !!}"></script>

<!-- DataTables -->
<script type="text/javascript" charset="utf8"
        src="{!! asset('plugins/DataTables-1.10.9/media/js/jquery.dataTables.js') !!}"></script>

<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('successMessage'))
                    <div class="alert alert-success">
                        {!! Session::get('successMessage') !!}
                    </div>
                @endif

                @if($errors -> any() )
                    <div class="alert alert-danger" style="width: 100%;">
                        @foreach ($errors->all() as $error)
                            {{--<button type="button" class="close" data-dismiss="alert">?</button>--}}
                            <p>{{ $error }}</p>
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-warning">
                    <div class="box-header">
                        <h3 class="box-title">Team list</h3>

                        <div class="box-tools">
                            <a href="{!! url('admin/team/page/show') !!}" class="btn btn-primary"
                               style="display: inline-block">Back team page</a>
                            <a href="{!! url('admin/team/list/create') !!}"
                               class="btn btn-warning" style="display: inline-block">Add employee</a>
                        </div>
                    </div>
                    <div class="box-body table-responsive no-padding">
                        <div id="example_wrapper" class="dataTables_wrapper">
                            <table id="example" class="display pageResize" cellspacing="0" width="100%">

                                <thead>
                                <th>ID</th>
                                <th>Image</th>
                                <th>Name</th>
                                <th>Position</th>
                                <th></th>
                                <th></th>
                                </thead>
                                <tbody>
                                @foreach($teamList as $employee)
                                    @if($employee['is_manager']==true)
                                        <tr style="background-color: green">
                                    @else
                                        <tr>
                                            @endif
                                            <td>{!! $employee['id'] !!}</td>
                                            <td><img src="{!! asset(\App\TeamItemsEntity::$path.$employee['image']) !!}"
                                                     width="100px">
                                            </td>
                                            <td>{!! $employee['name'] !!}</td>
                                            <td>{!! $employee['position_en'] !!}</td>

                                            <td>
                                                <a href="{!! url('admin/team/list/show/'.$employee['id']) !!}"
                                                   class="btn btn-warning">Edit</a>
                                            </td>
                                            <td>
                                                <button class="btn btn-danger delEmployee"
                                                        data-employee-id="{!! $employee['id'] !!}">Delete
                                                </button>
                                            </td>
                                        </tr>
                                        @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>


<script>
    $('#example').DataTable(
            {
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                "bFilter": true,
                "bLengthChange": true
            }
    );
</script>

<div style="display: none">
    {!! Form::open(array('action'=>'TeamController@destroyEmployee','method'=>'post','id'=>'formEmployeeDel')) !!}
    {!! Form::hidden('employeeId') !!}
    {!! Form::close() !!}
</div>


<script>

    $(document).ready(function () {

        $('.delEmployee').on('click', function (event) {

            var id = $(this).attr('data-employee-id');

            $('#formEmployeeDel input[name="employeeId"]').val(id);
            var r = confirm("Do you really want to delete this employee ?");
            if (r == true) {
                $('#formEmployeeDel').submit();
            }
        });
    });
</script>


@endsection