@extends( 'admin.master' )
@section( 'content' )

    <script src="{!! url('https://code.jquery.com/ui/1.12.0-beta.1/jquery-ui.min.js') !!}"></script>
    <link rel="stylesheet" href="{!! url('https://code.jquery.com/ui/1.11.4/themes/black-tie/jquery-ui.css') !!}">
    <script>
        $(function () {
            $("#accordion").accordion();
        });
    </script>

    <div class="content-wrapper">
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-no-padding">
                        <div class="box-header">
                            <h3 class="box-title">Page Pipelines</h3>

                            <div class="box-tools">
                                {{--<a href="{!! url('admin/pipelines/items/list') !!}" class="btn btn-primary">Pipeline--}}
                                {{--list</a>--}}

                            </div>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">

                                    @if(Session::has('successMessage'))
                                        <div class="alert alert-success">
                                            {!! Session::get('successMessage') !!}
                                        </div>
                                    @endif

                                    @if($errors -> any() )
                                        <div class="alert alert-danger" style="width: 100%;">
                                            @foreach ($errors->all() as $error)
                                                {{--<button type="button" class="close" data-dismiss="alert">?</button>--}}
                                                <p>{{ $error }}</p>
                                            @endforeach
                                        </div>
                                    @endif
                                </div>
                            </div>
                            {{--DATA--}}
                            {!! Form::open(array('action'=>'PipeLinesController@updatePage','method'=>'post','files'=>true)) !!}
                            {{--{!! Form::hidden('id',$pageData['id']) !!}--}}

                            {{--<div class="form-group clearfix">--}}
                            {{--<div class="container-fluid">--}}
                            {{--<div class="checkbox">--}}
                            {{--<label for="editPassOld" class="control-label">--}}
                            {{--{!! Form::checkbox('head_is_on',true,@$pageData['head_is_on']) !!}--}}
                            {{--On/Off header--}}
                            {{--</label>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--<hr>--}}
                            {{--<div class="form-group clearfix">--}}
                            {{--<label for="headerIMG" class="col-sm-4 col-md-2 control-label">Background image <img--}}
                            {{--src="{!! asset(\App\PipeLinesPageEntity::$pathHeader.@$pageData['head_image']) !!}"--}}
                            {{--class="img-responsive"--}}
                            {{--style="display: block;"></label>--}}

                            {{--<div class="col-sm-8 col-md-10">--}}
                            {{--{!! Form::file('head_image',['placeholder'=>'image','class'=>'form-control','id'=>'headerIMG']) !!}--}}
                            {{--@if($errors -> first('head_image') != '')--}}
                            {{--<span class="help-block">{!! $errors -> first('head_image') !!}</span>--}}
                            {{--@endif--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--<hr>--}}
                            <div class="form-group clearfix">
                                <div class="container-fluid">
                                    <div class="checkbox">
                                        <label for="editPassOld" class="control-label">
                                            {!! Form::checkbox('head_is_on',true,@$pageData['head_is_on']) !!}
                                            On/Off header
                                        </label>
                                    </div>
                                </div>
                            </div>

                            @include('admin.components.headerComponent',['headerObj'=>\App\HeaderComponentEntity::getHeaderObj(@$pageData['head_id'])])

                            <hr>

                            <div class="form-group clearfix">
                                <label for="headerIMG" class="col-sm-4 col-md-2 control-label">Picture above the three
                                    pipes <img
                                            src="{!! asset(\App\PipeLinesPageEntity::$path.$pageData['image']) !!}"
                                            class="img-responsive"
                                            style="display: block;">
                                    @if(!empty($pageData['image']))
                                        <a href="{!! url('admin/pipelines/page/destroyImage') !!}"
                                           class="btn btn-danger">Delete image</a>
                                    @endif

                                </label>

                                <div class="col-sm-8 col-md-10">
                                    {!! Form::file('image',['placeholder'=>'Парола','class'=>'form-control','id'=>'headerIMG']) !!}
                                    @if($errors -> first('image') != '')
                                        <span class="help-block">{!! $errors -> first('image') !!}</span>
                                    @endif
                                </div>
                            </div>
                            <hr>


                            <div class="form-group clearfix">
                                <label for="editPassNew" class="col-sm-4 col-md-2 control-label">Texts areas</label>

                                <div class="col-sm-8 col-md-10">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#pipeData_area_en"
                                                                                  aria-controls="pipeData_area_en"
                                                                                  role="tab"
                                                                                  data-toggle="tab">EN</a></li>
                                        <li role="presentation"><a href="#pipeData_area_de"
                                                                   aria-controls="pipeData_area_de"
                                                                   role="tab"
                                                                   data-toggle="tab">DE</a></li>
                                        <li role="presentation"><a href="#pipeData_area_fr"
                                                                   aria-controls="pipeData_area_fr"
                                                                   role="tab"
                                                                   data-toggle="tab">FR</a></li>
                                    </ul>

                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="pipeData_area_en">
                                            <label for="" class="col-sm-4 col-md-2 control-label">First text</label>

                                            <div class="col-sm-8 col-md-10">
                                                {!! Form::textarea( 'pipe_text_en',$pageData['pipe_text_en'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                            </div>
                                            <label for="" class="col-sm-4 col-md-2 control-label">Second text</label>

                                            <div class="col-sm-8 col-md-10">
                                                {!! Form::textarea( 's_pipe_text_en',$pageData['s_pipe_text_en'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="pipeData_area_de">
                                            <label for="" class="col-sm-4 col-md-2 control-label">First text</label>

                                            <div class="col-sm-8 col-md-10">
                                                {!! Form::textarea( 'pipe_text_de',$pageData['pipe_text_de'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                            </div>
                                            <label for="" class="col-sm-4 col-md-2 control-label">Second text</label>

                                            <div class="col-sm-8 col-md-10">
                                                {!! Form::textarea( 's_pipe_text_de', $pageData['s_pipe_text_de'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="pipeData_area_fr">
                                            <label for="" class="col-sm-4 col-md-2 control-label">First text</label>

                                            <div class="col-sm-8 col-md-10">
                                                {!! Form::textarea( 'pipe_text_fr',$pageData['pipe_text_fr'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                            </div>
                                            <label for="" class="col-sm-4 col-md-2 control-label">Second text</label>

                                            <div class="col-sm-8 col-md-10">
                                                {!! Form::textarea( 's_pipe_text_fr', $pageData['s_pipe_text_fr'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            {{--ACCORDEON--}}
                            <div id="accordion">

                                <h3>Column 1</h3>

                                <div>
                                    <div class="form-group clearfix">
                                        <div class="col-sm-8 col-md-10">
                                            <ul class="nav nav-tabs" role="tablist">
                                                <li role="presentation" class="active"><a href="#pcol1_area_en"
                                                                                          aria-controls="pcol1_area_en"
                                                                                          role="tab"
                                                                                          data-toggle="tab">EN</a></li>
                                                <li role="presentation"><a href="#pcol1_area_de"
                                                                           aria-controls="pcol1_area_de"
                                                                           role="tab"
                                                                           data-toggle="tab">DE</a></li>
                                                <li role="presentation"><a href="#pcol1_area_fr"
                                                                           aria-controls="pcol1_area_fr"
                                                                           role="tab"
                                                                           data-toggle="tab">FR</a></li>
                                            </ul>

                                            <div class="tab-content">
                                                <div role="tabpanel" class="tab-pane active" id="pcol1_area_en">
                                                    <label for="" class="col-sm-4 col-md-2 control-label">Tiltle and
                                                        text</label>
                                                    {!! Form::text( 'pipeCol[1][title_en]',$pipeCols[0]['title_en'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                    {!! Form::textarea( 'pipeCol[1][text_en]',$pipeCols[0]['text_en'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                                </div>
                                                <div role="tabpanel" class="tab-pane" id="pcol1_area_de">
                                                    <label for="" class="col-sm-4 col-md-2 control-label">Tiltle and
                                                        text</label>
                                                    {!! Form::text( 'pipeCol[1][title_de]',$pipeCols[0]['title_de'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                    {!! Form::textarea( 'pipeCol[1][text_de]',$pipeCols[0]['text_de'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                                </div>
                                                <div role="tabpanel" class="tab-pane" id="pcol1_area_fr">
                                                    <label for="" class="col-sm-4 col-md-2 control-label">Tiltle and
                                                        text</label>
                                                    {!! Form::text( 'pipeCol[1][title_fr]',$pipeCols[0]['title_fr'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                    {!! Form::textarea( 'pipeCol[1][text_fr]',$pipeCols[0]['text_fr'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <h3>Column 2</h3>

                                <div>
                                    <div class="form-group clearfix">
                                        <div class="col-sm-8 col-md-10">
                                            <ul class="nav nav-tabs" role="tablist">
                                                <li role="presentation" class="active"><a href="#pcol2_area_en"
                                                                                          aria-controls="pcol2_area_en"
                                                                                          role="tab"
                                                                                          data-toggle="tab">EN</a></li>
                                                <li role="presentation"><a href="#pcol2_area_de"
                                                                           aria-controls="pcol2_area_de"
                                                                           role="tab"
                                                                           data-toggle="tab">DE</a></li>
                                                <li role="presentation"><a href="#pcol2_area_fr"
                                                                           aria-controls="pcol2_area_fr"
                                                                           role="tab"
                                                                           data-toggle="tab">FR</a></li>
                                            </ul>

                                            <div class="tab-content">
                                                <div role="tabpanel" class="tab-pane active" id="pcol2_area_en">
                                                    <label for="" class="col-sm-4 col-md-2 control-label">Tiltle and
                                                        text</label>
                                                    {!! Form::text( 'pipeCol[2][title_en]',$pipeCols[1]['title_en'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                    {!! Form::textarea( 'pipeCol[2][text_en]',$pipeCols[1]['text_en'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                                </div>
                                                <div role="tabpanel" class="tab-pane" id="pcol2_area_de">
                                                    <label for="" class="col-sm-4 col-md-2 control-label">Tiltle and
                                                        text</label>
                                                    {!! Form::text( 'pipeCol[2][title_de]',$pipeCols[1]['title_de'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                    {!! Form::textarea( 'pipeCol[2][text_de]',$pipeCols[1]['text_de'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                                </div>
                                                <div role="tabpanel" class="tab-pane" id="pcol2_area_fr">
                                                    <label for="" class="col-sm-4 col-md-2 control-label">Tiltle and
                                                        text</label>
                                                    {!! Form::text( 'pipeCol[2][title_fr]',$pipeCols[1]['title_fr'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                    {!! Form::textarea( 'pipeCol[2][text_fr]',$pipeCols[1]['text_fr'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <h3>Column 3</h3>

                                <div>
                                    <div class="form-group clearfix">
                                        <div class="col-sm-8 col-md-10">
                                            <ul class="nav nav-tabs" role="tablist">
                                                <li role="presentation" class="active"><a href="#pcol3_area_en"
                                                                                          aria-controls="pcol3_area_en"
                                                                                          role="tab"
                                                                                          data-toggle="tab">EN</a>
                                                </li>
                                                <li role="presentation"><a href="#pcol3_area_de"
                                                                           aria-controls="pcol3_area_de"
                                                                           role="tab"
                                                                           data-toggle="tab">DE</a></li>
                                                <li role="presentation"><a href="#pcol3_area_fr"
                                                                           aria-controls="pcol3_area_fr"
                                                                           role="tab"
                                                                           data-toggle="tab">FR</a></li>
                                            </ul>

                                            <div class="tab-content">
                                                <div role="tabpanel" class="tab-pane active" id="pcol3_area_en">
                                                    <label for="" class="col-sm-4 col-md-2 control-label">Tiltle and
                                                        text</label>
                                                    {!! Form::text( 'pipeCol[3][title_en]',$pipeCols[2]['title_en'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                    {!! Form::textarea( 'pipeCol[3][text_en]',$pipeCols[2]['text_en'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                                </div>
                                                <div role="tabpanel" class="tab-pane" id="pcol3_area_de">
                                                    <label for="" class="col-sm-4 col-md-2 control-label">Tiltle and
                                                        text</label>
                                                    {!! Form::text( 'pipeCol[3][title_de]',$pipeCols[2]['title_de'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                    {!! Form::textarea( 'pipeCol[3][text_de]',$pipeCols[2]['text_de'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                                </div>
                                                <div role="tabpanel" class="tab-pane" id="pcol3_area_fr">
                                                    <label for="" class="col-sm-4 col-md-2 control-label">Tiltle and
                                                        text</label>
                                                    {!! Form::text( 'pipeCol[3][title_fr]',$pipeCols[2]['title_fr'], array( 'class' => 'form-control', 'rows' => '6' ) ) !!}
                                                    {!! Form::textarea( 'pipeCol[3][text_fr]',$pipeCols[2]['text_fr'], array( 'class' => 'form-control html5area', 'rows' => '6' ) ) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {{--END ACCORDEON--}}
                            <hr>
                            <div class="form-group clearfix">
                                <div class="container-fluid">
                                    <div class="checkbox">
                                        <label for="editPassOld" class="col-sm-4 col-md-2 control-label">
                                            {!! Form::checkbox('try_is_on',true,@$pageData['try_is_on']) !!}
                                            @if($errors -> first('try_is_on') != '')
                                                <span class="help-block">{!! $errors -> first('try_is_on') !!}</span>
                                            @endif
                                            On/Off Try component
                                        </label>
                                    </div>
                                </div>
                            </div>

                            @include('admin.components.tryComponent',['tryObj'=>\App\TryComponentEntity::getTryObj(@$pageData['try_id'])])
                            <hr>
                            <div class="col-sm-6 text-right">
                                {!! Form::submit('Save',['class'=>'btn btn-success']) !!}
                            </div>
                            {!! Form::close() !!}

                            {{--END OF DATA--}}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection