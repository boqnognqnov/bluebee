@extends( 'master' )

@section( 'content' )
  @include('components.intro', ['title'=>'404 - page not found','body'=>'','button'=>'','breadcrumb'=>'','background'=>['default'=>'/uploads/images/components/headerComponents/default/1459081665.jpg','small'=>'/uploads/images/components/headerComponents/default/1459081665.jpg','medium'=>'/uploads/images/components/headerComponents/default/1459081665.jpg']])
  <div class="c-row c-row--md c-row--border-top u-bgcolor-neutral-xxx-light u-pb-jota">
    <div class="u-align-horizontal u-max-width-sm c-main-section s-content">
      <p>
        In a genome, a deletion is known as a mutation causing part of the chromosome to be missing.
      </p>
      <p class="u-align-center">
        <img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="{!! asset('img/photos/text/600/404 page image.png') !!}" class="p-blazy js-blazy" width="200" alt="chromosome deletion graphic">
      </p>
      <p>
        You have discovered a missing page. We apologize for this. We are fixing the page to avoid this in the future.
      </p>
      <p>
        Maybe you can find what you are looking for in the search bar, or on one of these pages:
      </p>
      <div class="o-grid o-grid--gutter u-mt-jota u-mb-jota">
        <div class="o-grid__item u-1-of-4-bp3">
          <a class="c-button c-button--ghost-alpha c-button--md c-button--block" href="{!! url('/') !!}" role="button">Home</a>
        </div>
        <div class="o-grid__item u-2-of-4-bp3">
          <a class="c-button c-button--ghost-alpha c-button--md c-button--block" href="{!! url('/knowledge-center') !!}" role="button">Knowledge Center</a>
        </div>
        <div class="o-grid__item u-1-of-4-bp3">
          <a class="c-button c-button--ghost-alpha c-button--md c-button--block" href="{!! url('/contact') !!}" role="button">Contacts</a>
        </div>
      </div>
    </div>
  </div>
@endsection

@section( 'view-scripts' )

@endsection
