<?php
////trans('modelStatusMessages.default')
//return [
////  CONTROLLERS STATUS MESSAGES
//    'creatingError' => 'RunTime Error',
//    'imageFailed' => 'RunTime Error',
//    'successMessage' => 'The record has been successfully saved',
//    'default' => 'RunTime Error',
//
////  CONTROLLERS STATUS MESSAGES
//    'messageError' => 'Your message has not been send',
//    'messageSuccess' => 'Your message has been send',

//trans('modelStatusMessages.default')
return [
//  CONTROLLERS STATUS MESSAGES
    'creatingError' => 'RunTime Error',
    'imageFailed' => 'RunTime Error',
    'successMessage' => 'Your request has been successfully submitted ',
    'default' => 'RunTime Error',

//  CONTROLLERS STATUS MESSAGES
    'messageError' => 'Unfortunately something went wrong . Please try again, alternatively please contact us directly: +31 15 203 6002',
    'messageSuccess' => 'Your message has been successfully send',
    'noEmptyFavPartnersSlot' => 'In not empty favorite slot'


];