-- MySQL dump 10.13  Distrib 5.5.46, for Linux (x86_64)
--
-- Host: localhost    Database: bluebee
-- ------------------------------------------------------
-- Server version	5.5.46

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `benefits_in_elements`
--

DROP TABLE IF EXISTS `benefits_in_elements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `benefits_in_elements` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `inner_benefit_id` int(10) unsigned NOT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text_en` text COLLATE utf8_unicode_ci NOT NULL,
  `title_de` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text_de` text COLLATE utf8_unicode_ci NOT NULL,
  `title_fr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text_fr` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug_de` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug_fr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `benefits_in_elements_inner_benefit_id_foreign` (`inner_benefit_id`),
  CONSTRAINT `benefits_in_elements_inner_benefit_id_foreign` FOREIGN KEY (`inner_benefit_id`) REFERENCES `page_benefits_inner` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `benefits_in_elements`
--

LOCK TABLES `benefits_in_elements` WRITE;
/*!40000 ALTER TABLE `benefits_in_elements` DISABLE KEYS */;
/*!40000 ALTER TABLE `benefits_in_elements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category_downloads`
--

DROP TABLE IF EXISTS `category_downloads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category_downloads` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_de` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_fr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category_downloads`
--

LOCK TABLES `category_downloads` WRITE;
/*!40000 ALTER TABLE `category_downloads` DISABLE KEYS */;
/*!40000 ALTER TABLE `category_downloads` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category_events`
--

DROP TABLE IF EXISTS `category_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category_events` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_de` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_fr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category_events`
--

LOCK TABLES `category_events` WRITE;
/*!40000 ALTER TABLE `category_events` DISABLE KEYS */;
/*!40000 ALTER TABLE `category_events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category_news`
--

DROP TABLE IF EXISTS `category_news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category_news` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_de` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_fr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category_news`
--

LOCK TABLES `category_news` WRITE;
/*!40000 ALTER TABLE `category_news` DISABLE KEYS */;
/*!40000 ALTER TABLE `category_news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comment_events`
--

DROP TABLE IF EXISTS `comment_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comment_events` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `event_id` int(10) unsigned NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `is_approve` tinyint(1) NOT NULL DEFAULT '0',
  `is_readed` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `comment_events_event_id_foreign` (`event_id`),
  CONSTRAINT `comment_events_event_id_foreign` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comment_events`
--

LOCK TABLES `comment_events` WRITE;
/*!40000 ALTER TABLE `comment_events` DISABLE KEYS */;
/*!40000 ALTER TABLE `comment_events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comment_news`
--

DROP TABLE IF EXISTS `comment_news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comment_news` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `news_id` int(10) unsigned NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `is_approve` tinyint(1) NOT NULL DEFAULT '0',
  `is_readed` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `comment_news_news_id_foreign` (`news_id`),
  CONSTRAINT `comment_news_news_id_foreign` FOREIGN KEY (`news_id`) REFERENCES `news` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comment_news`
--

LOCK TABLES `comment_news` WRITE;
/*!40000 ALTER TABLE `comment_news` DISABLE KEYS */;
/*!40000 ALTER TABLE `comment_news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `component_favorite`
--

DROP TABLE IF EXISTS `component_favorite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `component_favorite` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `f_model_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `f_model_id` int(11) NOT NULL,
  `s_model_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `s_model_id` int(11) NOT NULL,
  `t_model_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `t_model_id` int(11) NOT NULL,
  `vector_model_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `vector_model_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `component_favorite`
--

LOCK TABLES `component_favorite` WRITE;
/*!40000 ALTER TABLE `component_favorite` DISABLE KEYS */;
INSERT INTO `component_favorite` VALUES (1,'',0,'',0,'',0,'',0,NULL,NULL),(2,'',0,'',0,'',0,'',0,NULL,NULL),(3,'',0,'',0,'',0,'',0,NULL,NULL),(4,'',0,'',0,'',0,'',0,NULL,NULL),(5,'',0,'',0,'',0,'',0,NULL,NULL),(6,'',0,'',0,'',0,'',0,NULL,NULL),(7,'',0,'',0,'',0,'',0,NULL,NULL);
/*!40000 ALTER TABLE `component_favorite` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `component_header`
--

DROP TABLE IF EXISTS `component_header`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `component_header` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text_en` text COLLATE utf8_unicode_ci NOT NULL,
  `title_de` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text_de` text COLLATE utf8_unicode_ci NOT NULL,
  `title_fr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text_fr` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `component_header`
--

LOCK TABLES `component_header` WRITE;
/*!40000 ALTER TABLE `component_header` DISABLE KEYS */;
INSERT INTO `component_header` VALUES (1,'','High Performance header','','','','','',NULL,NULL),(2,'','Security header','','','','','',NULL,NULL),(3,'','Convenience header','','','','','',NULL,NULL),(4,'','Cost Effective header','','','','','',NULL,NULL),(5,'','High Partners page header','','','','','',NULL,NULL),(6,'','This is temporary component','','','','','',NULL,NULL),(7,'','This is temporary component','','','','','',NULL,NULL),(8,'','This is temporary component','','','','','',NULL,NULL),(9,'','This is temporary component','','','','','',NULL,NULL),(10,'','This is temporary component','','','','','',NULL,NULL),(11,'','This is temporary component','','','','','',NULL,NULL),(12,'','This is temporary component','','','','','',NULL,NULL),(13,'','This is temporary component','','This is temporary component','','This is temporary component','',NULL,NULL),(14,'','This is temporary component','','This is temporary component','','This is temporary component','',NULL,NULL),(15,'','This is temporary component','','This is temporary component','','This is temporary component','',NULL,NULL),(16,'','This is temporary component','','This is temporary component','','This is temporary component','',NULL,NULL),(17,'','This is temporary component','','This is temporary component','','This is temporary component','',NULL,NULL),(18,'','High Partners page header','','High Partners page header','','High Partners page header','',NULL,NULL),(19,'','High Partners page header','','High Partners page header','','High Partners page header','',NULL,NULL),(20,'','High Partners page header','','High Partners page header','','High Partners page header','',NULL,NULL);
/*!40000 ALTER TABLE `component_header` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `component_opinion`
--

DROP TABLE IF EXISTS `component_opinion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `component_opinion` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text_en` text COLLATE utf8_unicode_ci NOT NULL,
  `button_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text_de` text COLLATE utf8_unicode_ci NOT NULL,
  `button_de` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text_fr` text COLLATE utf8_unicode_ci NOT NULL,
  `button_fr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `component_opinion`
--

LOCK TABLES `component_opinion` WRITE;
/*!40000 ALTER TABLE `component_opinion` DISABLE KEYS */;
INSERT INTO `component_opinion` VALUES (1,'This is temporary component','','','','','','','',NULL,NULL),(2,'','','','','','','','',NULL,NULL),(3,'','','','','','','','',NULL,NULL);
/*!40000 ALTER TABLE `component_opinion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `component_sign_up`
--

DROP TABLE IF EXISTS `component_sign_up`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `component_sign_up` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `text_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `button_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text_fr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `button_fr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text_de` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `button_de` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `component_sign_up`
--

LOCK TABLES `component_sign_up` WRITE;
/*!40000 ALTER TABLE `component_sign_up` DISABLE KEYS */;
INSERT INTO `component_sign_up` VALUES (1,'','','','','','','',NULL,NULL);
/*!40000 ALTER TABLE `component_sign_up` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `component_try`
--

DROP TABLE IF EXISTS `component_try`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `component_try` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text_en` text COLLATE utf8_unicode_ci NOT NULL,
  `button_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_de` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text_de` text COLLATE utf8_unicode_ci NOT NULL,
  `button_de` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_fr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text_fr` text COLLATE utf8_unicode_ci NOT NULL,
  `button_fr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `component_try`
--

LOCK TABLES `component_try` WRITE;
/*!40000 ALTER TABLE `component_try` DISABLE KEYS */;
INSERT INTO `component_try` VALUES (1,'This is temporary component','','','','','','','','','','',NULL,NULL),(2,'This is temporary component','','','','','','','','','','',NULL,NULL),(3,'This is temporary component','','','','','','','','','','',NULL,NULL),(4,'This is temporary component','','','','','','','','','','',NULL,NULL),(5,'This is temporary component','','','','','','','','','','',NULL,NULL),(6,'This is temporary component','','','','','','','','','','',NULL,NULL),(7,'This is temporary component','','','','','','','','','','',NULL,NULL),(8,'This is temporary component','','','','','','','','','','',NULL,NULL),(9,'This is temporary component','','','','','','','','','','',NULL,NULL),(10,'This is temporary component','','','This is temporary component','','','This is temporary component','','','','',NULL,NULL),(11,'This is temporary component','','','This is temporary component','','','This is temporary component','','','','',NULL,NULL),(12,'This is temporary component','','','This is temporary component','','','This is temporary component','','','','',NULL,NULL),(13,'This is temporary component','','','This is temporary component','','','This is temporary component','','','','',NULL,NULL),(14,'This is temporary component','','','This is temporary component','','','This is temporary component','','','','',NULL,NULL),(15,'This is temporary component','','','This is temporary component','','','This is temporary component','','','','',NULL,NULL),(16,'This is temporary component','','','This is temporary component','','','This is temporary component','','','','',NULL,NULL);
/*!40000 ALTER TABLE `component_try` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `downloads`
--

DROP TABLE IF EXISTS `downloads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `downloads` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text_en` text COLLATE utf8_unicode_ci NOT NULL,
  `title_de` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text_de` text COLLATE utf8_unicode_ci NOT NULL,
  `title_fr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text_fr` text COLLATE utf8_unicode_ci NOT NULL,
  `file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `downloads_category_id_foreign` (`category_id`),
  CONSTRAINT `downloads_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `category_downloads` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `downloads`
--

LOCK TABLES `downloads` WRITE;
/*!40000 ALTER TABLE `downloads` DISABLE KEYS */;
/*!40000 ALTER TABLE `downloads` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `events`
--

DROP TABLE IF EXISTS `events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `events` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text_en` text COLLATE utf8_unicode_ci NOT NULL,
  `title_de` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text_de` text COLLATE utf8_unicode_ci NOT NULL,
  `title_fr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text_fr` text COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `slug_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug_de` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug_fr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `events_category_id_foreign` (`category_id`),
  CONSTRAINT `events_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `category_events` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events`
--

LOCK TABLES `events` WRITE;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;
/*!40000 ALTER TABLE `events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `events_to_tag`
--

DROP TABLE IF EXISTS `events_to_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `events_to_tag` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `event_id` int(10) unsigned NOT NULL,
  `tag_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `events_to_tag_event_id_foreign` (`event_id`),
  KEY `events_to_tag_tag_id_foreign` (`tag_id`),
  CONSTRAINT `events_to_tag_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `tags_events` (`id`),
  CONSTRAINT `events_to_tag_event_id_foreign` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `events_to_tag`
--

LOCK TABLES `events_to_tag` WRITE;
/*!40000 ALTER TABLE `events_to_tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `events_to_tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `features_elements`
--

DROP TABLE IF EXISTS `features_elements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `features_elements` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text_en` text COLLATE utf8_unicode_ci NOT NULL,
  `title_de` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text_de` text COLLATE utf8_unicode_ci NOT NULL,
  `title_fr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text_fr` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `features_elements`
--

LOCK TABLES `features_elements` WRITE;
/*!40000 ALTER TABLE `features_elements` DISABLE KEYS */;
/*!40000 ALTER TABLE `features_elements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `history_download`
--

DROP TABLE IF EXISTS `history_download`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `history_download` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_download_id` int(10) unsigned NOT NULL,
  `views` int(11) NOT NULL,
  `download_rec_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `history_download_user_download_id_foreign` (`user_download_id`),
  CONSTRAINT `history_download_user_download_id_foreign` FOREIGN KEY (`user_download_id`) REFERENCES `user_history` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `history_download`
--

LOCK TABLES `history_download` WRITE;
/*!40000 ALTER TABLE `history_download` DISABLE KEYS */;
/*!40000 ALTER TABLE `history_download` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `history_page`
--

DROP TABLE IF EXISTS `history_page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `history_page` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_page_id` int(10) unsigned NOT NULL,
  `page_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `page_id` int(11) NOT NULL,
  `views` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `history_page_user_page_id_foreign` (`user_page_id`),
  CONSTRAINT `history_page_user_page_id_foreign` FOREIGN KEY (`user_page_id`) REFERENCES `user_history` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `history_page`
--

LOCK TABLES `history_page` WRITE;
/*!40000 ALTER TABLE `history_page` DISABLE KEYS */;
/*!40000 ALTER TABLE `history_page` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_apply`
--

DROP TABLE IF EXISTS `job_apply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_apply` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `position_id` int(10) unsigned NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cv` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `job_apply_position_id_foreign` (`position_id`),
  CONSTRAINT `job_apply_position_id_foreign` FOREIGN KEY (`position_id`) REFERENCES `positions_to_apply` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_apply`
--

LOCK TABLES `job_apply` WRITE;
/*!40000 ALTER TABLE `job_apply` DISABLE KEYS */;
/*!40000 ALTER TABLE `job_apply` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2014_10_12_000000_create_users_table',1),('2014_10_12_100000_create_password_resets_table',1),('2016_02_29_140420_user_by_coockie',1),('2016_02_29_140442_user_history',1),('2016_02_29_140623_page_contact',1),('2016_02_29_140643_component_opinion',1),('2016_02_29_140656_component_try',1),('2016_02_29_140712_page_index',1),('2016_02_29_140725_page_benefits',1),('2016_02_29_140732_page_benefits_inner',1),('2016_02_29_140801_benefits_in_elements',1),('2016_02_29_140820_page_features',1),('2016_02_29_140837_features_elements',1),('2016_02_29_141015_component_favorites',1),('2016_02_29_141027_page_methods',1),('2016_02_29_141044_tag_news',1),('2016_02_29_141056_comment_news',1),('2016_02_29_141114_news_to_tag',1),('2016_02_29_141151_news',1),('2016_02_29_141201_page_news',1),('2016_02_29_141229_tag_events',1),('2016_02_29_141243_category_events',1),('2016_02_29_141255_events_to_tag',1),('2016_02_29_141304_events',1),('2016_02_29_141317_page_events',1),('2016_02_29_141334_component_sign_up',1),('2016_02_29_141359_page_knowledge_center',1),('2016_02_29_141428_comment_events',1),('2016_02_29_141442_category_downloads',1),('2016_02_29_141505_downloads',1),('2016_02_29_141518_page_downloads',1),('2016_02_29_141541_partners',1),('2016_02_29_141554_page_partners',1),('2016_02_29_141654_page_cases',1),('2016_02_29_141703_page_about',1),('2016_02_29_141735_page_offices',1),('2016_02_29_141801_team',1),('2016_02_29_141808_page_team',1),('2016_02_29_141818_page_join_team',1),('2016_02_29_141840_positions_to_apply',1),('2016_02_29_141902_job_apply',1),('2016_02_29_142900_category_news',1),('2016_02_29_155130_history_download',1),('2016_02_29_155520_history_page',1),('2016_02_29_162634_page_case_inner',1),('2016_03_01_160405_component_header',1),('2016_03_02_093924_relations',1),('2016_03_10_110041_news_inner_page',1),('2016_03_11_114043_page_events_inner',1),('2016_03_14_090150_offices',1),('2016_03_14_092349_pipelines',1),('2016_03_14_092722_page_pipelines',1),('2016_03_14_093808_pipeline_cols',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text_en` text COLLATE utf8_unicode_ci NOT NULL,
  `title_de` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text_de` text COLLATE utf8_unicode_ci NOT NULL,
  `title_fr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text_fr` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `slug_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug_de` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug_fr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `news_category_id_foreign` (`category_id`),
  CONSTRAINT `news_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `category_news` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news`
--

LOCK TABLES `news` WRITE;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
/*!40000 ALTER TABLE `news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news_to_tag`
--

DROP TABLE IF EXISTS `news_to_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news_to_tag` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `news_id` int(10) unsigned NOT NULL,
  `tag_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `news_to_tag_news_id_foreign` (`news_id`),
  KEY `news_to_tag_tag_id_foreign` (`tag_id`),
  CONSTRAINT `news_to_tag_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `tags_news` (`id`),
  CONSTRAINT `news_to_tag_news_id_foreign` FOREIGN KEY (`news_id`) REFERENCES `news` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news_to_tag`
--

LOCK TABLES `news_to_tag` WRITE;
/*!40000 ALTER TABLE `news_to_tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `news_to_tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `offices`
--

DROP TABLE IF EXISTS `offices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `offices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text_en` text COLLATE utf8_unicode_ci NOT NULL,
  `title_de` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text_de` text COLLATE utf8_unicode_ci NOT NULL,
  `title_fr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text_fr` text COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address_de` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address_fr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `coordinates` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `offices`
--

LOCK TABLES `offices` WRITE;
/*!40000 ALTER TABLE `offices` DISABLE KEYS */;
/*!40000 ALTER TABLE `offices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page_about`
--

DROP TABLE IF EXISTS `page_about`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_about` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `head_is_on` tinyint(1) NOT NULL DEFAULT '0',
  `head_id` int(10) unsigned NOT NULL,
  `intro_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `intro_title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `intro_text_en` text COLLATE utf8_unicode_ci NOT NULL,
  `intro_title_de` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `intro_text_de` text COLLATE utf8_unicode_ci NOT NULL,
  `intro_title_fr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `intro_text_fr` text COLLATE utf8_unicode_ci NOT NULL,
  `intro_button_ref` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `services_title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `services_text_en` longtext COLLATE utf8_unicode_ci,
  `services_button_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `services_title_de` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `services_text_de` text COLLATE utf8_unicode_ci NOT NULL,
  `services_button_de` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `services_title_fr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `services_text_fr` text COLLATE utf8_unicode_ci NOT NULL,
  `services_button_fr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `team_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `achievement_title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `achievement_text_en` text COLLATE utf8_unicode_ci NOT NULL,
  `achievement_title_de` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `achievement_text_de` text COLLATE utf8_unicode_ci NOT NULL,
  `achievement_title_fr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `achievement_text_fr` text COLLATE utf8_unicode_ci NOT NULL,
  `team_title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `team_text_en` text COLLATE utf8_unicode_ci NOT NULL,
  `team_button_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `team_title_de` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `team_text_de` text COLLATE utf8_unicode_ci NOT NULL,
  `team_button_de` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `team_title_fr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `team_text_fr` text COLLATE utf8_unicode_ci NOT NULL,
  `team_button_fr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `partners_is_on` tinyint(1) NOT NULL DEFAULT '0',
  `try_is_on` tinyint(1) NOT NULL DEFAULT '0',
  `try_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `page_about_head_id_foreign` (`head_id`),
  KEY `page_about_try_id_foreign` (`try_id`),
  CONSTRAINT `page_about_head_id_foreign` FOREIGN KEY (`head_id`) REFERENCES `component_header` (`id`),
  CONSTRAINT `page_about_try_id_foreign` FOREIGN KEY (`try_id`) REFERENCES `component_try` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page_about`
--

LOCK TABLES `page_about` WRITE;
/*!40000 ALTER TABLE `page_about` DISABLE KEYS */;
INSERT INTO `page_about` VALUES (1,0,19,'1458034875.jpg','','','','','','','','','','','','','','','','','1458034801.jpg','','','','','','','','','','','','','','','',1,0,15,NULL,'2016-03-15 06:41:15');
/*!40000 ALTER TABLE `page_about` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page_benefits`
--

DROP TABLE IF EXISTS `page_benefits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_benefits` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `head_is_on` tinyint(1) NOT NULL DEFAULT '0',
  `head_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text_en` text COLLATE utf8_unicode_ci NOT NULL,
  `title_de` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text_de` text COLLATE utf8_unicode_ci NOT NULL,
  `title_fr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text_fr` text COLLATE utf8_unicode_ci NOT NULL,
  `opinion_is_on` tinyint(1) NOT NULL DEFAULT '0',
  `opinion_id` int(10) unsigned NOT NULL,
  `try_is_on` tinyint(1) NOT NULL DEFAULT '0',
  `try_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `page_benefits_try_id_foreign` (`try_id`),
  KEY `page_benefits_opinion_id_foreign` (`opinion_id`),
  CONSTRAINT `page_benefits_opinion_id_foreign` FOREIGN KEY (`opinion_id`) REFERENCES `component_opinion` (`id`),
  CONSTRAINT `page_benefits_try_id_foreign` FOREIGN KEY (`try_id`) REFERENCES `component_try` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page_benefits`
--

LOCK TABLES `page_benefits` WRITE;
/*!40000 ALTER TABLE `page_benefits` DISABLE KEYS */;
INSERT INTO `page_benefits` VALUES (1,0,'','','','','','','',0,1,0,1,NULL,NULL);
/*!40000 ALTER TABLE `page_benefits` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page_benefits_inner`
--

DROP TABLE IF EXISTS `page_benefits_inner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_benefits_inner` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `head_is_on` tinyint(1) NOT NULL DEFAULT '0',
  `head_id` int(10) unsigned NOT NULL,
  `favorites_is_on` tinyint(1) NOT NULL DEFAULT '0',
  `front_title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `front_text_en` text COLLATE utf8_unicode_ci NOT NULL,
  `front_title_de` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `front_text_de` text COLLATE utf8_unicode_ci NOT NULL,
  `front_title_fr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `front_text_fr` text COLLATE utf8_unicode_ci NOT NULL,
  `front_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `favorites_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `page_benefits_inner_head_id_foreign` (`head_id`),
  CONSTRAINT `page_benefits_inner_head_id_foreign` FOREIGN KEY (`head_id`) REFERENCES `component_header` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page_benefits_inner`
--

LOCK TABLES `page_benefits_inner` WRITE;
/*!40000 ALTER TABLE `page_benefits_inner` DISABLE KEYS */;
INSERT INTO `page_benefits_inner` VALUES (1,0,1,0,'High Performance','High Performance text En','High Performance','High Performance text De','High Performance','High Performance text Fr','',1,NULL,NULL),(2,0,2,0,'Security','Security text En','Security','Security text De','Security text Fr','','',2,NULL,NULL),(3,0,3,0,'Convenience','Convenience text En','Convenience','Convenience text De','Convenience text Fr','','',3,NULL,NULL),(4,0,4,0,'Cost Effective','Cost Effective text En','Cost Effective','Cost Effective text De','Cost Effective text Fr','','',4,NULL,NULL);
/*!40000 ALTER TABLE `page_benefits_inner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page_case_inner`
--

DROP TABLE IF EXISTS `page_case_inner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_case_inner` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `head_is_on` tinyint(1) NOT NULL DEFAULT '0',
  `head_id` int(10) unsigned NOT NULL,
  `case_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `case_text_en` text COLLATE utf8_unicode_ci NOT NULL,
  `case_second_title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `case_second_text_en` text COLLATE utf8_unicode_ci NOT NULL,
  `case_text_de` text COLLATE utf8_unicode_ci NOT NULL,
  `case_second_title_de` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `case_second_text_de` text COLLATE utf8_unicode_ci NOT NULL,
  `case_text_fr` text COLLATE utf8_unicode_ci NOT NULL,
  `case_second_title_fr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `case_second_text_fr` text COLLATE utf8_unicode_ci NOT NULL,
  `try_is_on` tinyint(1) NOT NULL DEFAULT '0',
  `try_id` int(10) unsigned NOT NULL,
  `front_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `front_title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `front_text_en` text COLLATE utf8_unicode_ci NOT NULL,
  `front_title_de` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `front_text_de` text COLLATE utf8_unicode_ci NOT NULL,
  `front_title_fr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `front_text_fr` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `page_case_inner_head_id_foreign` (`head_id`),
  KEY `page_case_inner_try_id_foreign` (`try_id`),
  CONSTRAINT `page_case_inner_try_id_foreign` FOREIGN KEY (`try_id`) REFERENCES `component_try` (`id`),
  CONSTRAINT `page_case_inner_head_id_foreign` FOREIGN KEY (`head_id`) REFERENCES `component_header` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page_case_inner`
--

LOCK TABLES `page_case_inner` WRITE;
/*!40000 ALTER TABLE `page_case_inner` DISABLE KEYS */;
INSERT INTO `page_case_inner` VALUES (1,0,7,'','','','','','','','','','',0,6,'','Research Center','','','','','',NULL,NULL),(2,0,8,'','','','','','','','','','',0,7,'','Clinical Lab','','','','','',NULL,NULL),(3,0,9,'','','','','','','','','','',0,8,'','Diagnostic Service Provider','','','','','',NULL,NULL),(4,0,10,'','','','','','','','','','',0,9,'','Sequencing Provider','','','','','',NULL,NULL);
/*!40000 ALTER TABLE `page_case_inner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page_cases`
--

DROP TABLE IF EXISTS `page_cases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_cases` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `head_is_on` tinyint(1) NOT NULL DEFAULT '0',
  `head_id` int(10) unsigned NOT NULL,
  `opinion_is_on` tinyint(1) NOT NULL DEFAULT '0',
  `opinion_id` int(10) unsigned NOT NULL,
  `try_is_on` tinyint(1) NOT NULL DEFAULT '0',
  `try_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `page_cases_head_id_foreign` (`head_id`),
  KEY `page_cases_opinion_id_foreign` (`opinion_id`),
  KEY `page_cases_try_id_foreign` (`try_id`),
  CONSTRAINT `page_cases_try_id_foreign` FOREIGN KEY (`try_id`) REFERENCES `component_try` (`id`),
  CONSTRAINT `page_cases_head_id_foreign` FOREIGN KEY (`head_id`) REFERENCES `component_header` (`id`),
  CONSTRAINT `page_cases_opinion_id_foreign` FOREIGN KEY (`opinion_id`) REFERENCES `component_opinion` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page_cases`
--

LOCK TABLES `page_cases` WRITE;
/*!40000 ALTER TABLE `page_cases` DISABLE KEYS */;
INSERT INTO `page_cases` VALUES (1,0,6,0,3,0,5,NULL,NULL);
/*!40000 ALTER TABLE `page_cases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page_contact`
--

DROP TABLE IF EXISTS `page_contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_contact` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `head_is_on` tinyint(1) NOT NULL DEFAULT '0',
  `head_id` int(10) unsigned NOT NULL,
  `try_is_on` tinyint(1) NOT NULL DEFAULT '0',
  `try_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `page_contact_head_id_foreign` (`head_id`),
  KEY `page_contact_try_id_foreign` (`try_id`),
  CONSTRAINT `page_contact_try_id_foreign` FOREIGN KEY (`try_id`) REFERENCES `component_try` (`id`),
  CONSTRAINT `page_contact_head_id_foreign` FOREIGN KEY (`head_id`) REFERENCES `component_header` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page_contact`
--

LOCK TABLES `page_contact` WRITE;
/*!40000 ALTER TABLE `page_contact` DISABLE KEYS */;
INSERT INTO `page_contact` VALUES (1,0,16,0,11,NULL,NULL);
/*!40000 ALTER TABLE `page_contact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page_contacts_inner`
--

DROP TABLE IF EXISTS `page_contacts_inner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_contacts_inner` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `head_is_on` tinyint(1) NOT NULL DEFAULT '0',
  `head_id` int(10) unsigned NOT NULL,
  `try_is_on` tinyint(1) NOT NULL DEFAULT '0',
  `try_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `page_contacts_inner_head_id_foreign` (`head_id`),
  KEY `page_contacts_inner_try_id_foreign` (`try_id`),
  CONSTRAINT `page_contacts_inner_try_id_foreign` FOREIGN KEY (`try_id`) REFERENCES `component_try` (`id`),
  CONSTRAINT `page_contacts_inner_head_id_foreign` FOREIGN KEY (`head_id`) REFERENCES `component_header` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page_contacts_inner`
--

LOCK TABLES `page_contacts_inner` WRITE;
/*!40000 ALTER TABLE `page_contacts_inner` DISABLE KEYS */;
INSERT INTO `page_contacts_inner` VALUES (1,0,17,0,12,NULL,NULL);
/*!40000 ALTER TABLE `page_contacts_inner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page_downloads`
--

DROP TABLE IF EXISTS `page_downloads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_downloads` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `head_is_on` tinyint(1) NOT NULL DEFAULT '0',
  `head_id` int(10) unsigned NOT NULL,
  `sign_up_is_on` tinyint(1) NOT NULL DEFAULT '0',
  `sign_up_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `page_downloads_head_id_foreign` (`head_id`),
  CONSTRAINT `page_downloads_head_id_foreign` FOREIGN KEY (`head_id`) REFERENCES `component_header` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page_downloads`
--

LOCK TABLES `page_downloads` WRITE;
/*!40000 ALTER TABLE `page_downloads` DISABLE KEYS */;
INSERT INTO `page_downloads` VALUES (1,0,13,0,1,NULL,NULL);
/*!40000 ALTER TABLE `page_downloads` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page_events`
--

DROP TABLE IF EXISTS `page_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_events` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `head_is_on` tinyint(1) NOT NULL DEFAULT '0',
  `head_id` int(10) unsigned NOT NULL,
  `sign_up_is_on` tinyint(1) NOT NULL DEFAULT '0',
  `sign_up_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `page_events_head_id_foreign` (`head_id`),
  CONSTRAINT `page_events_head_id_foreign` FOREIGN KEY (`head_id`) REFERENCES `component_header` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page_events`
--

LOCK TABLES `page_events` WRITE;
/*!40000 ALTER TABLE `page_events` DISABLE KEYS */;
INSERT INTO `page_events` VALUES (1,0,12,0,1,NULL,NULL);
/*!40000 ALTER TABLE `page_events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page_events_inner`
--

DROP TABLE IF EXISTS `page_events_inner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_events_inner` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `head_is_on` tinyint(1) NOT NULL DEFAULT '0',
  `head_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `favorites_is_on` tinyint(1) NOT NULL DEFAULT '0',
  `favorites_id` int(11) NOT NULL,
  `sign_up_is_on` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page_events_inner`
--

LOCK TABLES `page_events_inner` WRITE;
/*!40000 ALTER TABLE `page_events_inner` DISABLE KEYS */;
INSERT INTO `page_events_inner` VALUES (1,0,'',0,6,0,NULL,NULL);
/*!40000 ALTER TABLE `page_events_inner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page_features`
--

DROP TABLE IF EXISTS `page_features`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_features` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `head_is_on` tinyint(1) NOT NULL DEFAULT '0',
  `head_id` int(10) unsigned NOT NULL,
  `try_is_on` tinyint(1) NOT NULL DEFAULT '0',
  `try_id` int(10) unsigned NOT NULL,
  `favorites_is_on` tinyint(1) NOT NULL DEFAULT '0',
  `favorites_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `page_features_head_id_foreign` (`head_id`),
  KEY `page_features_try_id_foreign` (`try_id`),
  CONSTRAINT `page_features_try_id_foreign` FOREIGN KEY (`try_id`) REFERENCES `component_try` (`id`),
  CONSTRAINT `page_features_head_id_foreign` FOREIGN KEY (`head_id`) REFERENCES `component_header` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page_features`
--

LOCK TABLES `page_features` WRITE;
/*!40000 ALTER TABLE `page_features` DISABLE KEYS */;
INSERT INTO `page_features` VALUES (1,0,15,0,10,0,7,NULL,NULL);
/*!40000 ALTER TABLE `page_features` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page_index`
--

DROP TABLE IF EXISTS `page_index`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_index` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `head_is_on` tinyint(1) NOT NULL DEFAULT '0',
  `head_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `head_title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `head_text_en` text COLLATE utf8_unicode_ci NOT NULL,
  `head_button_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `head_title_de` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `head_text_de` text COLLATE utf8_unicode_ci NOT NULL,
  `head_button_de` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `head_title_fr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `head_text_fr` text COLLATE utf8_unicode_ci NOT NULL,
  `head_button_fr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `how_work_is_on` tinyint(1) NOT NULL,
  `why_blue_is_on` tinyint(1) NOT NULL,
  `partners_is_on` tinyint(1) NOT NULL,
  `opinion_is_on` tinyint(1) NOT NULL DEFAULT '0',
  `opinion_id` int(10) unsigned NOT NULL,
  `try_is_on` tinyint(1) NOT NULL DEFAULT '0',
  `try_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `page_index_try_id_foreign` (`try_id`),
  KEY `page_index_opinion_id_foreign` (`opinion_id`),
  CONSTRAINT `page_index_opinion_id_foreign` FOREIGN KEY (`opinion_id`) REFERENCES `component_opinion` (`id`),
  CONSTRAINT `page_index_try_id_foreign` FOREIGN KEY (`try_id`) REFERENCES `component_try` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page_index`
--

LOCK TABLES `page_index` WRITE;
/*!40000 ALTER TABLE `page_index` DISABLE KEYS */;
INSERT INTO `page_index` VALUES (1,0,'','','','','','','','','','',0,0,0,0,2,0,4,NULL,NULL);
/*!40000 ALTER TABLE `page_index` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page_join_team`
--

DROP TABLE IF EXISTS `page_join_team`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_join_team` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `head_is_on` tinyint(1) NOT NULL DEFAULT '0',
  `head_id` int(10) unsigned NOT NULL,
  `try_is_on` tinyint(1) NOT NULL DEFAULT '0',
  `try_id` int(10) unsigned NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text_en` text COLLATE utf8_unicode_ci NOT NULL,
  `title_de` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text_de` text COLLATE utf8_unicode_ci NOT NULL,
  `title_fr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text_fr` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `page_join_team_head_id_foreign` (`head_id`),
  KEY `page_join_team_try_id_foreign` (`try_id`),
  CONSTRAINT `page_join_team_try_id_foreign` FOREIGN KEY (`try_id`) REFERENCES `component_try` (`id`),
  CONSTRAINT `page_join_team_head_id_foreign` FOREIGN KEY (`head_id`) REFERENCES `component_header` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page_join_team`
--

LOCK TABLES `page_join_team` WRITE;
/*!40000 ALTER TABLE `page_join_team` DISABLE KEYS */;
INSERT INTO `page_join_team` VALUES (1,0,20,0,16,'','','','','','','',NULL,NULL);
/*!40000 ALTER TABLE `page_join_team` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page_knowledge_center`
--

DROP TABLE IF EXISTS `page_knowledge_center`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_knowledge_center` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `head_is_on` tinyint(1) NOT NULL DEFAULT '0',
  `head_id` int(10) unsigned NOT NULL,
  `sign_up_is_on` tinyint(1) NOT NULL DEFAULT '0',
  `sign_up_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `page_knowledge_center_head_id_foreign` (`head_id`),
  CONSTRAINT `page_knowledge_center_head_id_foreign` FOREIGN KEY (`head_id`) REFERENCES `component_header` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page_knowledge_center`
--

LOCK TABLES `page_knowledge_center` WRITE;
/*!40000 ALTER TABLE `page_knowledge_center` DISABLE KEYS */;
INSERT INTO `page_knowledge_center` VALUES (1,0,14,0,1,NULL,NULL);
/*!40000 ALTER TABLE `page_knowledge_center` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page_methods`
--

DROP TABLE IF EXISTS `page_methods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_methods` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `head_is_on` tinyint(1) NOT NULL DEFAULT '0',
  `head_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text_en` text COLLATE utf8_unicode_ci NOT NULL,
  `title_de` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text_de` text COLLATE utf8_unicode_ci NOT NULL,
  `title_fr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text_fr` text COLLATE utf8_unicode_ci NOT NULL,
  `one_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `one_title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `one_text_en` text COLLATE utf8_unicode_ci NOT NULL,
  `one_title_de` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `one_text_de` text COLLATE utf8_unicode_ci NOT NULL,
  `one_title_fr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `one_text_fr` text COLLATE utf8_unicode_ci NOT NULL,
  `second_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `second_title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `second_text_en` text COLLATE utf8_unicode_ci NOT NULL,
  `second_title_de` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `second_text_de` text COLLATE utf8_unicode_ci NOT NULL,
  `second_title_fr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `second_text_fr` text COLLATE utf8_unicode_ci NOT NULL,
  `third_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `third_title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `third_text_en` text COLLATE utf8_unicode_ci NOT NULL,
  `third_title_de` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `third_text_de` text COLLATE utf8_unicode_ci NOT NULL,
  `third_title_fr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `third_text_fr` text COLLATE utf8_unicode_ci NOT NULL,
  `fourth_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fourth_title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fourth_text_en` text COLLATE utf8_unicode_ci NOT NULL,
  `fourth_title_de` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fourth_text_de` text COLLATE utf8_unicode_ci NOT NULL,
  `fourth_title_fr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fourth_text_fr` text COLLATE utf8_unicode_ci NOT NULL,
  `why_blue_is_on` tinyint(1) NOT NULL DEFAULT '0',
  `try_is_on` tinyint(1) NOT NULL DEFAULT '0',
  `try_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `page_methods_try_id_foreign` (`try_id`),
  CONSTRAINT `page_methods_try_id_foreign` FOREIGN KEY (`try_id`) REFERENCES `component_try` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page_methods`
--

LOCK TABLES `page_methods` WRITE;
/*!40000 ALTER TABLE `page_methods` DISABLE KEYS */;
INSERT INTO `page_methods` VALUES (1,0,'','This is temporary title','','','','','','','Step 1','','','','','','','Step 2','','','','','','','Step 3','','','','','','','','','','','','',0,0,2,NULL,NULL);
/*!40000 ALTER TABLE `page_methods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page_news`
--

DROP TABLE IF EXISTS `page_news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_news` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `head_is_on` tinyint(1) NOT NULL DEFAULT '0',
  `head_id` int(11) NOT NULL,
  `sign_up_is_on` tinyint(1) NOT NULL DEFAULT '0',
  `sign_up_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page_news`
--

LOCK TABLES `page_news` WRITE;
/*!40000 ALTER TABLE `page_news` DISABLE KEYS */;
INSERT INTO `page_news` VALUES (1,0,11,0,1,NULL,NULL);
/*!40000 ALTER TABLE `page_news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page_news_inner`
--

DROP TABLE IF EXISTS `page_news_inner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_news_inner` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `head_is_on` tinyint(1) NOT NULL DEFAULT '0',
  `head_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `favorites_is_on` tinyint(1) NOT NULL DEFAULT '0',
  `favorites_id` int(11) NOT NULL,
  `sign_up_is_on` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page_news_inner`
--

LOCK TABLES `page_news_inner` WRITE;
/*!40000 ALTER TABLE `page_news_inner` DISABLE KEYS */;
INSERT INTO `page_news_inner` VALUES (1,0,'',0,5,0,NULL,NULL);
/*!40000 ALTER TABLE `page_news_inner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page_partners`
--

DROP TABLE IF EXISTS `page_partners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_partners` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `head_is_on` tinyint(1) NOT NULL DEFAULT '0',
  `head_id` int(10) unsigned NOT NULL,
  `try_is_on` tinyint(1) NOT NULL DEFAULT '0',
  `try_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `page_partners_head_id_foreign` (`head_id`),
  KEY `page_partners_try_id_foreign` (`try_id`),
  CONSTRAINT `page_partners_try_id_foreign` FOREIGN KEY (`try_id`) REFERENCES `component_try` (`id`),
  CONSTRAINT `page_partners_head_id_foreign` FOREIGN KEY (`head_id`) REFERENCES `component_header` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page_partners`
--

LOCK TABLES `page_partners` WRITE;
/*!40000 ALTER TABLE `page_partners` DISABLE KEYS */;
INSERT INTO `page_partners` VALUES (1,0,5,0,3,NULL,NULL);
/*!40000 ALTER TABLE `page_partners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page_pipelines`
--

DROP TABLE IF EXISTS `page_pipelines`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_pipelines` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `head_is_on` tinyint(1) NOT NULL DEFAULT '0',
  `pipe_text_en` text COLLATE utf8_unicode_ci NOT NULL,
  `pipe_text_de` text COLLATE utf8_unicode_ci NOT NULL,
  `pipe_text_fr` text COLLATE utf8_unicode_ci NOT NULL,
  `s_pipe_text_en` text COLLATE utf8_unicode_ci NOT NULL,
  `s_pipe_text_de` text COLLATE utf8_unicode_ci NOT NULL,
  `s_pipe_text_fr` text COLLATE utf8_unicode_ci NOT NULL,
  `why_blue_is_on` tinyint(1) NOT NULL DEFAULT '0',
  `try_is_on` tinyint(1) NOT NULL DEFAULT '0',
  `try_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page_pipelines`
--

LOCK TABLES `page_pipelines` WRITE;
/*!40000 ALTER TABLE `page_pipelines` DISABLE KEYS */;
INSERT INTO `page_pipelines` VALUES (1,0,'','','','','','',0,0,13,NULL,NULL);
/*!40000 ALTER TABLE `page_pipelines` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page_team`
--

DROP TABLE IF EXISTS `page_team`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page_team` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `head_is_on` tinyint(1) NOT NULL DEFAULT '0',
  `head_id` int(10) unsigned NOT NULL,
  `try_is_on` tinyint(1) NOT NULL DEFAULT '0',
  `try_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `page_team_head_id_foreign` (`head_id`),
  KEY `page_team_try_id_foreign` (`try_id`),
  CONSTRAINT `page_team_try_id_foreign` FOREIGN KEY (`try_id`) REFERENCES `component_try` (`id`),
  CONSTRAINT `page_team_head_id_foreign` FOREIGN KEY (`head_id`) REFERENCES `component_header` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page_team`
--

LOCK TABLES `page_team` WRITE;
/*!40000 ALTER TABLE `page_team` DISABLE KEYS */;
INSERT INTO `page_team` VALUES (1,0,18,0,14,NULL,NULL);
/*!40000 ALTER TABLE `page_team` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `partners`
--

DROP TABLE IF EXISTS `partners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partners` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text_en` text COLLATE utf8_unicode_ci NOT NULL,
  `title_de` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text_de` text COLLATE utf8_unicode_ci NOT NULL,
  `title_fr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text_fr` text COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `partners`
--

LOCK TABLES `partners` WRITE;
/*!40000 ALTER TABLE `partners` DISABLE KEYS */;
/*!40000 ALTER TABLE `partners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pipeline_cols`
--

DROP TABLE IF EXISTS `pipeline_cols`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pipeline_cols` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_de` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_fr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text_en` text COLLATE utf8_unicode_ci NOT NULL,
  `text_de` text COLLATE utf8_unicode_ci NOT NULL,
  `text_fr` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pipeline_cols`
--

LOCK TABLES `pipeline_cols` WRITE;
/*!40000 ALTER TABLE `pipeline_cols` DISABLE KEYS */;
INSERT INTO `pipeline_cols` VALUES (1,'This is title for col 1','','','','','',NULL,NULL),(2,'This is title for col 2','','','','','',NULL,NULL),(3,'This is title for col 3','','','','','',NULL,NULL);
/*!40000 ALTER TABLE `pipeline_cols` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pipelines`
--

DROP TABLE IF EXISTS `pipelines`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pipelines` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text_en` text COLLATE utf8_unicode_ci NOT NULL,
  `title_de` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text_de` text COLLATE utf8_unicode_ci NOT NULL,
  `title_fr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text_fr` text COLLATE utf8_unicode_ci NOT NULL,
  `pipe_file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pipelines`
--

LOCK TABLES `pipelines` WRITE;
/*!40000 ALTER TABLE `pipelines` DISABLE KEYS */;
/*!40000 ALTER TABLE `pipelines` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `positions_to_apply`
--

DROP TABLE IF EXISTS `positions_to_apply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `positions_to_apply` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `requirements_en` text COLLATE utf8_unicode_ci NOT NULL,
  `title_de` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `requirements_de` text COLLATE utf8_unicode_ci NOT NULL,
  `title_fr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `requirements_fr` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `positions_to_apply`
--

LOCK TABLES `positions_to_apply` WRITE;
/*!40000 ALTER TABLE `positions_to_apply` DISABLE KEYS */;
/*!40000 ALTER TABLE `positions_to_apply` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags_events`
--

DROP TABLE IF EXISTS `tags_events`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags_events` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_de` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_fr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags_events`
--

LOCK TABLES `tags_events` WRITE;
/*!40000 ALTER TABLE `tags_events` DISABLE KEYS */;
/*!40000 ALTER TABLE `tags_events` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags_news`
--

DROP TABLE IF EXISTS `tags_news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags_news` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_de` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title_fr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags_news`
--

LOCK TABLES `tags_news` WRITE;
/*!40000 ALTER TABLE `tags_news` DISABLE KEYS */;
/*!40000 ALTER TABLE `tags_news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `team`
--

DROP TABLE IF EXISTS `team`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `team` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `position_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `position_de` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `position_fr` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description_en` text COLLATE utf8_unicode_ci NOT NULL,
  `description_de` text COLLATE utf8_unicode_ci NOT NULL,
  `description_fr` text COLLATE utf8_unicode_ci NOT NULL,
  `is_manager` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `team`
--

LOCK TABLES `team` WRITE;
/*!40000 ALTER TABLE `team` DISABLE KEYS */;
/*!40000 ALTER TABLE `team` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_by_coockie`
--

DROP TABLE IF EXISTS `user_by_coockie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_by_coockie` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `unique_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_by_coockie`
--

LOCK TABLES `user_by_coockie` WRITE;
/*!40000 ALTER TABLE `user_by_coockie` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_by_coockie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_history`
--

DROP TABLE IF EXISTS `user_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_history` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `coockie_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_history_user_id_foreign` (`user_id`),
  KEY `user_history_coockie_id_foreign` (`coockie_id`),
  CONSTRAINT `user_history_coockie_id_foreign` FOREIGN KEY (`coockie_id`) REFERENCES `user_by_coockie` (`id`),
  CONSTRAINT `user_history_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_history`
--

LOCK TABLES `user_history` WRITE;
/*!40000 ALTER TABLE `user_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `is_admin` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'bluebee','bluebee@abv.bg','$2y$10$8vD8P7qH2kTOEWqg1KPT/e8kU.CeVv.OmRA6.E2Fgp1/tlQd9iu0y',1,NULL,NULL,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'bluebee'
--

--
-- Dumping routines for database 'bluebee'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-03-15 12:52:46
